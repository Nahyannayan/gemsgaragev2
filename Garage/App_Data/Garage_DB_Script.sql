USE [Garage]
GO

-- To Drop all tables
--sp_msforeachtable 'drop table ?'


CREATE TABLE [dbo].[School] (
    [Id]                   INT            IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    [SchoolCode]           NVARCHAR (50)  NULL,
    [SchoolName]           NVARCHAR (100) NULL,
    [Location]             INT            NULL,
    [Active]               BIT            NULL,
    [CreatedBy]            INT  NULL,
    [CreatedDate]          DATETIME       NULL,
    [UpdatedBy]            INT  NULL,
    [UpdatedDate]          DATETIME       NULL,
    [Deleted]              BIT            NULL
);



/****** Object:  Table [dbo].[UserRegistration]    Script Date: 11/09/2019 11:01:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRegistration](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](200) NULL,
	[FullName] [nvarchar](50) NULL,
	[Email] [nvarchar](100) NULL,
	[School] [int] NULL,
	[Role] [int] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[Deleted] [bit] NULL,
 CONSTRAINT [PK_UserRegistration] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/****** Object:  Table [dbo].[Category]    Script Date: 05/09/2019 04:08:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](50) NULL,
	[ParentCategoryId] [int] NULL,
	[ImagePath] [nvarchar](500) NULL,
	[Active] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[Deleted] [bit] NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Order]    Script Date: 05/09/2019 04:08:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderNo] [nvarchar](50) NULL,
	[OrderStatus] [int] NULL,
	[Remark] [nvarchar](50) NULL,
	[SchoolId] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[Deleted] [bit] NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Product] (
    [Id]                 INT             IDENTITY (1, 1) NOT NULL,
    [ProductName]        NVARCHAR (100)  NULL,
    [Description]        NVARCHAR (1000) NULL,
    [CategoryId]         INT             NULL,
    [SubCategoryId]      INT             NULL,
    [Quantity]           DECIMAL (5, 2)  NULL,
    [InStock]            DECIMAL (5, 2)  NULL,
    [UnitValue]          DECIMAL (10, 2) NULL,
    [City]               INT             NULL,
    [Location]           NVARCHAR (100)  NULL,
    [OriginalUnitValue]  DECIMAL (10, 2) NULL,
    [DepreciationValue]  DECIMAL (10, 2) NULL,
    [DateOfPurchase]     DATETIME        NULL,
    [Condition]          INT             NULL,
    [ExpectedRealisable] DECIMAL (10, 2) NULL,
    [ActualValue]        DECIMAL (10, 2) NULL,
    [ReasonForSelling]   NVARCHAR (500)  NULL,
    [Reference]			 NVARCHAR (200)  NULL,
    [Note]               NVARCHAR (200)  NULL,
    [FinanceNote]        NVARCHAR (200)  NULL,
    [MsoNote]            NVARCHAR (200)  NULL,
    [PrincipalNote]      NVARCHAR (200)  NULL,
	[NewProductId]		 INT			 NULL,
	[RejectedUserId]	 INT			 NULL,
	[RejectReason]		 NVARCHAR (200)  NULL,
    [Status]             INT             NULL,
    [ViewCount]          INT             NULL,
    [AssetType]          INT             NULL,
	[ApprovalReq]		 BIT DEFAULT 'FALSE' NOT NULL,
    [UserId]             INT             NULL,
    [SchoolId]           INT             NULL,
    [PostedDate]         DATETIME        NULL,
    [CreatedBy]          INT             NULL,
    [CreatedDate]        DATETIME        NULL,
    [UpdatedBy]          INT             NULL,
    [UpdatedDate]        DATETIME        NULL,
    [Deleted]            BIT             NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
/****** Object:  Table [dbo].[ProductImage]    Script Date: 05/09/2019 04:08:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductImage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ImageName] [nvarchar](50) NULL,
	[Extension] [nvarchar](10) NULL,
	[ProductId] [int] NULL,
	[ImageFilePath] [nvarchar](500) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[Deleted] [bit] NULL,
 CONSTRAINT [PK_ProductImage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductOrder]    Script Date: 05/09/2019 04:08:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductOrder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NULL,
	[ProductId] [int] NULL,
	[Quantity] [decimal](5, 2) NULL,
	[UnitValue] [decimal](10, 2) NULL,
	[ProductValue] [decimal](10, 2) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_ProductOrder] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[OrderTrack]    Script Date: 18-09-2019 08:34:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderTrack](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[UserRole] [int] NOT NULL,
	[Remarks] [nvarchar](200) NULL,
	[TrackStatus] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductPostTrack]    Script Date: 18-09-2019 08:34:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductPostTrack](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[UserRole] [int] NOT NULL,
	[TrackStatus] [int] NULL,
	[Remarks] [nvarchar](200) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SessionObj]') AND type in (N'U'))
BEGIN
 CREATE TABLE [SessionObj] (
    [Id] INT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    [UserId] INT NOT NULL,
    [ProductId] INT NULL,
    [ProductImageId] INT NULL,
	[ProductQuantity] INT NULL,
    [ObjType] TINYINT NULL,
	[CreatedBy] INT NOT NULL,
    [CreatedDate] DATETIME NOT NULL,
    [Deleted] BIT NOT NULL
    );
END

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Asset]') AND type in (N'U'))
BEGIN

CREATE TABLE [dbo].[Asset] (
    [Id]                 INT             IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    [ProductName]        NVARCHAR (100)  NULL,
    [Description]        NVARCHAR (1000) NULL,
    [CategoryId]         INT             NULL,
    [SubCategoryId]      INT             NULL,
    [Quantity]           DECIMAL (5, 2)  NULL,
    [Value]              DECIMAL (10, 2) NULL,
    [City]               INT             NULL,
    [Location]           NVARCHAR (100)  NULL,
    [OriginalUnitValue]  DECIMAL (10, 2) NULL,
    [DateOfPurchase]     DATETIME        NULL,
    [ExpectedRealisable] DECIMAL (10, 2) NULL,
    [ReasonForSelling]   NVARCHAR (500)  NULL,
    [Note]               NVARCHAR (200)  NULL,
    [Status]             INT             NULL,
    [UserId]             INT             NULL,
    [SchoolId]           INT             NULL,
    [PostedDate]         DATETIME        NULL,
    [CreatedBy]          INT             NULL,
    [CreatedDate]        DATETIME        NULL,
    [UpdatedBy]          INT             NULL,
    [UpdatedDate]        DATETIME        NULL,
    [Deleted]            BIT             NULL
);

END

GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AssetImage]') AND type in (N'U'))
BEGIN

CREATE TABLE [dbo].[AssetImage] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    [ImageName]     NVARCHAR (50)  NULL,
    [Extension]     NVARCHAR (10)  NULL,
    [AssetId]       INT            NULL,
    [ImageFilePath] NVARCHAR (500) NULL,
    [CreatedBy]     INT            NULL,
    [CreatedDate]   DATETIME       NULL,
    [UpdatedBy]     INT            NULL,
    [UpdatedDate]   DATETIME       NULL,
    [Deleted]       BIT            NULL
);

END

GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AssetTrack]') AND type in (N'U'))
BEGIN

CREATE TABLE [dbo].[AssetTrack] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    [AssetId]     INT            NOT NULL,
    [UserId]      INT            NOT NULL,
    [UserRole]    INT            NOT NULL,
    [Remarks]     NVARCHAR (200) NULL,
    [CreatedBy]   INT            NULL,
    [CreatedDate] DATETIME       NOT NULL,
    [Deleted]     BIT            NOT NULL
);

END

GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SoldProduct]') AND type in (N'U'))
BEGIN

CREATE TABLE [dbo].[SoldProduct] (
    [Id]				INT            IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    [OrderId]			INT			   NOT NULL,
    [ProductOrderId]    INT			   NOT NULL,
    [ProductId]			INT			   NOT NULL,
    [UserId]			INT            NOT NULL,
    [SchoolId]			INT            NOT NULL,
    [ScheduledDate]		DATETIME	   NULL,
    [DispatchDate]		DATETIME	   NULL,
    [Status]			INT			   NOT NULL,
    [CreatedBy]			INT            NULL,
    [CreatedDate]		DATETIME       NOT NULL,
	[UpdatedBy]			INT            NULL,
    [UpdatedDate]		DATETIME       NULL,
    [Deleted]			BIT            NOT NULL
);

END