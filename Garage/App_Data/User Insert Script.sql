USE Garage;

-- INSERT SCHOOL
IF NOT EXISTS (SELECT ID FROM [dbo].[School] WHERE [SchoolCode] = 'AAA' AND [SchoolName]= 'School A  - Qatar' ) 
BEGIN 
	INSERT [dbo].[School] ([SchoolCode], [SchoolName], [Location], [Active], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [Deleted])  
	VALUES 
			(N'AAA',N' School A - Qatar', 5, 1, 0, GetDate(), 0, GetDate(), 0)  
END 

IF NOT EXISTS (SELECT ID FROM [dbo].[School] WHERE [SchoolCode] = 'BBB' AND [SchoolName]= 'Scholl B  - Qatar' ) 
BEGIN 
	INSERT [dbo].[School] ([SchoolCode], [SchoolName], [Location], [Active], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [Deleted])  
	VALUES 
			(N'BBB', N' School B - Qatar', 5, 1, 0, GetDate(), 0, GetDate(), 0)  
END 

--INSERT USER

IF NOT EXISTS (SELECT ID FROM [dbo].[UserRegistration] WHERE [Email] = 'ICT_A@Gemseducation.com')  
BEGIN 
   INSERT [dbo].[UserRegistration] ([UserId], [FullName], [Email], [School], [Role], [Active], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [Deleted])  
	VALUES 
	(N'GUID965243658412', N'ICT A', N'ICT_A@Gemseducation.com', (SELECT Id FROM School WHERE SchoolCode = 'AAA'), 1, 1, 0, GetDate(), 0, GetDate(), 0)  
END
IF NOT EXISTS (SELECT ID FROM [dbo].[UserRegistration] WHERE [Email] = 'ICT_B@Gemseducation.com')  
BEGIN 
   INSERT [dbo].[UserRegistration] ([UserId], [FullName], [Email], [School], [Role], [Active], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [Deleted])  
	VALUES 
	(N'GUID965243658412', N'ICT B', N'ICT_B@Gemseducation.com', (SELECT Id FROM School WHERE SchoolCode = 'BBB'), 1, 1, 0, GetDate(), 0, GetDate(), 0)  
END 
IF NOT EXISTS (SELECT ID FROM [dbo].[UserRegistration] WHERE [Email] = 'Accouts_A@Gemseducation.com')  
BEGIN 
   INSERT [dbo].[UserRegistration] ([UserId], [FullName], [Email], [School], [Role], [Active], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [Deleted])  
	VALUES 
	(N'GUID965243658412', N'Accounts A', N'Accounts_A@Gemseducation.com', (SELECT Id FROM School WHERE SchoolCode = 'AAA'), 2, 1, 0, GetDate(), 0, GetDate(), 0)  
END 
IF NOT EXISTS (SELECT ID FROM [dbo].[UserRegistration] WHERE [Email] = 'Accounts_B@Gemseducation.com')  
BEGIN 
   INSERT [dbo].[UserRegistration] ([UserId], [FullName], [Email], [School], [Role], [Active], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [Deleted])  
	VALUES 
	(N'GUID965243658412', N'Accounts B', N'Accounts_B@Gemseducation.com', (SELECT Id FROM School WHERE SchoolCode = 'BBB'), 2, 1, 0, GetDate(), 0, GetDate(), 0)  
END
IF NOT EXISTS (SELECT ID FROM [dbo].[UserRegistration] WHERE [Email] = 'MSO_A@Gemseducation.com')  
BEGIN 
   INSERT [dbo].[UserRegistration] ([UserId], [FullName], [Email], [School], [Role], [Active], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [Deleted])  
	VALUES 
	(N'GUID965243658412', N'MSO A', N'MSO_A@Gemseducation.com', (SELECT Id FROM School WHERE SchoolCode = 'AAA'), 3, 1, 0, GetDate(), 0, GetDate(), 0)  
END
IF NOT EXISTS (SELECT ID FROM [dbo].[UserRegistration] WHERE [Email] = 'MSO_B@Gemseducation.com')  
BEGIN 
   INSERT [dbo].[UserRegistration] ([UserId], [FullName], [Email], [School], [Role], [Active], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [Deleted])  
	VALUES 
	(N'GUID965243658412', N'MSO B', N'MSO_B@Gemseducation.com', (SELECT Id FROM School WHERE SchoolCode = 'BBB'), 3, 1, 0, GetDate(), 0, GetDate(), 0)  
END
IF NOT EXISTS (SELECT ID FROM [dbo].[UserRegistration] WHERE [Email] = 'HOS_A@Gemseducation.com')  
BEGIN 
   INSERT [dbo].[UserRegistration] ([UserId], [FullName], [Email], [School], [Role], [Active], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [Deleted])  
	VALUES 
	(N'GUID965243658412', N'HOS A', N'HOS_A@Gemseducation.com', (SELECT Id FROM School WHERE SchoolCode = 'AAA'), 4, 1, 0, GetDate(), 0, GetDate(), 0)  
END
IF NOT EXISTS (SELECT ID FROM [dbo].[UserRegistration] WHERE [Email] = 'HOS_B@Gemseducation.com')  
BEGIN 
   INSERT [dbo].[UserRegistration] ([UserId], [FullName], [Email], [School], [Role], [Active], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [Deleted])  
	VALUES 
	(N'GUID965243658412', N'HOS B', N'HOS_B@Gemseducation.com',(SELECT Id FROM School WHERE SchoolCode = 'BBB') , 4, 1, 0, GetDate(), 0, GetDate(), 0)  
END
IF NOT EXISTS (SELECT ID FROM [dbo].[UserRegistration] WHERE [Email] = 'SSC_FIN@Gemseducation.com')  
BEGIN 
   INSERT [dbo].[UserRegistration] ([UserId], [FullName], [Email], [School], [Role], [Active], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [Deleted])  
	VALUES 
	(N'GUID965243658412', N'SSC_FIN', N'SSC_FIN@Gemseducation.com', (SELECT Id FROM School WHERE SchoolCode = 'AAA'), 7, 1, 0, GetDate(), 0, GetDate(), 0)  
END
IF NOT EXISTS (SELECT ID FROM [dbo].[UserRegistration] WHERE [Email] = 'SSC_IA@Gemseducation.com')  
BEGIN 
   INSERT [dbo].[UserRegistration] ([UserId], [FullName], [Email], [School], [Role], [Active], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [Deleted])  
	VALUES 
	(N'GUID965243658412', N'SSC_IA', N'SSC_IA@Gemseducation.com', (SELECT Id FROM School WHERE SchoolCode = 'AAA'), 8, 1, 0, GetDate(), 0, GetDate(), 0)  
END
IF NOT EXISTS (SELECT ID FROM [dbo].[UserRegistration] WHERE [Email] = 'SSC_VP@Gemseducation.com')  
BEGIN 
   INSERT [dbo].[UserRegistration] ([UserId], [FullName], [Email], [School], [Role], [Active], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [Deleted])  
	VALUES 
	(N'GUID965243658412', N'SSC_VP', N'SSC_VP@Gemseducation.com', (SELECT Id FROM School WHERE SchoolCode = 'AAA'), 6, 1, 0, GetDate(), 0, GetDate(), 0)  
END
IF NOT EXISTS (SELECT ID FROM [dbo].[UserRegistration] WHERE [Email] = 'SSC_Main@Gemseducation.com')  
BEGIN 
   INSERT [dbo].[UserRegistration] ([UserId], [FullName], [Email], [School], [Role], [Active], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [Deleted])  
	VALUES 
	(N'GUID965243658412', N'SSC_Main', N'SSC_Main@Gemseducation.com', (SELECT Id FROM School WHERE SchoolCode = 'AAA'), 10, 1, 0, GetDate(), 0, GetDate(), 0)  
END
IF NOT EXISTS (SELECT ID FROM [dbo].[UserRegistration] WHERE [Email] = 'sdgems@gemseducation.com')  
BEGIN 
   INSERT [dbo].[UserRegistration] ([UserId], [FullName], [Email], [School], [Role], [Active], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [Deleted])  
	VALUES 
	(N'GUID965243658412', N'SSC_Main', N'sdgems@gemseducation.com', (SELECT Id FROM School WHERE SchoolCode = 'AAA'), 5, 1, 0, GetDate(), 0, GetDate(), 0)  
END
