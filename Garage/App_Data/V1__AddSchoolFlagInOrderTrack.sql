
USE Garage;

GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'IsDifferentSchool' AND OBJECT_ID = OBJECT_ID(N'OrderTrack'))
BEGIN
ALTER TABLE OrderTrack ADD IsDifferentSchool BIT DEFAULT (0) NOT NULL;
END

GO