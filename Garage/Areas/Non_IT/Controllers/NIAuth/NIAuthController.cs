﻿using Garage.Models;
using Garage.Service;
using Garage.ViewModels;
using Garage.ViewModels.ShoppingCart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Garage.Utils;
using Garage.Service.Email.EmailModels;
using Garage.Service.Email;
using Garage.Utils.Security;
using Rotativa;
using Rotativa.Options;
using Garage.ViewModels.Email;
using Garage.Service.GemsEmail;
using Garage.Areas.Non_IT.Models;
using Garage.Areas.Non_IT.ViewModels;
using System.Web;
using System.IO;
using Garage.Areas.Non_IT.Service;
using Garage.ViewModels.ShoppingCart;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using Garage.Areas.Non_IT.Services;
using System.Security.Claims;
using Garage.Utils;
using Garage.Areas.Non_IT.ViewModels.ShoppingCart;
using static Garage.Areas.Non_IT.ViewModels.ShoppingCart.NIShoppingCart;
using Newtonsoft.Json;

namespace Garage.Areas.Non_IT.Controllers
{
    public class NIAuthController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly NIUserService userService = new NIUserService();
        private readonly NIProductService productService = new NIProductService();
        private readonly NISessionObjService sessionObjService = new NISessionObjService();
        private readonly NISchoolService schoolService = new NISchoolService();

        // GET: Login
        public ActionResult Index()
        {
            logger.Info("Login Controller - Open New Login Form");
            return View(new LoginForm());
        }


        #region Active Directory Auth Methods

        //[AllowAnonymous]
        //[HttpGet, Route("~/logout")]
        //public ActionResult logout()
        //{
        //    logger.Info("Auth Controller - After Logout");
        //    return RedirectToAction("Index", "Landing");
        //}

        [HttpGet, ActionName("SignIn")]
        public void SignIn()
        {
            if (!Request.IsAuthenticated)
            {
                HttpContext.GetOwinContext().Authentication.Challenge(new AuthenticationProperties { RedirectUri = "/login" }, OpenIdConnectAuthenticationDefaults.AuthenticationType);
            }
        }

        [AllowAnonymous]
        [HttpGet, Route("~/login")]
        public ActionResult Auth()
        {
            logger.Info("Login Controller - Submitting Login Form for Auth");
            var claims = ClaimsPrincipal.Current.Claims;
            var email = claims.Where(c => c.Type == "preferred_username").Select(c => c.Value).SingleOrDefault();


            var loggedInUser = userService.FindUserByEmail(email);

            if (loggedInUser != null)
            {
                NICurrentUser currentUser = new NICurrentUser();
                currentUser.user = loggedInUser;
                currentUser.school = schoolService.GetSchoolById(loggedInUser.School ?? 0);
                if (loggedInUser.NIRole.HasValue)
                {
                    currentUser.isNISuperAdmin = Enum.IsDefined(typeof(Garage.Utils.AppEnumeration.NIAdminRole), loggedInUser.NIRole.Value);
                    currentUser.HasGemsAdminRole = Enum.IsDefined(typeof(Garage.Utils.AppEnumeration.NIAdminRole), loggedInUser.NIRole.Value);
                }

                IdentitySignin(currentUser);
                Session["currentUser"] = currentUser;
                getSessionObjByUser(loggedInUser);
            }
            else
            {
                IdentitySignin(null);
                return RedirectToAction("HomePage", "Landing");
            }
            if (loggedInUser.Role.HasValue && loggedInUser.NIRole.HasValue)
                return RedirectToAction("Selection", "NILanding");
            else if (loggedInUser.Role.HasValue && !loggedInUser.NIRole.HasValue)
                return RedirectToAction("HomePage", "Landing");
            else if (!loggedInUser.Role.HasValue && loggedInUser.NIRole.HasValue)
                return RedirectToAction("HomePage", "NILanding");
            else if (!loggedInUser.Role.HasValue && !loggedInUser.NIRole.HasValue)
                return RedirectToAction("HomePage", "Landing");

            return null;
        }
        public void SignOut()
        {
            HttpContext.GetOwinContext().Authentication.SignOut(
                OpenIdConnectAuthenticationDefaults.AuthenticationType, CookieAuthenticationDefaults.AuthenticationType);
        }

        #endregion

        [HttpGet, ActionName("Logout")]
        public void Logout()
        {
            if (Areas.Non_IT.AppUtils.getCurrentUser() != null)
            {
                logger.Info("Login Controller - Log out the current user");
                saveSessionObjectBeforeSignOut();
                if (Areas.Non_IT.AppUtils.getCurrentUser() != null) { Session.Remove("CurrentUser"); }
                if (Session["NICart"] != null) { Session.Remove("NICart"); }
                if (Session["NIWishList"] != null) { Session.Remove("NIWishList"); }
            }
            if (Session["currentUser"] != null) { Session.Remove("currentUser"); }
            SignOut();
        }

        #region Auth Methods

        private void IdentitySignin(NICurrentUser currentUser, string providerKey = null, bool isPersistent = true)
        {
            logger.Info("Login Controller - Identity Sign In");
            var ITroleName = string.Empty;
            var NONITroleName = string.Empty;
            var currentUserJson = string.Empty;
            if (currentUser != null)
            {
                var currentuserrole = userService.GetRoleByEmail(currentUser.user.Email);
                if (currentuserrole != null)
                {
                    NONITroleName = currentuserrole.DISPLAY;
                }


                var ITRole = (new UserService()).FindUserByEmail(currentUser.user.Email).Role;
                if (ITRole.HasValue && ITRole != 0)
                {
                    ITroleName = EnumHelper<AppEnumeration.Role>.GetDisplayValue((AppEnumeration.Role)ITRole.Value);
                }

                currentUserJson = JsonConvert.SerializeObject(currentUser);
            }
            else
            {
                ITroleName = Areas.Non_IT.AppUtils.visitorRole;
            }
            var identity = User.Identity as ClaimsIdentity;
            if (ITroleName == Areas.Non_IT.AppUtils.visitorRole)
            {
                identity.AddClaim(new Claim(ClaimTypes.Role, ITroleName));
            }
            else
            {
                if (!string.IsNullOrEmpty(ITroleName))
                    identity.AddClaim(new Claim(ClaimTypes.Role, ITroleName));
                if (!string.IsNullOrEmpty(NONITroleName))
                    identity.AddClaim(new Claim(ClaimTypes.Role, NONITroleName));
            }


            identity.AddClaim(new Claim("CurrentUser", currentUserJson));
            AuthenticationManager.AuthenticationResponseGrant = new AuthenticationResponseGrant(new ClaimsPrincipal(identity), new AuthenticationProperties()
            {
                AllowRefresh = true,
                IsPersistent = isPersistent,
                ExpiresUtc = DateTime.UtcNow.AddDays(1)
            });
        }

        private IAuthenticationManager AuthenticationManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        #endregion

        #region Help Methods

        private void getSessionObjByUser(NIUserRegistration user)
        {
            var currentUser = Areas.Non_IT.AppUtils.getCurrentUser();
            var sessionObj = new List<NISessionObj>();

            sessionObj = sessionObjService.GetSessionObjByUserId(user.Id).ToList();

            if (sessionObj != null && sessionObj.Count > 0)
            {
                var cartSessionObj = sessionObj.Where(s => s.ObjType == (int)Garage.Utils.AppEnumeration.SessionObjType.Cart).ToList();
                if (cartSessionObj != null)
                {
                    var cartItems = cartSessionObj.ConvertAll(c => new NIShoppingCartDetails()
                    {
                        Product = productService.GetProductById(c.ProductId ?? 0),
                        ProductOrderQuantity = c.ProductQuantity ?? 0,
                        ProductImageId = c.ProductImageId ?? 0

                    }).ToDictionary(s => s.Product.Id, s => s);

                    Session["NICart"] = new NIShoppingCart() { CartItems = cartItems };
                }

                var wishListSessionObj = sessionObj.Where(s => s.ObjType == (int)Garage.Utils.AppEnumeration.SessionObjType.WishList).ToList();
                if (wishListSessionObj != null)
                {
                    var wishListItems = wishListSessionObj.ConvertAll(c => new NIShoppingCartDetails()
                    {
                        Product = productService.GetProductById(c.ProductId ?? 0),
                        ProductOrderQuantity = c.ProductQuantity ?? 0,
                        ProductImageId = c.ProductImageId ?? 0

                    }).ToDictionary(s => s.Product.Id, s => s);

                    Session["NIWishList"] = new NIShoppingCart() { CartItems = wishListItems };
                }
            }
        }

        private void saveSessionObjectBeforeSignOut()
        {
            sessionObjService.DeleteSessionObjByUserId(Areas.Non_IT.AppUtils.getCurrentUser().user.Id);

            var cart = Areas.Non_IT.AppUtils.getCart();
            if (cart != null)
            {
                var cartSessionObj = cart.CartItems.ToList().ConvertAll(s =>
                {
                    return new NISessionObj()
                    {
                        UserId = Areas.Non_IT.AppUtils.getCurrentUser().user.Id,
                        ProductId = s.Value.Product.Id,
                        ProductImageId = s.Value.ProductImageId,
                        ProductQuantity = s.Value.ProductOrderQuantity,
                        ObjType = (int)Garage.Utils.AppEnumeration.SessionObjType.Cart,
                        CreatedBy = Areas.Non_IT.AppUtils.getCurrentUser().user.Id,
                        CreatedDate = DateTime.Now,
                        Deleted = false
                    };
                });
                sessionObjService.AddSessionObjList(cartSessionObj);
            }

            var wishList = Areas.Non_IT.AppUtils.getWishList();
            if (wishList != null)
            {
                var wishListSessionObj = wishList.CartItems.ToList().ConvertAll(s =>
                {
                    return new NISessionObj()
                    {
                        UserId = Areas.Non_IT.AppUtils.getCurrentUser().user.Id,
                        ProductId = s.Value.Product.Id,
                        ProductImageId = s.Value.ProductImageId,
                        ProductQuantity = s.Value.ProductOrderQuantity,
                        ObjType = (int)Garage.Utils.AppEnumeration.SessionObjType.WishList,
                        CreatedBy = Areas.Non_IT.AppUtils.getCurrentUser().user.Id,
                        CreatedDate = DateTime.Now,
                        Deleted = false
                    };
                });
                sessionObjService.AddSessionObjList(wishListSessionObj);
            }
        }

        #endregion

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            logger.Info("Error : " + DateTime.Now + "   " + filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "NIErrorHandler");
            Non_IT.Helper.NonIT_Helper.AddErrorLog(new Error_Log()
            {
                Area = "NON IT",
                Controller = "NIAuth",
                ErrorMessage = Non_IT.AppUtils.getCurrentUser() == null ? "User Session Out" : filterContext.Exception.Message,
                ErrorDate = DateTime.Now,
                UserID = Non_IT.AppUtils.getCurrentUser() == null ? 0 : Non_IT.AppUtils.getCurrentUser().user.Id

            });
        }

    }
}