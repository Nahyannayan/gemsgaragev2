﻿using Garage.Models;
using Garage.Service;
using Garage.ViewModels;
using Garage.ViewModels.ShoppingCart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static Garage.Utils.AppEnumeration;
using static Garage.Utils.Mapper.OrderMapper;
using static Garage.Utils.Mapper.OrderTrackMapper;
using static Garage.Utils.Mapper.ProductOrderMapper;
using static Garage.Utils.Mapper.ProductMapper;
using static Garage.Utils.Mapper.SoldProductMapper;
using Garage.Utils;
using Garage.Service.Email.EmailModels;
using Garage.Service.Email;
using Garage.Utils.Security;
using Rotativa;
using Rotativa.Options;
using Garage.ViewModels.Email;
using Garage.Service.GemsEmail;
using Garage.Areas.Non_IT.Models;
using Garage.Areas.Non_IT.ViewModels;
using System.Web;
using System.IO;
using Garage.Areas.Non_IT.Service;
using Category = Garage.Areas.Non_IT.Models.Category;

namespace Garage.Areas.Non_IT.Controllers
{
    public class NICategoryController : Controller
    {
        // GET: Non_IT/Category
        private readonly NICategoryService NIcategoryService = new NICategoryService();
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //[AppAuthorize(Roles = "MSO ASSISTANT,Corporate IT,GEMS ADMIN")]
        [HttpGet, ActionName("List")]
        public ActionResult CategoryList()
        {
            var categoryList = NIcategoryService.GetAllCategories().OrderByDescending(p => p.CreatedDate).ToList();
            List<SubCategory> subcategory = NIcategoryService.GetSubCategories(null).OrderByDescending(p => p.CreatedDate).ToList();
            ViewBag.subcategoryList = (from i in categoryList join j in subcategory on i.ID equals j.CategoryID select j).ToList();
            ViewBag.categorymap = categoryList.ToDictionary(c => c.ID, c => c.CategoryName);
            return View("~/Areas/Non_IT/Views/NICategory/CategoryList.cshtml", subcategory);
        }

        [AppAuthorize]
        [HttpGet, ActionName("Add")]
        public ActionResult AddCategory()
        {

            List<Areas.Non_IT.Models.Category> parentCategories = NIcategoryService.GetAllCategories().ToList();
            var parentCategoryList = parentCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.CategoryName.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            ViewBag.ParentCategoryList = parentCategoryList;
            return View("~/Areas/Non_IT/Views/NICategory/Add.cshtml", new NICategoryViewModel() { Active = true });
        }

        [AppAuthorize]
        [HttpPost, ActionName("Save")]
        public ActionResult CategorySave(NICategoryViewModel categoryViewModel)
        {

            if (categoryViewModel.ParentCategoryId != 0)
            {
                Areas.Non_IT.Models.SubCategory SubCategory = new Areas.Non_IT.Models.SubCategory();
                SubCategory.SubCategoryName = categoryViewModel.CategoryName;
                SubCategory.CategoryID = categoryViewModel.ParentCategoryId;
                if (categoryViewModel.ImageFile != null && categoryViewModel.ImageFile.ContentLength != 0)
                    SubCategory.ImagePath = Areas.Non_IT.AppUtils.uploadProductImage(categoryViewModel.ImageFile);
                SubCategory.Active = categoryViewModel.Active;
                SubCategory.CreatedBy = Areas.Non_IT.AppUtils.getCurrentUser().user.Id;
                SubCategory.CreatedDate = DateTime.Now;
                SubCategory.UpdatedBy = Areas.Non_IT.AppUtils.getCurrentUser().user.Id;
                SubCategory.UpdatedDate = DateTime.Now;
                SubCategory.Deleted = false;
                ViewBag.savedCategory = NIcategoryService.AddSubCategory(SubCategory);
            }
            else
            {
                Areas.Non_IT.Models.Category Category = new Areas.Non_IT.Models.Category();
                Category.CategoryName = categoryViewModel.CategoryName;
                if (categoryViewModel.ImageFile != null && categoryViewModel.ImageFile.ContentLength != 0)
                    Category.ImagePath = Areas.Non_IT.AppUtils.uploadProductImage(categoryViewModel.ImageFile);
                Category.Active = categoryViewModel.Active;
                Category.CreatedBy = Areas.Non_IT.AppUtils.getCurrentUser().user.Id;
                Category.CreatedDate = DateTime.Now;
                Category.UpdatedBy = Areas.Non_IT.AppUtils.getCurrentUser().user.Id;
                Category.UpdatedDate = DateTime.Now;
                Category.Deleted = false;
                ViewBag.savedCategory = NIcategoryService.AddCategory(Category);
            }
            return RedirectToAction("List", "NICategory");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Delete")]
        public ActionResult DeleteCategory(string id)
        {
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            logger.Info("Category Controller - Delete the exsisting category from database.");
            NIcategoryService.DeleteSubCategory(OriginalID);
            return RedirectToAction("List", "NICategory");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Edit")]
        public ActionResult EditCategory(string id)
        {
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            logger.Info("Category Controller - Edit the saved category from database");
            SubCategory category = NIcategoryService.GetSubCategories(OriginalID).Single();
            NICategoryViewModel obj = new NICategoryViewModel();
            var parentCategories = NIcategoryService.GetAllCategories().ToList();
            var parentCategoryList = parentCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.CategoryName.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            ViewBag.ParentCategoryList = parentCategoryList;
            obj.Active = category.Active.Value;
            obj.Id = category.ID;
            obj.CategoryName = category.SubCategoryName;
            // obj.ImageFile = (HttpPostedFileBase)new MemoryPostedFile(System.IO.File.ReadAllBytes(category.ImagePath), category.ImagePath, System.IO.Path.GetFileName(category.ImagePath));
            obj.ParentCategoryId = category.CategoryID.Value;
            obj.EncryptedId = id;
            return View("~/Areas/Non_IT/Views/NICategory/EditCategory.cshtml", obj);
        }

        [AppAuthorize]
        [HttpPost, ActionName("Update")]
        public ActionResult UpdateCategory(NICategoryViewModel categoryViewModel)
        {
            logger.Info("Category Controller - Update the existing category from database.");
            Areas.Non_IT.Models.SubCategory SubCategory = new Areas.Non_IT.Models.SubCategory();
            SubCategory.SubCategoryName = categoryViewModel.CategoryName;
            SubCategory.CategoryID = categoryViewModel.ParentCategoryId;
            if (categoryViewModel.ImageFile != null && categoryViewModel.ImageFile.ContentLength != 0)
            {
                SubCategory.ImagePath = Areas.Non_IT.AppUtils.uploadProductImage(categoryViewModel.ImageFile);
            }
            SubCategory.Active = categoryViewModel.Active;
            SubCategory.ID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(categoryViewModel.EncryptedId));  
            SubCategory.CreatedBy = Areas.Non_IT.AppUtils.getCurrentUser().user.Id;
            SubCategory.CreatedDate = DateTime.Now;
            SubCategory.UpdatedBy = Areas.Non_IT.AppUtils.getCurrentUser().user.Id;
            SubCategory.UpdatedDate = DateTime.Now;
            SubCategory.Deleted = false;

            NIcategoryService.UpdateSubCategory(SubCategory);
            return RedirectToAction("List", "NICategory");
        }


        [AppAuthorize]
        [HttpGet, ActionName("sub-nicategory-list")]
        public ActionResult GetSubcategoriesByParentCategoryId(int parentCategoryId)
        {
            logger.Info("Category Controller - Getting All Subcategories by Ajax Call");
            List<SubCategory> subCategories = NIcategoryService.GetSubCategoriesByCID(parentCategoryId).ToList();
            return Json(subCategories, JsonRequestBehavior.AllowGet);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            logger.Error("Error : " + DateTime.Now + "   " + filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "NIErrorHandler");
            Non_IT.Helper.NonIT_Helper.AddErrorLog(new Error_Log()
            {
                Area = "NON IT",
                Controller = "NIcategory",
                ErrorMessage = Non_IT.AppUtils.getCurrentUser() == null ? "User Session Out" : filterContext.Exception.Message,
                ErrorDate = DateTime.Now,
                UserID = Non_IT.AppUtils.getCurrentUser() == null ? 0 : Non_IT.AppUtils.getCurrentUser().user.Id

            });
        }
    }
}