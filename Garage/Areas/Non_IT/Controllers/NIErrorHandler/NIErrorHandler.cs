﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garage.Areas.Non_IT.Controllers.NIErrorHandler
{
    public class NIErrorHandler : Controller
    {
        // GET: Non_IT/NIErrorHandler
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ActionResult Index()
        {
            logger.Info("Error Controller - Custom Error Page");
            return View("CustomError");
        }
    }
}