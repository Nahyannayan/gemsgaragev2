﻿using Garage.Areas.Non_IT.Helper;
using Garage.Areas.Non_IT.Models;
using Garage.Areas.Non_IT.Service;
using Garage.Areas.Non_IT.Services;
using Garage.Areas.Non_IT.ViewModels.ShoppingCart;
using Garage.Models;
using Garage.Service;
using Garage.Utils;
using Garage.Utils.Security;
using Garage.ViewModels;
using Garage.ViewModels.ShoppingCart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garage.Areas.Non_IT.Controllers.NILanding
{
    public class NILandingController : Controller
    {
        // GET: Non_IT/NILanding
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly NIProductService NIproductService = new NIProductService();
        private readonly NIProductPostTrackService NIproductPostTrackService = new NIProductPostTrackService();
        private readonly NIFileService NIfileService = new NIFileService();
        private readonly NICategoryService NIcategoryService = new NICategoryService();


        [AppAuthorize]
        [HttpGet, ActionName("Selection")]
        public ActionResult Selection()
        {
            logger.Info("Landing Controller - Selection");
            var loggedInUser = Non_IT.AppUtils.getCurrentUser().user;

            if (loggedInUser.Role.HasValue && !loggedInUser.NIRole.HasValue && loggedInUser.IsITAccess && !loggedInUser.IsNonITAccess)
                return RedirectToAction("HomePage", "Landing");
            else
                return View("~/Areas/Non_IT/Views/NILanding/Selection.cshtml");
        }
        [AllowAnonymous]
        [HttpGet, ActionName("HomePage")]
        public ActionResult HomePage()
        {
            logger.Info("Landing Controller - Listing products in home page");
            var recentProductList = NIproductService.GetAllPublishedProducts().OrderByDescending(p => p.CreatedDate).Take(8).ToList();
            var mostWatchedProductList = NIproductService.GetAllPublishedProducts().Where(p => p.ViewCount > 0).OrderByDescending(p => p.ViewCount).Take(8).ToList();
            ViewBag.RecentProducts = NonIT_Helper.GetProductListView(recentProductList);
            ViewBag.MostWatchedProducts = NonIT_Helper.GetProductListView(mostWatchedProductList);
            ViewBag.ParentCategories = (new NICategoryService()).GetAllCategories().ToList();
            return View("~/Areas/Non_IT/Views/NILanding/HomePage.cshtml");
        }


        [HttpGet, ActionName("MostWatchedProducts")]
        public ActionResult MostWatchedProducts()
        {
            logger.Info("Landing Controller - Listing most watched products in home page");
            var productList = NIproductService.GetAllPublishedProducts().Where(p => p.ViewCount > 0).OrderByDescending(p => p.ViewCount).Take(25).ToList();
            ViewBag.MostWatchedProducts = NonIT_Helper.GetProductListView(productList);
            ViewBag.ParentCategories = (new NICategoryService()).GetAllCategories().ToList();
            return View("~/Areas/Non_IT/Views/NILanding/MostWatchedProducts.cshtml");
        }


        [HttpGet, ActionName("RecentProducts")]
        public ActionResult RecentProducts()
        {
            logger.Info("Landing Controller - Listing Recent products in recent prodcuts page");
            //nahyan on 29jan20
            //  var productList = productService.GetAllPublishedProducts().Where(p => p.CreatedDate > DateTime.Now.AddDays(-7)).OrderByDescending(p => p.CreatedDate).ToList();
            //  var productList = productService.GetAllPublishedProducts().OrderByDescending(p => p.CreatedDate).ToList();
            var productList = NIproductService.GetAllPublishedProducts().OrderByDescending(p => p.CreatedDate).ThenBy(p => p.UnitValue).ToList();
            //  var productList1 = productService.GetAllPublishedProducts().OrderByDescending(p => p.UnitValue).ToList();
            ViewBag.RecentProducts = NonIT_Helper.GetProductListView(productList);
            //    ViewBag.MostWatchedProducts = NonIT_Helper.GetProductListView(productList);
            ViewBag.ParentCategories = (new NICategoryService()).GetAllCategories().ToList();
            return View("~/Areas/Non_IT/Views/NILanding/RecentProducts.cshtml");
        }

        [AllowAnonymous]
        [HttpPost, ActionName("SearchProducts")]
        public ActionResult SearchProducts(string searchString)
        {
            logger.Info("Landing Controller - Listing search products in search result page");
            List<NIProduct> searchedProducts;

            if (!string.IsNullOrEmpty(searchString))
            {
                searchedProducts = NIproductService.GetAllPublishedProducts().Where(p => p.ProductName.Contains(searchString, StringComparison.OrdinalIgnoreCase)).ToList();
            }
            else
            {
                searchedProducts = NIproductService.GetAllPublishedProducts().ToList();
            }
            ViewBag.SearchedProducts = Helper.NonIT_Helper.GetProductListView(searchedProducts);
            ViewBag.SearchString = searchString;
            ViewBag.ParentCategories = (new NICategoryService()).GetAllCategories().ToList();
            return View("~/Areas/Non_IT/Views/NILanding/SearchProducts.cshtml");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Cart")]
        public ActionResult CartList()
        {
            logger.Info("Landing Controller - Displaying cart products in cart page");
            if (Session["NICart"] != null)
            {
                var cart = (NIShoppingCart)Session["NICart"];
                Session["NICart"] = cart.UpdateProductFromSession(cart);
            }

            ViewBag.ParentCategories = (new NICategoryService()).GetAllCategories().ToList();
            return View("~/Areas/Non_IT/Views/NILanding/Cart.cshtml");
        }

        [AppAuthorize]
        [HttpGet, ActionName("WishList")]
        public ActionResult WishList()
        {
            logger.Info("Landing Controller - Displaying wishlist products in wish list page");
            if (Session["NIWishList"] != null)
            {
                var wishList = (NIShoppingCart)Session["NIWishList"];
                Session["NIWishList"] = wishList.UpdateProductFromSession(wishList);
            }
            ViewBag.ParentCategories = (new NICategoryService()).GetAllCategories().ToList();
            return View("~/Areas/Non_IT/Views/NILanding/WishList.cshtml", new AddToCartForm());
        }

        [AppAuthorize]
        [HttpGet, ActionName("CartItem")]
        public ActionResult CartItem()
        {
            logger.Info("Landing Controller - Displaying cart items to School Admins except PO");
            ViewBag.SchoolCartItems = Helper.NonIT_Helper.getCartItemsByCrrentUserSchool(Non_IT.AppUtils.getCurrentUser());
            return View("~/Areas/Non_IT/Views/NILanding/CartItem.cshtml");
        }

        [AppAuthorize]
        [HttpGet, ActionName("cart-item-count")]
        public ActionResult GetCartItemCount()
        {
            logger.Info("Landing Controller - Getting Cart Item Count by Ajax Call");
            var cartItemCount = Helper.NonIT_Helper.getCartItemsByCrrentUserSchool(Non_IT.AppUtils.getCurrentUser());
            return Json(new { cartItemCount }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet, ActionName("HelpPage")]
        public ActionResult HelpPage()
        {
            logger.Info("Landing Controller - Opens Help Page");
            ViewBag.ParentCategories = NIcategoryService.GetAllCategories().ToList();
            return View("~/Areas/Non_IT/Views/NILanding/HelpPage.cshtml");
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            logger.Error("Error : " + DateTime.Now + "   " + filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "NIErrorHandler");
            Non_IT.Helper.NonIT_Helper.AddErrorLog(new Error_Log()
            {
                Area = "NON IT",
                Controller = "NILanding",
                ErrorMessage = Non_IT.AppUtils.getCurrentUser() == null ? "User Session Out" : filterContext.Exception.Message,
                ErrorDate = DateTime.Now,
                UserID = Non_IT.AppUtils.getCurrentUser() == null ? 0 : Non_IT.AppUtils.getCurrentUser().user.Id

            });
        }
    }
}