﻿using Garage.Models;
using Garage.Service;
using Garage.ViewModels;
using Garage.ViewModels.ShoppingCart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static Garage.Utils.AppEnumeration;
using static Garage.Utils.Mapper.OrderMapper;
using static Garage.Utils.Mapper.OrderTrackMapper;
using static Garage.Utils.Mapper.ProductOrderMapper;
using static Garage.Utils.Mapper.ProductMapper;
using static Garage.Utils.Mapper.SoldProductMapper;
using Garage.Utils;
using Garage.Service.Email.EmailModels;
using Garage.Service.Email;
using Garage.Utils.Security;
using Rotativa;
using Rotativa.Options;
using Garage.ViewModels.Email;
using Garage.Service.GemsEmail;
using Garage.Areas.Non_IT.Service;
using Garage.Areas.Non_IT.Services;
using Garage.Areas.Non_IT.ViewModels.ShoppingCart;
using Garage.Areas.Non_IT.Models;
using Garage.Areas.Non_IT.ViewModel;
using Garage.Areas.Non_IT.ViewModels.Email;
using Garage.Areas.Non_IT.ViewModels;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Garage.Areas.Non_IT.Controllers
{
    public class NIOrderController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly NIProductService NIproductService = new NIProductService();
        private readonly NIOrderService orderService = new NIOrderService();
        private readonly NIUserService userService = new NIUserService();
        private readonly NIProductOrderService productOrderService = new NIProductOrderService();
        private readonly NIOrderTrackService orderTrackService = new NIOrderTrackService();
        private readonly NISoldProductService soldProductService = new NISoldProductService();
        private readonly NISchoolService schoolService = new NISchoolService();
        private readonly NISessionObjService sessionObjService = new NISessionObjService();
        private readonly NIProductPostTrackService NIproductPostTrackService = new NIProductPostTrackService();
        private readonly NICategoryService NIcategoryService = new NICategoryService();
        private readonly NIGeneralService NIgeneralservice = new NIGeneralService();
        private readonly NIAssetTrackService NIassetTrackService = new NIAssetTrackService();

        [AppAuthorize]
        [HttpGet, ActionName("Orders")]
        public ActionResult Index()
        {
            logger.Info("Order Controller - Listing all the orders from database");
            ViewBag.OrderList = GetOrderDetails();
            return View("~/Areas/Non_IT/Views/NIOrder/Orders.cshtml");
        }

        [AppAuthorize]
        [HttpPost, ActionName("AddToCart")]
        public ActionResult AddToCart(AddToCartForm addToCartForm, bool? removeFromWishList, string returnUrl)
        {
            logger.Info("Order Controller - Adding product to the Cart Object");
            if (Session["NICart"] == null)
            {
                NIShoppingCart cart = new NIShoppingCart();
                Session["NICart"] = cart.AddToCart(addToCartForm);
            }
            else
            {
                var cart = (NIShoppingCart)Session["NICart"];
                cart.AddToCart(addToCartForm);
            }

            if (removeFromWishList != null && removeFromWishList == true && Session["NIWishList"] != null)
            {
                var wishList = (NIShoppingCart)Session["NIWishList"];
                wishList.RemoveFromCart(addToCartForm.ProductId);
            }
            sessionObjService.saveSessionObjectToDatabase();
            TempData["AddedToCart"] = true;
            if (!string.IsNullOrEmpty(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("HomePage", "NILanding");
        }

        [AppAuthorize]
        [HttpPost, ActionName("AddToWishList")]
        public ActionResult AddToWishList(AddToCartForm addToCartForm, bool? removeFromCart, string returnUrl)
        {
            logger.Info("Order Controller - Adding product to the Wishlist Object");
            if (Session["NIWishList"] == null)
            {
                NIShoppingCart wishList = new NIShoppingCart();
                Session["NIWishList"] = wishList.AddToCart(addToCartForm);
            }
            else
            {
                var wishList = (NIShoppingCart)Session["NIWishList"];
                wishList.AddToCart(addToCartForm);
            }

            if (removeFromCart != null && removeFromCart == true && Session["NICart"] != null)
            {
                var cart = (NIShoppingCart)Session["NICart"];
                cart.RemoveFromCart(addToCartForm.ProductId);
            }
            sessionObjService.saveSessionObjectToDatabase();
            TempData["AddedToWishList"] = true;
            if (!string.IsNullOrEmpty(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("HomePage", "NILanding");
        }

        [AppAuthorize]
        [HttpPost, ActionName("RemoveFromCart")]
        public ActionResult RemoveFromCart(int productId)
        {
            logger.Info("Order Controller - Removing product from the Cart Object");
            if (Session["NICart"] != null)
            {
                var cart = (NIShoppingCart)Session["NICart"];
                cart.RemoveFromCart(productId);
            }
            sessionObjService.saveSessionObjectToDatabase();
            return RedirectToAction("Cart", "NILanding");
        }

        [AppAuthorize]
        [HttpPost, ActionName("RemoveFromWishList")]
        public ActionResult RemoveFromWishList(int productId)
        {
            logger.Info("Order Controller - Removing product from the WishList Object");
            if (Session["NIWishList"] != null)
            {
                var wishList = (NIShoppingCart)Session["NIWishList"];
                wishList.RemoveFromCart(productId);
            }
            sessionObjService.saveSessionObjectToDatabase();
            return RedirectToAction("WishList", "NILanding");
        }

        [AppAuthorize]
        [HttpPost, ActionName("Checkout")]
        public ActionResult Checkout(List<ProductOrderForm> productOrderForm)
        {
            logger.Info("Order Controller - Checkout the Cart to place the order");
            if (isInValidOrder(productOrderForm))
            {
                return Json(new { isValid = false });
            }

            if (Session["NICart"] != null)
            {
                var order = orderService.PlaceOrder(productOrderForm);
                Session.Remove("NICart");
                OrderApprovalEmail(order);
            }
            sessionObjService.saveSessionObjectToDatabase();

            return Json(new { isValid = true });
        }

        [AppAuthorize]
        [HttpGet, ActionName("Track")]
        public ActionResult TrackOrder(string id, string IsSameSchool, string LastApproval, string ViewOnly)
        {
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            var _IsSameSchool = Garage.Security.DataProtection.Decrypt(IsSameSchool);
            var _LastApproval = Garage.Security.DataProtection.Decrypt(LastApproval);
            var _ViewOnly = Garage.Security.DataProtection.Decrypt(ViewOnly);
            logger.Info("Order Controller - Tracking the Order by Order Id from database.");
            var ProductID = 0;
            var currentUser = Non_IT.AppUtils.getCurrentUser();
            ViewBag.ReturnUrl = Request.UrlReferrer.AbsolutePath;//Return Url
            var order = orderService.GetOrderById(OriginalID);
            ViewBag.Order = order;
            var productOrder = productOrderService.GetPOByOrderId(order.Id);
            var listOfProductId = productOrder.Select(p => p.ProductId);
            var productList = NIproductService.GetAllProduct().Where(p => listOfProductId.Contains(p.Id)).ToList();
            ViewBag.ProductDetails = getProductDetails(productList);
            var OrderTrack = orderTrackService.GetOrderTrackByOrderId(order.Id);
            ViewBag.NIVoucher = productOrderService.GetOrderVouchersDetail(OriginalID, currentUser.school.Id);
            ViewBag.OrderTrack = OrderTrack;
            ViewBag.ProductOrder = productOrder;
            ViewBag.IsSameSchool = _IsSameSchool == "1" ? true : false;

            ProductID = productOrder.Last().ProductId.Value;
            ViewBag.Product = (new NIProductService()).GetProductById(ProductID);
            var lastupdate = OrderTrack.OrderByDescending(p => p.CreatedDate).First();
            ViewBag.LastApproval = _LastApproval == "1" ? true : false;
            if (lastupdate.NextApprovalUserRole == currentUser.user.NIRole)
            {
                ViewBag.ViewOnly = true;
            }
            else
            {
                ViewBag.ViewOnly = false;
            }

            return View("~/Areas/Non_IT/Views/NIOrder/OrderTrack.cshtml");
        }

        [AppAuthorize]
        [HttpGet, ActionName("MSOApproval")]
        public ActionResult MSOApproval(int id, bool IsSameSchool)
        {
            logger.Info("Order Controller - Tracking the Order by Order Id from database.");
            ViewBag.ReturnUrl = Request.UrlReferrer.AbsolutePath;//Return Url
            var order = orderService.GetOrderById(id);
            ViewBag.Order = order;
            var productOrder = productOrderService.GetPOByOrderId(order.Id);
            var listOfProductId = productOrder.Select(p => p.ProductId);
            var productList = NIproductService.GetAllProduct().Where(p => listOfProductId.Contains(p.Id)).ToList();
            ViewBag.ProductDetails = getProductDetails(productList);
            ViewBag.OrderTrack = orderTrackService.GetOrderTrackByOrderId(order.Id);
            ViewBag.ProductOrder = productOrder;
            ViewBag.IsSameSchool = IsSameSchool;
            return View("~/Areas/Non_IT/Views/NIOrder/MSOTrack.cshtml");
        }

        [AppAuthorize]
        [HttpGet, ActionName("TrackStatus")]
        public ActionResult TrackOrderStatus(int id, bool print = false)
        {
            logger.Info("Order Controller - Tracking the Order by Order Id from database - Model.");

            var order = orderService.GetOrderById(id);
            var productOrder = productOrderService.GetPOByOrderId(order.Id).FirstOrDefault();
            var product = NIproductService.GetProductById(productOrder.ProductId ?? 0);
            var sellerSchool = schoolService.GetSchoolById(product.SchoolId ?? 0);
            ViewBag.Product = product;
            ViewBag.ProductOrder = productOrder;
            ViewBag.SellerSchool = sellerSchool;
            ViewBag.Order = order;
            ViewBag.OrderTrack = orderTrackService.GetOrderTrackByOrderId(order.Id);
            var productTrack = NIproductPostTrackService.GetPostTrackByProductId(product.Id).Where(pt => pt.UserRole == (int)AppEnumeration.NIUserRole.HOS).FirstOrDefault();
            ViewBag.PublishedDate = productTrack.CreatedDate;
            var user = userService.GetUserById(order.CreatedBy ?? 0);
            var buyerSchool = schoolService.GetSchoolById(user.School ?? 0);
            ViewBag.BuyerSchool = buyerSchool;

            if (print)
            {

                return new PartialViewAsPdf("~/Areas/Non_IT/Views/NIOrder/Pdf/OrderTrackModalPdf.cshtml")
                {
                    PageOrientation = Orientation.Portrait,
                    PageSize = Size.A3,
                    CustomSwitches = "--footer-center \" [page] Page of [toPage] Pages\" --footer-line --footer-font-size \"9\" --footer-spacing 5 --footer-font-name \"calibri light\"",
                    FileName = "OrderTrack.pdf"
                };
            }
            return PartialView("~/Areas/Non_IT/Views/NIOrder/OrderTrackModal.cshtml");
        }


        [AppAuthorize]
        [HttpGet, ActionName("Remove")]
        public ActionResult RemoveIndividualProductFromOrder(int orderId, int productId)
        {
            logger.Info("Order Controller - Rejecting Single Product in Order");
            NIOrder order = orderService.GetOrderById(orderId);
            var productOrderList = productOrderService.GetPOByOrderId(orderId);
            if (productOrderList.Count == 1)
            {
                orderService.UpdateOrder(Helper.NonIT_Helper.cancelOrder(order));
            }
            var productOrder = productOrderList.FirstOrDefault(po => po.ProductId == productId);
            productOrderService.UpdateProductOrder(Helper.NonIT_Helper.deleteProductOrder(productOrder));
            updateStock(productOrder);
            var soldProduct = Helper.NonIT_Helper.rejectMap(productOrder); // sold product reject map
            soldProductService.UpdateSoldProduct(soldProduct);
            return TrackOrder(Garage.Security.DataProtection.Encrypt(orderId.ToString()), Garage.Security.DataProtection.Encrypt("1"), Garage.Security.DataProtection.Encrypt("0"), Garage.Security.DataProtection.Encrypt("0"));
        }


        [AppAuthorize]
        [HttpGet, ActionName("Reject")]
        public ActionResult RejectOrder(int id)
        {
            logger.Info("Order Controller - Rejecting Order while Approval");
            NIOrder order = orderService.GetOrderById(id);
            orderService.UpdateOrder(Helper.NonIT_Helper.cancelOrder(order));
            var productOrder = productOrderService.GetPOByOrderId(order.Id);
            updateStock(productOrder, order);
            var soldProductList = Helper.NonIT_Helper.rejectMap(order); // sold product reject map
            soldProductList.ForEach(p => soldProductService.UpdateSoldProduct(p));
            PurchaseRejectedMail(order);
            return RedirectToAction("Orders", "NIOrder");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Approval")]
        public ActionResult ApprovalOrder(string id, bool IsDaxUpdated = false)
        {
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            logger.Info("Order Controller - Approval Order process");
            NIOrderVouchersDetail _NiVOrder = new NIOrderVouchersDetail();
            var details = Request.Headers.GetValues("detail");
            if (details != null && details.Count() > 0)
            {
                var serializer = new JavaScriptSerializer();
                _NiVOrder = serializer.Deserialize<NIOrderVouchersDetail>(details[0].ToString());
                _NiVOrder.OrderId = OriginalID;
            }
            var watch = new System.Diagnostics.Stopwatch();
            watch.Start();
            var currentUser = Non_IT.AppUtils.getCurrentUser();
            NIOrder order = orderService.GetOrderById(OriginalID);
            order.UpdatedBy = currentUser.user.Id;
            orderTrackService.AddOrderTrack(Helper.NonIT_Helper.orderTrackMap(order, (order.SchoolId == currentUser.school.Id) ? true : false));
            if ((AppEnumeration.NIUserRole)currentUser.user.NIRole == AppEnumeration.NIUserRole.ACCOUNTS && order.SchoolId == currentUser.school.Id)
            {
                var POService = productOrderService.GetProductOrderById(order.Id);
                POService.DaxUpdatedAccepted = IsDaxUpdated;
                productOrderService.UpdateProductOrder(POService);
            }

            if ((AppEnumeration.NIUserRole)currentUser.user.NIRole == AppEnumeration.NIUserRole.GCOREALM || (AppEnumeration.NIUserRole)currentUser.user.NIRole == AppEnumeration.NIUserRole.PROCUREMENT)
            {
                order.OrderStatus = Convert.ToInt32(OrderStatus.Procured);
                order = orderService.UpdateOrder(order);
                updateProductStatus(productOrderService.GetPOByOrderId(order.Id));

                var soldProductList = Helper.NonIT_Helper.waitingForBookValueTransferMap(order);
                soldProductList.ForEach(p => soldProductService.UpdateSoldProduct(p));
                OrderApproveEmail(order);
            }
            else
            {
                orderService.UpdateOrder(order);
                if ((AppEnumeration.NIUserRole)currentUser.user.NIRole != AppEnumeration.NIUserRole.ACCOUNTS)
                {
                    order.OrderStatus = Convert.ToInt32(OrderStatus.InProgress);
                    OrderApprovalEmail(order);
                }



            }
            if ((AppEnumeration.NIUserRole)currentUser.user.NIRole == AppEnumeration.NIUserRole.ACCOUNTS)
            {

                productOrderService.AddOrderVouchersDetail(_NiVOrder);
                var school = schoolService.GetSchoolById(order.SchoolId ?? 0);
                var productOrder = productOrderService.GetPOByOrderId(order.Id).FirstOrDefault();
                var POService = productOrderService.GetProductOrderById(order.Id);
                var product = (new NIProductService()).GetProductById(POService.ProductId.Value);
                var orderDetails = new NIEmailNIOrderDetails()
                {
                    ProductDetail = Helper.NonIT_Helper.viewMap(product),
                    ProductOrder = productOrder,
                    BuyerSchoolName = school.SchoolName
                };
                var emailbody = EmailFactory.ParseTemplate(orderDetails, Non_IT.AppUtils.orderApproveSellerMailTemplate);
                var productCreatorEmail = userService.GetUserById(product.CreatedBy.Value).Email;
                GemsToken.SendMail(new List<string>() { productCreatorEmail }, "Purchase Approval", emailbody, true);
            }
            watch.Stop();
            logger.Info("Order Controller - Execution Time: " + watch.ElapsedMilliseconds + " ms");
            return RedirectToAction("Orders", "NIOrder");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Completed")]
        public ActionResult OrderCompleted(int id)
        {
            logger.Info("Order Controller - Order Complete process");
            NIOrder order = orderService.GetOrderById(id);
            var completedOrder = orderService.UpdateOrder(Helper.NonIT_Helper.orderCompleteMap(order));
            orderTrackService.AddOrderTrack(Helper.NonIT_Helper.orderTrackMap(order, false));
            var soldProductList = soldProductService.GetSoldProductsByOrderId(order.Id);
            var updatedsoldProductList = Helper.NonIT_Helper.updateDispatchedListMap(soldProductList.ToList());
            updatedsoldProductList.ForEach(sp => soldProductService.UpdateSoldProduct(sp));

            return RedirectToAction("Orders", "NIOrder");
        }



        public ActionResult AccountApproval(int id, bool IsSameSchool, bool LastApproval)
        {
            logger.Info("Order Controller - Tracking the Order by Order Id from database.");

            var order = orderService.GetOrderById(id);
            ViewBag.Order = order;
            var productOrder = productOrderService.GetPOByOrderId(order.Id);
            var listOfProductId = productOrder.Select(p => p.ProductId);
            var productList = NIproductService.GetAllProduct().Where(p => listOfProductId.Contains(p.Id)).ToList();
            ViewBag.ProductDetails = getProductDetails(productList);
            ViewBag.OrderTrack = orderTrackService.GetOrderTrackByOrderId(order.Id);
            ViewBag.ProductOrder = productOrder;
            ViewBag.IsSameSchool = IsSameSchool;
            ViewBag.LastApproval = LastApproval;

            NIProduct product = NIproductService.GetProductById(NIproductService.GetProductByOrderId(id).ProductId.Value);

            NIProductForm productForm = Helper.NonIT_Helper.ReMapNIProduct(product);

            productForm.productViewModel.ReasonForReject = "";
            List<Garage.Areas.Non_IT.Models.Category> parentCategories = NIcategoryService.GetAllCategories().ToList();
            var parentCategoryList = parentCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.CategoryName.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            ViewBag.ParentCategoryList = parentCategoryList;

            List<Garage.Areas.Non_IT.Models.SubCategory> subCategories = NIcategoryService.GetSubCategories(null).ToList();
            var subCategoryList = subCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.SubCategoryName.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });


            ViewBag.SubCategoryList = subCategoryList;

            List<Areas.Non_IT.Models.ConditionEnum> AllConditions = NIgeneralservice.GetAllCondition().ToList();
            var AllConditionsList = AllConditions.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.DISPLAY.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            ViewBag.AllConditionsList = AllConditionsList;

            ViewBag.ProductTrack = NIproductPostTrackService.GetPostTrackByProductId(product.Id).OrderByDescending(p => p.CreatedDate).FirstOrDefault();
            ViewBag.ReferenceFiles = (new NIFileService()).GetProductImageByProductId(product.Id, false);

            return View("~/Areas/Non_IT/Views/NIOrder/AccountApproval.cshtml", productForm);
        }


        [HttpPost, ActionName("OrderApproval")]
        public ActionResult ApprovalOrderProduct(NIProductForm productForm)
        {
            logger.Info("Product Controller - Saving the Approval Status for the product");

            NIProduct approvedProduct;
            var OrderID = 0;
            NIProduct product = NIproductService.GetProductById(productForm.productViewModel.Id);
            NIProduct updatedProduct = Helper.NonIT_Helper.mapNIProduct(productForm, product);
            var currentUser = Non_IT.AppUtils.getCurrentUser();
            approvedProduct = NIproductService.UpdateProduct(updatedProduct);
            if (currentUser.user.NIRole == (int)AppEnumeration.NIUserRole.ACCOUNTS)
            {
                var IsSameSchool = HttpUtility.ParseQueryString(HttpContext.Request.UrlReferrer.Query)["IsSameSchool"];
                if (!string.IsNullOrEmpty(IsSameSchool) && IsSameSchool.ToLower() == "false")
                {
                    OrderID = int.Parse(HttpUtility.ParseQueryString(HttpContext.Request.UrlReferrer.Query)["OrderID"]);
                    NIOrder order = orderService.GetOrderById(int.Parse(OrderID.ToString()));

                    orderTrackService.AddOrderTrack(Helper.NonIT_Helper.orderTrackMap(order, false));

                }
                else
                {
                    var LastApproval = HttpUtility.ParseQueryString(HttpContext.Request.UrlReferrer.Query)["LastApproval"];
                    if (!string.IsNullOrEmpty(LastApproval) && LastApproval.ToLower() == "true")
                    {
                        OrderID = int.Parse(HttpUtility.ParseQueryString(HttpContext.Request.UrlReferrer.Query)["OrderID"]);
                        NIOrder order = orderService.GetOrderById(int.Parse(OrderID.ToString()));

                        orderTrackService.AddOrderTrack(Helper.NonIT_Helper.orderTrackMap(order, true));

                    }
                }

            }

            return RedirectToAction("AccountApproval", "NIOrder", new { @OrderID = OrderID, @IsSameSchool = false });

        }

        #region Help Methods

        private List<NIProductDetail> getProductDetails(List<NIProduct> productList) => productList.ConvertAll(p => Helper.NonIT_Helper.viewMap(p));

        private void OrderApproveEmail(NIOrder order)
        {
            var school = schoolService.GetSchoolById(order.SchoolId ?? 0);
            var productOrder = productOrderService.GetPOByOrderId(order.Id).FirstOrDefault();
            var product = NIproductService.GetProductById(productOrder.ProductId ?? 0);
            var toBuyerEmailId = userService.GetUserById(order.CreatedBy.Value).Email;
            var toSellerEmailId = userService.GetUserById(product.UserId.Value);
            var SellerEmails = userService.GetUserBySchoolAndRoleID(toSellerEmailId.School.Value, (int)Utils.AppEnumeration.NIUserRole.ACCOUNTS);

            var orderDetails = new NIEmailNIOrderDetails()
            {
                ProductDetail = Helper.NonIT_Helper.viewMap(product),
                ProductOrder = productOrder,
                BuyerSchoolName = school.SchoolName
            };
            var emailbodyBuyer = EmailFactory.ParseTemplate(orderDetails, Non_IT.AppUtils.orderApproveBuyerMailTemplate);
            var emailbodySellerr = EmailFactory.ParseTemplate(orderDetails, Non_IT.AppUtils.orderApproveAccountSellerMailTemplate);
            //emailService.send(toBuyerEmailId, "Purchase has been Approved", emailbodyBuyer);
            //emailService.send(toSellerEmailId, "Purchase has been Approved", emailbodySellerr);

            GemsToken.SendMail(new List<string>() { toBuyerEmailId }, "Purchase has been Approved", emailbodyBuyer, true);
            GemsToken.SendMail(SellerEmails, "Purchase has been Approved", emailbodySellerr, true);
        }


        private void OrderApprovalEmail(NIOrder order)
        {
            List<string> toEmailIds = new List<string>();
            var user = Non_IT.AppUtils.getCurrentUser();
            var school = schoolService.GetSchoolById(order.SchoolId ?? 0);
            var productOrder = productOrderService.GetPOByOrderId(order.Id).FirstOrDefault();
            var product = NIproductService.GetProductById(productOrder.ProductId ?? 0);

            var Tracking = (new NIOrderTrackService()).GetOrderTrackByOrderId(order.Id);
            var nextApprover = Tracking.OrderByDescending(p => p.CreatedDate).First().NextApprovalUserRole.Value;//);// userService.GetPurchaseNextApprovalRoleID(Tracking.Count());



            if (product.CategoryId.Value == Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ProcurementCategoryID"].ToString()) && user.user.NIRole.Value == (int)AppEnumeration.NIUserRole.HOS)
            {
                toEmailIds = userService.GetAllEmailsBasedOnlyOnRole(nextApprover);
            }
            else
            {
                toEmailIds = userService.GetAllUsers(user.school.Id, nextApprover).ToList();
            }//GetAllEmailsBasedOnlyOnRole

            var orderDetails = new NIEmailNIOrderDetails()
            {
                ProductDetail = Helper.NonIT_Helper.viewMap(product),
                ProductOrder = productOrder,
                BuyerSchoolName = school.SchoolName
            };
            var emailbody = EmailFactory.ParseTemplate(orderDetails, Non_IT.AppUtils.OrderApprovalMailTemplate);

            GemsToken.SendMail(toEmailIds, "Purchase Approval", emailbody, true);
        }

        private void PurchaseRejectedMail(NIOrder order)
        {
            var toEmailIds = userService.GetUserBySchoolAndRoleID(Non_IT.AppUtils.getCurrentUser().school.Id, (int)AppEnumeration.NIUserRole.MSOASSISTANT);

            var productOrder = productOrderService.GetPOByOrderId(order.Id).FirstOrDefault();
            var product = NIproductService.GetProductById(productOrder.ProductId ?? 0);
            var orderDetails = new NIEmailNIOrderDetails()
            {
                ProductDetail = Helper.NonIT_Helper.viewMap(product),
                ProductOrder = productOrder
            };
            var emailbody = EmailFactory.ParseTemplate(orderDetails, Non_IT.AppUtils.PurchaseRejectedMailTemplate);
            //emailService.send(toEmailIds, "Purchase Rejected", emailbody);
            GemsToken.SendMail(toEmailIds, "Purchase Rejected", emailbody, true);

        }

        private void updateStock(NIProductOrder productOrder)
        {
            var product = NIproductService.GetProductById(productOrder.ProductId ?? 0);
            product.InStock = product.InStock + productOrder.Quantity;
            NIproductService.UpdateProduct(product);
        }

        private void updateStock(List<NIProductOrder> productOrder, NIOrder order)
        {
            List<int?> productIds = productOrder.Select(p => p.ProductId).ToList();
            var productList = NIproductService.GetAllProduct().Where(p => productIds.Contains(p.Id)).ToList();
            if (order.OrderStatus == (int)OrderStatus.Cancelled)
            {
                productList.ForEach(p => p.InStock = p.InStock + (productOrder.Where(po => po.ProductId == p.Id).FirstOrDefault().Quantity));
            }
            else
            {
                productList.ForEach(p => p.InStock = p.InStock - (productOrder.Where(po => po.ProductId == p.Id).FirstOrDefault().Quantity));
            }
            productList.ForEach(p => NIproductService.UpdateProduct(p));
        }

        private void updateProductStatus(List<NIProductOrder> productOrder)
        {
            List<int?> productIds = productOrder.Select(p => p.ProductId).ToList();

            var productList = NIproductService.GetAllProduct().Where(p => productIds.Contains(p.Id)).ToList();

            productList.ForEach(p =>
                {
                    var soldQuantity = orderService.getSoldQuantityByProductId(p.Id);
                    if (p.Quantity == soldQuantity)
                    {
                        p.Status = (int)ProductStatus.SoldOut;
                    }
                });

            productList.ForEach(p => NIproductService.UpdateProduct(p));
        }

        private List<NIOrder> getOrderList()
        {
            var _user = Areas.Non_IT.AppUtils.getCurrentUser();
            var result = orderService.GetAllOrders().Where(o => o.OrderStatus == (int)OrderStatus.OrderPlaced || o.OrderStatus == (int)OrderStatus.InProgress || o.OrderStatus == (int)OrderStatus.Procured && o.Deleted == false).OrderByDescending(p => p.CreatedDate).ToList();

            if (_user.user.NIRole.Value == (int)AppEnumeration.NIAdminRole.GEMSADMINROLE ||
                _user.user.NIRole.Value == (int)AppEnumeration.NIAdminRole.GCOREALM ||
                _user.user.NIRole.Value == (int)AppEnumeration.NIUserRole.PROCUREMENT)
            {
                return result;
            }
            else
            {
                result = result.Where(o => o.SchoolId == _user.school.Id).ToList();
            }
            return result;
        }

        private List<NIOrder> getCompletedOrderList() => orderService.GetAllOrders().Where(o => o.OrderStatus == (int)OrderStatus.Completed && o.Deleted == false && o.SchoolId == Non_IT.AppUtils.getCurrentUser().school.Id).OrderByDescending(p => p.CreatedDate).ToList();

        private List<NIOrder> getAllCompletedOrders() => orderService.GetAllOrders().Where(o => o.OrderStatus == (int)OrderStatus.Completed && o.Deleted == false).OrderByDescending(p => p.CreatedDate).ToList();

        private bool isInValidOrder(List<ProductOrderForm> productOrderForm) => productOrderForm.Exists(po => po.quantity > (NIproductService.GetProductById(po.productId).InStock));

        private List<string> getAllFinanceEmails() => userService.GetAllUsers().Where(u => u.School == Non_IT.AppUtils.getCurrentUser().school.Id && u.NIRole == (int)AppEnumeration.UserRole.Finance).Select(u => u.Email).ToList();

        //private List<string> getAllEmailsBasedOnRole(int userRole)
        //{
        //    if (userRole == (int)NIUserRole.GCOREALM)
        //    {
        //        return userService.GetAllUsers().Where(u => u.NIRole == userRole && (u.Role == null || u.Role.Value == 0)).Select(u => u.Email).ToList();
        //    }
        //    else
        //    {
        //        return userService.GetAllUsers().Where(u => u.School == Non_IT.AppUtils.getCurrentUser().school.Id && u.NIRole == userRole).Select(u => u.Email).ToList();
        //    }
        //}

        private string getUserMailByRoleAndId(int userRole, int userId) => userService.GetAllUsers().Where(u => u.Id == userId && u.NIRole == userRole).Select(u => u.Email).FirstOrDefault();


        private int getNextApproverByCurrentRole(int currentUserRole)
        {
            AppEnumeration.UserRole userRole;

            switch ((AppEnumeration.UserRole)currentUserRole)
            {
                case AppEnumeration.UserRole.ICTEngineer:
                    userRole = (AppEnumeration.UserRole.MSO);
                    break;
                case AppEnumeration.UserRole.Finance:
                    userRole = (AppEnumeration.UserRole.ICTEngineer);
                    break;
                case AppEnumeration.UserRole.MSO:
                    userRole = (AppEnumeration.UserRole.Principal);
                    break;
                case AppEnumeration.UserRole.Principal:
                    userRole = (AppEnumeration.UserRole.Finance);
                    break;
                default:
                    userRole = (AppEnumeration.UserRole.Finance);
                    break;
            }
            return (int)userRole;
        }

        private List<NIOrderDetails> GetOrderDetails()
        {
            var nextApprover = 0;
            List<NIOrderDetails> returnData = new List<NIOrderDetails>();
            var Records = new List<NIOrder>();

            Records = getOrderList();
            var currentUser = Non_IT.AppUtils.getCurrentUser();

            if (Non_IT.AppUtils.getCurrentUser().user.NIRole != (int)AppEnumeration.NIUserRole.GCOREALM && Non_IT.AppUtils.getCurrentUser().user.NIRole != (int)AppEnumeration.NIUserRole.PROCUREMENT)
            {
                Records = Records.Where(p => p.SchoolId == Non_IT.AppUtils.getCurrentUser().user.School.Value).ToList();
            }
            var OrderDetailList = Records.ConvertAll(o =>
                   {
                       var lastOrderTrackEntry = orderTrackService.GetOrderTrackByOrderId(o.Id).Where(ot => ot.IsDifferentSchool == false).OrderByDescending(ot => ot.CreatedDate).FirstOrDefault();
                       var productOrder = productOrderService.GetPOByOrderId(o.Id).FirstOrDefault();
                       var product = NIproductService.GetProductById(productOrder.ProductId ?? 0);
                       var nextRole = lastOrderTrackEntry.NextApprovalUserRole;
                       if (nextRole == (int)AppEnumeration.NIUserRole.PROCUREMENT || nextRole == (int)AppEnumeration.NIUserRole.PROCUREMENT)
                       {
                           nextApprover = new NIUserService().GetPurchaseNextApprovalRoleID(lastOrderTrackEntry.TrackStatus.Value, ((currentUser.user.NIRole.Value == (int)AppEnumeration.NIUserRole.GCOREALM || currentUser.user.NIRole.Value == (int)AppEnumeration.NIUserRole.PROCUREMENT) && product.CategoryId == Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ProcurementCategoryID"].ToString())) ? true : false);
                       }
                       else
                       {
                           nextApprover = new NIUserService().GetPurchaseNextApprovalRoleID(lastOrderTrackEntry.TrackStatus.Value);
                       }
                       var StepCount = orderTrackService.GetOrderTrackByOrderId(o.Id).Count();

                       var soldProduct = soldProductService.GetSoldProductsByOrderId(o.Id).FirstOrDefault();
                       var pickUpScheduled = soldProduct == null ? null : soldProduct.ScheduledDate;
                       var IsSameSchool = (((lastOrderTrackEntry.UserRole == (int)AppEnumeration.NIUserRole.GCOREALM || lastOrderTrackEntry.UserRole == (int)AppEnumeration.NIUserRole.PROCUREMENT) && lastOrderTrackEntry.NextApprovalUserRole == 3)) ? false : true;
                       var LastUpdateUserID = lastOrderTrackEntry.UserId;


                       return new NIOrderDetails()
                       {
                           Order = o,
                           ProductOrder = productOrder,
                           Product = product,
                           NextApprover = userService.GetRoleIDByID(nextApprover),
                           NextApproverRole = nextApprover,
                           PickUpScheduled = pickUpScheduled,
                           IsSameSchool = IsSameSchool,
                           LastApproval = lastOrderTrackEntry.IsDifferentSchool,
                           LastUpdateUserID = LastUpdateUserID,
                           SchoolCode = product.SchoolId.HasValue ? Helper.NonIT_Helper.GetSchoolDetails(product.SchoolId.Value).SchoolCode : "",
                           NIOrderProductVoucher = (productOrder.OrderId.HasValue ? (new NIProductOrderService()).GetOrderVouchersDetail(productOrder.OrderId.Value, product.SchoolId.Value) : null)
                       };
                   });


            if (currentUser.user.NIRole != (int)AppEnumeration.NIUserRole.MSOASSISTANT && currentUser.user.NIRole != (int)AppEnumeration.NIUserRole.ACCOUNTS)
            {
                if (currentUser.user.NIRole == (int)AppEnumeration.NIUserRole.PROCUREMENT)
                {
                    OrderDetailList = OrderDetailList.Where(p => p.Product.CategoryId.Value == Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ProcurementCategoryID"].ToString())).ToList();
                }
                else if (currentUser.user.NIRole == (int)AppEnumeration.NIUserRole.GCOREALM)
                {
                    OrderDetailList = OrderDetailList.Where(p => p.Product.CategoryId.Value != Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ProcurementCategoryID"].ToString())).ToList();
                }
                OrderDetailList =  OrderDetailList.Where(o => o.NextApproverRole == currentUser.user.NIRole || o.IsSameSchool == false).ToList();
            }

            return OrderDetailList;
        }

        private List<NICompletedOrders> GetCompletedOrderDetails()
        {
            var OrderDetailList = getAllCompletedOrders().ConvertAll(o =>
            {
                var productOrder = productOrderService.GetPOByOrderId(o.Id).FirstOrDefault();
                var product = NIproductService.GetProductById(productOrder.ProductId ?? 0);
                return new NICompletedOrders()
                {
                    Order = o,
                    ProductOrder = productOrder,
                    Product = product,
                };
            });
            return OrderDetailList;
        }

        #endregion

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            logger.Error("Error : " + DateTime.Now + "   " + filterContext.Exception);
            filterContext.Result = RedirectToAction("ndex", "NIErrorHandler");
            Non_IT.Helper.NonIT_Helper.AddErrorLog(new Error_Log()
            {
                Area = "NON IT",
                Controller = "NIOrder",
                ErrorMessage = Non_IT.AppUtils.getCurrentUser() == null ? "User Session Out" : filterContext.Exception.Message,
                ErrorDate = DateTime.Now,
                UserID = Non_IT.AppUtils.getCurrentUser() == null ? 0 : Non_IT.AppUtils.getCurrentUser().user.Id

            });
        }
    }
}