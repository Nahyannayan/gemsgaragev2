﻿using Garage.Models;
using Garage.Service;
using Garage.Utils;
using Garage.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Garage.Utils.Mapper.ProductMapper;
using static Garage.Utils.Mapper.ProductTrackMapper;
using static Garage.Utils.Mapper.AssetTrackMapper;
using static Garage.Utils.Mapper.OrderTrackMapper;
using static Garage.Utils.Mapper.AssetMapper;
using static Garage.Utils.Mapper.ProductImageMapper;
using static Garage.Utils.Mapper.SoldProductMapper;
using static Garage.Utils.AppEnumeration;
using Garage.Service.Email.EmailModels;
using Garage.Service.Email;
using Garage.Utils.Security;
using Rotativa;
using Rotativa.Options;
using Garage.ViewModels.EmailModels;
using Garage.ViewModels.Email;
using Garage.Service.GemsEmail;
using Garage.Areas.Non_IT.Service;
using Garage.Areas.Non_IT.Models;
using Garage.Areas.Non_IT.ViewModels;
using Garage.Areas.Non_IT.Services;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using Garage.Areas.Non_IT.Helper;
using Garage.Areas.Non_IT.ViewModel;
using Newtonsoft.Json;
using Garage.Areas.Non_IT.ViewModels.Email;

namespace Garage.Areas.Non_IT.Controllers
{
    public class NIProductController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly NICategoryService NIcategoryService = new NICategoryService();
        private readonly NIProductService NIproductService = new NIProductService();
        private readonly NIFileService NIfileService = new NIFileService();
        private readonly NIProductPostTrackService NIproductPostTrackService = new NIProductPostTrackService();
        private readonly NIAssetTrackService NIassetTrackService = new NIAssetTrackService();
        private readonly NIUserService NIuserService = new NIUserService();
        private readonly NISoldProductService soldProductService = new NISoldProductService();
        private readonly NIProductOrderService productOrderService = new NIProductOrderService();
        private readonly NIOrderService orderService = new NIOrderService();
        private readonly NIOrderTrackService orderTrackService = new NIOrderTrackService();
        private readonly NISchoolService schoolService = new NISchoolService();
        private readonly NIGeneralService NIgeneralservice = new NIGeneralService();


        // GET: Non_IT/NIProduct
        [AppAuthorize]
        [HttpGet, ActionName("Add")]
        public ActionResult AddProduct()
        {


            logger.Info("Product Controller -Adding New product");

            List<Non_IT.Models.Category> parentCategories = NIcategoryService.GetAllCategories().ToList();
            var parentCategoryList = parentCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.CategoryName.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            ViewBag.ParentCategoryList = parentCategoryList;

            List<Garage.Areas.Non_IT.Models.SubCategory> subCategories = NIcategoryService.GetSubCategories(null).ToList();
            var subCategoryList = subCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.SubCategoryName.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            ViewBag.SubCategoryList = subCategoryList;

            ViewBag.Conditions = NIgeneralservice.GetAllCondition().ToList().ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.DISPLAY.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            ViewBag.AssetTypes = NIgeneralservice.GetAllAssetTypes().ToList().ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.DISPLAY.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });


            return View("~/Areas/Non_IT/Views/NIProduct/AddProduct.cshtml", new NIProductForm());

        }

        [AppAuthorize]
        [HttpPost, ActionName("Save")]
        public ActionResult SaveProduct(NIProductForm productForm)
        {

            logger.Info("Product Controller - Saving product to the database");

            var newProduct = productForm.productViewModel;

            NIProduct savedProduct = NIproductService.AddProduct(newProduct, (int)ProductStatus.SavedAsDraft);
            productForm.productViewModel.Id = savedProduct.Id;

            if (productForm.productFileViewModel != null && productForm.productFileViewModel.ImageFile != null)
            {
                for (int i = 0; i < productForm.productFileViewModel.ImageFile.Length; i++)
                {
                    Helper.NonIT_Helper.SaveNIProductFiles(productForm.productFileViewModel.ImageFile[i], savedProduct.Id, true);
                }
            }
            var ReferenceFiles = "";
            if (productForm.productFileViewModel != null)
            {
                ReferenceFiles = productForm.productFileViewModel.ReferenceFiles;
            }
            if (!string.IsNullOrEmpty(ReferenceFiles))
            {
                List<int> deserializefilesID = (new JavaScriptSerializer()).Deserialize<List<int>>(ReferenceFiles);

                foreach (int id in deserializefilesID)
                    NIproductService.UpdateNIProductFileByID(id, savedProduct.Id);
            }

            ViewBag.savedProduct = savedProduct;
            TempData["productName"] = savedProduct.ProductName;
            return RedirectToAction("Draft", "NISchoolAdmin");

        }

        [AppAuthorize]
        [HttpPost, ActionName("Post")]
        public ActionResult PostProduct(NIProductForm productForm, bool? isNewProduct)
        {

            logger.Info("Product Controller - Saving the Product in the database As Post status");
            NIProduct savedProduct;
            if (isNewProduct ?? false)
            {
                var newProduct = productForm.productViewModel;

                savedProduct = NIproductService.AddProduct(newProduct, (int)ProductStatus.Posted);


                productForm.productViewModel.Id = savedProduct.Id;
                for (int i = 0; i < productForm.productFileViewModel.ImageFile.Length; i++)
                {
                    Helper.NonIT_Helper.SaveNIProductFiles(productForm.productFileViewModel.ImageFile[i], savedProduct.Id, true);
                }
                var ReferenceFiles = productForm.productFileViewModel.ReferenceFiles;
                if (!string.IsNullOrEmpty(ReferenceFiles))
                {
                    List<int> deserializefilesID = (new JavaScriptSerializer()).Deserialize<List<int>>(ReferenceFiles);

                    foreach (int id in deserializefilesID)
                        NIproductService.UpdateNIProductFileByID(id, savedProduct.Id);
                }

            }
            else
            {
                NIProduct product = NIproductService.GetProductById(productForm.productViewModel.Id);
                NIProduct postingProducts = Helper.NonIT_Helper.mapNIProduct(productForm, product);
                postingProducts.Status = (int)ProductStatus.Posted;
                postingProducts.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
                postingProducts.UpdatedDate = DateTime.Now;

                savedProduct = NIproductService.UpdateProduct(postingProducts);
            }
            if (savedProduct.AssetType.Value == (int)AppEnumeration.AssetType.Reuse)
            {
                NIproductPostTrackService.AddNIProductPostTrack(Helper.NonIT_Helper.niprodcutTrackmap(savedProduct));
            }
            else
            {
                NIassetTrackService.AddAssetTrack(Helper.NonIT_Helper.assetTrackmap(savedProduct));
            }

            Helper.NonIT_Helper.ApprovalMailTrigger(savedProduct);
            TempData["productName"] = savedProduct.ProductName;
            return RedirectToAction("ApprovalRequest", "NISchoolAdmin");

        }

        [AppAuthorize]
        [HttpPost, ActionName("UploadFiles")]
        public JsonResult SaveReferenceFiles()
        {
            int? ProductID = null;
            var FileExtension = System.Configuration.ConfigurationManager.AppSettings["FileExtension"].ToString();
            logger.Info("Product Controller - Saving product to the database");
            List<Areas.Non_IT.Models.ProductFile> FileDetails = new List<Areas.Non_IT.Models.ProductFile>();
            if (!string.IsNullOrEmpty(Request.Form.ToString()))
                ProductID = Convert.ToInt32(Request.Form.GetValues("ProductID")[0].ToString());
            for (int i = 0; i < Request.Files.Count; i++)
            {
                if (FileExtension.Split(',').Contains(System.IO.Path.GetExtension(Request.Files[i].FileName).Replace(".", String.Empty)))
                {
                    FileDetails.Add(Helper.NonIT_Helper.SaveNIProductFiles(Request.Files[i], ProductID, false));
                }

            }
            string myJsonString = (new JavaScriptSerializer()).Serialize(FileDetails);
            return Json(FileDetails, JsonRequestBehavior.AllowGet);

        }

        [AppAuthorize]
        [HttpPost, ActionName("DeleteFile")]

        [AppAuthorize]
        public JsonResult DeleteProductFile(int ID)
        {

            logger.Info("Product Controller - Delete product file to the database");
            var FileDetails = NIfileService.DeleteProductFileByID(ID);
            return Json(FileDetails, JsonRequestBehavior.AllowGet);

        }

        [AppAuthorize]
        [HttpGet, ActionName("DownloadFile")]
        public FileResult Download(int ID)
        {

            var result = NIfileService.GetProductFilesByID(ID, false);
            byte[] fileBytes = System.IO.File.ReadAllBytes(ConfigurationManager.AppSettings["NonITFilePath"] + result.FilePath);
            string fileName = result.FileName;
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

        }

        [AppAuthorize]
        [HttpGet, ActionName("Details")]
        public ActionResult ProductDetails(int id)
        {
            logger.Info("Product Controller - Viewing Product details in Admin Page");

            NIProduct niproduct = NIproductService.GetProductById(id);
            ViewBag.ProductDetail = Helper.NonIT_Helper.viewMap(niproduct);
            ViewBag.ProductFile = NIfileService.GetProductFilesByProductId(id);
            ViewBag.Product = niproduct;
            return PartialView("~/Areas/Non_IT/Views/Shared/_NIProductDetailsViewModal.cshtml");

        }
        [AppAuthorize]
        [HttpGet, ActionName("SoldDetails")]
        public ActionResult ProductSoldDetails(int id)
        {
            logger.Info("Product Controller - Viewing Product details in Admin Page");

            NIProduct niproduct = NIproductService.GetProductById(id);
            ViewBag.ProductDetail = Helper.NonIT_Helper.viewMap(niproduct);
            ViewBag.ProductFile = NIfileService.GetProductFilesByProductId(id);
            ViewBag.Product = niproduct;
            return PartialView("~/Areas/Non_IT/Views/Shared/_NIProductSoldDetails.cshtml");

        }
        [AppAuthorize]
        [HttpGet, ActionName("DetailsWithOrder")]
        public ActionResult DetailsWithOrder(int id)
        {
            logger.Info("Product Controller - Viewing Product details in Admin Page");
            var currentUser = Non_IT.AppUtils.getCurrentUser();
            var ProductOrder = NIproductService.GetProductOrderById(id);
            ViewBag.ProductOrder = ProductOrder;
            NIProduct niproduct = NIproductService.GetProductById(ProductOrder.ProductId.Value);
            ViewBag.ProductDetail = Helper.NonIT_Helper.viewMap(niproduct);
            ViewBag.Product = niproduct;
            ViewBag.OrderDetail = (new NIProductOrderService()).GetOrderVouchersDetail(ProductOrder.OrderId.Value, currentUser.school.Id);
            return PartialView("~/Areas/Non_IT/Views/Shared/_NIProductDetailsViewModal_Sale.cshtml");

        }

        [AppAuthorize]
        [HttpGet, ActionName("Track")]
        public ActionResult TrackProduct(int id)
        {

            logger.Info("Product Controller - Viewing Prodcut track in modal");

            var product = NIproductService.GetProductById(id);
            ViewBag.Product = product;
            var userListMap = NIuserService.GetAllUsers().ToDictionary(u => u.Id, u => u);
            ViewBag.UserListMap = userListMap;
            ViewBag.PublishedUser = userListMap[product.CreatedBy.Value];
            ViewBag.ProductTrack = NIproductPostTrackService.GetPostTrackByProductId(id);
            return PartialView("~/Areas/Non_IT/Views/Shared/_NIProductTrackModal.cshtml");

        }

        [AppAuthorize]
        [HttpGet, ActionName("Delete")]
        public ActionResult DeleteProduct(string id)
        {
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            logger.Info("Product Controller - Delete Individual product from Database");

            var deletedProduct = NIproductService.DeleteProduct(OriginalID);
            TempData["productName"] = deletedProduct.ProductName;
            TempData["deleted"] = deletedProduct.Deleted;
            return RedirectToAction("Draft", "NISchoolAdmin");

        }

        [AppAuthorize]
        [HttpGet, ActionName("Edit")]
        public ActionResult EditProduct(string id)
        {

            logger.Info("Product Controller - Edit Individual product from database");
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            NIProduct product = NIproductService.GetProductById(OriginalID);
            ViewBag.Product = product;
            if (product.RejectedUserId != null)
            {
                var rejectedUser = NIuserService.GetUserById(product.RejectedUserId ?? 0);
                ViewBag.User = rejectedUser;
            }
            NIProductForm productForm = Helper.NonIT_Helper.ReMapNIProduct(product);
            List<Areas.Non_IT.Models.Category> parentCategories = NIcategoryService.GetAllCategories().ToList();
            var parentCategoryList = parentCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.CategoryName.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            ViewBag.ParentCategoryList = parentCategoryList;

            List<Areas.Non_IT.Models.SubCategory> subCategories = NIcategoryService.GetSubCategories(null).Where(p => p.CategoryID == product.CategoryId.Value).ToList();
            var subCategoryList = subCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.SubCategoryName.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            ViewBag.SubCategoryList = subCategoryList;
            List<Areas.Non_IT.Models.ConditionEnum> AllConditions = NIgeneralservice.GetAllCondition().ToList();
            var AllConditionsList = AllConditions.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.DISPLAY.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            ViewBag.Conditions = AllConditionsList;
            ViewBag.ReferenceFiles = NIfileService.GetProductImageByProductId(product.Id, false);
            return View("~/Areas/Non_IT/Views/NIProduct/ProductEdit.cshtml", productForm);

        }

        [AppAuthorize]
        [HttpPost, ActionName("Update")]
        public ActionResult UpdateProduct(NIProductForm productForm)
        {

            logger.Info("Product Controller - Updating the exsisting product");

            NIProduct product = NIproductService.GetProductById(productForm.productViewModel.Id);
            var updatedProduct = NIproductService.UpdateProduct(Helper.NonIT_Helper.mapNIProduct(productForm, product));
            TempData["productName"] = updatedProduct.ProductName;
            TempData["editFlag"] = true;
            if (product.Status == (int)ProductStatus.Posted)
            {
                return RedirectToAction("ApprovalRequest", "NISchoolAdmin");
            }
            return RedirectToAction("Draft", "NISchoolAdmin");

        }

        [HttpGet, ActionName("ApprovedAsset")]
        public ActionResult ApprovedAsset()
        {
            List<NIProduct> result = new List<NIProduct>();
            var approvedAssets = NonIT_Helper.GetAllApprovedAssets().OrderByDescending(p => p.UpdatedDate).ToList();
            var currentUserRole = Non_IT.AppUtils.getCurrentUser().user.NIRole;


            foreach (NIProduct product in approvedAssets)
            {
                var role = NIassetTrackService.getNextAssetApproverByAssetId(product);
                if (currentUserRole.Value == (int)role)
                {
                    result.Add(product);
                }
            }
            approvedAssets = result;
            ViewBag.ApprovedAssets = approvedAssets;
            ViewBag.ApproverNameMap = NonIT_Helper.GetNextApproverName(approvedAssets);
            ViewBag.ValidApprovalMap = NonIT_Helper.GetValidApprovalMap(approvedAssets);
            ViewBag.SchoolNameMap = Helper.NonIT_Helper.getSchoolName(approvedAssets);
            List<Areas.Non_IT.Models.ConditionEnum> AllConditions = NIgeneralservice.GetAllCondition().ToList();
            var AllConditionsList = AllConditions.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.DISPLAY.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            ViewBag.Conditions = AllConditionsList;
            return View("~/Areas/Non_IT/Views/NIProduct/ApprovedAssets.cshtml");

        }

        [HttpGet, ActionName("AssetApproval")]
        public ActionResult AssetApproval(string id)
        {
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            logger.Info("Product Controller - Viewing the asset detail for Approval");

            NIProduct product = NIproductService.GetProductById(OriginalID);
            ViewBag.Product = product;
            NIProductForm productForm = Helper.NonIT_Helper.ReMapNIProduct(product);
            productForm.productViewModel.ReasonForReject = string.Empty;
            List<Garage.Areas.Non_IT.Models.Category> parentCategories = NIcategoryService.GetAllCategories().ToList();
            var parentCategoryList = parentCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.CategoryName.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            ViewBag.ParentCategoryList = parentCategoryList;
            List<Garage.Areas.Non_IT.Models.SubCategory> subCategories = NIcategoryService.GetSubCategories(null).ToList();
            var subCategoryList = subCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.SubCategoryName.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            ViewBag.SubCategoryList = subCategoryList;
            List<Areas.Non_IT.Models.ConditionEnum> AllConditions = NIgeneralservice.GetAllCondition().ToList();
            var AllConditionsList = AllConditions.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.DISPLAY.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            ViewBag.Conditions = AllConditionsList;

            var assetTrack = NIassetTrackService.GetPostTrackByAssetId(product.Id).OrderByDescending(p => p.CreatedDate).FirstOrDefault();
            ViewBag.AssetTrack = assetTrack;
            ViewBag.ReferenceFiles = NIfileService.GetProductImageByProductId(product.Id, false);
            return View("~/Areas/Non_IT/Views/NIProduct/AssetApproval.cshtml", productForm);


        }

        [AppAuthorize]
        [HttpPost, ActionName("AssetApproval")]
        public ActionResult Approval(NIProductForm productForm)
        {

            logger.Info("Product Controller - Saving the Approval Status for the asset");

            NIProduct approvedAsset = null;

            NIProduct product = NIproductService.GetProductById(productForm.productViewModel.Id);
            NIProduct updatedProduct = Helper.NonIT_Helper.mapNIProduct(productForm, product);
            Helper.NonIT_Helper.AssetDispose(updatedProduct);

            TempData["productName"] = updatedProduct.ProductName;
            TempData["approved"] = true;
            return RedirectToAction("ApprovalRequest", "NISchoolAdmin");

        }


        [AppAuthorize]
        [HttpGet, ActionName("ApprovalView")]
        public ActionResult ProductApprovalView(string id)
        {
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            logger.Info("Product Controller - Viewing the prodcut detail for Approval");

            NIProduct product = NIproductService.GetProductById(OriginalID);
            ViewBag.Product = product;
            NIProductForm productForm = NonIT_Helper.ReMapNIProduct(product);
            productForm.productViewModel.ReasonForReject = "";
            List<Garage.Areas.Non_IT.Models.Category> parentCategories = NIcategoryService.GetAllCategories().ToList();
            var parentCategoryList = parentCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.CategoryName.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            ViewBag.ParentCategoryList = parentCategoryList;

            List<Garage.Areas.Non_IT.Models.SubCategory> subCategories = NIcategoryService.GetSubCategories(null).ToList();
            var subCategoryList = subCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.SubCategoryName.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });


            ViewBag.SubCategoryList = subCategoryList;

            List<Areas.Non_IT.Models.ConditionEnum> AllConditions = NIgeneralservice.GetAllCondition().ToList();
            var AllConditionsList = AllConditions.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.DISPLAY.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            ViewBag.AllConditionsList = AllConditionsList;

            ViewBag.ProductTrack = NIproductPostTrackService.GetPostTrackByProductId(product.Id).OrderByDescending(p => p.CreatedDate).FirstOrDefault();
            ViewBag.ReferenceFiles = NIfileService.GetProductImageByProductId(product.Id, false);
            //ViewBag.ProductTrackList = productPostTrackService.GetPostTrackByProductId(product.Id).OrderByDescending(p => p.CreatedDate).ToList();
            return View("~/Areas/Non_IT/Views/NIProduct/ProductApproval.cshtml", productForm);

        }

        [AppAuthorize]
        [HttpPost, ActionName("Approval")]
        public ActionResult ApprovalProduct(NIProductForm productForm)
        {

            logger.Info("Product Controller - Saving the Approval Status for the product");

            NIProduct approvedProduct;

            NIProduct product = NIproductService.GetProductById(productForm.productViewModel.Id);
            NIProduct updatedProduct = Helper.NonIT_Helper.mapNIProduct(productForm, product);
            var currentUser = Non_IT.AppUtils.getCurrentUser();
            if (currentUser.user.NIRole == (int)AppEnumeration.NIUserRole.GCOREALM || currentUser.user.NIRole == (int)AppEnumeration.NIUserRole.PROCUREMENT)
            {
                updatedProduct.Status = (int)ProductStatus.Published;
                updatedProduct.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
                updatedProduct.UpdatedDate = DateTime.Now;
                approvedProduct = NIproductService.UpdateProduct(updatedProduct);
                Helper.NonIT_Helper.ProductStatusEmail(updatedProduct);
            }
            else
            {
                updatedProduct.Status = (int)ProductStatus.Posted;
                updatedProduct.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
                updatedProduct.UpdatedDate = DateTime.Now;
                approvedProduct = NIproductService.UpdateProduct(updatedProduct);

                var ReferenceFiles = productForm.productFileViewModel.ReferenceFiles;
                if (!string.IsNullOrEmpty(ReferenceFiles))
                {
                    List<int> deserializefilesID = (new JavaScriptSerializer()).Deserialize<List<int>>(ReferenceFiles);
                    if (deserializefilesID.Count > 0)
                    {
                        foreach (int id in deserializefilesID)
                        {
                            NIproductService.UpdateNIProductFileByID(id, updatedProduct.Id);
                        }
                    }

                }
                Helper.NonIT_Helper.ApprovalMailTrigger(approvedProduct);
            }

            if (approvedProduct.AssetType == (int)AppEnumeration.AssetType.Reuse)
            {
                NIproductPostTrackService.AddNIProductPostTrack(Helper.NonIT_Helper.niprodcutTrackmap(approvedProduct));
            }
            else
            {
                NIassetTrackService.AddAssetTrack(Helper.NonIT_Helper.assetTrackmap(approvedProduct));
            }

            TempData["productName"] = approvedProduct.ProductName;
            TempData["approved"] = true;
            return RedirectToAction("ApprovalRequest", "NISchoolAdmin");

        }

        [AppAuthorize]
        [HttpPost, ActionName("Reject")]
        public ActionResult RejectProduct(NIProductForm productForm)
        {

            logger.Info("Product Controller - Rejecting the product");

            NIProduct product = NIproductService.GetProductById(productForm.productViewModel.Id);
            product = Helper.NonIT_Helper.logRejectReason(product, productForm);
            if (product.AssetType == (int)AppEnumeration.AssetType.Reuse)
            {
                NIproductPostTrackService.DeleteProductTrackByProduct(product.Id);
            }
            else
            {
                NIassetTrackService.DeleteAssetTrackByProduct(product.Id);
            }
            Helper.NonIT_Helper.RejectAssetEmail(product);
            TempData["productName"] = product.ProductName;
            TempData["rejected"] = true;
            return RedirectToAction("ApprovalRequest", "NISchoolAdmin");

        }


        [AppAuthorize]
        [HttpGet, ActionName("ApprovalMail")]
        public ActionResult ApprovalMail(int productId, int userRole)
        {

            logger.Info("Product Controller - Approval Mail Send to Particular Role");
            var product = NIproductService.GetProductById(productId);
            Helper.NonIT_Helper.ApprovalMailRemainder(product, userRole);
            return Json(new { isSend = true }, JsonRequestBehavior.AllowGet);

        }

        [AppAuthorize]
        [HttpGet, ActionName("DisposedAssets")]
        public ActionResult DisposedAssets(bool print = false)
        {

            ViewBag.DisposedAssets = Helper.NonIT_Helper.GetDisposedAssets();
            if (print)
            {
                return new PartialViewAsPdf("~/Areas/Non_IT/Views/NIProduct/Pdf/DisposedProductsPdf.cshtml")
                {
                    PageOrientation = Orientation.Portrait,
                    PageSize = Size.A3,
                    CustomSwitches = "--footer-center \" [page] Page of [toPage] Pages\" --footer-line --footer-font-size \"9\" --footer-spacing 5 --footer-font-name \"calibri light\"",
                    FileName = "DisposedAssets.pdf"
                };
            }
            return View("~/Areas/Non_IT/Views/NIProduct/DisposedAssets.cshtml");

        }

        [AppAuthorize]
        [HttpGet, ActionName("View")]
        public ActionResult ViewProduct(int id)
        {

            logger.Info("Product Controller - Viewing Individual product");
            var currentUser = Non_IT.AppUtils.getCurrentUser();
            NIProduct product = NIproductService.GetProductById(id);
            if (currentUser != null && !currentUser.isNISuperAdmin && product.SchoolId != currentUser.school.Id)
            {
                product.ViewCount = product.ViewCount + 1;
                NIproductService.UpdateProduct(product);
            }
            ViewBag.productDetail = Helper.NonIT_Helper.viewMap(product);
            //ViewBag.productImage = productImageService.GetSingleProductImageByProductId(product.Id);
            return PartialView("~/Areas/Non_IT/Views/Shared/_NIProductDetailsModal.cshtml", new AddToCartForm());

        }

        [AppAuthorize]
        [HttpGet, ActionName("RecentProducts")]
        public ActionResult RecentProducts()
        {
            logger.Info("Landing Controller - Listing Recent products in recent prodcuts page");
            //nahyan on 29jan20
            //  var productList = productService.GetAllPublishedProducts().Where(p => p.CreatedDate > DateTime.Now.AddDays(-7)).OrderByDescending(p => p.CreatedDate).ToList();
            //  var productList = productService.GetAllPublishedProducts().OrderByDescending(p => p.CreatedDate).ToList();
            var productList = NIproductService.GetAllPublishedProducts().OrderByDescending(p => p.CreatedDate).ThenBy(p => p.UnitValue).ToList();
            //  var productList1 = productService.GetAllPublishedProducts().OrderByDescending(p => p.UnitValue).ToList();
            ViewBag.RecentProducts = Helper.NonIT_Helper.GetProductListView(productList);
            ViewBag.ParentCategories = NIcategoryService.GetAllCategories().ToList();
            return View();

        }

        [AppAuthorize]
        [HttpGet, ActionName("List")]
        public ActionResult ProductListByCategory(string categoryId, string subCategoryId)
        {

            logger.Info("Product Controller - Listing of product in E-Commerce Page by Category");
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(categoryId));
            int? _subCategoryId = null;
            if (!string.IsNullOrEmpty(subCategoryId))
            {
                _subCategoryId = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(subCategoryId));
            }
            List<NIProduct> productList;
            Garage.Areas.Non_IT.Models.Category currentCategory = new Garage.Areas.Non_IT.Models.Category();
            var AllCategory = NIcategoryService.GetAllCategories().ToList();
            List<Garage.Areas.Non_IT.Models.SubCategory> subCategoryList = NIcategoryService.GetSubCategoriesByCID(OriginalID).ToList();
            if (subCategoryId == null)
            {
                currentCategory = NIcategoryService.GetCategoryById(OriginalID);
                productList = NIproductService.GetAllPublishedProducts().Where(p => p.CategoryId == OriginalID).ToList();
            }
            else
            {
                currentCategory = NIcategoryService.GetCategoryBySubCategoryID(_subCategoryId ?? 0);
                productList = NIproductService.GetAllPublishedProducts().Where(p => p.SubCategoryId == _subCategoryId).ToList();
            }
            ViewBag.ProductList = Helper.NonIT_Helper.GetProductListView(productList);
            ViewBag.CurrentCategory = currentCategory;
            ViewBag.SubCategoryList = subCategoryList;
            ViewBag.ParentCategories = AllCategory;
            return View("~/Areas/Non_IT/Views/NIProduct/ProductList.cshtml");

        }

        [AppAuthorize]
        [HttpGet, ActionName("SoldProducts")]
        public ActionResult SoldProducts()
        {

            logger.Info("Product Controller - List of Sold Products from database");

            var soldProductsList = ViewBag.SoldProducts = soldProductService.GetAllSoldProducts().Where(p => p.Status == (int)SoldProductStatus.Scheduled).OrderByDescending(p => p.CreatedDate).ToList();
            ViewBag.SoldProducts = Helper.NonIT_Helper.GetSoldProductDetailList(soldProductsList);
            return View("~/Areas/Non_IT/Views/NIProduct/SoldProducts.cshtml");

        }

        [AppAuthorize]
        [HttpGet, ActionName("Sold")]
        public ActionResult Sold()
        {

            logger.Info("Product Controller - List of Sold Products from database for school users");
            var soldProductsList = soldProductService.GetAllSoldProducts().Where(p => (Areas.Non_IT.AppUtils.getCurrentUser().user.NIRole.Value == (int)AppEnumeration.NIAdminRole.GEMSADMINROLE) || p.SchoolId == Non_IT.AppUtils.getCurrentUser().school.Id).OrderByDescending(p => p.CreatedDate).ToList();
            ViewBag.SoldProducts = Helper.NonIT_Helper.GetSoldProductDetailList(soldProductsList);
            return View("~/Areas/Non_IT/Views/NIProduct/Sold.cshtml");

        }

        [AppAuthorize]
        [HttpGet, ActionName("AssetTransferTrack")]
        public ActionResult AssetTransferTrack(int id, bool print = false)
        {

            var soldProduct = soldProductService.GetSoldProductById(id);
            var order = orderService.GetOrderById(soldProduct.OrderId);
            var product = NIproductService.GetProductById(soldProduct.ProductId);
            ViewBag.UserListMap = NIuserService.GetAllUsers().ToDictionary(u => u.Id, u => u);
            ViewBag.Order = order;
            ViewBag.Product = product;
            ViewBag.OrderTrack = orderTrackService.GetOrderTrackByOrderId(order.Id);
            ViewBag.ProductTrack = NIproductPostTrackService.GetPostTrackByProductId(product.Id);
            ViewBag.SoldProduct = soldProduct;
            ViewBag.SoldSchool = schoolService.GetSchoolById(product.SchoolId ?? 0);
            ViewBag.ProcuredSchool = schoolService.GetSchoolById(order.SchoolId ?? 0);
            ViewBag.ProductOrder = productOrderService.GetProductOrderById(soldProduct.ProductOrderId);
            if (print)
            {
                return new PartialViewAsPdf("~/Areas/Non_IT/Views/NIProduct/Pdf/AssetTransferTrackPdf.cshtml")
                {
                    PageOrientation = Orientation.Portrait,
                    PageSize = Size.A3,
                    CustomSwitches = "--footer-center \" [page] Page of [toPage] Pages\" --footer-line --footer-font-size \"9\" --footer-spacing 5 --footer-font-name \"calibri light\"",
                    FileName = "AssetTransferTrackPdf.pdf"
                };
            }
            return View("~/Areas/Non_IT/Views/NIProduct/AssetTransferTrack.cshtml");

        }

        [AppAuthorize]
        [HttpPost, ActionName("Scheduled")]
        public ActionResult Scheduled(string value, string pk)
        {
            var scheduledDate = DateTime.Parse(value);
            var soldProductId = Convert.ToInt32(pk);
            soldProductService.UpdateSoldProduct(Helper.NonIT_Helper.updateScheduleDate(scheduledDate, soldProductId));
            var soldProduct = soldProductService.GetSoldProductById(soldProductId);
            var order = orderService.GetOrderById(soldProduct.OrderId);
            orderTrackService.AddOrderTrack(Helper.NonIT_Helper.sellerOrderTrackMap(order));
            var productOrderList = productOrderService.GetPOByOrderId(order.Id);
            var listOfProductId = productOrderList.Select(p => p.ProductId);
            var productList = NIproductService.GetAllProducts().Where(p => listOfProductId.Contains(p.Id)).ToList();
            List<NIProductDetail> productDetails = Helper.NonIT_Helper.getProductDetails(productList);
            var buyer = NIuserService.GetUserById(order.CreatedBy ?? 0);
            var ICTEmailList = Helper.NonIT_Helper.getAllMSOEmailBySchoolId(buyer.School ?? 0);
            var soldProductList = soldProductService.GetAllSoldProducts().Where(s => s.OrderId == order.Id).ToList();
            var productDetail = productDetails.Where(pd => pd.Id == soldProduct.ProductId.ToString()).FirstOrDefault();
            Helper.NonIT_Helper.AfterScheduledMail(order);
            return Json(new { isUpdated = true, soldProduct }, JsonRequestBehavior.AllowGet);

        }

        [AppAuthorize]
        [HttpGet, ActionName("BookValueTransfer")]
        public ActionResult BookValueTransfer(int id, bool daxupdate, string bookvalue)
        {
            NIOrderVouchersDetail _NiVOrder = new NIOrderVouchersDetail();
            var details = Request.Headers.GetValues("detail");
            if (details != null && details.Count() > 0)
            {
                var serializer = new JavaScriptSerializer();
                _NiVOrder = serializer.Deserialize<NIOrderVouchersDetail>(details[0].ToString());

            }
            var soldProduct = soldProductService.GetSoldProductById(id);
            var order = orderService.GetOrderById(soldProduct.OrderId);
            var POService = productOrderService.GetProductOrderById(soldProduct.OrderId);
            var product = (new NIProductService()).GetProductById(POService.ProductId.Value);
            POService.ProductValue = POService.Quantity.Value * (product.OriginalUnitValue.Value - _NiVOrder.UnitDepValue.Value);
            POService.DaxUpdated = daxupdate;
            productOrderService.UpdateProductOrder(POService);
            orderTrackService.AddOrderTrack(Helper.NonIT_Helper.sellerOrderTrackMap(order));
            soldProductService.UpdateSoldProduct(Helper.NonIT_Helper.waitingForScheduleMap(soldProduct));

            var OrderUser = NIuserService.GetUserById(order.CreatedBy.Value);

            var ProductAccountEmails = NIuserService.GetUserBySchoolAndRoleID(OrderUser.School.Value, 3);
            var school = schoolService.GetSchoolById(order.SchoolId ?? 0);
            var productOrder = productOrderService.GetPOByOrderId(order.Id).FirstOrDefault();
            var orderDetails = new NIEmailNIOrderDetails()
            {
                ProductDetail = Helper.NonIT_Helper.viewMap(product),
                ProductOrder = productOrder,
                BuyerSchoolName = school.SchoolName
            };
            var emailbody = EmailFactory.ParseTemplate(orderDetails, Non_IT.AppUtils.orderApproveAccountBuyerMailTemplate);

            GemsToken.SendMail(ProductAccountEmails, "Purchase Approval", emailbody, true);


            if (_NiVOrder != null)
                productOrderService.AddOrderVouchersDetail(_NiVOrder);


            return RedirectToAction("Sold", "NIProduct");

        }

        [AppAuthorize]
        [HttpGet, ActionName("Dispatched")]
        public ActionResult DispatchedProducts(bool print = false)
        {

            logger.Info("Product Controller - List of Dispatched Products from database for school users");
            var soldProductsList = soldProductService.GetAllSoldProducts().Where(p => p.Status == (int)SoldProductStatus.Dispatched).OrderByDescending(p => p.CreatedDate).ToList();
            ViewBag.SoldProducts = Helper.NonIT_Helper.GetSoldProductDetailList(soldProductsList);
            if (print)
            {
                return new PartialViewAsPdf("~/Areas/Non_IT/Views/NIProduct/Pdf/DispatchedProductsPdf.cshtml")
                {
                    PageOrientation = Orientation.Portrait,
                    PageSize = Size.A3,
                    CustomSwitches = "--footer-center \" [page] Page of [toPage] Pages\" --footer-line --footer-font-size \"9\" --footer-spacing 5 --footer-font-name \"calibri light\"",
                    FileName = "DispatchedProducts.pdf"
                };
            }
            return View("~/Areas/Non_IT/Views/NIProduct/DispatchedProducts.cshtml");


        }

        [AppAuthorize]
        [HttpGet, ActionName("AssetTrack")]
        public ActionResult AssetTrack(int id, bool print = false)
        {

            logger.Info("Product Controller - Viewing Prodcut track in modal");
            var product = NIproductService.GetProductById(id);
            ViewBag.Product = product;
            ViewBag.UserListMap = NIuserService.GetAllUsers().ToDictionary(u => u.Id, u => u);
            ViewBag.AssetTrack = NIassetTrackService.GetPostTrackByAssetId(id);
            if (product.ApprovalReq)
            {
                if (print)
                {
                    return new PartialViewAsPdf("~/Areas/Non_IT/Views/NIProduct/Pdf/CorpReqAssetTrackPdf.cshtml")
                    {
                        PageOrientation = Orientation.Portrait,
                        PageSize = Size.A3,
                        CustomSwitches = "--footer-center \" [page] Page of [toPage] Pages\" --footer-line --footer-font-size \"9\" --footer-spacing 5 --footer-font-name \"calibri light\"",
                        FileName = "CorpReqAssetTrackPdf.pdf"
                    };
                }
                return PartialView("~/Areas/Non_IT/Views/NIProduct/CorpReqAssetTrack.cshtml");
            }
            else
            {
                if (print)
                {
                    return new PartialViewAsPdf("~/Areas/Non_IT/Views/NIProduct/Pdf/AssetTrackPdf.cshtml")
                    {
                        PageOrientation = Orientation.Portrait,
                        PageSize = Size.A3,
                        CustomSwitches = "--footer-center \" [page] Page of [toPage] Pages\" --footer-line --footer-font-size \"9\" --footer-spacing 5 --footer-font-name \"calibri light\"",
                        FileName = "AssetTrackPdf.pdf"
                    };
                }
                return PartialView("~/Areas/Non_IT/Views/NIProduct/AssetTrack.cshtml");
            }

        }


        #region Disposal
        [AppAuthorize]
        [HttpGet, ActionName("DisposeAsset")]
        public ActionResult ApprovedAssets(string id)
        {
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            logger.Info("Product Controller - Viewing the asset detail for Approval");

            NIProduct product = NIproductService.GetProductById(OriginalID);
            ViewBag.Product = product;
            NIProductForm productForm = Helper.NonIT_Helper.ReMapNIProduct(product);


            List<Garage.Areas.Non_IT.Models.Category> parentCategories = NIcategoryService.GetAllCategories().ToList();
            var parentCategoryList = parentCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.CategoryName.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            ViewBag.ReferenceFiles = NIfileService.GetProductImageByProductId(product.Id, false);
            ViewBag.ParentCategoryList = parentCategoryList;

            List<Garage.Areas.Non_IT.Models.SubCategory> subCategories = NIcategoryService.GetSubCategories(null).ToList();
            var subCategoryList = subCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.SubCategoryName.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });

            ViewBag.SubCategoryList = subCategoryList;

            var assetTrack = NIassetTrackService.GetPostTrackByAssetId(product.Id).OrderByDescending(p => p.CreatedDate).FirstOrDefault();
            ViewBag.AssetTrack = assetTrack;
            ViewBag.Conditions = NIgeneralservice.GetAllCondition().ToList().ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.DISPLAY.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            return View("~/Areas/Non_IT/Views/NIProduct/CorpAssetApproval.cshtml", productForm);
        }


        [AppAuthorize]
        [HttpPost, ActionName("DisposeAsset")]
        public ActionResult ApprovedAssets(NIProductForm productForm)
        {

            logger.Info("Product Controller - Saving the Approval Status for the asset");
            NIProduct approvedAsset = null;
            NIProduct product = NIproductService.GetProductById(productForm.productViewModel.Id);
            NIProduct updatedProduct = Helper.NonIT_Helper.mapNIProduct(productForm, product);

            Helper.NonIT_Helper.AdminAssetDispose(updatedProduct, productForm.Remarks);
            TempData["productName"] = updatedProduct.ProductName;
            return RedirectToAction("ApprovedAsset", "NIProduct");

        }

        [AppAuthorize]
        [HttpPost, ActionName("RejectAsset")]
        public ActionResult RejectAsset(NIProductForm productForm)
        {

            logger.Info("Product Controller - Rejecting the product");
            NIProduct product = NIproductService.GetProductById(productForm.productViewModel.Id);
            product = Helper.NonIT_Helper.logRejectReason(product, productForm);
            NIassetTrackService.DeleteAssetTrackByProduct(product.Id);
            Helper.NonIT_Helper.RejectAssetEmail(product);
            TempData["productName"] = product.ProductName;
            TempData["rejected"] = true;
            return RedirectToAction("ApprovedAsset", "NIProduct");


        }
        #endregion

        [AppAuthorize]
        [HttpGet, ActionName("RejectedProducts")]
        public ActionResult RejectedProducts(List<NIProduct> rejectedProducts)
        {

            logger.Info("Product Controller - Listing of Rejected Products");
            logger.Info("Product Controller - Listing of Rejected Products");
            if (rejectedProducts != null && (rejectedProducts.Any()))
            {
                ViewBag.DraftedProducts = rejectedProducts;
            }
            else
            {
                ViewBag.DraftedProducts = Helper.NonIT_Helper.GetRejectedProducts();
            }
            return View("RejectedProducts");

        }

        [AppAuthorize]
        [HttpPost, ActionName("Reuse")]
        public ActionResult ReUseAsset(AssetReuseForm assetReuseForm)
        {

            logger.Info("Asset Controller - Reuse Asset");
            NIProduct reUsableProduct = NIproductService.GetProductById(assetReuseForm.ProductId);
            var productForm = Helper.NonIT_Helper.ReMapNIProduct(reUsableProduct);
            reUsableProduct.Quantity = reUsableProduct.Quantity - assetReuseForm.ReuseQuantity;
            //product.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            reUsableProduct.UpdatedDate = DateTime.Now;
            NIproductService.UpdateProduct(reUsableProduct);
            NIProduct newProduct = NIproductService.AddProduct((Helper.NonIT_Helper.MapDisposeToPublished(productForm, assetReuseForm)));
            var productImages = NIfileService.AddProductImageList(Helper.NonIT_Helper.SetNewImagesFromProduct(reUsableProduct, newProduct.Id));
            var assetTrackList = NIassetTrackService.GetPostTrackByAssetId(reUsableProduct.Id);
            NIproductPostTrackService.AddProductPostTrackList(Helper.NonIT_Helper.DiposedTrackToPostTrack(assetTrackList, newProduct));
            Helper.NonIT_Helper.ReUseMail(newProduct);
            TempData["productName"] = newProduct.ProductName;
            TempData["published"] = true;
            return RedirectToAction("DisposedAssets", "NIProduct");

        }

        [AppAuthorize]
        [HttpPost, ActionName("BulkApproval")]
        public bool BulkApproval(string IDs, string ProductRef, string Remarks)
        {
            bool returnvalue = false;
            try
            {
                var NiRoleID = Non_IT.AppUtils.getCurrentUser().user.NIRole;
                var currentUserRole = (AppEnumeration.NIDisposeUserRole)NiRoleID;
                var arr = IDs.Split(',');
                var reference = ProductRef.Split(',');
                if (currentUserRole == NIDisposeUserRole.HOS || currentUserRole == NIDisposeUserRole.GCOREALM || NiRoleID == (int)AppEnumeration.NIUserRole.PROCUREMENT )
                {
                    for (int i = 0; i < arr.Length; i++)
                    {
                        string ProductID = arr[i];
                        NIProduct updatedProduct = NIproductService.GetProductById(int.Parse(ProductID));
                        if (currentUserRole == NIDisposeUserRole.GCOREALM || NiRoleID == (int)AppEnumeration.NIUserRole.PROCUREMENT)
                            updatedProduct.Status = (int)ProductStatus.Published;
                        else
                            updatedProduct.Status = (int)ProductStatus.Posted;
                        updatedProduct.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
                        updatedProduct.UpdatedDate = DateTime.Now;
                        if (currentUserRole == NIDisposeUserRole.HOS)
                            updatedProduct.PrincipalNote = Remarks;
                        if (currentUserRole == NIDisposeUserRole.GCOREALM || NiRoleID == (int)AppEnumeration.NIUserRole.PROCUREMENT)
                            updatedProduct.ApprovalRemarks = Remarks;
                        var approvedProduct = NIproductService.UpdateProduct(updatedProduct);
                        Helper.NonIT_Helper.ApprovalMailTrigger(approvedProduct);
                        if (approvedProduct.AssetType == (int)AppEnumeration.AssetType.Reuse)
                        {
                            NIproductPostTrackService.AddNIProductPostTrack(Helper.NonIT_Helper.niprodcutTrackmap(approvedProduct));
                        }
                        else
                        {
                            Helper.NonIT_Helper.AssetDispose(updatedProduct);
                        }
                    }
                }

                if (currentUserRole == NIDisposeUserRole.SSCFINANCE || currentUserRole == NIDisposeUserRole.SSCINTERNALAUDIT ||
                    currentUserRole == NIDisposeUserRole.SVPTreasury || currentUserRole == NIDisposeUserRole.CFO
                    )
                {
                    for (int i = 0; i < arr.Length; i++)
                    {
                        string ProductID = arr[i];
                        NIProduct updatedProduct = NIproductService.GetProductById(int.Parse(ProductID));
                        Helper.NonIT_Helper.AdminAssetDispose(updatedProduct, Remarks);

                    }

                }
                returnvalue = true;
                TempData["succeed"] = "1";
            }
            catch (Exception ex)
            {

            }
            return returnvalue;
        }



        public class Vouchers
        {
            public string Id { get; set; }
            public string VouchersNumber { get; set; }
            public string Narration { get; set; }

        }
        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            logger.Error("Error : " + DateTime.Now + "   " + filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "NIErrorHandler");
            Non_IT.Helper.NonIT_Helper.AddErrorLog(new Error_Log()
            {
                Area = "NON IT",
                Controller = "NIProduct",
                ErrorMessage = Non_IT.AppUtils.getCurrentUser() == null ? "User Session Out" : filterContext.Exception.Message,
                ErrorDate = DateTime.Now,
                UserID = Non_IT.AppUtils.getCurrentUser() == null ? 0 : Non_IT.AppUtils.getCurrentUser().user.Id

            });
        }
    }
}
