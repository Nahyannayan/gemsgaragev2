﻿using Garage.Areas.Non_IT.Models;
using Garage.Areas.Non_IT.Service;
using Garage.Areas.Non_IT.Services;
using Garage.Areas.Non_IT.ViewModels;
using Garage.Service;
using Garage.Service.Email;
using Garage.Utils;
using Garage.Utils.Security;
using Garage.ViewModels;
using Rotativa;
using Rotativa.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Garage.Utils.AppEnumeration;

namespace Garage.Areas.Non_IT.Controllers
{

    public class NISchoolAdminController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly NIProductService niproductService = new NIProductService();
        private readonly NISoldProductService nisoldproductService = new NISoldProductService();
        private readonly NIUserService niUserService = new NIUserService();


        [AppAuthorize]
        [HttpGet, ActionName("Draft")]
        public ActionResult Draft()
        {
            logger.Info("SchoolAdmin Controller - Listing of Drafted Products for Individual user");
            ViewBag.DraftList = getDraftList().OrderByDescending(p => p.CreatedDate).ToList();
            return View("~/Areas/Non_IT/Views/NISchoolAdmin/Draft.cshtml");
        }

        private List<NIDraftProduct> getDraftList()
        {
            List<NIDraftProduct> Draft = new List<NIDraftProduct>();
            var result = niproductService.GetAllProduct().Where(p => p.Deleted == false && (Areas.Non_IT.AppUtils.getCurrentUser().user.NIRole.Value == (int)AppEnumeration.NIAdminRole.GEMSADMINROLE || p.SchoolId == Areas.Non_IT.AppUtils.getCurrentUser().school.Id)).ToList();
            var DraftedProducts = result.ConvertAll(p => new Non_IT.ViewModels.NIDraftProduct() { Id = p.Id, ProductName = p.ProductName, CreatedDate = p.CreatedDate ?? DateTime.Now, DraftType = (AppEnumeration.DraftType)p.AssetType, Product = p });
            Draft.AddRange(DraftedProducts);
            return Draft;
            // getDraftedProducts() => productService.GetAllProducts().Where(p => p.Status == (int)ProductStatus.SavedAsDraft && p.UserId == AppUtils.getCurrentUser().user.Id && p.Deleted == Convert.ToBoolean(YNStatue.No)).OrderByDescending(p => p.CreatedDate).ToList()
        }


        [HttpGet, ActionName("ApprovalRequest")]
        public ActionResult ApprovalRequest()
        {
            logger.Info("SchoolAdmin Controller - Listing of Approval Request for Individual user");
            ViewBag.ApprovalRequestList = Helper.NonIT_Helper.getApprovalRequest();
            return View("~/Areas/Non_IT/Views/NISchoolAdmin/ApprovalRequest.cshtml");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Completed")]
        public ActionResult CompletedOrders(bool print = false)
        {
            logger.Info("Order Controller - Listing all the orders from database");
            ViewBag.OrderList = Helper.NonIT_Helper.GetCompletedOrderDetails();
            if (print)
            {
                return new PartialViewAsPdf("~/Areas/Non_IT/Views/NISchoolAdmin/Pdf/CompletedOrdersPdf.cshtml")
                {
                    PageOrientation = Orientation.Portrait,
                    PageSize = Size.A3,
                    CustomSwitches = "--footer-center \" [page] Page of [toPage] Pages\" --footer-line --footer-font-size \"9\" --footer-spacing 5 --footer-font-name \"calibri light\"",
                    FileName = "CompletedOrders.pdf"
                };
            }
            return View("~/Areas/Non_IT/Views/NISchoolAdmin/CompletedOrders.cshtml");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Dispatched")]
        public ActionResult DispatchedProducts(bool print = false)
        {
            logger.Info("Product Controller - List of Dispatched Products from database for school users");
            var soldProductsList = nisoldproductService.GetAllSoldProducts().Where(p => (Areas.Non_IT.AppUtils.getCurrentUser().user.NIRole.Value == (int)AppEnumeration.NIAdminRole.GEMSADMINROLE || p.SchoolId == Areas.Non_IT.AppUtils.getCurrentUser().school.Id) && p.Status == (int)SoldProductStatus.Dispatched).OrderByDescending(p => p.CreatedDate).ToList();
            var SoldProducts = Helper.NonIT_Helper.GetSoldProductDetailList(soldProductsList);
            if (Areas.Non_IT.AppUtils.getCurrentUser().user.NIRole.Value == (int)AppEnumeration.NIUserRole.PROCUREMENT)
            {
                SoldProducts = SoldProducts.Where(p => p.Product.CategoryId == Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ProcurementCategoryID"].ToString())).ToList();
            }
            else if (Areas.Non_IT.AppUtils.getCurrentUser().user.NIRole.Value == (int)AppEnumeration.NIUserRole.GCOREALM)
            {
                SoldProducts = SoldProducts.Where(p => p.Product.CategoryId != Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ProcurementCategoryID"].ToString())).ToList();
            }
            ViewBag.SoldProducts = SoldProducts;
            if (print)
            {
                return new PartialViewAsPdf("~/Areas/Non_IT/Views/NISchoolAdmin/Pdf/DispatchedProductsPdf.cshtml")
                {
                    PageOrientation = Orientation.Portrait,
                    PageSize = Size.A3,
                    CustomSwitches = "--footer-center \" [page] Page of [toPage] Pages\" --footer-line --footer-font-size \"9\" --footer-spacing 5 --footer-font-name \"calibri light\"",
                    FileName = "DispatchedAsset.pdf"
                };
            }
            return View("~/Areas/Non_IT/Views/NISchoolAdmin/DispatchedProducts.cshtml");
        }

        [AppAuthorize]
        [HttpGet, ActionName("DisposedAsset")]
        public ActionResult Disposed(bool print = false)
        {
            ViewBag.DisposedProductList = Helper.NonIT_Helper.GetDisposedProducts();
            ViewBag.UserListMap = niUserService.GetAllUsers().ToDictionary(u => u.Id, u => u);
            if (print)
            {
                return new PartialViewAsPdf("~/Areas/Non_IT/Views/NISchoolAdmin/Pdf/DisposedAssetPdf.cshtml")
                {
                    PageOrientation = Orientation.Portrait,
                    PageSize = Size.A3,
                    CustomSwitches = "--footer-center \" [page] Page of [toPage] Pages\" --footer-line --footer-font-size \"9\" --footer-spacing 5 --footer-font-name \"calibri light\"",
                    FileName = "DisposedAsset.pdf"
                };
            }
            return View("~/Areas/Non_IT/Views/NISchoolAdmin/DisposedAsset.cshtml");
        }
        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            logger.Error("Error : " + DateTime.Now + "   " + filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "NIErrorHandler");
            Non_IT.Helper.NonIT_Helper.AddErrorLog(new Error_Log()
            {
                Area = "NON IT",
                Controller = "NISchool",
                ErrorMessage = Non_IT.AppUtils.getCurrentUser() == null ? "User Session Out" : filterContext.Exception.Message,
                ErrorDate = DateTime.Now,
                UserID = Non_IT.AppUtils.getCurrentUser() == null ? 0 : Non_IT.AppUtils.getCurrentUser().user.Id

            });
        }
    }
}