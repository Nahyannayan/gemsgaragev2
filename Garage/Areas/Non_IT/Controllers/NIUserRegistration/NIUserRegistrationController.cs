﻿using Garage.Areas.Non_IT.Models;
using Garage.Areas.Non_IT.Service;
using Garage.Areas.Non_IT.Services;
using Garage.Areas.Non_IT.ViewModels;
using Garage.Service;
using Garage.Service.Email;
using Garage.Utils;
using Garage.Utils.Security;
using Garage.ViewModels;
using Rotativa;
using Rotativa.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Garage.Utils.AppEnumeration;

namespace Garage.Areas.Non_IT.Controllers
{
    public class NIUserRegistrationController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly NIUserService userService = new NIUserService();
        private readonly NISchoolService schoolService = new NISchoolService();

        [AppAuthorize (Roles = "Corporate IT")]
        [HttpGet, ActionName("List")]
        public ActionResult UserList()
        {
            logger.Info("User Registration Controller - Listing all the users in the database");

            ViewBag.userList = userService.GetAllUsers().Where(u => u.Deleted == false && u.IsNonITAccess).OrderByDescending(u => u.CreatedDate).ToList();
            ViewBag.Roles = userService.GetAllNIMasterRoles();
            return View("~/Areas/Non_IT/Views/NIUserRegistration/UserRegistrationList.cshtml");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Edit")]
        public ActionResult UserEdit(string id)
        {
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));

            logger.Info("User Registration Controller - Edit the user");

            NIUserRegistration userRegistration = userService.GetUserById(OriginalID);
            NIUserRegistrationForm userRegistrationForm = remap(userRegistration);
            ViewBag.SchoolList = getSchoolSelectList();
            ViewBag.Roles = userService.GetAllNIMasterRoles().ToList().ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.RoleName,
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            userRegistrationForm.EncryptedId = id;
            return View("~/Areas/Non_IT/Views/NIUserRegistration/UserRegistrationEdit.cshtml", userRegistrationForm);
        }

        [AppAuthorize]
        [HttpGet, ActionName("Delete")]
        public ActionResult UserRegistration(string id)
        {
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            logger.Info("User Registration Controller - Delete Individual User from Database");

            NIUserRegistration userRegistration = userService.GetUserById(OriginalID);
            var deletedUser = userService.DeleteUser(userRegistration);

            return RedirectToAction("List", "NIUserRegistration");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Add")]
        public ActionResult UserAdd()
        {
            logger.Info("User Registration Controller - Create a User Registration Form");

            ViewBag.SchoolList = getSchoolSelectList();
            ViewBag.Roles = userService.GetAllNIMasterRoles().ToList().ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.RoleName,
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            return View("~/Areas/Non_IT/Views/NIUserRegistration/UserRegistrationAdd.cshtml", new NIUserRegistrationForm() { Active = true });
        }

        [AppAuthorize]
        [HttpPost, ActionName("Save")]
        public ActionResult UserSave(NIUserRegistrationForm userRegistrationForm)
        {
            logger.Info("User Registration Controller - Saving the user in the database");

            ViewBag.savedCategory = userService.AddUser(map(userRegistrationForm));
            return RedirectToAction("List", "NIUserRegistration");
        }

        [AppAuthorize]
        [HttpPost]
        public JsonResult CheckEmailId(string email)
        {
            NIUserRegistration userRegistration = new NIUserRegistration();
            bool isValid = !userService.GetAllUsers().Where(u => u.Deleted == false).ToList().Exists(p => p.Email.Equals(email, StringComparison.CurrentCultureIgnoreCase));
            return Json(isValid);
        }

        [AppAuthorize]
        [HttpPost, ActionName("Update")]
        public ActionResult UpdateUserRegistration(NIUserRegistrationForm userRegistrationForm)
        {
            logger.Info("User Registration Controller - update the exsisting user from database");
            userRegistrationForm.Id = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(userRegistrationForm.EncryptedId));
            NIUserRegistration userRegistration = userService.GetUserById(userRegistrationForm.Id);
            userService.UpdateUser(map(userRegistrationForm, userRegistration));
            return RedirectToAction("List", "NIUserRegistration");
        }


        private NIUserRegistrationForm remap(NIUserRegistration userRegistration)
        {
            NIUserRegistrationForm userRegistrationForm = new NIUserRegistrationForm();

            userRegistrationForm.Id = userRegistration.Id;
            userRegistrationForm.UserId = userRegistration.UserId;
            userRegistrationForm.FullName = userRegistration.FullName;
            userRegistrationForm.Email = userRegistration.Email;
            userRegistrationForm.School = userRegistration.School ?? 0;
            userRegistrationForm.Role = userRegistration.Role ?? 0;
            userRegistrationForm.Active = userRegistration.Active ?? false;
            userRegistrationForm.Deleted = false;
            userRegistrationForm.NIRole = userRegistration.NIRole ?? 0;
            return userRegistrationForm;
        }
        private NIUserRegistration map(NIUserRegistrationForm userRegistrationForm)
        {
            NIUserRegistration userRegistration = new NIUserRegistration();

            userRegistration.UserId = userRegistrationForm.UserId;
            userRegistration.FullName = userRegistrationForm.FullName;
            userRegistration.Email = userRegistrationForm.Email;
            userRegistration.School = userRegistrationForm.School;
            userRegistration.NIRole = userRegistrationForm.NIRole;
            userRegistration.IsNonITAccess = true;
            userRegistration.Active = userRegistrationForm.Active;
            userRegistration.CreatedBy = Areas.Non_IT.AppUtils.getCurrentUser().user.Id;
            userRegistration.CreatedDate = DateTime.Now;
            userRegistration.UpdatedBy = Areas.Non_IT.AppUtils.getCurrentUser().user.Id;
            userRegistration.UpdatedDate = DateTime.Now;
            userRegistration.Deleted = false;

            return userRegistration;
        }
        private List<SelectListItem> getSchoolSelectList() => schoolService.GetAllSchools().ToList().ConvertAll(s =>
        {
            return new SelectListItem()
            {
                Text = s.SchoolName + " - " + s.SchoolCode,
                Value = s.Id.ToString(),
                Selected = false
            };
        });

        public static NIUserRegistration map(NIUserRegistrationForm userRegistrationForm, NIUserRegistration userRegistration)
        {

            userRegistration.UserId = userRegistrationForm.UserId;
            userRegistration.FullName = userRegistrationForm.FullName;
            userRegistration.Email = userRegistrationForm.Email;
            userRegistration.School = userRegistrationForm.School;
            userRegistration.Active = userRegistrationForm.Active;
            userRegistration.UpdatedBy = Areas.Non_IT.AppUtils.getCurrentUser().user.Id;
            userRegistration.UpdatedDate = DateTime.Now;
            userRegistration.Deleted = false;
            userRegistration.IsNonITAccess = true;
            userRegistration.NIRole = userRegistrationForm.NIRole;
            return userRegistration;
        }
        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            logger.Error(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "ErrorHandler");
        }
    }
}