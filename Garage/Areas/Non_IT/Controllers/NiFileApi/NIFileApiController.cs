﻿using Garage.Models;
using Garage.Service;
using Garage.Utils;
using Garage.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Garage.Utils.Mapper.ProductMapper;
using static Garage.Utils.Mapper.ProductTrackMapper;
using static Garage.Utils.Mapper.AssetTrackMapper;
using static Garage.Utils.Mapper.OrderTrackMapper;
using static Garage.Utils.Mapper.AssetMapper;
using static Garage.Utils.Mapper.ProductImageMapper;
using static Garage.Utils.Mapper.SoldProductMapper;
using static Garage.Utils.AppEnumeration;
using Garage.Service.Email.EmailModels;
using Garage.Service.Email;
using Garage.Utils.Security;
using Rotativa;
using Rotativa.Options;
using Garage.ViewModels.EmailModels;
using Garage.ViewModels.Email;
using Garage.Service.GemsEmail;
using Garage.Areas.Non_IT.Service;
using Garage.Areas.Non_IT.Models;
using Garage.Areas.Non_IT.ViewModels;
using Garage.Areas.Non_IT.Services;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using Garage.Areas.Non_IT.Helper;
using Garage.Areas.Non_IT.Models;

namespace Garage.Areas.Non_IT.Controllers
{
    public class NIFileApiController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly NIFileService NIfileService = new NIFileService();
        private readonly AssetImageService assetImageService = new AssetImageService();
        private readonly NICategoryService categoryService = new NICategoryService();

        [AppAuthorize]
        [HttpGet, ActionName("ProductImage")]
        public FileResult GetProductImage(int productImageId)
        {
            logger.Info("File API Controller - Get Product Image by Product Id");
            Areas.Non_IT.Models.ProductFile productImage = NIfileService.GetProductImageById(productImageId);
            var file = Areas.Non_IT.AppUtils.FileDirectory + @"\" + productImage.FilePath;
            if (System.IO.File.Exists(file))
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(file);
                string fileName = productImage.FileName;
                return File(fileBytes, MimeTypeMap.GetMimeType(productImage.Extension), fileName);
            }
            return null;
        }


        [HttpGet, ActionName("AssetImage")]
        public FileResult GetAssetImage(int assetImageId)
        {
            logger.Info("File API Controller - Get Asset Image by Asset Id");
            AssetImage assetImage = assetImageService.GetAssetImageById(assetImageId);
            var file = assetImage.ImageFilePath;
            if (System.IO.File.Exists(file))
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(assetImage.ImageFilePath);
                string fileName = assetImage.ImageName + assetImage.Extension;
                return File(fileBytes, MimeTypeMap.GetMimeType(assetImage.Extension), fileName);
            }
            return null;
        }

        [AppAuthorize]
        [HttpGet, ActionName("CategoryImage")]
        public FileResult GetCategoryImage(int categoryId)
        {
            logger.Info("File API Controller - List Get Category Image by Category Id");
            Non_IT.Models.Category category = categoryService.GetCategoryById(categoryId);
            var file = Areas.Non_IT.AppUtils.FileDirectory + @"\" + category.ImagePath;
            if (System.IO.File.Exists(file))
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(file);
                var currentFileName = Path.GetFileNameWithoutExtension(category.ImagePath);
                var fileExtension = Path.GetExtension(category.ImagePath).ToString().ToLower();
                return File(fileBytes, MimeTypeMap.GetMimeType(fileExtension), currentFileName + fileExtension);
            }
            return null;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            logger.Error("Error : " + DateTime.Now + "   " + filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "NIErrorHandler");
            Non_IT.Helper.NonIT_Helper.AddErrorLog(new Error_Log()
            {
                Area = "NON IT",
                Controller = "NIFileApi",
                ErrorMessage = Non_IT.AppUtils.getCurrentUser() == null ? "User Session Out" : filterContext.Exception.Message,
                ErrorDate = DateTime.Now,
                UserID = Non_IT.AppUtils.getCurrentUser() == null ? 0 : Non_IT.AppUtils.getCurrentUser().user.Id

            });

        }
    }
}