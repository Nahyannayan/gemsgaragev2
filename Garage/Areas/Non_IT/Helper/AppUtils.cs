﻿using Garage.Areas.Non_IT.Models;
using Garage.Areas.Non_IT.ViewModels;
using Garage.Areas.Non_IT.ViewModels.ShoppingCart;
using Garage.Service;
using Garage.ViewModels;
using Garage.ViewModels.ShoppingCart;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Configuration;

namespace Garage.Areas.Non_IT
{
    public class AppUtils
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //Email template path
        public static string publishedMailTemplate = HttpContext.Current.Server.MapPath("~/Areas/Non_IT/Views/EmailTemplate/AssetPublished/AssetPublished.cshtml");
        public static string rejectedMailTemplate = HttpContext.Current.Server.MapPath("~/Areas/Non_IT/Views/EmailTemplate/AssetRejected/AssetRejected.cshtml");
        public static string orderApproveBuyerMailTemplate = HttpContext.Current.Server.MapPath("~/Areas/Non_IT/Views/EmailTemplate/OrderApprovedBuyer/OrderApprovedBuyer.cshtml");
        public static string orderApproveSellerMailTemplate = HttpContext.Current.Server.MapPath("~/Areas/Non_IT/Views/EmailTemplate/OrderApprovedSeller/OrderApprovedSeller.cshtml");

        public static string orderApproveAccountSellerMailTemplate = HttpContext.Current.Server.MapPath("~/Areas/Non_IT/Views/EmailTemplate/OrderApprovedSeller/OrderApprovedSellerAccount.cshtml");
        public static string orderApproveAccountBuyerMailTemplate = HttpContext.Current.Server.MapPath("~/Areas/Non_IT/Views/EmailTemplate/OrderApprovedSeller/OrderApprovedBuyerAccount.cshtml");

        public static string reuseMailTemplate = HttpContext.Current.Server.MapPath("~/Areas/Non_IT/Views/EmailTemplate/AssetReuse/AssetReuse.cshtml");
        public static string PurchaseRejectedMailTemplate = HttpContext.Current.Server.MapPath("~/Areas/Non_IT/Views/EmailTemplate/PurchaseRejected/PurchaseRejected.cshtml");
        public static string AfterScheduledMailTemplate = HttpContext.Current.Server.MapPath("~/Areas/Non_IT/Views/EmailTemplate/AfterScheduled/AfterScheduled.cshtml");
        public static string AssetApprovalMailTemplate = HttpContext.Current.Server.MapPath("~/Areas/Non_IT/Views/EmailTemplate/AssetApproval/AssetApproval.cshtml");
        public static string OrderApprovalMailTemplate = HttpContext.Current.Server.MapPath("~/Areas/Non_IT/Views/EmailTemplate/OrderApproval/OrderApproval.cshtml");

        private static readonly UserService userService = new UserService();
        private static readonly SchoolService schoolService = new SchoolService();

        public static string baseUrl = WebConfigurationManager.AppSettings["baseUrl"];
        public static string visitorRole = "Visitor";
        public static string noPreviewImage = "/Content/img/no-preview-image.jpg";
        public static string FileDirectory = ConfigurationManager.AppSettings["NonITFilePath"];
        public static string uploadProductImage(HttpPostedFileBase files)
        {
            var path = String.Empty;

            if (files != null && files.ContentLength > 0)
            {
                var currentFileName = Path.GetFileNameWithoutExtension(files.FileName);
                var currentTime = DateTime.Now.ToString("_dd-MM-yyyy_HHmmss");
                var currentDate = DateTime.Now.ToString("dd-MM-yyyy");
                var fileExtension = Path.GetExtension(files.FileName).ToString().ToLower();
                var fileName = currentFileName + currentTime + fileExtension;
                var directory = FileDirectory + @"\" + currentDate;
                if (!Directory.Exists(directory))
                    Directory.CreateDirectory(directory);
                path = directory + @"\" + fileName;
                files.SaveAs(path);
                path = currentDate + @"\" + fileName;
            }
            return path;
        }

        //public static CurrentUser getCurrentUser() => (CurrentUser)HttpContext.Current.Session["CurrentUser"];

        public static NICurrentUser getCurrentUser()
        {
            logger.Info("Get Current User From Claim");
            NICurrentUser currentUser = null;
            var claims = ClaimsPrincipal.Current.Claims;

            var currentUserJson = claims.Where(c => c.Type == "CurrentUser").Select(c => c.Value).SingleOrDefault();
            if (string.IsNullOrEmpty(currentUserJson))
            {
                if (HttpContext.Current.Session["currentUser"] != null)
                {
                    currentUser = (NICurrentUser)HttpContext.Current.Session["currentUser"];
                }
                else
                    return null;
            }
            else
            {
                currentUser = JsonConvert.DeserializeObject<NICurrentUser>(currentUserJson);
            }
            logger.Info("Fetched Current User From Claim");
            return currentUser;
        }

        public static NIShoppingCart getCart() => (NIShoppingCart)HttpContext.Current.Session["NICart"];

        public static NIShoppingCart getWishList() => (NIShoppingCart)HttpContext.Current.Session["NIWishList"];

        public static string getDateOnly(DateTime datetime)
        {
            return datetime.ToString("dd-MMM-yyyy");
        }

    }
}