﻿using Garage.Areas.Non_IT.Models;
using Garage.Areas.Non_IT.Service;
using Garage.Areas.Non_IT.Service.Email;
using Garage.Areas.Non_IT.Services;
using Garage.Areas.Non_IT.ViewModel;
using Garage.Areas.Non_IT.ViewModels;
using Garage.Areas.Non_IT.ViewModels.Email;
using Garage.Models;
using Garage.Service;
using Garage.Service.GemsEmail;
using Garage.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;
using Garage.Areas.Non_IT.Models;

using Category = Garage.Models.Category;
using Garage.Utils;

namespace Garage.Areas.Non_IT.Helper
{

    public static class NonIT_Helper
    {

        public static Areas.Non_IT.Models.ProductFile SaveNIProductFiles(HttpPostedFileBase Files, int? ProductID, bool IsProductImage)
        {
            Areas.Non_IT.Models.ProductFile ObjProductFile = new Areas.Non_IT.Models.ProductFile();

            var currentDate = DateTime.Now.ToString("dd-MM-yyyy");
            string path = ConfigurationManager.AppSettings["NonITFilePath"] + @"\" + currentDate;


            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            HttpPostedFileBase file = Files;

            var FileNAme = DateTime.Now.ToString("HHmmss") + "_" + System.IO.Path.GetFileName(file.FileName);
            var FilePath = path + @"\" + FileNAme;
            ObjProductFile.FileName = System.IO.Path.GetFileName(file.FileName);
            ObjProductFile.Extension = System.IO.Path.GetExtension(file.FileName);
            ObjProductFile.FilePath = @"\" + currentDate + @"\" + FileNAme;
            ObjProductFile.Deleted = false;
            ObjProductFile.IsProductImage = IsProductImage;
            ObjProductFile.ProductID = ProductID;
            ObjProductFile.CreatedDate = DateTime.Now;
            ObjProductFile.CreatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
            var result = (new NIFileService()).AddNIProducFiles(ObjProductFile);
            file.SaveAs(FilePath);

            return ObjProductFile;
        }
        public static NIProduct mapNIProduct(NIProductForm productForm)
        {
            NIProduct product = new NIProduct();
            var newProduct = productForm.productViewModel;
            product.ProductName = newProduct.ProductName; ;
            product.Description = newProduct.Description;
            product.CategoryId = newProduct.CategoryId;
            product.SubCategoryId = newProduct.SubCategoryId;
            product.Quantity = newProduct.Quantity;
            product.InStock = newProduct.Quantity;
            product.PurchaseValue = newProduct.PurchaseValue;
            product.DepreciationValue = newProduct.DepreciationValue;
            product.UnitValue = newProduct.UnitValue;
            product.City = Non_IT.AppUtils.getCurrentUser().school.Location;
            product.Location = Non_IT.AppUtils.getCurrentUser().school.SchoolName;
            product.OriginalUnitValue = newProduct.OriginalUnitValue;
            product.DepreciationValue = newProduct.DepreciationValue;
            product.DateOfPurchase = newProduct.DateOfPurchase;
            product.Condition = (int)newProduct.Condition;
            product.ExpectedRealisable = newProduct.ExpectedRealisable;
            product.ActualValue = newProduct.ActualValue;
            product.ReasonForSelling = newProduct.ReasonForSelling;
            product.Reference = newProduct.Reference;
            product.DAXAssestItemTag = newProduct.DAXReference;
            product.Note = newProduct.Note;
            product.FinanceNote = newProduct.FinanceNote;
            product.MsoNote = newProduct.MsoNote;
            product.PrincipalNote = newProduct.PrincipalNote;
            product.ApprovalRemarks = newProduct.ApprovalRemarks;
            product.AssetType = (int)newProduct.assetType;
            product.Status = (int)ProductStatus.SavedAsDraft;
            product.UserId = Non_IT.AppUtils.getCurrentUser().user.Id;
            product.SchoolId = Non_IT.AppUtils.getCurrentUser().school.Id;
            product.Status = (int)ProductStatus.SavedAsDraft;
            product.ViewCount = 0;
            product.CreatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
            product.CreatedDate = DateTime.Now;
            product.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
            product.UpdatedDate = DateTime.Now;
            product.Deleted = false;

            return product;
        }


        public static NIProduct mapNIProduct(NIProductForm productForm, NIProduct product)
        {
            var currentUser = Non_IT.AppUtils.getCurrentUser();

            var updatedProduct = productForm.productViewModel;
            product.ProductName = updatedProduct.ProductName;

            product.Description = updatedProduct.Description;
            product.CategoryId = updatedProduct.CategoryId;
            product.SubCategoryId = updatedProduct.SubCategoryId;
            product.Quantity = updatedProduct.Quantity;
            product.InStock = updatedProduct.Quantity;
            product.DAXUpdated = updatedProduct.IsDAXUpdate;
            if (updatedProduct.DepreciationValue != null)
            {
                product.DepreciationValue = updatedProduct.DepreciationValue;
            }

            if (updatedProduct.PurchaseValue != null)
            {
                product.PurchaseValue = updatedProduct.PurchaseValue;
            }
            if (updatedProduct.UnitValue != null)
            {
                product.UnitValue = updatedProduct.UnitValue;
            }
            if (!currentUser.isNISuperAdmin)
            {
                product.City = currentUser.school.Location;
                product.Location = currentUser.school.SchoolName;
            }
            if (updatedProduct.OriginalUnitValue != null)
            {
                product.OriginalUnitValue = updatedProduct.OriginalUnitValue;
            }
            if (updatedProduct.DepreciationValue != null)
            {
                product.DepreciationValue = updatedProduct.DepreciationValue;
            }
            if (updatedProduct.DateOfPurchase != null)
            {
                product.DateOfPurchase = updatedProduct.DateOfPurchase;
            }
            product.Condition = (int)updatedProduct.Condition;
            if (updatedProduct.ExpectedRealisable != null)
            {
                product.ExpectedRealisable = updatedProduct.ExpectedRealisable;
            }
            if (updatedProduct.ActualValue != null)
            {
                product.ActualValue = updatedProduct.ActualValue;
            }
            product.ReasonForSelling = updatedProduct.ReasonForSelling;
            if (updatedProduct.Reference != null)
            {
                product.Reference = updatedProduct.Reference;
            }
            if (updatedProduct.DAXReference != null)
            {
                product.DAXAssestItemTag = updatedProduct.DAXReference;
            }


            if (!string.IsNullOrEmpty(updatedProduct.Note))
            {
                product.Note = updatedProduct.Note;
            }
            if (!string.IsNullOrEmpty(updatedProduct.FinanceNote))
            {
                product.FinanceNote = updatedProduct.FinanceNote;
            }
            if (!string.IsNullOrEmpty(updatedProduct.MsoNote))
            {
                product.MsoNote = updatedProduct.MsoNote;
            }
            if (!string.IsNullOrEmpty(updatedProduct.PrincipalNote))
            {
                product.PrincipalNote = updatedProduct.PrincipalNote;
            }
            if (!string.IsNullOrEmpty(updatedProduct.ApprovalRemarks))
            {
                product.ApprovalRemarks = updatedProduct.ApprovalRemarks;
            }
            if (updatedProduct.assetType != null && updatedProduct.assetType > 0)
            {
                product.AssetType = (int)updatedProduct.assetType;
            }
            if (updatedProduct.ApprovalReq)
            {
                product.ApprovalReq = updatedProduct.ApprovalReq;
            }

            product.UpdatedBy = currentUser.user.Id;
            product.UpdatedDate = DateTime.Now;
            return product;
        }

        public static NIProductPostTrack niprodcutTrackmap(NIProduct product)
        {
            var currentUser = Non_IT.AppUtils.getCurrentUser();
            NIProductPostTrack productPostTrack = new NIProductPostTrack();

            productPostTrack.UserId = currentUser.user.Id;
            productPostTrack.UserRole = currentUser.user.NIRole ?? 0;// currentUser.user.Role ?? 0;
            productPostTrack.TrackStatus = currentUser.user.NIRole ?? 0;    //TODO: Optinal if not used, Remove from table. 
            productPostTrack.ProductId = product.Id;
            productPostTrack.CreatedBy = currentUser.user.Id;
            productPostTrack.NextApprovalUserRole = (new NIUserService()).GetNextApprovalRoleID(currentUser.user.NIRole.Value, "REUSE", (currentUser.user.NIRole.Value == (int)AppEnumeration.NIUserRole.PROCUREMENT || currentUser.user.NIRole.Value == (int)AppEnumeration.NIUserRole.HOS) ? product.CategoryId == Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ProcurementCategoryID"].ToString()) : false);
            productPostTrack.CreatedDate = DateTime.Now;
            productPostTrack.Deleted = false;
            return productPostTrack;
        }

        public static void ApprovalMailTrigger(NIProduct product)
        {
            if (!Non_IT.AppUtils.getCurrentUser().isNISuperAdmin && Non_IT.AppUtils.getCurrentUser().user.NIRole != (int)Utils.AppEnumeration.NIUserRole.GCOREALM)
            {
                int nextApprover = 0;
                if (product.CategoryId == Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ProcurementCategoryID"].ToString()) && Non_IT.AppUtils.getCurrentUser().user.NIRole == (int)Utils.AppEnumeration.NIUserRole.HOS)
                {
                    nextApprover = (int)AppEnumeration.NIUserRole.PROCUREMENT;
                }
                else
                    nextApprover = (new NIUserService()).GetNextApproval(product.AssetType.Value, Non_IT.AppUtils.getCurrentUser().user.NIRole.Value);

                List<string> toEmailIds = getAllEmailsBasedOnRole(nextApprover, Non_IT.AppUtils.getCurrentUser().school.Id);
                var productDetails = new NIEmailNIProductDetails() { NIProductDetail = viewMap(product) };

                var emailbody = EmailFactory.ParseTemplate(productDetails, Non_IT.AppUtils.AssetApprovalMailTemplate);
                GemsToken.SendMail(toEmailIds, "Asset Approval", emailbody, true);
            }
        }
        public static NIProductDetail viewMap(NIProduct product)
        {
            var postTrackList = (new NIProductPostTrackService()).GetPostTrackByProductId(product.Id);
            var principalTrackEntry = postTrackList.Where(pt => pt.UserRole == (int)Utils.AppEnumeration.NIUserRole.HOS).FirstOrDefault();
            var publishedDate = principalTrackEntry == null ? product.CreatedDate : principalTrackEntry.CreatedDate;
            var currentUser = Non_IT.AppUtils.getCurrentUser();


            NIProductDetail productDetail = new NIProductDetail();
            productDetail.Product = product;
            productDetail.Id = product.Id.ToString();
            Garage.Areas.Non_IT.Models.Category category = (new NICategoryService()).GetCategoryById(product.CategoryId ?? 0);


            Garage.Areas.Non_IT.Models.NIUserRegistration user = (new NIUserService()).GetUserById(product.UserId ?? 0);
            Garage.Areas.Non_IT.Models.NISchool school = (new NIUserService()).GetSchoolById(product.SchoolId ?? 0);
            if (product.SubCategoryId != null)
            {
                SubCategory subCategory = (new NICategoryService()).GetSubCategoryByID(product.SubCategoryId ?? 0);
                productDetail.SubCategoryId = subCategory.SubCategoryName;
            }
            //productDetail.ProductImageId = productImage.Id.ToString();
            productDetail.ProductName = product.ProductName;
            productDetail.Description = product.Description;
            productDetail.CategoryId = category.CategoryName;
            productDetail.Quantity = Convert.ToInt32(product.Quantity).ToString();
            productDetail.InStock = Convert.ToInt32(product.InStock);
            productDetail.PurchaseValue = product.PurchaseValue.ToString();
            productDetail.PurchaseValueIndecimal = product.PurchaseValue ?? 0;
            productDetail.UnitValue = product.UnitValue.ToString();
            productDetail.UnitValueIndecimal = product.UnitValue ?? 0;
            productDetail.City = product.City.HasValue ? GetCityByID(product.City.Value).DISPLAY : "";
            productDetail.Location = school.SchoolName;
            productDetail.OriginalUnitValue = product.OriginalUnitValue.ToString();
            productDetail.DepreciationValue = product.DepreciationValue.ToString();
            if (product.DateOfPurchase != null)
            {
                productDetail.DateOfPurchase = product.DateOfPurchase;
                productDetail.age = (DateTime.Now.Year - product.DateOfPurchase.Value.Year).ToString();
            }

            productDetail.Condition = product.Condition.HasValue ? (new NIGeneralService()).GetConditionByID(product.Condition.Value).DISPLAY : "";
            productDetail.ExpectedRealisable = product.ExpectedRealisable.ToString();
            productDetail.ActualValue = product.ActualValue.ToString();
            productDetail.ReasonForSelling = product.ReasonForSelling;
            productDetail.RejectReason = product.RejectReason;
            productDetail.Reference = product.Reference;
            productDetail.DAXAssestItemTag = product.DAXAssestItemTag;
            productDetail.Note = product.Note;
            productDetail.FinanceNote = product.FinanceNote;
            productDetail.MsoNote = product.MsoNote;
            productDetail.PrinicipalNote = product.PrincipalNote;
            productDetail.ApprovalRemarks = product.ApprovalRemarks;
            productDetail.UserId = user.Id;
            productDetail.UserName = user.FullName;
            //productDetail.UserRole = EnumHelper<Utils.AppEnumeration.UserRole>.GetDisplayValue((Utils.AppEnumeration.UserRole)user.Role);
            productDetail.SchoolName = school.SchoolName;
            productDetail.SchoolId = school.Id;
            productDetail.Status = ((ProductStatus)product.Status).ToString();

            productDetail.imageIdList = (new NIFileService()).GetProductImageByProductId(product.Id).ConvertAll(pi => pi.ID);
            productDetail.PublishedDate = publishedDate;
            return productDetail;
        }

        public static NIProductForm ReMapNIProduct(NIProduct product)
        {
            NIProductForm productForm = new NIProductForm();
            NIProductViewModel productViewModel = new NIProductViewModel();

            productViewModel.Id = product.Id; ;
            productViewModel.ProductName = product.ProductName; ;
            productViewModel.Description = product.Description;
            productViewModel.CategoryId = product.CategoryId;
            productViewModel.SubCategoryId = product.SubCategoryId;
            productViewModel.Quantity = (int)product.Quantity;
            productViewModel.InStock = product.InStock;
            productViewModel.PurchaseValue = product.PurchaseValue;
            productViewModel.DepreciationValue = product.DepreciationValue;
            productViewModel.UnitValue = product.UnitValue;
            productViewModel.City = product.City.ToString();
            productViewModel.Location = product.Location;
            productViewModel.OriginalUnitValue = product.OriginalUnitValue;
            productViewModel.DepreciationValue = product.DepreciationValue;
            productViewModel.DateOfPurchase = product.DateOfPurchase;
            productViewModel.Condition = product.Condition.Value;
            productViewModel.ExpectedRealisable = product.ExpectedRealisable;
            productViewModel.ActualValue = product.ActualValue.HasValue ? product.ActualValue.Value : 0;
            productViewModel.ReasonForSelling = product.ReasonForSelling;
            productViewModel.ReasonForReject = "";
            productViewModel.Reference = product.Reference;
            productViewModel.DAXReference = product.DAXAssestItemTag;
            productViewModel.Note = product.Note;
            productViewModel.FinanceNote = product.FinanceNote;
            productViewModel.MsoNote = product.MsoNote;
            productViewModel.PrincipalNote = product.PrincipalNote;
            productViewModel.ApprovalRemarks = product.ApprovalRemarks;
            productViewModel.assetType = product.AssetType.Value;
            productViewModel.ApprovalReq = product.ApprovalReq;
            productViewModel.Status = ((ProductStatus)product.Status).ToString();
            productForm.productViewModel = productViewModel;
            productForm.productFileViewModel = new NIProductFileViewModel();
            productForm.imageIdList = (new NIFileService()).GetProductImageByProductId(product.Id).ConvertAll(pi => pi.ID);
            return productForm;
        }

        public static List<string> getAllEmailsBasedOnRole(int userRole, int schoolId)
        {
            return (new NIUserService()).GetAllUsers(schoolId, userRole).ToList();
        }

        public static List<NIApprovalRequest> getApprovalRequest()
        {
            var _user = Areas.Non_IT.AppUtils.getCurrentUser();
            List<NIApprovalRequest> approvalRequest = new List<NIApprovalRequest>();
            var PItems = (new NIProductService()).GetPostedProducts().ToList();
            if (_user.user.NIRole.HasValue && (_user.user.NIRole.Value == (int)AppEnumeration.NIAdminRole.GCOREALM || _user.user.NIRole.Value != (int)AppEnumeration.NIUserRole.PROCUREMENT ))
            {
                PItems = PItems.Where(p => p.Status != (int)ProductStatus.Published).ToList();
            }

            if (_user.user.NIRole.HasValue && _user.user.NIRole.Value != (int)AppEnumeration.NIUserRole.PROCUREMENT && _user.user.NIRole.Value != (int)AppEnumeration.NIAdminRole.GCOREALM && _user.user.NIRole.Value != (int)AppEnumeration.NIAdminRole.GEMSADMINROLE)
            {
                PItems = PItems.Where(p => p.SchoolId == _user.school.Id).ToList();
            }
            var productApprovalRequest = PItems.ConvertAll(p =>
            {
                var nextApproverName = string.Empty;
                var role = 0;
                if (p.AssetType == (int)Utils.AppEnumeration.AssetType.Reuse)
                {
                    role = (new NIProductPostTrackService()).getNextProductApproverByProductId(p);
                }
                else
                {
                    role = (new NIAssetTrackService()).getNextAssetApproverByAssetId(p);
                }

                if (role != 0)
                {
                    nextApproverName = (new NIUserService()).GetRoleIDByID(role);
                }

                return new NIApprovalRequest()
                {
                    Id = p.Id,
                    ProductName = p.ProductName,
                    CreatedDate = p.CreatedDate ?? DateTime.Now,
                    PublishedDate = p.UpdatedDate ?? DateTime.Now,
                    NextApprover = nextApproverName,
                    NextApproverAsInt = role,
                    Product = p,
                    SchoolCode = p.SchoolId.HasValue ? GetSchoolDetails(p.SchoolId).SchoolCode : ""
                };
            });
            // added on 31 march
            if (_user.user.NIRole == (int)Utils.AppEnumeration.NIDisposeUserRole.GCOREALM || _user.user.NIRole == (int)Utils.AppEnumeration.NIUserRole.PROCUREMENT)
            {
                productApprovalRequest = productApprovalRequest.Where(p => p.NextApproverAsInt == _user.user.NIRole).ToList();
            }
            approvalRequest.AddRange(productApprovalRequest);
            approvalRequest.OrderByDescending(a => a.CreatedDate).ToList();
            return approvalRequest;
        }

        public static Dictionary<int, string> GetNextApproverName(List<NIProduct> productList)
        {
            Dictionary<int, string> approverNameMap = new Dictionary<int, string>();
            var nextApproverName = string.Empty;
            foreach (NIProduct product in productList)
            {
                var role = (new NIAssetTrackService()).getNextAssetApproverByAssetId(product);
                if (role != 0)
                {
                    nextApproverName = EnumHelper<Utils.AppEnumeration.NIDisposeUserRole>.GetDisplayValue((Utils.AppEnumeration.NIDisposeUserRole)role);
                }
                approverNameMap.Add(product.Id, nextApproverName);
            }
            return approverNameMap;
        }

        public static List<NIProduct> GetAllApprovedAssets()
        {
            return (new NIProductService()).GetAllProduct().Where(p => p.AssetType == (int)Utils.AppEnumeration.AssetType.Dispose && p.Deleted == false && p.Status == (int)ProductStatus.Approved).ToList();
        }
        public static Dictionary<int, bool> GetValidApprovalMap(List<NIProduct> productList)
        {
            Dictionary<int, bool> validApprovalMap = new Dictionary<int, bool>();

            foreach (NIProduct product in productList)
            {
                var validApproval = false;
                var role = (new NIAssetTrackService()).getNextAssetApproverByAssetId(product);
                if (role == Non_IT.AppUtils.getCurrentUser().user.NIRole)
                {
                    validApproval = true;
                }
                validApprovalMap.Add(product.Id, validApproval);
            }
            return validApprovalMap;
        }
        public static void ProductStatusEmail(NIProduct product)
        {
            var CreatedUser = (new NIUserService()).GetUserById(product.UserId.Value);
            List<string> toEmailIds = getAllEmailsBasedOnRole(CreatedUser.NIRole.Value, product.SchoolId ?? 0);
            var productDetails = new NIEmailNIProductDetails() { NIProductDetail = viewMap(product) };


            if (product.Status == (int)ProductStatus.Published)
            {
                var emailbody = EmailFactory.ParseTemplate(productDetails, Non_IT.AppUtils.publishedMailTemplate);
                //emailService.send(toEmailIds, "Product Published", emailbody);
                GemsToken.SendMail(toEmailIds, "Product Published", emailbody, true);
            }
        }

        public static NIAssetTrack assetTrackmap(NIProduct product)
        {
            NIAssetTrack assetTrack = new NIAssetTrack();
            var currentUser = Non_IT.AppUtils.getCurrentUser();
            assetTrack.UserId = currentUser.user.Id;
            assetTrack.UserRole = currentUser.user.NIRole ?? 0;
            assetTrack.AssetId = product.Id;
            assetTrack.CreatedBy = currentUser.user.Id;
            assetTrack.CreatedDate = DateTime.Now;
            assetTrack.Deleted = false;
            assetTrack.NextApprovalUserRole = (new NIUserService()).GetNextApprovalRoleID(currentUser.user.NIRole.Value, "DISPOSE", (currentUser.user.NIRole.Value == (int)AppEnumeration.NIUserRole.HOS || currentUser.user.NIRole.Value == (int)AppEnumeration.NIUserRole.PROCUREMENT) ? product.CategoryId == Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ProcurementCategoryID"].ToString()) : false);
            return assetTrack;
        }

        public static NIProduct logRejectReason(NIProduct product, NIProductForm productForm)
        {
            return (new NIProductService()).UpdateProduct(mapRejectStatus(product, productForm));
        }

        public static NIProduct mapRejectStatus(NIProduct product, NIProductForm productForm)
        {
            product.RejectReason = productForm.productViewModel.ReasonForReject;
            product.RejectedUserId = Non_IT.AppUtils.getCurrentUser().user.Id;
            product.Status = (int)ProductStatus.SavedAsDraft;
            product.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
            product.UpdatedDate = DateTime.Now;
            product.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
            product.UpdatedDate = DateTime.Now;
            return product;
        }

        public static void RejectAssetEmail(NIProduct product)
        {
            List<string> toEmailIds = getAllEmailsBasedOnRole((int)Utils.AppEnumeration.NIUserRole.MSOASSISTANT, product.SchoolId ?? 0);
            var productDetails = new NIEmailNIProductDetails() { NIProductDetail = viewMap(product) };

            var emailbody = EmailFactory.ParseTemplate(productDetails, Non_IT.AppUtils.rejectedMailTemplate);
            //emailService.send(toEmailIds, "Product Rejected", emailbody);
            GemsToken.SendMail(toEmailIds, "Product Rejected", emailbody, true);
        }

        public static List<NIAssetDisposed> GetDisposedAssets()
        {
            var productList = (new NIProductService()).GetAllProduct().Where(p => p.AssetType == (int)Utils.AppEnumeration.AssetType.Dispose && p.Deleted == false && p.Status == (int)ProductStatus.Disposed).ToList();
            var assetDiposedList = productList.ConvertAll(p =>
            {
                var diposedSchool = (new NISchoolService()).GetSchoolById(p.SchoolId ?? 0);
                NIAssetTrack assetTrackDisposed;
                if (p.ApprovalReq)
                { 
                    assetTrackDisposed = (new NIAssetTrackService()).GetPostTrackByAssetId(p.Id).Where(at => at.UserRole == (int)Utils.AppEnumeration.NIDisposeUserRole.GCOREALM || at.UserRole == (int)Utils.AppEnumeration.NIUserRole.PROCUREMENT).FirstOrDefault();
                }
                else
                {
                    assetTrackDisposed = (new NIAssetTrackService()).GetPostTrackByAssetId(p.Id).Where(at => at.UserRole == (int)Utils.AppEnumeration.NIDisposeUserRole.SSCINTERNALAUDIT).FirstOrDefault();
                }
                var assetTrackPosted = (new NIAssetTrackService()).GetPostTrackByAssetId(p.Id).Where(at => (at.UserRole == (int)Utils.AppEnumeration.NIUserRole.MSOASSISTANT) || (at.UserRole == (int)Utils.AppEnumeration.NIUserRole.MSO)).FirstOrDefault();
                return new NIAssetDisposed()
                {
                    Product = p,
                    DisposedDate = assetTrackDisposed.CreatedDate,
                    PostedDate = assetTrackPosted.CreatedDate,
                    School = diposedSchool
                };
            });

            return assetDiposedList.OrderByDescending(p => p.DisposedDate).ToList();
        }

        public static List<NIProductListView> GetProductListView(List<NIProduct> productList) =>
           productList.ConvertAll(p =>
           {
               var school = (new SchoolService()).GetSchoolById(p.SchoolId ?? 0);
               var postTrackList = (new NIProductPostTrackService()).GetPostTrackByProductId(p.Id);
               var principalTrackEntry = postTrackList.Where(pt => pt.UserRole == (int)Utils.AppEnumeration.NIUserRole.HOS).FirstOrDefault();
               var publishedDate = principalTrackEntry == null ? p.CreatedDate : principalTrackEntry.CreatedDate;
               return new NIProductListView()
               {
                   Product = p,
                   imageIdList = (new NIFileService()).GetProductImageByProductId(p.Id).ConvertAll(pi => pi.ID),
                   SchoolCode = school.SchoolCode,
                   PublishedDate = publishedDate
               };
           });

        public static NIOrderTrack orderTrackMap(NIOrder order, bool IsSameSchool = true)
        {
            var currentUser = Non_IT.AppUtils.getCurrentUser();

            var ProductOrder = (new NIProductOrderService()).GetProductOrderById(order.Id);
            var Product = (new NIProductService()).GetProductById(ProductOrder.ProductId.Value);


            NIOrderTrack orderTrack = new NIOrderTrack();
            var Tracking = (new NIOrderTrackService()).GetOrderTrackByOrderId(order.Id);

            if (currentUser.user.NIRole == 1 && Tracking.Count() == 0)
            {
                orderTrack.TrackStatus = 1;
            }

            if (currentUser.user.NIRole == 2 && Tracking.Count() == 0)
            {
                orderTrack.TrackStatus = 2;
            }

            orderTrack.UserId = currentUser.user.Id;
            orderTrack.UserRole = currentUser.user.NIRole ?? 0;
            orderTrack.TrackStatus = Tracking.Count() == 0 ? orderTrack.TrackStatus : Tracking.OrderByDescending(p => p.CreatedDate).First().TrackStatus + 1;  //TODO: Optinal if not used, Remove from table. 
            orderTrack.OrderId = order.Id;
            orderTrack.CreatedBy = currentUser.user.Id;
            orderTrack.CreatedDate = DateTime.Now;
            orderTrack.NextApprovalUserRole = (new NIUserService()).GetPurchaseNextApprovalRoleID(orderTrack.TrackStatus.Value, ((currentUser.user.NIRole.Value == (int)AppEnumeration.NIUserRole.HOS || currentUser.user.NIRole.Value == (int)AppEnumeration.NIUserRole.PROCUREMENT) && Product.CategoryId == Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ProcurementCategoryID"].ToString())) ? true : false);
            orderTrack.Deleted = false;
            orderTrack.IsDifferentSchool = (currentUser.school.Id != order.SchoolId) ? true : false;
            return orderTrack;
        }

        public static NIOrderTrack sellerOrderTrackMap(NIOrder order)
        {
            var currentUser = Non_IT.AppUtils.getCurrentUser();
            NIOrderTrack orderTrack = new NIOrderTrack();

            var ProductOrder = (new NIProductOrderService()).GetProductOrderById(order.Id);
            var Product = (new NIProductService()).GetProductById(ProductOrder.ProductId.Value);


            var Tracking = (new NIOrderTrackService()).GetOrderTrackByOrderId(order.Id);
            
            orderTrack.UserId = currentUser.user.Id;
            orderTrack.UserRole = currentUser.user.NIRole ?? 0; // Seller Finance or ICT
            orderTrack.NextApprovalUserRole = (new NIUserService()).GetPurchaseNextApprovalRoleID(Tracking.Count(), (currentUser.user.NIRole.Value == (int)AppEnumeration.NIUserRole.PROCUREMENT && Product.CategoryId == Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ProcurementCategoryID"].ToString())) ? true : false);
            orderTrack.TrackStatus = Tracking.OrderByDescending(p => p.CreatedDate).First().TrackStatus + 1;  //TODO: Optinal if not used, Remove from table. 
            orderTrack.OrderId = order.Id;
            orderTrack.IsDifferentSchool = true;
            orderTrack.CreatedBy = currentUser.user.Id;
            orderTrack.CreatedDate = DateTime.Now;
            orderTrack.Deleted = false;

            return orderTrack;
        }
        public static NIOrder map(int index)
        {
            var currentUser = Non_IT.AppUtils.getCurrentUser();
            NIOrder order = new NIOrder();
            order.OrderNo = "#OD" + Convert.ToString((int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds) + index;
            order.OrderStatus = Convert.ToInt32(OrderStatus.OrderPlaced);
            order.SchoolId = currentUser.school.Id;
            order.CreatedBy = currentUser.user.Id;
            order.CreatedDate = DateTime.Now;
            order.UpdatedBy = currentUser.user.Id;
            order.UpdatedDate = DateTime.Now;
            order.Deleted = false;

            return order;

        }

        public static NIOrder cancelOrder(NIOrder order)
        {
            order.OrderStatus = (int)OrderStatus.Cancelled;
            order.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
            order.UpdatedDate = DateTime.Now;
            return order;
        }

        public static NIOrder orderCompleteMap(NIOrder order)
        {
            order.OrderStatus = (int)OrderStatus.Completed;
            order.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
            order.UpdatedDate = DateTime.Now;
            return order;
        }
        public static List<NIProductOrder> map(Order order, List<ProductOrderForm> productOrderForms)
        {
            var currentUser = Non_IT.AppUtils.getCurrentUser();
            var productOrderList = productOrderForms.ConvertAll(pof =>
            {
                var product = (new NIProductService()).GetProductById(pof.productId);
                return new NIProductOrder()
                {
                    OrderId = order.Id,
                    ProductId = pof.productId,
                    Quantity = pof.quantity,
                    UnitValue = product.UnitValue,
                    ProductValue = (pof.quantity > 0 && product.UnitValue > 0) ? pof.quantity * product.UnitValue : 0,
                    CreatedBy = currentUser.user.Id,
                    CreatedDate = DateTime.Now,
                    UpdatedBy = currentUser.user.Id,
                    UpdatedDate = DateTime.Now,
                    Deleted = false
                };

            });

            return productOrderList;

        }

        public static NIProductOrder map(NIOrder order, ProductOrderForm productOrderForm)
        {
            var product = (new NIProductService()).GetProductById(productOrderForm.productId);
            var currentUser = Non_IT.AppUtils.getCurrentUser();
            var productOrder = new NIProductOrder()
            {
                OrderId = order.Id,
                ProductId = product.Id,
                Quantity = productOrderForm.quantity,
                UnitValue = product.UnitValue,
                ProductValue = (productOrderForm.quantity > 0 && product.UnitValue > 0) ? productOrderForm.quantity * product.UnitValue : 0,
                CreatedBy = currentUser.user.Id,
                CreatedDate = DateTime.Now,
                UpdatedBy = currentUser.user.Id,
                UpdatedDate = DateTime.Now,
                Deleted = false
            };

            return productOrder;
        }

        public static NIProductOrder deleteProductOrder(NIProductOrder productOrder)
        {
            productOrder.Deleted = true;
            productOrder.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
            productOrder.UpdatedDate = DateTime.Now;
            return productOrder;
        }

        public static List<NISoldProduct> map(NIOrder order)
        {
            var productOrderList = (new NIProductOrderService()).GetPOByOrderId(order.Id);
            var soldProductList = new List<NISoldProduct>();

            foreach (var productOrder in productOrderList)
            {
                NISoldProduct soldProduct = new NISoldProduct();
                var product = (new NIProductService()).GetProductById(productOrder.ProductId ?? 0);

                soldProduct.OrderId = order.Id;
                soldProduct.ProductOrderId = productOrder.Id;
                soldProduct.ProductId = product.Id;
                soldProduct.UserId = product.UserId ?? 0;
                soldProduct.SchoolId = product.SchoolId ?? 0;
                soldProduct.ScheduledDate = null;
                soldProduct.DispatchDate = null;
                soldProduct.Status = (int)SoldProductStatus.WaitingForApproval;
                soldProduct.CreatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
                soldProduct.CreatedDate = DateTime.Now;
                soldProduct.Deleted = false;
                soldProductList.Add(soldProduct);
            }
            return soldProductList;
        }

        public static List<NISoldProduct> rejectMap(NIOrder order)
        {
            var soldProductList = (new NIProductService()).GetSoldProductsByOrderId(order.Id).ToList();
            soldProductList.ForEach(s =>
            {
                s.Status = (int)SoldProductStatus.Rejected;
                s.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
                s.UpdatedDate = DateTime.Now;
            });
            return soldProductList;
        }
        public static NISoldProduct rejectMap(NIProductOrder prodOrder)
        {
            var soldProduct = (new NIProductService()).GetSoldProductByProdOrderId(prodOrder.Id);

            soldProduct.Status = (int)SoldProductStatus.Rejected;
            soldProduct.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
            soldProduct.UpdatedDate = DateTime.Now;

            return soldProduct;
        }

        public static List<NISoldProduct> waitingForBookValueTransferMap(NIOrder order)
        {
            var soldProductList = (new NIProductService()).GetSoldProductsByOrderId(order.Id).ToList();
            soldProductList.ForEach(s =>
            {
                s.Status = (int)SoldProductStatus.WaitingForBookValueTransfer;
                s.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
                s.UpdatedDate = DateTime.Now;
            });
            return soldProductList;
        }

        public static NISoldProduct waitingForScheduleMap(NISoldProduct soldProduct)
        {
            soldProduct.Status = (int)SoldProductStatus.WaitingForSchedule;
            soldProduct.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
            soldProduct.UpdatedDate = DateTime.Now;

            return soldProduct;
        }

        public static NISoldProduct updateScheduleDate(DateTime scheduledDate, int id)
        {
            var soldProduct = (new NIProductService()).GetSoldProductById(id);
            soldProduct.ScheduledDate = scheduledDate;
            soldProduct.Status = (int)SoldProductStatus.Scheduled;
            soldProduct.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
            soldProduct.UpdatedDate = DateTime.Now;

            return soldProduct;
        }

        public static NISoldProduct updateDispatchedMap(NISoldProduct soldProduct)
        {
            soldProduct.Status = (int)SoldProductStatus.Dispatched;
            soldProduct.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
            soldProduct.UpdatedDate = DateTime.Now;

            return soldProduct;
        }

        public static List<NISoldProduct> updateDispatchedListMap(List<NISoldProduct> soldProducts)
        {
            soldProducts.ForEach(soldProduct =>
            {
                soldProduct.Status = (int)SoldProductStatus.Dispatched;
                soldProduct.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
                soldProduct.UpdatedDate = DateTime.Now;
            });

            return soldProducts;
        }
        public static List<NISoldProductDetail> GetSoldProductDetailList(List<NISoldProduct> soldProductList)

                => soldProductList.ConvertAll(s =>
                {
                    DateTime? _Date = null;
                    var schoolId = (new NIOrderService()).GetOrderById(s.OrderId).SchoolId;
                    var school = (new NISchoolService()).GetSchoolById(schoolId ?? 0);
                    var product = (new NIProductService()).GetProductById(s.ProductId);
                    var soldSchool = (new NISchoolService()).GetSchoolById(product.SchoolId ?? 0);
                    var productTrackList = (new NIProductPostTrackService()).GetPostTrackByProductId(product.Id);
                    var publishedDate = productTrackList.Where(pt => pt.UserRole == (int)Utils.AppEnumeration.NIUserRole.HOS).Select(pt => pt.CreatedDate).FirstOrDefault();
                    var orderTrackList = (new NIOrderTrackService()).GetOrderTrackByOrderId(s.OrderId);
                    var buyerFinanceTrackEntry = orderTrackList.Where(ot => ot.UserRole == (int)Utils.AppEnumeration.NIUserRole.ACCOUNTS && ot.IsDifferentSchool == false).FirstOrDefault();
                    var ProductFiles = (new NIFileService()).GetProductFilesByProductId(s.ProductId);
                    if (buyerFinanceTrackEntry != null)
                    {
                        _Date = buyerFinanceTrackEntry.CreatedDate;
                    }
                    return new NISoldProductDetail()
                    {
                        SoldProduct = s,
                        Product = product,
                        ProductOrder = (new NIProductOrderService()).GetProductOrderById(s.ProductOrderId),
                        School = school,
                        SoldSchool = soldSchool,
                        OrderApprovedDate = _Date,
                        PublishedDate = publishedDate,
                        PickUpDate = s.ScheduledDate,
                        ProductFile = ProductFiles
                    };
                });
        public static List<NISchoolCartItem> getCartItemsByCrrentUserSchool(NICurrentUser currentUser)
        {
            var schoolCartItems = new List<NISchoolCartItem>();
            var currentSchoolIctList = (new NIUserService()).GetAllUsers().Where(u => u.School == currentUser.school.Id && (u.NIRole == (int)Utils.AppEnumeration.NIUserRole.MSOASSISTANT || u.NIRole == (int)Utils.AppEnumeration.NIUserRole.MSO)).ToList();
            var sessionObj = (new NISessionObjService()).GetAllsessionObjs().Where(s => currentSchoolIctList.Select(u => u.Id).Contains(s.UserId) && s.ObjType == (int)Utils.AppEnumeration.SessionObjType.Cart).ToList();
            if (sessionObj != null && sessionObj.Count() > 0)
            {
                schoolCartItems = sessionObj.ConvertAll(s =>
                {
                    var product = (new NIProductService()).GetProductById(s.ProductId ?? 0);
                    var imageIdList = (new NIFileService()).GetProductImageByProductId(product.Id).ConvertAll(pi => pi.ID);
                    var orderQuantity = s.ProductQuantity ?? 0;
                    var orderValue = orderQuantity * product.UnitValue;
                    var IctName = (new NIUserService()).GetUserById(s.UserId).FullName;
                    return new NISchoolCartItem()
                    {
                        Product = product,
                        ImageIdList = imageIdList,
                        OrderQuantity = orderQuantity,
                        OrderValue = orderValue ?? 0,
                        IctName = IctName
                    };
                });

                return schoolCartItems;
            }
            return schoolCartItems;
        }
        public static List<NICompletedOrders> GetCompletedOrderDetails()
        {
            var OrderDetailList = GetCompletedOrderList().ConvertAll(o =>
            {
                var productOrder = (new NIProductOrderService()).GetPOByOrderId(o.Id).FirstOrDefault();
                var product = (new NIProductService()).GetProductById(productOrder.ProductId ?? 0);
                var orderTrack = (new NIOrderTrackService()).GetOrderTrackByOrderId(o.Id);
                var soldProduct = (new NISoldProductService()).GetSoldProductsByOrderId(o.Id).FirstOrDefault();
                return new NICompletedOrders()
                {
                    Order = o,
                    ProductOrder = productOrder,
                    Product = product,
                    PurchaseDate = orderTrack.Where(ot => ot.UserRole == (int)Utils.AppEnumeration.NIUserRole.ACCOUNTS && ot.IsDifferentSchool == false).Select(ot => ot.CreatedDate).FirstOrDefault(),
                    ScheduledDate = soldProduct.ScheduledDate ?? DateTime.Now,
                    OrderVouchersDetail = (new NIProductOrderService()).GetOrderVouchersDetail(productOrder.OrderId.Value, Areas.Non_IT.AppUtils.getCurrentUser().school.Id)

                };
            });
            if(Areas.Non_IT.AppUtils.getCurrentUser().user.NIRole.Value == (int)AppEnumeration.NIUserRole.PROCUREMENT)
            {
                OrderDetailList = OrderDetailList.Where(p => p.Product.CategoryId == Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ProcurementCategoryID"].ToString())).ToList();
            }
            else if (Areas.Non_IT.AppUtils.getCurrentUser().user.NIRole.Value == (int)AppEnumeration.NIUserRole.GCOREALM)
            {
                OrderDetailList = OrderDetailList.Where(p => p.Product.CategoryId != Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ProcurementCategoryID"].ToString())).ToList();
            }
            return OrderDetailList;
        }

        public static List<NIOrder> GetCompletedOrderList()
        {
            var currentUser = Non_IT.AppUtils.getCurrentUser();
            List<NIOrder> AllOrders = new List<NIOrder>();
            if (currentUser.user.NIRole.Value == (int)AppEnumeration.NIAdminRole.GEMSADMINROLE || currentUser.user.NIRole.Value == (int)AppEnumeration.NIAdminRole.GCOREALM || currentUser.user.NIRole.Value == (int)AppEnumeration.NIUserRole.PROCUREMENT)
            {
                AllOrders = (new NIOrderService()).GetAllOrders().Where(o => o.OrderStatus == (int)OrderStatus.Completed && o.Deleted == false).OrderByDescending(p => p.CreatedDate).ToList();
            }
            else
            {
                AllOrders = (new NIOrderService()).GetAllOrders().Where(o => o.OrderStatus == (int)OrderStatus.Completed && o.Deleted == false && o.SchoolId == currentUser.school.Id).OrderByDescending(p => p.CreatedDate).ToList();
            }
            return AllOrders;
        }

        public static void ApprovalMailRemainder(NIProduct product, int nextApprover)
        {
            var currentUser = Non_IT.AppUtils.getCurrentUser();
            if (!currentUser.isNISuperAdmin && currentUser.user.NIRole != (int)Utils.AppEnumeration.UserRole.Principal)
            {
                List<string> toEmailIds = NonIT_Helper.getAllEmailsBasedOnRole(nextApprover, currentUser.school.Id);
                var productDetails = new NIEmailNIProductDetails() { NIProductDetail = NonIT_Helper.viewMap(product) };

                var emailbody = EmailFactory.ParseTemplate(productDetails, Non_IT.AppUtils.AssetApprovalMailTemplate);
                //emailService.send(toEmailIds, "Asset Approval", emailbody);
                GemsToken.SendMail(toEmailIds, "Asset Approval", emailbody, true);
            }
        }

        public static List<NIAssetDisposed> GetDisposedProducts()
        {
            var _User = Areas.Non_IT.AppUtils.getCurrentUser();
            var productList = (new NIProductService()).GetAllProduct().Where(p => p.Status == (int)ProductStatus.Disposed &&   p.Deleted == false).OrderByDescending(p => p.CreatedDate).ToList();
            if (_User.user.NIRole.Value != (int)AppEnumeration.NIAdminRole.GEMSADMINROLE && _User.user.NIRole.Value != (int)AppEnumeration.NIUserRole.GCOREALM && _User.user.NIRole.Value != (int)AppEnumeration.NIUserRole.PROCUREMENT)
            {
                productList = productList.Where(p => p.SchoolId == _User.school.Id).ToList();
            }
            
                var assetDiposedList = productList.ConvertAll(p =>
            {
                NIAssetTrack assetTrackDisposed;
                if (p.ApprovalReq)
                {
                    assetTrackDisposed = (new NIAssetTrackService()).GetPostTrackByAssetId(p.Id).Where(at => at.UserRole == (int)Utils.AppEnumeration.NIAdminRole.CFO).FirstOrDefault();
                }
                else
                {
                    assetTrackDisposed = (new NIAssetTrackService()).GetPostTrackByAssetId(p.Id).Where(at => at.UserRole == (int)Utils.AppEnumeration.NIAdminRole.SSCINTERNALAUDIT).FirstOrDefault();
                }
                var assetTrackPosted = (new NIAssetTrackService()).GetPostTrackByAssetId(p.Id).Where(at => (at.UserRole == (int)Utils.AppEnumeration.NIDisposeUserRole.MSOASSISTANT || at.UserRole == (int)Utils.AppEnumeration.NIDisposeUserRole.MSO)).FirstOrDefault();
                return new NIAssetDisposed()
                {
                    Product = p,
                    DisposedDate = assetTrackDisposed.CreatedDate,
                    PostedDate = assetTrackPosted.CreatedDate,
                    School = GetSchoolDetails(p.SchoolId.Value)

                };
            });
            if (Areas.Non_IT.AppUtils.getCurrentUser().user.NIRole.Value == (int)AppEnumeration.NIUserRole.PROCUREMENT)
            {
                assetDiposedList = assetDiposedList.Where(p => p.Product.CategoryId == Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ProcurementCategoryID"].ToString())).ToList();
            }
            else if (Areas.Non_IT.AppUtils.getCurrentUser().user.NIRole.Value == (int)AppEnumeration.NIUserRole.GCOREALM)
            {
                assetDiposedList = assetDiposedList.Where(p => p.Product.CategoryId != Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ProcurementCategoryID"].ToString())).ToList();
            }
            return assetDiposedList.OrderByDescending(p => p.DisposedDate).ToList();
        }

        public static Garage.Areas.Non_IT.Models.City GetCityByID(int CityID)
        {
            return (new NIGeneralService()).GetCityByID(CityID);
        }
        public static Garage.Areas.Non_IT.Models.ConditionEnum GetConditionByID(int ID)
        {
            return (new NIGeneralService()).GetConditionByID(ID);
        }

        public static List<Garage.Areas.Non_IT.Models.ConditionEnum> GetAllCondition()
        {
            return (new NIGeneralService()).GetAllCondition();
        }

        public static void DisposeApprovalMail(NIProduct product)
        {
            var currentUser = Non_IT.AppUtils.getCurrentUser();

            int nextApprover = 0;
            if (product.CategoryId == Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ProcurementCategoryID"].ToString()) && currentUser.user.NIRole.Value == (int)AppEnumeration.NIUserRole.HOS)
            {
                nextApprover = (int)AppEnumeration.NIUserRole.PROCUREMENT;
            }
            else
                nextApprover = (new NIUserService()).GetNextApproval(product.AssetType.Value, Non_IT.AppUtils.getCurrentUser().user.NIRole.Value);

            List<string> toEmailIds = null;
            if (currentUser.isNISuperAdmin || currentUser.user.NIRole == (int)Utils.AppEnumeration.NIUserRole.HOS)
            {
                if (currentUser.user.NIRole == (int)Utils.AppEnumeration.NIUserRole.HOS && nextApprover == (int)Utils.AppEnumeration.NIUserRole.GCOREALM)
                {
                    toEmailIds = (new NIUserService()).GetAllUsers(0, nextApprover);
                }
                else
                {
                    toEmailIds = (new NIUserService()).GetAllEmailsBasedOnlyOnRole(nextApprover);
                }
            }
            else
            {
                toEmailIds = getAllEmailsBasedOnRole(nextApprover, currentUser.school.Id);
            }
            var productDetails = new NIEmailNIProductDetails() { NIProductDetail = viewMap(product) };

            var emailbody = EmailFactory.ParseTemplate(productDetails, Non_IT.AppUtils.AssetApprovalMailTemplate);

            if (currentUser.user.NIRole != (int)Utils.AppEnumeration.NIAdminRole.CFO)
            {
                //emailService.send(toEmailIds, "Asset Approval", emailbody);
                GemsToken.SendMail(toEmailIds, "Asset Approval", emailbody, true);
            }


        }
        public static List<NIProduct> GetRejectedProducts()
        {
            return (new NIProductService()).GetAllProduct().Where(p => p.Status == (int)ProductStatus.Reject && p.SchoolId == Non_IT.AppUtils.getCurrentUser().school.Id && p.Deleted == false).ToList();
        }

        public static NIProduct MapDisposeToPublished(NIProductForm productForm, AssetReuseForm assetReuseForm)
        {
            NIProduct product = new NIProduct();
            NIProduct oldProduct = (new NIProductService()).GetProductById(productForm.productViewModel.Id);
            NIUserRegistration user = (new NIUserService()).GetUserById(oldProduct.UserId ?? 0);
            NISchool school = (new NISchoolService()).GetSchoolById(oldProduct.SchoolId ?? 0);

            var newProduct = productForm.productViewModel;
            product.ProductName = newProduct.ProductName;
            product.Description = newProduct.Description;
            product.CategoryId = newProduct.CategoryId;
            product.SubCategoryId = newProduct.SubCategoryId;
            product.Quantity = assetReuseForm.ReuseQuantity;
            product.InStock = assetReuseForm.ReuseQuantity;
            product.PurchaseValue = newProduct.PurchaseValue;
            product.DepreciationValue = newProduct.DepreciationValue;
            product.UnitValue = newProduct.UnitValue;
            product.City = school.Location;
            product.Location = school.SchoolName;
            product.OriginalUnitValue = newProduct.OriginalUnitValue;
            product.DepreciationValue = newProduct.DepreciationValue;
            product.DateOfPurchase = newProduct.DateOfPurchase;
            product.Condition = (int)newProduct.Condition;
            product.ExpectedRealisable = newProduct.ExpectedRealisable;
            product.ActualValue = newProduct.ActualValue;
            product.ReasonForSelling = newProduct.ReasonForSelling;
            product.Reference = newProduct.Reference;
            product.DAXAssestItemTag = newProduct.DAXReference;
            product.Note = newProduct.Note;
            product.FinanceNote = newProduct.FinanceNote;
            product.MsoNote = newProduct.MsoNote;
            product.ApprovalRemarks = newProduct.ApprovalRemarks;
            product.PrincipalNote = newProduct.PrincipalNote;
            product.AssetType = (int)Utils.AppEnumeration.AssetType.Reuse;
            product.Status = (int)ProductStatus.Published;
            product.UserId = user.Id;
            product.SchoolId = school.Id;
            product.ViewCount = 0;
            product.CreatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
            product.CreatedDate = DateTime.Now;
            product.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
            product.UpdatedDate = DateTime.Now;
            product.Deleted = false;

            return product;
        }

        public static List<Areas.Non_IT.Models.ProductFile> SetNewImagesFromProduct(NIProduct product, int newProductId)
        {
            List<Areas.Non_IT.Models.ProductFile> productImagelist = new List<Areas.Non_IT.Models.ProductFile>();
            var productImages = (new NIFileService()).GetProductImageByProductId(product.Id);
            foreach (Areas.Non_IT.Models.ProductFile savedProductImage in productImages)
            {
                Areas.Non_IT.Models.ProductFile productImage = new Areas.Non_IT.Models.ProductFile();
                if (savedProductImage != null)
                {

                    productImage.FilePath = savedProductImage.FilePath;
                    productImage.IsProductImage = savedProductImage.IsProductImage;
                    productImage.FileName = savedProductImage.FileName;
                    productImage.Extension = savedProductImage.Extension;
                    productImage.ProductID = newProductId;
                    productImage.CreatedBy = savedProductImage.CreatedBy;
                    productImage.CreatedDate = DateTime.Now;
                    productImage.UpdatedBy = savedProductImage.UpdatedBy;
                    productImage.UpdatedDate = DateTime.Now;
                    productImage.Deleted = false;
                    productImagelist.Add(productImage);
                }
            }
            return productImagelist;
        }
        public static List<NIProductPostTrack> DiposedTrackToPostTrack(List<NIAssetTrack> asstTrackList, NIProduct product)
        {
            List<NIProductPostTrack> productPostTrackList = new List<NIProductPostTrack>();
            foreach (var assetTrack in asstTrackList)
            {
                if (assetTrack.UserRole <= (int)Utils.AppEnumeration.NIUserRole.GCOREALM)
                {
                    NIProductPostTrack productPostTrack = new NIProductPostTrack();
                    productPostTrack.UserId = assetTrack.UserId;
                    productPostTrack.UserRole = assetTrack.UserRole;
                    productPostTrack.TrackStatus = assetTrack.UserRole;    //TODO: Optinal if not used, Remove from table. 
                    productPostTrack.ProductId = product.Id;
                    productPostTrack.CreatedBy = assetTrack.CreatedBy;
                    productPostTrack.CreatedDate = assetTrack.CreatedDate;
                    productPostTrack.Deleted = assetTrack.Deleted;
                    productPostTrackList.Add(productPostTrack);
                }
            }
            return productPostTrackList;
        }

        public static void ReUseMail(NIProduct product)
        {
            var toEmailId = (new NIUserService()).GetUserById(product.UserId ?? 0).Email;
            var productDetails = new NIEmailNIProductDetails() { NIProductDetail = viewMap(product) };
            var emailbody = EmailFactory.ParseTemplate(productDetails, Non_IT.AppUtils.reuseMailTemplate);
            //emailService.send(toEmailId, "Asset Reused", emailbody);
            GemsToken.SendMail(new List<string>() { toEmailId }, "Asset Reused", emailbody, true);
        }

        public static bool IsUserMSO(string Email)
        {
            return (new NIUserService()).IsUserMSO(Email);
        }
        public static bool IsAssistantUserMSO(string Email)
        {
            return (new NIUserService()).IsUserAssistanctMSO(Email);
        }
        public static Garage.Areas.Non_IT.Models.Role GetRoleByEmail(string Email)
        {
            return (new NIUserService()).GetRoleByEmail(Email);
        }

        public static List<NIProductDetail> getProductDetails(List<NIProduct> productList)
        {

            return productList.ConvertAll(p => viewMap(p));
        }
        public static List<string> getAllMSOEmailBySchoolId(int schoolId)
        {

            return (new NIUserService()).GetAllUsers().Where(u => u.Deleted == false && u.School == schoolId && u.NIRole == (int)AppEnumeration.NIUserRole.MSO).Select(u => u.Email).ToList();

        }
        public static void AfterScheduledMail(NIOrder order)
        {
            var toEmailIds = getAllEmailsBasedOnRole((int)AppEnumeration.NIUserRole.MSO, order.SchoolId ?? 0);

            var productOrder = (new NIProductOrderService()).GetPOByOrderId(order.Id).FirstOrDefault();
            var product = (new NIProductService()).GetProductById(productOrder.ProductId ?? 0);
            var orderDetails = new NIEmailNIOrderDetails()
            {
                ProductDetail = viewMap(product),
                ProductOrder = productOrder
            };
            var emailbody = EmailFactory.ParseTemplate(orderDetails, Non_IT.AppUtils.AfterScheduledMailTemplate);
            //emailService.send(toEmailIds, "After Scheduled", emailbody);
            GemsToken.SendMail(toEmailIds, "After Scheduled", emailbody, true);

        }
        public static void AddErrorLog(Error_Log _Error_Log)
        {
            (new NIErrorLogServies()).ErrorLog(_Error_Log);
        }
        public static Dictionary<int, string> getSchoolName(List<NIProduct> productList)
        {
            Dictionary<int, string> SchoolNameMap = new Dictionary<int, string>();
            var schoolname = string.Empty;
            foreach (NIProduct product in productList)
            {

                var diposedSchool = (new NISchoolService()).GetSchoolById(product.SchoolId ?? 0);
                SchoolNameMap.Add(product.Id, diposedSchool.SchoolCode);
            }
            return SchoolNameMap;
        }
        public static NISchool GetSchoolDetails(int? SchoolID)
        {
            if (SchoolID.HasValue)
                return (new NISchoolService()).GetSchoolById(SchoolID.Value);
            else
                return null;
        }

        public static void AssetDispose(NIProduct product)
        {
            var currentUser = Non_IT.AppUtils.getCurrentUser();
            var currentUserRole = (AppEnumeration.NIDisposeUserRole)currentUser.user.NIRole;
            var approvedAsset = new NIProduct();
            if (product.ApprovalReq && currentUserRole == AppEnumeration.NIDisposeUserRole.CFO)
            {
                product.Status = (int)ProductStatus.Disposed;
                product.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
                product.UpdatedDate = DateTime.Now;

                approvedAsset = (new NIProductService()).UpdateProduct(product);
            }

            else if (product.ApprovalReq == false && currentUserRole == AppEnumeration.NIDisposeUserRole.SSCINTERNALAUDIT)
            {
                product.Status = (int)ProductStatus.Disposed;
                product.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
                product.UpdatedDate = DateTime.Now;
                approvedAsset = (new NIProductService()).UpdateProduct(product);
            }
            else if (currentUserRole == AppEnumeration.NIDisposeUserRole.GCOREALM || currentUser.user.NIRole.Value == (int)AppEnumeration.NIUserRole.PROCUREMENT)
            {
                product.Status = (int)ProductStatus.Approved;
                product.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
                product.UpdatedDate = DateTime.Now;
                approvedAsset = (new NIProductService()).UpdateProduct(product);
            }
            else if (currentUserRole == AppEnumeration.NIDisposeUserRole.HOS || currentUserRole == AppEnumeration.NIDisposeUserRole.MSOASSISTANT || currentUserRole == AppEnumeration.NIDisposeUserRole.ACCOUNTS || currentUserRole == AppEnumeration.NIDisposeUserRole.MSO)
            {
                product.Status = (int)ProductStatus.Posted;
                product.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
                product.UpdatedDate = DateTime.Now;
                approvedAsset = (new NIProductService()).UpdateProduct(product);
            }
           (new NIAssetTrackService()).AddAssetTrack(Helper.NonIT_Helper.assetTrackmap(approvedAsset));
            if (currentUserRole != AppEnumeration.NIDisposeUserRole.CFO)
            {
                Helper.NonIT_Helper.DisposeApprovalMail(product);
            }
        }

        public static void AdminAssetDispose(NIProduct product, string Remarks)
        {
            var approvedAsset = new NIProduct();
            var currentUser = Non_IT.AppUtils.getCurrentUser();
            var currentUserRole = (AppEnumeration.NIDisposeUserRole)currentUser.user.NIRole;

            if (currentUserRole == AppEnumeration.NIDisposeUserRole.SSCFINANCE)
            {
                product = (new NIProductService()).UpdateProduct(product);
            }

            if ((currentUserRole == (AppEnumeration.NIDisposeUserRole.SSCINTERNALAUDIT)) && (!product.ApprovalReq))
            {
                product.Status = (int)ProductStatus.Disposed;
                product.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
                product.UpdatedDate = DateTime.Now;
                approvedAsset = (new NIProductService()).UpdateProduct(product);
            }
            else if ((currentUserRole == (AppEnumeration.NIDisposeUserRole.CFO)) && (product.ApprovalReq))
            {
                product.Status = (int)ProductStatus.Disposed;
                product.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
                product.UpdatedDate = DateTime.Now;
                approvedAsset = (new NIProductService()).UpdateProduct(product);

            }
            if (product.Status != (int)ProductStatus.Disposed)
                Helper.NonIT_Helper.DisposeApprovalMail(product);

            var result = Helper.NonIT_Helper.assetTrackmap(product);
            result.Remarks = Remarks;
            (new NIAssetTrackService()).AddAssetTrack(result);
        }
    }
}