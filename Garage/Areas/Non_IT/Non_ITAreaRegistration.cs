﻿using System.Web.Mvc;

namespace Garage.Areas.Non_IT
{
    public class Non_ITAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Non_IT";
            }
        }


        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Non_IT_default",
                "Non_IT/{controller}/{action}/{id}",
                defaults: new { action = "Index", controller = "NICategory", id = UrlParameter.Optional },
                 namespaces: new[] { "Garage.Controllers" }
            );
        }
    }
}