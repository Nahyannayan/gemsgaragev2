﻿using Garage.Areas.Non_IT.Models;
using Garage.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Garage.Areas.Non_IT
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private GarageNonIT _context = null;
        private DbSet<T> dbset = null;

        public GenericRepository()
        {
            this._context = new GarageNonIT();
            dbset = _context.Set<T>();
        }

        public GenericRepository(GarageNonIT _context)
        {
            this._context = _context;
            dbset = _context.Set<T>();
        }

        public IEnumerable<T> GetAll()
        {
            return dbset.ToList();
        }

        public T GetById(object id)
        {
            return dbset.Find(id);
        }

        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            IQueryable<T> query = dbset.Where(predicate);
            return query;
        }

        public T Insert(T obj)
        {
            return dbset.Add(obj);
        }
        
        public IEnumerable<T> InsertAll(IEnumerable<T> obj)
        {
            return dbset.AddRange(obj);
        }

        public T Update(T obj)
        {
            var entity = dbset.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
            return entity;
        }

        public T Delete(object id)
        {
            T existing = dbset.Find(id);
            var entity = dbset.Remove(existing);
            return entity;
        }

        public IEnumerable<T> DeleteAll(IEnumerable<T> obj)
        {
            var entity = dbset.RemoveRange(obj);
            _context.SaveChanges();
            return entity;
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}