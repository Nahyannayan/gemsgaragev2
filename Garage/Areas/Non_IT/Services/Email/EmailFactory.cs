﻿using Garage.Service.Email;
using log4net;
using RazorEngine;
using System;
using System.IO;
using System.Reflection;
using System.Web;
using static Garage.Utils.AppEnumeration;

namespace Garage.Areas.Non_IT.Service.Email
{
    public static class EmailFactory
    {
        private static readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static string ParseTemplate<T>(T model, string templatePath)
        {
            try
            {
                var content = templatePath.ReadTemplateContent();
                return Razor.Parse(content, model);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                //throw new Exception(ex.Message);
            }
            finally
            {
                // final cleanup code
            }
            return "";

        }
    }
}