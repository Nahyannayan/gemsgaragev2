﻿using Garage.Areas.Non_IT.Models;
using Garage.Areas.Non_IT.ViewModel;
using Garage.Models;
using Garage.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.Areas.Non_IT.Service.Email.EmailModels
{
    public class OrderScheduled
    {
        public NIProductDetail  ProductDetail { get; set; }
        public NIProductOrder ProductOrder { get; set; }
        public NISoldProduct SoldProduct { get; set; }
        public NISchool School { get; set; }
    }
}