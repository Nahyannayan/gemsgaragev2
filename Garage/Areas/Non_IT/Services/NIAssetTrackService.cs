﻿using Garage.Areas.Non_IT.Models;
using Garage.Models;
using Garage.Repository;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using static Garage.Utils.AppEnumeration;
using Garage.Areas.Non_IT.Models;

namespace Garage.Areas.Non_IT.Service
{
    public class NIAssetTrackService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        GarageNonIT context = null;

        public NIAssetTrackService()
        {
            this.context = new GarageNonIT();
        }

        public NIAssetTrackService(GarageNonIT context)
        {
            this.context = context;
        }

        public IEnumerable<NIAssetTrack> GetAllAssetTrack()
        {

            return context.NIAssetTracks.Where(p => p.Deleted == false).ToList();
        }

        public NIAssetTrack GetPostTrackById(int id)
        {

            return context.NIAssetTracks.Single(p => p.Id == id);
        }

        public NIAssetTrack AddAssetTrack(NIAssetTrack assetTrack)
        {
            logger.Info("save product post track to DB");
            var savedAssetTrack = context.NIAssetTracks.Add(assetTrack);
            context.SaveChanges();
            logger.Info("saved product post track to DB");
            return savedAssetTrack;
        }

        public IEnumerable<NIAssetTrack> AddAssetTrackList(List<NIAssetTrack> assetTracks)
        {
            logger.Info("save list of product post track to DB");
            var savedAssetTrack = context.NIAssetTracks.AddRange(assetTracks);
            context.SaveChanges();
            logger.Info("saved list of product post track to DB");
            return savedAssetTrack;
        }

        public NIAssetTrack UpdateProduct(NIAssetTrack assetTrack)
        {
            logger.Info("update product post track to DB");
            NIAssetTrack updatedAssetTrack = GetPostTrackById(assetTrack.Id);
            updatedAssetTrack = assetTrack;
            context.SaveChanges();
            logger.Info("updated product post track to DB");
            return updatedAssetTrack;
        }

        public NIAssetTrack DeleteAssetTrack(NIAssetTrack assetTrack)
        {
            logger.Info("delete product post track to DB");
            NIAssetTrack deletedAssetTrack = GetPostTrackById(assetTrack.Id);
            deletedAssetTrack.Deleted = true;
            context.SaveChanges();
            logger.Info("deleted product post track to DB");
            return deletedAssetTrack;
        }

        public IEnumerable<NIAssetTrack> DeleteAssetTrackByProduct(int assetId)
        {
            logger.Info("Delete all product post track by Product Id to DB");
            var deletedProductTrack = context.NIAssetTracks.Where(a => a.AssetId == assetId).ToList();
            deletedProductTrack.ForEach(p => p.Deleted = true);
            context.NIAssetTracks.RemoveRange(deletedProductTrack);
            context.SaveChanges();
            logger.Info("deleted product post track to DB");
            return deletedProductTrack;
        }

        public List<NIAssetTrack> GetPostTrackByAssetId(int id)
        {
            return context.NIAssetTracks.Where(a => a.AssetId == id && a.Deleted == false).ToList();
        }

        public int getNextAssetApproverByAssetId(NIProduct product)
        {
            int result = 0;
            var assetTrackList = GetPostTrackByAssetId(product.Id);
            if (assetTrackList != null && assetTrackList.Count > 0)
            {
                result = assetTrackList.OrderByDescending(pt => pt.CreatedDate).FirstOrDefault().NextApprovalUserRole.Value;
                //if (role != (int)Utils.AppEnumeration.Role.CFO)
                //{
                //    result = context.Roles.Single(p => p.ID == role).ID + 1;
                //}
            }
            return result;
        }

    }
}