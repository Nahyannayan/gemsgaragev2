﻿using Garage.Models;
using Garage.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;
using System.ComponentModel.DataAnnotations;
using log4net;
using System.Reflection;
using Garage.Areas.Non_IT.Models;
using Garage.Utils;
using Category = Garage.Areas.Non_IT.Models.Category;

namespace Garage.Areas.Non_IT.Service
{

    public class NICategoryService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        GarageNonIT context = null;

        public NICategoryService()
        {
            this.context = new GarageNonIT();
        }

        public NICategoryService(GarageNonIT context)
        {
            this.context = context;
        }

        public IEnumerable<Areas.Non_IT.Models.Category> GetAllCategories()
        {
            List<Areas.Non_IT.Models.Category> categories = new List<Areas.Non_IT.Models.Category>();
            var AllCategory = context.Categories.Where(p => p.Deleted == false).ToList();
            return AllCategory;
        }

        public IEnumerable<Areas.Non_IT.Models.SubCategory> GetSubCategories(int? Id)
        {
            List<Areas.Non_IT.Models.SubCategory> SubCategories = new List<Areas.Non_IT.Models.SubCategory>();
            SubCategories = context.SubCategories.Where(p => (Id.HasValue == false || p.ID == Id.Value) && p.Deleted == false).ToList();
            return SubCategories;
        }

        public Areas.Non_IT.Models.Category GetCategoryBySubCategoryID(int Id)
        {
            var SubCategory = context.SubCategories.Single(p => (p.ID == Id) && p.Deleted == false);
            var Category = context.Categories.Single(p => (p.ID == SubCategory.CategoryID) && p.Deleted == false);
            return Category;
        }

        public Areas.Non_IT.Models.SubCategory GetSubCategoryByID(int Id)
        {
            var SubCategory = context.SubCategories.Single(p => (p.ID == Id) && p.Deleted == false);
            return SubCategory;
        }
        public Areas.Non_IT.Models.Category GetCategoryById(int Id)
        {
            var Category = context.Categories.Single(p => (p.ID == Id) && p.Deleted == false);
            return Category;
        }
        public IEnumerable<Areas.Non_IT.Models.SubCategory> GetSubCategoriesByCID(int Id)
        {
            List<Areas.Non_IT.Models.SubCategory> SubCategories = new List<Areas.Non_IT.Models.SubCategory>();
            SubCategories = context.SubCategories.Where(p => p.CategoryID == Id && p.Deleted == false).ToList();
            return SubCategories;
        }


        public Areas.Non_IT.Models.Category AddCategory(Areas.Non_IT.Models.Category category)
        {
            logger.Info("save category to DB");
            var savedCategory = context.Categories.Add(category);
            context.SaveChanges();
            logger.Info("saved category to DB");
            return savedCategory;
        }

        public Areas.Non_IT.Models.SubCategory AddSubCategory(Areas.Non_IT.Models.SubCategory subcategory)
        {
            logger.Info("save category to DB");
            var savedCategory = context.SubCategories.Add(subcategory);
            context.SaveChanges();
            logger.Info("saved category to DB");
            return savedCategory;
        }



        public Areas.Non_IT.Models.SubCategory UpdateSubCategory(Areas.Non_IT.Models.SubCategory subcategory)
        {
            logger.Info("update category to DB");
            var updatedsubCategory = context.SubCategories.Single(p => p.ID == subcategory.ID);
            updatedsubCategory.Active = subcategory.Active;
            updatedsubCategory.CategoryID = subcategory.CategoryID;
            updatedsubCategory.SubCategoryName = subcategory.SubCategoryName;
            if (subcategory.ImagePath != null)
                updatedsubCategory.ImagePath = subcategory.ImagePath;
            updatedsubCategory.UpdatedDate = DateTime.Now;
            updatedsubCategory.Deleted = subcategory.Deleted;
            updatedsubCategory.UpdatedBy = subcategory.UpdatedBy;
            context.SaveChanges();
            logger.Info("updated category to DB");
            return updatedsubCategory;
        }



        public Areas.Non_IT.Models.SubCategory DeleteSubCategory(int subcategoryID)
        {
            logger.Info("delete category to DB");
            var deletesubcategory = context.SubCategories.Single(p => p.ID == subcategoryID);
            deletesubcategory.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
            deletesubcategory.UpdatedDate = DateTime.Now;
            deletesubcategory.Deleted = true;
            context.SaveChanges();
            logger.Info("deleted category to DB");
            return deletesubcategory;
        }

    }
}