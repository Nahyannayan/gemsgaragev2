﻿using Garage.Models;
using Garage.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;
using System.ComponentModel.DataAnnotations;
using log4net;
using System.Reflection;
using Garage.Areas.Non_IT.Models;
using Garage.Utils;

namespace Garage.Areas.Non_IT.Services
{
    public class NIErrorLogServies
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        GarageNonIT context = null;

        public NIErrorLogServies()
        {
            this.context = new GarageNonIT();
        }

        public NIErrorLogServies(GarageNonIT context)
        {
            this.context = context;
        }

        public void ErrorLog(Areas.Non_IT.Models.Error_Log _Error_Log)
        { 
            context.Error_Log.Add(_Error_Log);
            context.SaveChanges();
        }
    }
}