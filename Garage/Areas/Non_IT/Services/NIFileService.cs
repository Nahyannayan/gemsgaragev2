﻿using Garage.Models;
using Garage.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;
using System.ComponentModel.DataAnnotations;
using log4net;
using System.Reflection;
using Garage.Areas.Non_IT.Models;
using Garage.Utils;

namespace Garage.Areas.Non_IT.Services
{
    public class NIFileService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        GarageNonIT context = null;

        public NIFileService()
        {
            this.context = new GarageNonIT();
        }

        public NIFileService(GarageNonIT context)
        {
            this.context = context;
        }



        public Areas.Non_IT.Models.ProductFile AddNIProducFiles(Areas.Non_IT.Models.ProductFile file)
        {
            var FileID = context.ProductFiles.Add(file);
            context.SaveChanges();
            return FileID;
        }
        public Areas.Non_IT.Models.ProductFile GetProductFilesByID(int ID, bool IsProductImage = true)
        {
            return context.ProductFiles.Single(p => p.ID == ID && p.IsProductImage == IsProductImage && p.Deleted == false);

        }

        public List<Areas.Non_IT.Models.ProductFile> GetProductFilesByProductId(int ID)
        {
            return context.ProductFiles.Where(p => p.ProductID == ID && p.IsProductImage == false && p.Deleted == false).ToList();
        }
        public List<Areas.Non_IT.Models.ProductFile> GetProductImageByProductId(int ID, bool IsProductImage = true)
        {
            return context.ProductFiles.Where(p => p.ProductID == ID && p.IsProductImage == IsProductImage && p.Deleted == false).ToList();
        }
        public  Areas.Non_IT.Models.ProductFile GetProductImageById(int ID)
        {
            return context.ProductFiles.Single(p => p.ID == ID && p.IsProductImage == true);
        }
        public Areas.Non_IT.Models.ProductFile GetSingleProductImageByProductId(int ProductID)
        {
            return context.ProductFiles.First(p => p.ProductID == ProductID && p.IsProductImage == true && p.Deleted == false);
        }

        public bool DeleteProductFileByID(int ID)
        {
            var deleteitem = context.ProductFiles.First(p => p.ID == ID);
            deleteitem.Deleted = true;
            deleteitem.UpdatedDate = DateTime.Now;
            context.SaveChanges();
            return true;
        }

        public IEnumerable<Areas.Non_IT.Models.ProductFile> AddProductImageList(List<Areas.Non_IT.Models.ProductFile> categories)
        {
            logger.Info("save list of product image to DB");
            var savedProductImage = context.ProductFiles.AddRange(categories);
            context.SaveChanges();
            logger.Info("saved list of product image to DB");
            return savedProductImage;
        }
    }
}