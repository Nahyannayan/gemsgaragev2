﻿using Garage.Models;
using Garage.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;
using System.ComponentModel.DataAnnotations;
using log4net;
using System.Reflection;
using Garage.Areas.Non_IT.Models;
using Garage.Utils;
using Garage.Areas.Non_IT.ViewModels;

namespace Garage.Areas.Non_IT.Services
{
    public class NIGeneralService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        GarageNonIT NonITcontext = null;

        public NIGeneralService()
        {
            this.NonITcontext = new GarageNonIT();
        }

        public NIGeneralService(GarageNonIT NonITcontext)
        {
            this.NonITcontext = NonITcontext;
        }
        public Garage.Areas.Non_IT.Models.City GetCityByID(int CityID)
        {
            return NonITcontext.Cities.Single(p => p.ID == CityID);
        }
        public Garage.Areas.Non_IT.Models.ConditionEnum GetConditionByID(int ID)
        {
            return NonITcontext.ConditionEnums.Single(p => p.ID == ID);
        }

        public List<Garage.Areas.Non_IT.Models.ConditionEnum> GetAllCondition()
        {
            return NonITcontext.ConditionEnums.OrderBy(p => p.OrderID).ToList();
        }

        public IEnumerable<Areas.Non_IT.Models.AssetType> GetAllAssetTypes()
        {
            List<Areas.Non_IT.Models.AssetType> AssetTypes = new List<Areas.Non_IT.Models.AssetType>();
            AssetTypes = NonITcontext.AssetTypes.ToList();
            return AssetTypes;
        }

        public void AddEmailLogs(EmailLog _log)
        {
            NonITcontext.EmailLogs.Add(_log);
            NonITcontext.SaveChanges();
        }

    }
}