﻿using Garage.Models;
using Garage.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.Mapper.OrderMapper;
using static Garage.Utils.Mapper.ProductOrderMapper;
using static Garage.Utils.Mapper.SoldProductMapper;
using static Garage.Utils.Mapper.OrderTrackMapper;
using static Garage.Utils.AppEnumeration;
using System.ComponentModel.DataAnnotations;
using log4net;
using System.Reflection;
using Garage.ViewModels;
using Garage.Areas.Non_IT.Models;
using Garage.Areas.Non_IT.Service;
using Garage.Service;

namespace Garage.Areas.Non_IT.Services
{
    public class NIOrderService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        GarageNonIT context = null;

        private NIProductOrderService productOrderService = new NIProductOrderService();
        private Garage.Areas.Non_IT.Service.NIProductService productService = new Garage.Areas.Non_IT.Service.NIProductService();
        private NISoldProductService soldProductService = new NISoldProductService();
        private NIOrderTrackService orderTrackService = new NIOrderTrackService();

        public NIOrderService()
        {
            this.context = new GarageNonIT();
        }

        public NIOrderService(GarageNonIT context)
        {
            this.context = context;
        }

        public IEnumerable<NIOrder> GetAllOrders()
        {

            return context.NIOrders.Where(p => p.Deleted == false).ToList();

        }


        public NIOrder GetOrderById(int id)
        {

            return context.NIOrders.Single(p => p.Id == id);
        }

        public NIOrder AddOrder(NIOrder order)
        {
            logger.Info("save order to DB");
            var savedOrder = context.NIOrders.Add(order);
            context.SaveChanges();
            logger.Info("saved order to DB");
            return savedOrder;
        }

        public IEnumerable<NIOrder> AddOrderList(List<NIOrder> categories)
        {
            logger.Info("save list of orders to DB");
            var savedOrder = context.NIOrders.AddRange(categories);
            context.SaveChanges();
            logger.Info("saved list of orders to DB");
            return savedOrder;
        }

        public NIOrder UpdateOrder(NIOrder order)
        {
            logger.Info("update order to DB");
            var updatedOrder = context.NIOrders.Single(p => p.Id == order.Id);
            updatedOrder = order;
            //context.SaveChanges();
            using (var dbcontext = new GarageNonIT())
            {
                var std = dbcontext.NIOrders.Single(p => p.Id == order.Id);

                std.OrderNo = order.OrderNo;
                std.OrderStatus = order.OrderStatus;
                std.Remark = order.Remark;
                std.SchoolId = order.SchoolId;
                std.Deleted = order.Deleted;
                std.Id = order.Id;
                dbcontext.SaveChanges();
                return std;
            }
            logger.Info("updated order to DB");
            return updatedOrder;
        }

        public NIOrder DeleteOrder(NIOrder order)
        {
            logger.Info("delete order to DB");
            var deletedOrder = context.NIOrders.Single(p => p.Id == order.Id);
            deletedOrder.Deleted = true;
            context.SaveChanges();

            logger.Info("deleted order to DB");
            return deletedOrder;
        }

        public decimal getSoldQuantityByProductId(int productId)
        {
            var productOrders = productOrderService.GetAllProductOrders();
            var orders = GetAllOrders();
            var productOrderList = productOrders
                .Join(orders, po => po.OrderId, o => o.Id, (po, o) => new { po, o })
                .Where(obj => obj.o.OrderStatus == (int)OrderStatus.Procured && obj.po.ProductId == productId && obj.po.Deleted == false)
                .Select(obj => obj.po);

            return productOrderList.Sum(po => po.Quantity) ?? 0;
        }

        public NIOrder PlaceOrder(List<ProductOrderForm> productOrderForms)
        {
            NIOrder savedOrder = null;
            var index = 0;
            foreach (var productOrderForm in productOrderForms)
            {
                savedOrder = AddOrder(Helper.NonIT_Helper.map(index));// Create New Order
                var productOrder = productOrderService.AddProductOrder(Helper.NonIT_Helper.map(savedOrder, productOrderForm));// Create New Product Order Again Order
                var product = productService.GetProductById(productOrder.ProductId ?? 0);
                product.InStock = product.InStock - productOrder.Quantity;// Reduce and Update Product Quantity for Product Order
                productService.UpdateProduct(product);
                orderTrackService.AddOrderTrack(Helper.NonIT_Helper.orderTrackMap(savedOrder));// Initiate Track entry for this order
                soldProductService.AddSoldProductList(Helper.NonIT_Helper.map(savedOrder)); // Add sold Product for all order
                index = index + 1;
            }
            return savedOrder;
        }
    }
}