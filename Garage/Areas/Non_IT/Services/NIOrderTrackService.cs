﻿using Garage.Areas.Non_IT.Models;
using Garage.Models;
using Garage.Repository;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using static Garage.Utils.AppEnumeration;
using Garage.Areas.Non_IT.Models;

namespace Garage.Areas.Non_IT.Service
{
    public class NIOrderTrackService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        GarageNonIT context = null;

        public NIOrderTrackService()
        {
            this.context = new GarageNonIT();
        }

        public NIOrderTrackService(GarageNonIT context)
        {
            this.context = context;
        }

        public IEnumerable<NIOrderTrack> GetAllOrderTracks()
        {
            return context.NIOrderTracks.Where(p => p.Deleted == false).ToList();
        }

        public NIOrderTrack GetOrderTrackById(int id)
        {
            return context.NIOrderTracks.Single(p => p.Id == id);
        }

        public NIOrderTrack AddOrderTrack(NIOrderTrack orderTrack)
        {
            logger.Info("save order track to DB");
          
            var savedOrderTrack = context.NIOrderTracks.Add(orderTrack);
            context.SaveChanges();
            logger.Info("saved order track to DB");
            return savedOrderTrack;
        }

        public IEnumerable<NIOrderTrack> AddOrderTrackList(List<NIOrderTrack> orderTracks)
        {
            logger.Info("save list of order track to DB");
            var savedOrderTrack = context.NIOrderTracks.AddRange(orderTracks);
            context.SaveChanges();
            logger.Info("saved list of order track to DB");
            return savedOrderTrack;
        }

        public NIOrderTrack UpdateOrderTrack(NIOrderTrack orderTrack)
        {
            logger.Info("update order track to DB");
            var updatedOrderTrack = context.NIOrderTracks.Single(p => p.Id == orderTrack.Id);
            context.SaveChanges();
            logger.Info("updated order track to DB");
            return updatedOrderTrack;
        }

        public NIOrderTrack DeleteOrderTrack(NIOrderTrack orderTrack)
        {
            logger.Info("delete order track to DB");
            var deletedOrderTrack = context.NIOrderTracks.Single(p => p.Id == orderTrack.Id);
            deletedOrderTrack.Deleted = true;
            context.SaveChanges();
            logger.Info("deleted order track to DB");
            return deletedOrderTrack;
        }

        public List<NIOrderTrack> GetOrderTrackByOrderId(int id)
        {

            return context.NIOrderTracks.Where(p => p.OrderId == id).ToList();
        }

    }
}