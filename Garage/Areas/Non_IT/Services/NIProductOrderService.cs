﻿using Garage.Areas.Non_IT.Models;
using Garage.Models;
using Garage.Repository;
using Garage.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using static Garage.Utils.AppEnumeration;

namespace Garage.Areas.Non_IT.Services
{
    public class NIProductOrderService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private GarageNonIT context = null;

        public NIProductOrderService()
        {
            this.context = new GarageNonIT();
        }

        public NIProductOrderService(GarageNonIT context)
        {
            this.context = context;
        }

        public IEnumerable<NIProductOrder> GetAllProductOrders()
        {

            return context.NIProductOrders.Where(p => p.Deleted == false).ToList();
        }

        public NIProductOrder GetProductOrderById(int id)
        {


            return context.NIProductOrders.Single(p => p.Id == id);
        }

        public NIProductOrder AddProductOrder(NIProductOrder productOrder)
        {
            logger.Info("save product order to DB");
            var savedProductOrder = context.NIProductOrders.Add(productOrder);
            context.SaveChanges();
            logger.Info("saved product order to DB");
            return savedProductOrder;
        }

        public IEnumerable<NIProductOrder> AddProductOrderList(List<NIProductOrder> categories)
        {
            logger.Info("save list of product order to DB");
            var savedProductOrder = context.NIProductOrders.AddRange(categories);
            context.SaveChanges();
            logger.Info("saved list of product order to DB");
            return savedProductOrder;
        }

        public NIProductOrder UpdateProductOrder(NIProductOrder productOrder)
        {
            logger.Info("update product order to DB");
            var updatedProductOrder = context.NIProductOrders.Single(p => p.Id == productOrder.Id);
            updatedProductOrder.DaxUpdated = productOrder.DaxUpdated;
            updatedProductOrder.ProductValue = productOrder.ProductValue;
            updatedProductOrder.Quantity = productOrder.Quantity;
            updatedProductOrder.UnitValue = productOrder.UnitValue;


            context.SaveChanges();
            logger.Info("updated product order to DB");
            return updatedProductOrder;
        }

        public NIProductOrder DeleteProductOrder(NIProductOrder productOrder)
        {
            logger.Info("delete product order to DB");
            var deletedProductOrder = context.NIProductOrders.Single(p => p.Id == productOrder.Id);
            deletedProductOrder.Deleted = false;
            context.SaveChanges();
            logger.Info("updated product order to DB");
            return deletedProductOrder;
        }

        public List<NIProductOrder> GetPOByOrderId(int id)
        {

            return context.NIProductOrders.Where(p => p.OrderId == id && p.Deleted == false).ToList();
        }
        public List<NIProductOrder> GetPOByProdAndOrderId(int orderId, int prodId)
        {
            return context.NIProductOrders.Where(p => p.OrderId == orderId && p.ProductId == prodId && p.Deleted == false).ToList();
        }
        public IEnumerable<NIOrderVouchersDetail> AddOrderVouchersDetail(List<NIOrderVouchersDetail> Detail)
        {
            logger.Info("save order vouchers to DB");
            Detail.ForEach(p => p.CreatedDate = DateTime.Now);
            Detail.ForEach(p => p.CreatedBy = Non_IT.AppUtils.getCurrentUser().user.Id);

            var savedProductOrder = context.NIOrderVouchersDetails.AddRange(Detail);
            context.SaveChanges();
            logger.Info("save order vouchers to DB");
            return savedProductOrder;
        }
        public NIOrderVouchersDetail AddOrderVouchersDetail(NIOrderVouchersDetail Detail)
        {
            logger.Info("save order vouchers to DB");
            Detail.CreatedDate = DateTime.Now;
            Detail.CreatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
            Detail.SchoolID = Non_IT.AppUtils.getCurrentUser().school.Id;
            var savedProductOrder = context.NIOrderVouchersDetails.Add(Detail);
            context.SaveChanges();
            logger.Info("save order vouchers to DB");
            return savedProductOrder;
        }
        public  NIOrderVouchersDetail GetOrderVouchersDetail(int OrderId, int SchoolID)
        {
            return context.NIOrderVouchersDetails.SingleOrDefault(p => p.OrderId == OrderId && p.SchoolID == SchoolID);

        }
        public NIOrderVouchersDetail UpdateNIOrderVouchersDetail(NIOrderVouchersDetail productOrder)
        {
            logger.Info("update product order to DB");
            var updatedProductOrder = context.NIOrderVouchersDetails.Single(p => p.Id == productOrder.Id);
            updatedProductOrder.NewAssetTagID = productOrder.NewAssetTagID;
            updatedProductOrder.UpdatedDate = DateTime.Now;
            updatedProductOrder.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
            context.SaveChanges();
            logger.Info("updated product order to DB");
            return updatedProductOrder;
        }
    }
}