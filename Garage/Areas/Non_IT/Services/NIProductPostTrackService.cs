﻿using Garage.Areas.Non_IT.Models;
using Garage.Models;
using Garage.Repository;
using Garage.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using static Garage.Utils.AppEnumeration;

namespace Garage.Areas.Non_IT.Services
{
    public class NIProductPostTrackService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private GenericRepository<ProductPostTrack> productPostTrackRepository = null;
        GarageNonIT context = null;
        public NIProductPostTrackService()
        {
            this.context = new GarageNonIT();
        }

        public NIProductPostTrackService(GarageNonIT context)
        {
            this.context = context;
        }

        public IEnumerable<NIProductPostTrack> GetAllProductPostTrack()
        {
            return context.NIProductPostTracks.Where(p => p.Deleted == Convert.ToBoolean(AppEnumeration.YNStatue.No)).ToList();
        }

        public NIProductPostTrack GetPostTrackById(int id)
        {

            return context.NIProductPostTracks.Single(p => p.Id == id);
        }


        public NIProductPostTrack AddNIProductPostTrack(NIProductPostTrack productPostTrack)
        {
            logger.Info("save product post track to DB");
            var savedProductPostTrack = context.NIProductPostTracks.Add(productPostTrack);
            context.SaveChanges();
            logger.Info("saved product post track to DB");
            return savedProductPostTrack;
        }

        public IEnumerable<NIProductPostTrack> AddProductPostTrackList(List<NIProductPostTrack> productPostTracks)
        {
            logger.Info("save list of product post track to DB");
            var savedProductPostTrack = context.NIProductPostTracks.AddRange(productPostTracks);
            context.SaveChanges();
            logger.Info("saved list of product post track to DB");
            return savedProductPostTrack;
        }

        public NIProductPostTrack UpdateProduct(NIProductPostTrack productPostTrack)
        {
            logger.Info("update product post track to DB");
            var updateproductPostTrack = context.NIProductPostTracks.Single(p => p.Id == productPostTrack.Id);
            updateproductPostTrack = productPostTrack;
            context.SaveChanges();
            logger.Info("updated product post track to DB");
            return updateproductPostTrack;
        }

        public NIProductPostTrack DeleteProductPostTrack(NIProductPostTrack productPostTrack)
        {
            logger.Info("delete product post track to DB");


            var deletedProductPostTrack = context.NIProductPostTracks.Single(p => p.Id == productPostTrack.Id);
            deletedProductPostTrack.Deleted = Convert.ToBoolean(AppEnumeration.YNStatue.Yes);
            context.SaveChanges();

            logger.Info("deleted product post track to DB");
            return deletedProductPostTrack;
        }

        public List<NIProductPostTrack> DeleteProductTrackByProduct(int productId)
        {
            logger.Info("Delete all product post track by Product Id to DB");
            var deletedProductTrack = context.NIProductPostTracks.Where(p => p.ProductId == productId).ToList();
            context.NIProductPostTracks.RemoveRange(deletedProductTrack);

            context.SaveChanges();
            logger.Info("deleted product post track to DB");
            return deletedProductTrack.ToList();
        }

        public List<NIProductPostTrack> GetPostTrackByProductId(int id)
        {
            return context.NIProductPostTracks.Where(p => p.ProductId == id).ToList();

        }

        public int getNextProductApproverByProductId(NIProduct product)
        {
            int result = 0;
            var postTrackList = GetPostTrackByProductId(product.Id);
            if (postTrackList != null && postTrackList.Count > 0)
            {
              var  postresult = postTrackList.OrderByDescending(pt => pt.CreatedDate).FirstOrDefault();
                if(postresult != null && postresult.NextApprovalUserRole.HasValue)
                {
                    return postresult.NextApprovalUserRole.Value;
                }
               //result = context.Roles.Single(p => p.ID == result).NEXTAPPROVALROLEID;
                //if (role != (int)AppEnumeration.UserRole.Principal)
                //{
                //    result = (int)((AppEnumeration.UserRole)role + 1);
                //}
            }
            return result;
        }
    }
}