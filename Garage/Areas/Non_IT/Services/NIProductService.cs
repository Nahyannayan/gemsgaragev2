﻿using Garage.Models;
using Garage.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;
using System.ComponentModel.DataAnnotations;
using log4net;
using System.Reflection;
using Garage.Areas.Non_IT.Models;
using Garage.Utils;
using Garage.Areas.Non_IT.ViewModels;

namespace Garage.Areas.Non_IT.Service
{

    public class NIProductService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        GarageNonIT context = null;

        public NIProductService()
        {
            this.context = new GarageNonIT();
        }

        public NIProductService(GarageNonIT context)
        {
            this.context = context;
        }



        public Areas.Non_IT.Models.NIProduct AddProduct(NIProductViewModel newProduct, int ProductStatus)
        {
            Areas.Non_IT.Models.NIProduct product = new Areas.Non_IT.Models.NIProduct();

            product.ProductName = newProduct.ProductName;
            product.Description = newProduct.Description;
            product.CategoryId = newProduct.CategoryId;
            product.SubCategoryId = newProduct.SubCategoryId;
            product.Quantity = newProduct.Quantity;
            product.InStock = newProduct.Quantity;
            product.UnitValue = newProduct.UnitValue;
            product.City = Areas.Non_IT.AppUtils.getCurrentUser().school.Location;
            product.Location = Areas.Non_IT.AppUtils.getCurrentUser().school.SchoolName;
            product.OriginalUnitValue = newProduct.OriginalUnitValue;
            product.DepreciationValue = newProduct.DepreciationValue;
            product.DateOfPurchase = newProduct.DateOfPurchase;
            product.Condition = (int)newProduct.Condition;
            product.ExpectedRealisable = newProduct.ExpectedRealisable;
            product.ActualValue = newProduct.ActualValue;
            product.ReasonForSelling = newProduct.ReasonForSelling;
            product.Reference = newProduct.Reference;
            product.DAXAssestItemTag = newProduct.DAXReference;
            product.Note = newProduct.Note;
            product.FinanceNote = newProduct.FinanceNote;
            product.MsoNote = newProduct.MsoNote;
            product.PrincipalNote = newProduct.PrincipalNote;
            product.AssetType = (int)newProduct.assetType;
            product.Status = ProductStatus;
            product.UserId = Areas.Non_IT.AppUtils.getCurrentUser().user.Id;
            product.SchoolId = Areas.Non_IT.AppUtils.getCurrentUser().school.Id;
            product.ViewCount = 0;
            product.CreatedBy = Areas.Non_IT.AppUtils.getCurrentUser().user.Id;
            product.CreatedDate = DateTime.Now;
            product.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
            product.UpdatedDate = DateTime.Now;
            product.Deleted = false;

            logger.Info("save product to DB");
            var savedProduct = context.NIProducts.Add(product);
            context.SaveChanges();
            logger.Info("saved product to DB");
            return savedProduct;
        }


        public Areas.Non_IT.Models.NIProduct AddProduct(NIProduct newProduct)
        {

            logger.Info("save product to DB");
            var savedProduct = context.NIProducts.Add(newProduct);
            context.SaveChanges();
            logger.Info("saved product to DB");
            return savedProduct;

        }
        public NIProduct UpdateProduct(NIProduct product)
        {
            logger.Info("update product to DB");
            var updatedProduct = context.NIProducts.Single(p => p.Id == product.Id);
            updatedProduct.Status = product.Status;
            updatedProduct.UpdatedDate = product.UpdatedDate;
            updatedProduct.UpdatedBy = product.UpdatedBy;
            updatedProduct.ProductName = updatedProduct.ProductName;
            updatedProduct.Description = product.Description;
            updatedProduct.CategoryId = product.CategoryId;
            updatedProduct.SubCategoryId = updatedProduct.SubCategoryId;
            updatedProduct.Quantity = product.Quantity;
            updatedProduct.InStock = product.InStock;
            updatedProduct.ViewCount = product.ViewCount;
            updatedProduct.DAXUpdated = product.DAXUpdated;
            if (product.DepreciationValue != null)
            {
                updatedProduct.DepreciationValue = product.DepreciationValue;
            }
            if (product.PurchaseValue != null)
            {
                updatedProduct.PurchaseValue = product.PurchaseValue;
            }
            if (product.UnitValue != null)
            {
                updatedProduct.UnitValue = product.UnitValue;
            }
            if (product.OriginalUnitValue != null)
            {
                updatedProduct.OriginalUnitValue = product.OriginalUnitValue;
            }
            if (product.DepreciationValue != null)
            {
                updatedProduct.DepreciationValue = product.DepreciationValue;
            }
            if (product.DateOfPurchase != null)
            {
                updatedProduct.DateOfPurchase = product.DateOfPurchase;
            }
            updatedProduct.Condition = (int)product.Condition;
            if (product.ExpectedRealisable != null)
            {
                updatedProduct.ExpectedRealisable = product.ExpectedRealisable;
            }
            if (product.ActualValue != null)
            {
                updatedProduct.ActualValue = product.ActualValue;
            }
            updatedProduct.ReasonForSelling = product.ReasonForSelling;
            if (product.Reference != null)
            {
                updatedProduct.Reference = product.Reference;
            }
            if (product.DAXAssestItemTag != null)
            {
                updatedProduct.DAXAssestItemTag = product.DAXAssestItemTag;
            }


            if (!string.IsNullOrEmpty(product.Note))
            {
                updatedProduct.Note = product.Note;
            }
            if (!string.IsNullOrEmpty(product.FinanceNote))
            {
                updatedProduct.FinanceNote = product.FinanceNote;
            }
            if (!string.IsNullOrEmpty(product.MsoNote))
            {
                updatedProduct.MsoNote = product.MsoNote;
            }
            if (!string.IsNullOrEmpty(product.PrincipalNote))
            {
                updatedProduct.PrincipalNote = product.PrincipalNote;
            }
            if (!string.IsNullOrEmpty(product.ApprovalRemarks))
            {
                updatedProduct.ApprovalRemarks = product.ApprovalRemarks;
            }

            if (product.ApprovalReq)
            {
                updatedProduct.ApprovalReq = product.ApprovalReq;
            }

            context.SaveChanges();
            logger.Info("updated product to DB");
            return updatedProduct;
        }
        public int UpdateNIProductFileByID(int ID, int ProductID)
        {
            var updateproduct = context.ProductFiles.Single(p => p.ID == ID);
            updateproduct.ProductID = ProductID;
            updateproduct.UpdatedDate = DateTime.Now;
            updateproduct.UpdatedBy = Non_IT.AppUtils.getCurrentUser().user.Id;
            return context.SaveChanges();
        }

        public NIProduct GetProductById(int id)
        {
            return context.NIProducts.Single(p => p.Id == id);

        }
        public List<NIProduct> GetPostedProducts()
        {
            var objuser = Areas.Non_IT.AppUtils.getCurrentUser();
            var result = context.NIProducts.Where(p => (p.Status == (int)ProductStatus.Posted || p.Status == (int)ProductStatus.Published || p.Status == (int)ProductStatus.Approved) && p.Deleted == false).OrderByDescending(p => p.CreatedDate).ToList();

            return result;
        }

        public List<NIProduct> GetAllProduct()
        {
            return context.NIProducts.ToList();
        }
        public IEnumerable<NIProduct> GetAllProducts()
        {
            return context.NIProducts.Where(p => p.Deleted == false).ToList();
        }
        public NIProduct DeleteProduct(int id)
        {
            logger.Info("delete product to DB");
            NIProduct deletedProduct = GetProductById(id);
            deletedProduct.Deleted = true;
            context.SaveChanges();
            logger.Info("deleted product to DB");
            return deletedProduct;
        }

        public IEnumerable<NIProduct> GetAllPublishedProducts()
        {
            return context.NIProducts.Where(p => p.Status == (int)ProductStatus.Published).ToList();
        }
        public IEnumerable<NISoldProduct> GetSoldProductsByOrderId(int id)
        {

            return context.NISoldProducts.Where(p => p.OrderId == id).ToList();
        }
        public NIProductOrder GetProductByOrderId(int id)
        {

            return context.NIProductOrders.Single(p => p.OrderId == id);
        }

        public NIProductOrder GetProductOrderById(int id)
        {

            return context.NIProductOrders.Single(p => p.Id == id);
        }
        public NISoldProduct GetSoldProductByProdOrderId(int prodOrderId)
        {
            return context.NISoldProducts.Where(p => p.ProductOrderId == prodOrderId).FirstOrDefault();

        }

        public NISoldProduct GetSoldProductById(int id)
        {

            return context.NISoldProducts.Where(p => p.Id == id).FirstOrDefault();
        }
    }
}