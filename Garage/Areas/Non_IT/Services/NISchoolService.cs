﻿using Garage.Models;
using Garage.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;
using System.ComponentModel.DataAnnotations;
using log4net;
using System.Reflection;
using Garage.Areas.Non_IT.Models;

namespace Garage.Areas.Non_IT.Service
{
    public class NISchoolService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private GarageNonIT context = null;

        public NISchoolService()
        {
            this.context = new GarageNonIT();
        }

        public NISchoolService(GarageNonIT context)
        {
            this.context = context;
        }

        public IEnumerable<NISchool> GetAllSchools()
        {

             return context.NISchools.Where(s => s.Deleted == false).ToList();
        }

        public NISchool GetSchoolById(int id) {
            if (id == 0)
                return null;
            return context.NISchools.Single(p => p.Id == id);
        }  
         

    }
}