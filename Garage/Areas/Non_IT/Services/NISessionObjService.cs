﻿using Garage.Models;
using Garage.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;
using System.ComponentModel.DataAnnotations;
using log4net;
using System.Reflection;
using Garage.Utils;
using Garage.Areas.Non_IT.Models;

namespace Garage.Areas.Non_IT.Service
{
    public class NISessionObjService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private GarageNonIT context = null;

        public NISessionObjService()
        {
            this.context = new GarageNonIT();
        }

        public NISessionObjService(GarageNonIT context)
        {
            this.context = context;
        }

        public IEnumerable<NISessionObj> GetAllsessionObjs()
        {
            return context.NISessionObjs.Where(s => s.Deleted == false).ToList();
        }

        public NISessionObj GetSessionObjById(int id) {

            return context.NISessionObjs.Single(s => s.Id == id);
        }  

        public IEnumerable<NISessionObj> GetSessionObjByUserId(int id)
        {
            return context.NISessionObjs.Where(s => s.UserId == id);
        }

        public NISessionObj AddSessionObj(NISessionObj sessionObj)
        {
            logger.Info("save sessionObj to DB");
            var savedSessionObj = context.NISessionObjs.Add(sessionObj);
            context.SaveChanges();
            logger.Info("saved sessionObj to DB");
            return savedSessionObj;
        }

        public IEnumerable<NISessionObj> AddSessionObjList(List<NISessionObj> categories)
        {
            logger.Info("save list of sessionObj to DB");
            var savedSessionObj = context.NISessionObjs.AddRange(categories);
            context.SaveChanges();
            logger.Info("saved list of categories to DB");
            return savedSessionObj;
        }

        public NISessionObj UpdateSessionObj(NISessionObj sessionObj)
        {
            logger.Info("update sessionObj to DB");
            var updatedSessionObj = context.NISessionObjs.Single(s => s.Id == sessionObj.Id);
            updatedSessionObj = sessionObj;
            context.SaveChanges();
            logger.Info("updated sessionObj to DB");
            return updatedSessionObj;
        }

        public NISessionObj DeleteSessionObj(NISessionObj sessionObj)
        {
            logger.Info("delete sessionObj to DB");
            var deletedSessionObj = context.NISessionObjs.Single(s => s.Id == sessionObj.Id);
            deletedSessionObj.Deleted = true;
            context.SaveChanges();
            logger.Info("deleted sessionObj to DB");
            return deletedSessionObj;
        }

        public void DeleteSessionObjByUserId(int userId)
        {
            logger.Info("delete sessionObj by user id to DB");
            List<NISessionObj> sessionObjList = context.NISessionObjs.Where(s => s.UserId == userId).ToList();
            if (sessionObjList != null && sessionObjList.Count > 0)
            {
                context.NISessionObjs.RemoveRange(sessionObjList);
                context.SaveChanges();
            }
            logger.Info("Delete Session Object by user Id to DB");
        }

        public void saveSessionObjectToDatabase()
        {
            DeleteSessionObjByUserId(Non_IT.AppUtils.getCurrentUser().user.Id);

            var cart = Non_IT.AppUtils.getCart();
            if (cart != null)
            {
                var cartSessionObj = cart.CartItems.ToList().ConvertAll(s =>
                {
                    return new NISessionObj()
                    {
                        UserId = Non_IT.AppUtils.getCurrentUser().user.Id,
                        ProductId = s.Value.Product.Id,
                        ProductImageId = s.Value.ProductImageId,
                        ProductQuantity = s.Value.ProductOrderQuantity,
                        ObjType = (int)AppEnumeration.SessionObjType.Cart,
                        CreatedBy = Non_IT.AppUtils.getCurrentUser().user.Id,
                        CreatedDate = DateTime.Now,
                        Deleted = false
                    };
                });
                AddSessionObjList(cartSessionObj);
            }

            var wishList = Non_IT.AppUtils.getWishList();
            if (wishList != null)
            {
                var wishListSessionObj = wishList.CartItems.ToList().ConvertAll(s =>
                {
                    return new NISessionObj()
                    {
                        UserId = Non_IT.AppUtils.getCurrentUser().user.Id,
                        ProductId = s.Value.Product.Id,
                        ProductImageId = s.Value.ProductImageId,
                        ProductQuantity = s.Value.ProductOrderQuantity,
                        ObjType = (int)AppEnumeration.SessionObjType.WishList,
                        CreatedBy = Non_IT.AppUtils.getCurrentUser().user.Id,
                        CreatedDate = DateTime.Now,
                        Deleted = false
                    };
                });
                AddSessionObjList(wishListSessionObj);
            }
        }

    }
}