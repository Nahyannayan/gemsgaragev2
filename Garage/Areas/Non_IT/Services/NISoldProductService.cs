﻿using Garage.Areas.Non_IT.Models;
using Garage.Models;
using Garage.Repository;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using static Garage.Utils.AppEnumeration;

namespace Garage.Areas.Non_IT.Service
{
    public class NISoldProductService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private GarageNonIT context = null;

        public NISoldProductService()
        {
            this.context = new GarageNonIT();
        }

        public NISoldProductService(GarageNonIT context)
        {
            this.context = context;
        }

        public IEnumerable<NISoldProduct> GetAllSoldProducts()
        {
            return context.NISoldProducts.Where(p => p.Deleted == false).ToList();
        }

        public NISoldProduct GetSoldProductById(int id)
        {
            return context.NISoldProducts.SingleOrDefault(p => p.Id == id);

        }

        public NISoldProduct GetSoldProductByProdOrderId(int prodOrderId)
        {

            return context.NISoldProducts.SingleOrDefault(p => p.ProductOrderId == prodOrderId);
        }

        public NISoldProduct AddSoldProduct(NISoldProduct soldProduct)
        {
            logger.Info("save soldProduct to DB");
            var savedSoldProduct = context.NISoldProducts.Add(soldProduct);
            context.SaveChanges();
            logger.Info("saved soldProduct to DB");
            return savedSoldProduct;
        }

        public IEnumerable<NISoldProduct> AddSoldProductList(List<NISoldProduct> soldProducts)
        {
            logger.Info("save list of soldProduct to DB");
            var savedSoldProduct = context.NISoldProducts.AddRange(soldProducts);
            context.SaveChanges();
            logger.Info("saved list of soldProduct to DB");
            return savedSoldProduct;
        }

        public NISoldProduct UpdateSoldProduct(NISoldProduct soldProduct)
        {
            logger.Info("update soldProduct to DB");
            var updatedSoldProduct = context.NISoldProducts.Single(p => p.Id == soldProduct.Id);
            updatedSoldProduct.ScheduledDate = soldProduct.ScheduledDate;
            updatedSoldProduct.DispatchDate = soldProduct.DispatchDate;
            updatedSoldProduct.Status = soldProduct.Status;
            context.SaveChanges();
            logger.Info("updated soldProduct to DB");
            return updatedSoldProduct;
        }

        public NISoldProduct DeleteSoldProduct(SoldProduct soldProduct)
        {
            logger.Info("delete soldProduct to DB");
            var deletedSoldProduct = context.NISoldProducts.Single(p => p.Id == soldProduct.Id);
            deletedSoldProduct.Deleted = true;
            context.SaveChanges();
            logger.Info("deleted soldProduct to DB");
            return deletedSoldProduct;
        }

        public IEnumerable<NISoldProduct> GetSoldProductsByOrderId(int id)
        {
            return context.NISoldProducts.Where(p => p.Id == id).ToList();
        }
    }
}