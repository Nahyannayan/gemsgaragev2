﻿using Garage.Models;
using Garage.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;
using System.ComponentModel.DataAnnotations;
using log4net;
using System.Reflection;
using Garage.Areas.Non_IT.Models;
using Garage.Utils;
using Garage.Areas.Non_IT.ViewModels;


namespace Garage.Areas.Non_IT.Services
{
    public class NIUserService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        GarageNonIT NonITcontext = null;

        public NIUserService()
        {
            this.NonITcontext = new GarageNonIT();
        }

        public NIUserService(GarageNonIT NonITcontext)
        {
            this.NonITcontext = NonITcontext;
        }


        public List<string> GetAllUsers(int schoolId, int userRole)
        {
            if (userRole == (int)AppEnumeration.NIAdminRole.GCOREALM)
            {
                return NonITcontext.NIUserRegistrations.Where(u => u.NIRole == userRole && (u.Role == null || u.Role.Value == 0)).Select(u => u.Email).ToList();
            }

            return NonITcontext.NIUserRegistrations.Where(u => u.School == schoolId && u.NIRole == userRole).Select(u => u.Email).ToList();
        }
        public Garage.Areas.Non_IT.Models.NIUserRegistration GetUserById(int id)
        {
            return NonITcontext.NIUserRegistrations.Single(u => u.Id == id);
        }

        public List<string> GetUserBySchoolAndRoleID(int schoolId, int RoleId)
        {
            return NonITcontext.NIUserRegistrations.Where(u => u.School.Value == schoolId && u.NIRole == RoleId).Select(u => u.Email).ToList();
        }

        public Garage.Areas.Non_IT.Models.NISchool GetSchoolById(int id)
        {
            return NonITcontext.NISchools.Single(u => u.Id == id);
        }

        public IEnumerable<Garage.Areas.Non_IT.Models.NIUserRegistration> GetAllUsers()
        {
            return NonITcontext.NIUserRegistrations.Where(u => u.Deleted == false).ToList();
        }
        public NIUserRegistration FindUserByEmail(string email)
        {
            return NonITcontext.NIUserRegistrations.Where(u => u.Email.Equals(email, StringComparison.OrdinalIgnoreCase) && u.Deleted == false).FirstOrDefault();
        }

        public List<string> GetAllEmailsBasedOnlyOnRole(int userRole)
        {
            return NonITcontext.NIUserRegistrations.Where(u => u.Deleted == false && u.NIRole == userRole && u.Deleted == false).Select(u => u.Email).ToList();
        }

        public Garage.Areas.Non_IT.Models.Role GetRoleByEmail(string Email)
        {
            var NIRole = NonITcontext.NIUserRegistrations.Single(u => u.Email == Email).NIRole;
            if (NIRole == null)
                return null;
            else if (NIRole.Value == 0)
                return null;
            return NonITcontext.Roles.Single(u => u.ID == NIRole);
        }
        public int? GetNextApprovalRoleID(int RoleID, string WorkFlow, bool IsProcurement = false, bool IsSameSchool = true)
        {
            return NonITcontext.Roles.Single(u => u.ROLEID == RoleID && u.IsSameSchool == IsSameSchool && u.IsProcurement == IsProcurement && (WorkFlow == "" || u.WORKFLOW.Trim() == WorkFlow.Trim())).NEXTAPPROVALROLEID;

        }
        public int GetPurchaseNextApprovalRoleID(int ID, bool IsProcurement = false, bool IsSameSchool = true)
        {
            if (ID == 0)
                return 0;
            return NonITcontext.PurchaseWFs.First(u => u.ID == ID && u.ISPROCUREMENT == IsProcurement).NextApprovalRoleID.Value;

        }
        public int GetPurchaseRoleByID(int ID)
        {
            return NonITcontext.PurchaseWFs.First(u => u.ID == ID).MasterRole.Value;
        }
        public string GetRoleIDByID(int RoleID)
        {
            if (RoleID == 0)
                return "";
            return NonITcontext.Roles_M.Single(u => u.ID == RoleID).RoleName;

        }
        public string GetMasterRoleIDByID(int RoleID)
        {
            if (RoleID == 0)
                return "";
            return NonITcontext.Roles_M.Single(u => u.RoleID == RoleID).RoleName;

        }
        public bool IsUserMSO(string Email)
        {
            var NIRole = NonITcontext.NIUserRegistrations.Single(u => u.Email == Email).NIRole;
            return (NIRole == 1 ? true : false);
        }
        public bool IsUserAssistanctMSO(string Email)
        {
            var NIRole = NonITcontext.NIUserRegistrations.Single(u => u.Email == Email).NIRole;
            return (NIRole == 5 ? true : false);
        }
        public List<Roles_M> GetAllNIMasterRoles()
        {
            return NonITcontext.Roles_M.ToList();

        }

        public int GetNextApproval(int AssetType, int CurrentRole)
        {
            return NonITcontext.Roles.Where(p => p.WORKFLOW == (AssetType == 1 ? "REUSE" : "DISPOSE") && p.ROLEID == CurrentRole).First().NEXTAPPROVALROLEID;
        }
        public NIUserRegistration UpdateUser(NIUserRegistration user)
        {
            logger.Info("update user to DB");

            //context.SaveChanges();

            var update = NonITcontext.NIUserRegistrations.Single(p => p.Id == user.Id);
            update.FullName = user.FullName;
            update.Active = user.Active;
            update.Email = user.Email;
            if (user.NIRole.HasValue && user.NIRole == 0)
                update.NIRole = null;
            else
            {
                update.NIRole = user.NIRole;
            }
            update.UpdatedDate = DateTime.Now;
            NonITcontext.SaveChanges();
            return update;


        }
        public NIUserRegistration DeleteUser(NIUserRegistration user)
        {
            logger.Info("delete user to DB");

            var update = NonITcontext.NIUserRegistrations.Single(p => p.Id == user.Id);
            update.Deleted = true;
            update.UpdatedDate = DateTime.Now;
            NonITcontext.SaveChanges();
            return update;
        }
        public NIUserRegistration AddUser(NIUserRegistration user)
        {
            logger.Info("save user to DB");
            var savedUserRegistrations = NonITcontext.NIUserRegistrations.Add(user);
            NonITcontext.SaveChanges();
            return savedUserRegistrations;
        }
        public Garage.Areas.Non_IT.Models.Role GetITRole(int Id)
        {
            return NonITcontext.Roles.Single(u => u.ID == Id);
        }
    }
}