﻿using Garage.Areas.Non_IT.Models;
using Garage.Areas.Non_IT.ViewModel;
using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.Areas.Non_IT.ViewModels.Email
{
    public class NIEmailNIOrderDetails
    {
        public NIProductDetail ProductDetail { get; set; }
        public NIProductOrder ProductOrder { get; set; }
        public string BuyerSchoolName { get; set; }
    }
}