﻿using Garage.Areas.Non_IT.ViewModel;
using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.Areas.Non_IT.ViewModels.Email
{
    public class NIEmailNIProductDetails
    {
        public NIProductDetail NIProductDetail { get; set; }
    }
}