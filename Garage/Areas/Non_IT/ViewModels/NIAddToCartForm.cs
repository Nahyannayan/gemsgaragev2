﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.Areas.Non_IT.ViewModels
{
    public class NIAddToCartForm
    {
        public int ProductId { get; set; }
        public int? UserQuantity { get; set; }
    }
}