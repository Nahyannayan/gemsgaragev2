﻿using Garage.Areas.Non_IT.Models;
using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;

namespace Garage.Areas.Non_IT.ViewModels
{
    public class NIApprovalRequest
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime PublishedDate { get; set; }
        public string NextApprover { get; set; } = string.Empty;
        public int NextApproverAsInt { get; set; } = 0;

        public NIProduct Product { get; set; }
        public Asset Asset { get; set; }
        public string SchoolCode { get; set; }
    }
}