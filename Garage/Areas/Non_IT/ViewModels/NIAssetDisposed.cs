﻿using Garage.Areas.Non_IT.Models;
using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.Areas.Non_IT.ViewModels
{
    public class NIAssetDisposed
    {
        public NIProduct Product { get; set; }
        public  NISchool School { get; set; }
        public DateTime PostedDate { get; set; }
        public DateTime DisposedDate { get; set; }
    }
}