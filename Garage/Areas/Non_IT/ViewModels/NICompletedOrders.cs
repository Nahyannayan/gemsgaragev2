﻿using Garage.Areas.Non_IT.Models;
using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.Areas.Non_IT.ViewModels
{
    public class NICompletedOrders
    {
        public NIOrder Order { get; set; }
        public NIProductOrder ProductOrder { get; set; }
        public NIProduct Product { get; set; }
        public DateTime PurchaseDate { get; set; }
        public DateTime ScheduledDate { get; set; }
        public NIOrderVouchersDetail OrderVouchersDetail { get; set; }
    }
}