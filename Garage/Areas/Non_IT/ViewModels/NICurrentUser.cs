﻿using Garage.Areas.Non_IT.Models;
using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.Areas.Non_IT.ViewModels
{
    public class NICurrentUser
    {
        public NIUserRegistration user { get; set; }
        public NISchool school { get; set; }
        public bool isNISuperAdmin { get; set; }
        public bool HasGemsAdminRole { get; set; }
    }
}