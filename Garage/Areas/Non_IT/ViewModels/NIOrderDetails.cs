﻿using Garage.Areas.Non_IT.Models;
using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.Areas.Non_IT.ViewModel
{
    public class NIOrderDetails
    {
        public NIOrder Order { get; set; }
        public NIProductOrder ProductOrder { get; set; }
        public NIProduct Product { get; set; }
        public string NextApprover { get; set; }
        public int NextApproverRole { get; set; }
        public DateTime? PickUpScheduled { get; set; }
        public bool IsSameSchool { get; set; }
        public int LastUpdateUserID { get; set; }
        public bool LastApproval { get; set; }
        public string SchoolCode { get; set; }
        public NIOrderVouchersDetail NIOrderProductVoucher { get; set; }
    }
}