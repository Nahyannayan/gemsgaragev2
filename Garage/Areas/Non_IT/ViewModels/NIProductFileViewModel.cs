﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Garage.Areas.Non_IT.ViewModels
{
    public class NIProductFileViewModel
    {
        public int Id { get; set; }
        public string ImageName { get; set; }
        public string Extension { get; set; }
        public int ProductId { get; set; }
        public HttpPostedFileBase[] ImageFile { get; set; }
        public System.String ReferenceFiles { get; set; }
        public string CreatedBy { get; set; }
    }  
}