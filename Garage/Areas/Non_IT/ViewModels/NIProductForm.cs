﻿
using System.Collections.Generic;
using System.Web;
using static Garage.Utils.AppEnumeration;

namespace Garage.Areas.Non_IT.ViewModels
{
    public class NIProductForm
    {
        public Garage.Areas.Non_IT.ViewModels.NIProductViewModel productViewModel { get; set; }
        public Garage.Areas.Non_IT.ViewModels.NIProductFileViewModel productFileViewModel { get; set; }
        public List<int> imageIdList { get; set; } 

        public string Remarks { get; set; }
    }

}