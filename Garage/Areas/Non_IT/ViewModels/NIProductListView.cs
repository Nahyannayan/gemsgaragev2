﻿using Garage.Areas.Non_IT.Models;
using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.Areas.Non_IT.ViewModels
{
    public class NIProductListView
    {
        public NIProduct Product { get; set; }
        public List<int> imageIdList { get; set; }
        public string SchoolCode { get; set; }
        public DateTime? PublishedDate { get; set; }
    }
}