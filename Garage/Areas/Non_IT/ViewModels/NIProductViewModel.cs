﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;
using static Garage.Utils.AppEnumeration;

namespace Garage.Areas.Non_IT.ViewModels
{
    public class NIProductViewModel
    {
        public int Id { get; set; }
        [Required]
        public string ProductName { get; set; }
        public string Description { get; set; }
        public int? CategoryId { get; set; }
        public int? SubCategoryId { get; set; }
        public int? Quantity { get; set; }
        public decimal? InStock { get; set; }
        public decimal? UnitValue { get; set; }
        public string City { get; set; }
        public string Location { get; set; }
        public decimal? OriginalUnitValue { get; set; }
        public decimal? DepreciationValue { get; set; }
        public DateTime? DateOfPurchase { get; set; }
        public int Condition { get; set; }
        public decimal? ExpectedRealisable { get; set; }
        public decimal? ActualValue { get; set; }
        public decimal? PurchaseValue { get; set; }
        public string ReasonForSelling { get; set; }
        public string ReasonForReject { get; set; }
        public string Reference { get; set; }
        public string DAXReference { get; set; }
        public string Note { get; set; }
        public string FinanceNote { get; set; }
        public string MsoNote { get; set; }
        public string PrincipalNote { get; set; }
        public int assetType { get; set; }
        public string Status { get; set; }
        public string PublishedBy { get; set; }
        public bool ApprovalReq { get; set; }
        public bool IsDAXUpdate { get; set; }
        public string ApprovalRemarks { get; set; }
        

    }
}