﻿using Garage.Areas.Non_IT.Models;
using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.Areas.Non_IT.ViewModels
{
    public class NISchoolCartItem
    {
        public NIProduct Product { get; set; }
        public List<int> ImageIdList { get; set; }
        public int OrderQuantity { get; set; }
        public decimal OrderValue { get; set; }
        public string IctName { get; set; }
    }
}