﻿using Garage.Areas.Non_IT.Models;
using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.Areas.Non_IT.ViewModels
{
    public class NISoldProductDetail
    {
        public NISoldProduct SoldProduct { get; set; }
        public NIProduct Product { get; set; }
        public NIProductOrder ProductOrder { get; set; }
        public NISchool School { get; set; }
        public NISchool SoldSchool { get; set; }
        public DateTime? OrderApprovedDate { get; set; }
        public DateTime? PublishedDate { get; set; }
        public DateTime? PickUpDate { get; set; }
        public List<Areas.Non_IT.Models.ProductFile> ProductFile { get; set; }
    }
}