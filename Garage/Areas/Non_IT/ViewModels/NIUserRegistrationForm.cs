﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.Areas.Non_IT.ViewModels
{
    public class NIUserRegistrationForm
    {
        public int Id { get; set; }
        public string EncryptedId { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public int School { get; set; }
        public int Role { get; set; }
        public int NIRole { get; set; }
        public bool Active { get; set; }
        public bool Deleted { get; set; }
    }
}