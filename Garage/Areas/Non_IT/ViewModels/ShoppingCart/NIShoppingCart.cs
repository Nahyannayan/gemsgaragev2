﻿using Garage.Areas.Non_IT.Models;
using Garage.Areas.Non_IT.Service;
using Garage.Areas.Non_IT.Services;
using Garage.Models;
using Garage.Service;
using Garage.Utils;
using Garage.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;

namespace Garage.Areas.Non_IT.ViewModels.ShoppingCart
{
    public class NIShoppingCart
    {
        public Dictionary<int, NIShoppingCartDetails> CartItems { get; set; }

        private readonly NIProductService NIproductService = new NIProductService();
        private readonly NIFileService productImageService = new NIFileService();

        public NIShoppingCart()
        {
            this.CartItems = new Dictionary<int, NIShoppingCartDetails>();
        }

        public class NIShoppingCartDetails
        {
            public NIProduct Product { get; set; }
            public int ProductOrderQuantity { get; set; }
            public int ProductImageId { get; set; }
        }

        public NIShoppingCart AddToCart(AddToCartForm addToCartForm)
        {
            if (this.CartItems.ContainsKey(addToCartForm.ProductId)) { this.CartItems.Remove(addToCartForm.ProductId); }
            var product = NIproductService.GetProductById(addToCartForm.ProductId);
            var ProductImageId = productImageService.GetSingleProductImageByProductId(product.Id).ID;
            var ProductOrderQuantity = addToCartForm.UserQuantity ?? 0;
            var cartItem = new NIShoppingCartDetails()
            {
                Product = product,
                ProductOrderQuantity = ProductOrderQuantity,
                ProductImageId = ProductImageId
            };
            this.CartItems.Add(product.Id, cartItem);
            return this;
        }

        public NIShoppingCart UpdateProductFromSession(NIShoppingCart cart)
        {
            var listOfProductId = cart.CartItems.Select(p => p.Key).ToList();
            var productList = NIproductService.GetAllProduct().Where(p => listOfProductId.Contains(p.Id)).ToList();
            productList.ForEach(p =>
            {
                var shoppingCart = cart.CartItems[p.Id];
                shoppingCart.Product = p;
            });
            return cart;
        }

        public NIShoppingCart RemoveFromCart(int productId)
        {
            if (this.CartItems.ContainsKey(productId)) { this.CartItems.Remove(productId); }
            return this;
        }

        public List<Garage.Areas.Non_IT.Models.NISessionObj> AddCartToDb(NIShoppingCart shoppingCart)
        {
            var cartItems = shoppingCart.CartItems.Values.ToList();
            return cartItems.ConvertAll(c => new NISessionObj()
            {
                UserId = Non_IT.AppUtils.getCurrentUser().user.Id,
                ProductId = c.Product.Id,
                ProductImageId = c.ProductImageId,
                ProductQuantity = c.ProductOrderQuantity,
                ObjType = (int)AppEnumeration.SessionObjType.Cart,
                CreatedBy = Non_IT.AppUtils.getCurrentUser().user.Id,
                CreatedDate = DateTime.Now,
                Deleted = false
            });
        }
    }
}