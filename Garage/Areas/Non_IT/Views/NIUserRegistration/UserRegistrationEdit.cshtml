﻿@using Garage.Utils
@model Garage.Areas.Non_IT.ViewModels.NIUserRegistrationForm
@{
    Layout = "~/Views/Shared/_AdminLayout.cshtml";
    var currentUser = Garage.Areas.Non_IT.AppUtils.getCurrentUser();
}
@section header {
    @if (currentUser.user.Role == (int)AppEnumeration.Role.CorpIT)
    {
        @RenderPage("~/Areas/Non_IT/Views/Shared/_Header.cshtml")}
    else
    {
        @RenderPage("~/Areas/Non_IT/Views/Shared/_HeaderAdmin.cshtml")
    }
}
@section sidemenu {
    @if ( currentUser.user.Role == (int)AppEnumeration.Role.CorpIT)
    {
        @RenderPage("~/Areas/Non_IT/Views/Shared/_SideMenuSchoolAdmins.cshtml")}
    else
    {
        @RenderPage("~/Areas/Non_IT/Views/Shared/_SideMenuAdmin.cshtml")
    }
}
@{
    ViewBag.Title = "User Edit";
}
<div class="page_content">
    <div class="title_name category_title_top">User Registration</div>
    <div class="category_fillter_top">
        <div class="padding_left">
            <a href="@Url.Action("List", "NIUserRegistration")" class="fillter_button">
                <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                Back
            </a>
        </div>
    </div>
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header mytheme_header">
                User Registration Form
                @*<div class="right_header">
                        <a class="wish_list" href="#">Move to wish list</a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>*@
            </div>
            @using (Html.BeginForm("Update", "NIUserRegistration", FormMethod.Post, new { role = "form", id = "userEditForm" }))
            {
                @Html.AntiForgeryToken()

                <!-- Modal body -->
                <div class="modal-body mytheme_body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="post_product_field1">
                                <tr>
                                    <td colspan="2">
                                        @Html.HiddenFor(m => m.EncryptedId)
                                        @Html.LabelFor(m => m.UserId, "User ID")
                                        @Html.TextBoxFor(m => m.UserId, new { @class = "popup_textBox" })
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        @Html.LabelFor(m => m.FullName, "Full Name")
                                        @Html.TextBoxFor(m => m.FullName, new { @class = "popup_textBox" })
                                    </td>
                                    <td>
                                        @Html.LabelFor(m => m.Email, "Email")
                                        @Html.TextBoxFor(m => m.Email, new { @class = "popup_textBox" })
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        @{ var schoolList = (List<SelectListItem>)ViewBag.SchoolList; }
                                        @Html.LabelFor(m => m.School, "School")
                                        @Html.DropDownListFor(m => m.School, schoolList, "Select School", new { @class = "popup_textBox", id = "school" })
                                    </td>
                                    <td>
                                        @{ var rolesList = (List<SelectListItem>)ViewBag.Roles; }
                                        @Html.LabelFor(m => m.NIRole, "Role")
                                        @Html.DropDownListFor(m => m.NIRole, rolesList, "Select Role", new { @class = "popup_textBox" })
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        @Html.CheckBoxFor(m => m.Active) Active
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer mytheme_footer">
                    <ul class="product_post_button">
                        <li><button type="submit" class="btn btn-primary">Update User</button></li>
                    </ul>
                </div>
            }
        </div>
    </div>
</div>
@section footerscript {
    <script src="~/Scripts/js/jquery.validate.min.js"></script>
    <script src="~/Scripts/js/additional-methods.min.js"></script>
    <script type="text/javascript">

        var adminRoleValues = JSON.parse('@Json.Encode(Enum.GetValues(typeof(AppEnumeration.AdminRole)))');

        $("#userEditForm").validate({
            ignore: 'input[type=hidden]', // ignore hidden fields
            errorClass: 'text-danger',
            successClass: 'success',
            highlight: function (element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function (element, errorClass) {
                $(element).removeClass(errorClass);
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            rules: {
                "UserId": {
                    required: true,
                    maxlength: 200
                },
                "FullName": {
                    required: true,
                    maxlength: 50
                },
                "Email": {
                    required: true,
                    maxlength: 100,
                    email: true
                },
                "School": {
                    required: () => !adminRoleValues.includes(Number($('#Role').val()))
                },
                "Role": {
                    required: true
                }
            },
            messages: {
                "UserId": {
                    required: "Please Enter User Id",
                    maxlength: "Please Enter no more than 200 Characters"
                },
                "FullName": {
                    required: "Please Enter Full Name",
                    maxlength: "Please Enter Less than 50 Characters"
                },
                "Email": {
                    required: "Please Enter Email",
                    maxlength: "Please Enter Less than 100 Characters",
                    email: "Please Enter Valid Email"
                },
                "School": {
                    required: "Please Select School"
                },
                "Role": {
                    required: "Please Select Role"
                }
            }
        });

        $('#userEditForm').submit(function () {
            if ($('#userEditForm').valid()) {
                $('#cover-spin').show();
            }
        });


    </script>
}
