function getResponsiveBreakpoint() {
    var envs = ["xs", "sm", "md", "lg", "xl"];
    var env = "";

    var $el = $("<span>");
    $el.appendTo($("body"));

    for (var i = envs.length - 1; i >= 0; i--) {
        env = envs[i];
        $el.addClass("d-" + env + "-none");
        if ($el.is(":hidden")) {
            break;
        }
    }
    $el.remove();
    return env;
}

$(document).ready(function () {

    var env = getResponsiveBreakpoint();
    var sidebar_cookie = Cookies.get('sidebar_cookie');
    if (env === "xs" || env === "sm" || sidebar_cookie) {
        $('#sidebar, #content').toggleClass('active');
   }
    // sidebar menu
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar, #content').toggleClass('active');
        if (sidebar_cookie === undefined || sidebar_cookie === '') {
            Cookies.set('sidebar_cookie', true, { expires: 1 });
        } else {
            Cookies.remove('sidebar_cookie');
        }
  });


    $("a.sidemenu_hover").mouseover(function () {
        $(this).parent('li').addClass("open");
    });

    $("a.sidemenu_hover").mouseout(function () {
        $(this).parent('li').removeClass("open");
    });


// sidebar menu active
$('ul.main_menu li a').click(function () {
  $('.main_menu').find('li.active').removeClass('active');
  $(this).parents("li").addClass('active');
});


// Alert Message

  $("#info-alert").hide();
  $("#myWish").click(function showAlert() {
    $("#info-alert").fadeTo(2000, 500).slideUp(500, function() {
      $("#info-alert").slideUp(500);
    });
  });


});