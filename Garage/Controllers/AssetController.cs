﻿using Garage.Models;
using Garage.Service;
using Garage.ViewModels;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using static Garage.Utils.Mapper.AssetMapper;
using static Garage.Utils.Mapper.AssetTrackMapper;
using static Garage.Utils.Mapper.AssetImageMapper;
using static Garage.Utils.AppEnumeration;
using Garage.Utils;
using Garage.Utils.Security;

namespace Garage.Controllers
{
    public class AssetController : Controller
    {
        //comment to test
        readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly CategoryService categoryService = new CategoryService();
        private readonly AssetService assetService = new AssetService();
        private readonly ProductService productService = new ProductService();
        private readonly ProductImageService productImageService = new ProductImageService();
        private readonly AssetTrackService assetTrackService = new AssetTrackService();
        private readonly AssetImageService assetImageService = new AssetImageService();

        ////[AppAuthorize]
        //[HttpGet, ActionName("DraftedAssets")]
        //public ActionResult DraftedAssets()
        //{

        //    //ViewBag.DraftedAssets = getDraftedAssets();
        //    return View();
        //}

        ////[AppAuthorize]
        //[HttpGet, ActionName("PostedAssets")]
        //public ActionResult PostedAssets()
        //{
        //    if (AppUtils.getCurrentUser().isSuperAdmin)
        //    {
        //        ViewBag.PostedAssets = getAllPostedAssets();
        //        return View("CorporatePostedAssets");
        //    }
        //    else
        //    {
        //        //ViewBag.PostedAssets = getPostedAssets();
        //    }
        //    return View();
        //}

        ////[AppAuthorize]
        //[HttpGet, ActionName("AssetApproval")]
        //public ActionResult AssetApproval(int id)
        //{
        //    logger.Info("Asset Controller - Viewing the asset detail for Approval");

        //    Asset asset = assetService.GetAssetById(id);
        //    ViewBag.Asset = asset;
        //    AssetForm assetForm = remap(asset);
        //    ViewBag.ParentCategoryList = getCategorySelectList(getParentCategory());
        //    ViewBag.SubCategoryList = getCategorySelectList(getSubCategory());
        //    var assetTrack = assetTrackService.GetPostTrackByAssetId(asset.Id).OrderByDescending(p => p.CreatedDate).FirstOrDefault();
        //    ViewBag.AssetTrack = assetTrack;
        //    return View(assetForm);
        //}

        ////[AppAuthorize]
        //[HttpPost, ActionName("AssetApproval")]
        //public ActionResult Approval(AssetForm assetForm)
        //{
        //    logger.Info("Asset Controller - Saving the Approval Status for the asset");

        //    //Asset approvedAsset;

        //    Asset asset = assetService.GetAssetById(assetForm.Id);
        //    Asset updatedAsset = map(assetForm, asset);

        //    if ((Role)AppUtils.getCurrentUser().user.Role == Role.Principal)
        //    {
        //        //approvedAsset = assetService.UpdateAsset(mapApproveStatus(updatedAsset));
        //        //TODO: Change the asset Approval template
        //        //List<string> toEmailIds = getAllEmailsBasedOnRole((int)UserRole.ICTEngineer);
        //        //var corporateMailIds = getAllCorporateITEmails();
        //        //toEmailIds.AddRange(corporateMailIds);

        //        //var emailModel = new ProductApproval()
        //        //{
        //        //    ProductDetail = viewMap(updatedProduct),
        //        //    ProductImage = assetImageService.GetSingleProductImageByProductId(updatedProduct.Id)
        //        //};
        //        //var emailbody = EmailFactory.ParseTemplate<ProductApproval>(emailModel, EmailType.ProductPublished);
        //        //emailService.send(toEmailIds, "Product Published", emailbody);
        //    }
        //    else
        //    {
        //          //  approvedAsset = assetService.UpdateAsset(mapPostStatus(updatedAsset));
        //    }

        //    if ((Role)AppUtils.getCurrentUser().user.Role == (Role.CorpInternalAudit) || (Role)AppUtils.getCurrentUser().user.Role == (Role.SVPTreasury) || (Role)AppUtils.getCurrentUser().user.Role == (Role.CFO))
        //    {
        //        //var userRole = AppUtils.getCurrentUser().user.Role + 1;
        //        //var emailModel = new ProductApproval()
        //        //{
        //        //    ProductDetail = viewMap(updatedProduct),
        //        //    ProductImage = assetImageService.GetSingleProductImageByProductId(updatedProduct.Id)
        //        //};
        //        //var emailbody = EmailFactory.ParseTemplate<ProductApproval>(emailModel, EmailType.ProductApproval);
        //        //emailService.send(getAllEmailsBasedOnRole(userRole ?? 0), "Review New Product", emailbody);
        //        //approvedAsset = assetService.UpdateAsset(mapDisposedStatus(updatedAsset));
        //    }
        //    //assetTrackService.AddAssetTrack(assetTrackmap(approvedAsset));
        //    return RedirectToAction("PostedAssets", "Asset");
        //}


        ////[AppAuthorize]
        //[HttpGet, ActionName("Add")]
        //public ActionResult AddAsset()
        //{
        //    logger.Info("Asset Controller - Adding New Asset");
        //    ViewBag.ParentCategoryList = getCategorySelectList(getParentCategory());
        //    ViewBag.SubCategoryList = getCategorySelectList(getSubCategory());
        //    return View("AssetAdd", new AssetForm());
        //}

        ////[AppAuthorize]
        //[HttpPost, ActionName("Add")]
        //public ActionResult SaveAsset(AssetForm assetForm)
        //{
        //    logger.Info("Asset Controller - Saving Asset to the database");
        //    Asset savedAsset = assetService.AddAsset(map(assetForm));
        //    assetForm.Id = savedAsset.Id;
        //    assetImageService.AddAssetImageList(assetImageMapper(assetForm));
        //    ViewBag.SavedAsset = savedAsset;
        //    TempData["productName"] = savedAsset.ProductName;
        //    return RedirectToAction("Draft", "SchoolAdmin");
        //}


        ////[AppAuthorize]
        //[HttpGet, ActionName("Edit")]
        //public ActionResult EditAsset(int id)
        //{
        //    logger.Info("Asset Controller - Edit Individual Asset from database");

        //    Asset asset = assetService.GetAssetById(id);
        //    ViewBag.Product = asset;
        //    AssetForm assetForm = remap(asset);
        //    ViewBag.ParentCategoryList = getCategorySelectList(getParentCategory());
        //    ViewBag.CurrentSubCategoryList = getCategorySelectList(getCurrentSubCategory(assetForm.CategoryId??0));
        //    ViewBag.SubCategoryList = getCategorySelectList(getSubCategory());
        //    return View("AssetEdit", assetForm);
        //}

        ////[AppAuthorize]
        //[HttpPost, ActionName("Update")]
        //public ActionResult UpdateAsset(AssetForm assetForm)
        //{
        //    logger.Info("Asset Controller - Updating the exsisting Asset");

        //    Asset asset = assetService.GetAssetById(assetForm.Id);
        //    var updatedAsset = assetService.UpdateAsset(map(assetForm, asset));
        //    TempData["productName"] = updatedAsset.ProductName;
        //    //if (asset.Status == (int)AssetStatus.Posted)
        //    //{
        //    //    return RedirectToAction("ApprovalRequest", "SchoolAdmin");
        //    //}
        //    return RedirectToAction("Draft", "SchoolAdmin");
        //}

        ////[AppAuthorize]
        //[HttpPost, ActionName("Post")]
        //public ActionResult PostAsset(AssetForm assetForm, bool? isNewAsset)
        //{
        //    logger.Info("Asset Controller - Saving the Asset in the database As Post status");
        //    Asset postAsset;
        //    if (isNewAsset ?? false)
        //    {
        //        Asset newAsset = map(assetForm);
        //        //mapPostStatus(newAsset);
        //        Asset savedAsset = assetService.AddAsset(newAsset);
        //        assetForm.Id = savedAsset.Id;
        //        assetImageService.AddAssetImageList(assetImageMapper(assetForm));
        //        postAsset = savedAsset;
        //    }
        //    else
        //    {
        //        Asset asset = assetService.GetAssetById(assetForm.Id);
        //        Asset postingAssets = map(assetForm, asset);
        //        //postAsset = assetService.UpdateAsset(mapPostStatus(postingAssets));
        //    }
        //    //assetTrackService.AddAssetTrack(assetTrackmap(postAsset));
        //    //TempData["productName"] = postAsset.ProductName;
        //    return RedirectToAction("ApprovalRequest", "SchoolAdmin");
        //}

        ////[AppAuthorize]
        //[HttpPost, ActionName("Reuse")]
        //public ActionResult ReUseAsset(AssetForm assetForm)
        //{
        //    logger.Info("Asset Controller - Reuse Asset");
        //    Asset reusableAsset = assetService.GetAssetById(assetForm.Id);
        //    AssetImage assetImage = assetImageService.GetSingleAssetImageByAssetId(reusableAsset.Id);
        //    Product savedProduct = productService.AddProduct(assetReusableToProduct(reusableAsset));
        //    productImageService.AddProductImage(assetImageReusableToProductImage(assetImage, savedProduct));
        //    return Approval(assetForm);
        //}

        //[AppAuthorize]
        [HttpGet, ActionName("Reject")]
        public ActionResult RejectAsset(int id)
        {
            logger.Info("Asset Controller - Rejecting the asset");

            //Asset rejectedAsset;
            Asset asset = assetService.GetAssetById(id);
            //rejectedAsset = assetService.UpdateAsset(mapRejectStatus(asset));
            return RedirectToAction("PostedAssets", "Asset");
        }

        //[AppAuthorize]
        [HttpGet, ActionName("View")]
        public ActionResult AssetView(int id)
        {
            logger.Info("Asset Controller - Viewing Individual asset");
            Asset asset = assetService.GetAssetById(id);
            AssetForm assetForm = viewMap(asset);
            ViewBag.ParentCategoryList = getCategorySelectList(getParentCategory());
            ViewBag.SubCategoryList = getCategorySelectList(getSubCategory());
            return View("AssetView", assetForm);
        }

        //[AppAuthorize]
        [HttpGet, ActionName("Track")]
        public ActionResult AssetTrack(int id)
        {
            logger.Info("Asset Controller - Viewing Prodcut track in modal");
            ViewBag.Asset = assetService.GetAssetById(id);
            ViewBag.AssetTrack = assetTrackService.GetPostTrackByAssetId(id);
            return PartialView("AssetTrack");
        }

        ////[AppAuthorize]
        //[HttpGet, ActionName("Delete")]
        //public ActionResult DeleteAsset(int id)
        //{
        //    logger.Info("Asset Controller - Delete Individual asset from Database");

        //    Asset asset = assetService.GetAssetById(id);
        //    assetService.DeleteAsset(asset);
        //    return RedirectToAction("Draft", "SchoolAdmin");
        //}


        #region Help Methods

        private List<Category> getParentCategory() => categoryService.GetAllCategories().Where(p => p.ParentCategoryId == null || p.ParentCategoryId == 0 && p.Active == Convert.ToBoolean(YNStatue.Yes)).ToList();

        private List<Category> getSubCategory() => categoryService.GetAllCategories().Where(p => p.ParentCategoryId != null && p.ParentCategoryId != 0).ToList();

        private List<Category> getCurrentSubCategory(int parentCategoryId) => categoryService.GetAllCategories().Where(p => p.ParentCategoryId == parentCategoryId).ToList();

        private List<SelectListItem> getCategorySelectList(List<Category> categories) => categories.ConvertAll(c => new SelectListItem() { Text = c.CategoryName.ToString(), Value = c.Id.ToString(), Selected = false });

        //private List<Asset> getDraftedAssets() => assetService.GetAllAssets().Where(p => p.Status == (int)AssetStatus.SavedAsDraft && p.UserId == AppUtils.getCurrentUser().user.Id && p.Deleted == Convert.ToBoolean(YNStatue.No)).OrderByDescending(p => p.CreatedDate).ToList();

        //private List<Asset> getPostedAssets() => assetService.GetAllAssets().Where(p => p.Status != (int)AssetStatus.SavedAsDraft && p.SchoolId == AppUtils.getCurrentUser().school.Id && p.Deleted == Convert.ToBoolean(YNStatue.No)).OrderByDescending(p => p.CreatedDate).ToList();

        private List<Asset> getAllPostedAssets() => assetService.GetAllAssets().Where(p => p.Deleted == Convert.ToBoolean(YNStatue.No)).OrderByDescending(p => p.CreatedDate).ToList();

        #endregion

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            logger.Error(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "ErrorHandler");
        }
    }
}