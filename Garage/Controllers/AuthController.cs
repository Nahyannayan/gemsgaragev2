﻿using Garage.Models;
using Garage.Service;
using Garage.Utils;
using Garage.ViewModels;
using Garage.ViewModels.ShoppingCart;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using static Garage.Utils.AppEnumeration;
using static Garage.ViewModels.ShoppingCart.ShoppingCart;

namespace Garage.Controllers
{
    public class AuthController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly UserService userService = new UserService();
        private readonly ProductService productService = new ProductService();
        private readonly SessionObjService sessionObjService = new SessionObjService();
        private readonly SchoolService schoolService = new SchoolService();

        // GET: Login
        public ActionResult Index()
        {
            logger.Info("Login Controller - Open New Login Form");
            return View(new LoginForm());
        }


        #region Active Directory Auth Methods

        [AllowAnonymous]
        [HttpGet, Route("~/logout")]
        public ActionResult logout()
        {
            logger.Info("Auth Controller - After Logout");
            return RedirectToAction("Index", "Landing");
        }

        [HttpGet, ActionName("SignIn")]
        public void SignIn()
        {
            if (!Request.IsAuthenticated)
            {
                HttpContext.GetOwinContext().Authentication.Challenge(new AuthenticationProperties { RedirectUri = "/login" }, OpenIdConnectAuthenticationDefaults.AuthenticationType);
            }
        }

        public void SignOut()
        {
            HttpContext.GetOwinContext().Authentication.SignOut(
                OpenIdConnectAuthenticationDefaults.AuthenticationType, CookieAuthenticationDefaults.AuthenticationType);
        }




        #endregion

        //[AllowAnonymous]
        //[HttpGet, Route("~/login")]
        //public ActionResult Auth()
        //{
        //    logger.Info("Login Controller - Submitting Login Form for Auth");
        //    var claims = ClaimsPrincipal.Current.Claims;
        //     var email = claims.Where(c => c.Type == "preferred_username").Select(c => c.Value).SingleOrDefault();
        //  //  var email = "nahyan.nayan@gemseducation.com";

        //    var loggedInUser = userService.FindUserByEmail(email);

        //    if (loggedInUser != null)
        //    {
        //        CurrentUser currentUser = new CurrentUser();
        //        currentUser.user = loggedInUser;
        //        currentUser.school = schoolService.GetSchoolById(loggedInUser.School ?? 0);
        //        currentUser.isSuperAdmin = Enum.IsDefined(typeof(AdminRole), loggedInUser.Role);

        //        IdentitySignin(currentUser);
        //        getSessionObjByUser(loggedInUser);
        //    }
        //    else
        //    {
        //        IdentitySignin(null);
        //    }

        //    return RedirectToAction("HomePage", "NILanding");
        //}

        [HttpGet, ActionName("Logout")]
        public void Logout()
        {
            if (AppUtils.getCurrentUser() != null)
            {
                logger.Info("Login Controller - Log out the current user");
                saveSessionObjectBeforeSignOut();
                if (AppUtils.getCurrentUser() != null) { Session.Remove("CurrentUser"); }
                if (Session["Cart"] != null) { Session.Remove("Cart"); }
                if (Session["WishList"] != null) { Session.Remove("WishList"); }
            }
            SignOut();
        }

        #region Auth Methods

        public void IdentitySignin(CurrentUser currentUser, string providerKey = null, bool isPersistent = true)
        {
            logger.Info("Login Controller - Identity Sign In");
            var roleName = string.Empty;
            var currentUserJson = string.Empty;
            if (currentUser != null)
            {
                roleName = EnumHelper<Role>.GetDisplayValue((Role)currentUser.user.Role);
                currentUserJson = JsonConvert.SerializeObject(currentUser);
            }
            else
            {
                roleName = AppUtils.visitorRole;
            }
            var identity = User.Identity as ClaimsIdentity;
            identity.AddClaim(new Claim(ClaimTypes.Role, roleName));
            identity.AddClaim(new Claim("CurrentUser", currentUserJson));
            AuthenticationManager.AuthenticationResponseGrant = new AuthenticationResponseGrant(new ClaimsPrincipal(identity), new AuthenticationProperties()
            {
                AllowRefresh = true,
                IsPersistent = isPersistent,
                ExpiresUtc = DateTime.UtcNow.AddDays(1)
            });
        }

        //private void IdentitySignout()
        //{
        //    logger.Info("Login Controller - Identity Sign Out");
        //    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie,
        //                                    DefaultAuthenticationTypes.ExternalCookie);
        //}

        private IAuthenticationManager AuthenticationManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        #endregion

        #region Help Methods

        private void getSessionObjByUser(UserRegistration user)
        {
            var sessionObj = sessionObjService.GetSessionObjByUserId(user.Id).ToList();
            if (sessionObj != null && sessionObj.Count > 0)
            {
                var cartSessionObj = sessionObj.Where(s => s.ObjType == (int)SessionObjType.Cart).ToList();
                if (cartSessionObj != null)
                {
                    var cartItems = cartSessionObj.ConvertAll(c => new ShoppingCartDetails()
                    {
                        Product = productService.GetProductById(c.ProductId ?? 0),
                        ProductOrderQuantity = c.ProductQuantity ?? 0,
                        ProductImageId = c.ProductImageId ?? 0

                    }).ToDictionary(s => s.Product.Id, s => s);

                    Session["Cart"] = new ShoppingCart() { CartItems = cartItems };
                }

                var wishListSessionObj = sessionObj.Where(s => s.ObjType == (int)SessionObjType.WishList).ToList();
                if (wishListSessionObj != null)
                {
                    var wishListItems = wishListSessionObj.ConvertAll(c => new ShoppingCartDetails()
                    {
                        Product = productService.GetProductById(c.ProductId ?? 0),
                        ProductOrderQuantity = c.ProductQuantity ?? 0,
                        ProductImageId = c.ProductImageId ?? 0

                    }).ToDictionary(s => s.Product.Id, s => s);

                    Session["WishList"] = new ShoppingCart() { CartItems = wishListItems };
                }
            }
        }

        private void saveSessionObjectBeforeSignOut()
        {
            sessionObjService.DeleteSessionObjByUserId(AppUtils.getCurrentUser().user.Id);

            var cart = AppUtils.getCart();
            if (cart != null)
            {
                var cartSessionObj = cart.CartItems.ToList().ConvertAll(s =>
                {
                    return new SessionObj()
                    {
                        UserId = AppUtils.getCurrentUser().user.Id,
                        ProductId = s.Value.Product.Id,
                        ProductImageId = s.Value.ProductImageId,
                        ProductQuantity = s.Value.ProductOrderQuantity,
                        ObjType = (int)SessionObjType.Cart,
                        CreatedBy = AppUtils.getCurrentUser().user.Id,
                        CreatedDate = DateTime.Now,
                        Deleted = Convert.ToBoolean(YNStatue.No)
                    };
                });
                sessionObjService.AddSessionObjList(cartSessionObj);
            }

            var wishList = AppUtils.getWishList();
            if (wishList != null)
            {
                var wishListSessionObj = wishList.CartItems.ToList().ConvertAll(s =>
                {
                    return new SessionObj()
                    {
                        UserId = AppUtils.getCurrentUser().user.Id,
                        ProductId = s.Value.Product.Id,
                        ProductImageId = s.Value.ProductImageId,
                        ProductQuantity = s.Value.ProductOrderQuantity,
                        ObjType = (int)SessionObjType.WishList,
                        CreatedBy = AppUtils.getCurrentUser().user.Id,
                        CreatedDate = DateTime.Now,
                        Deleted = Convert.ToBoolean(YNStatue.No)
                    };
                });
                sessionObjService.AddSessionObjList(wishListSessionObj);
            }
        }

        #endregion

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            logger.Error(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "ErrorHandler");
        }

    }
}