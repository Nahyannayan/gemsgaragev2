﻿using Garage.Models;
using Garage.Service;
using Garage.Utils.Security;
using Garage.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static Garage.Utils.AppEnumeration;
using static Garage.Utils.Mapper.CategoryMapper;


namespace Garage.Controllers
{
    public class CategoryController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly CategoryService categoryService = new CategoryService();

        [AppAuthorize (Roles = "Corporate IT")]
        [HttpGet, ActionName("List")]
        public ActionResult CategoryList()
        {
            logger.Info("Category Controller - List all categories from database");
             var categoryList = categoryService.GetAllCategories().Where(c => c.Deleted == Convert.ToBoolean(YNStatue.No)).OrderByDescending(p => p.CreatedDate).ToList();
            var subcategory = categoryList.Where(c => c.ParentCategoryId != 0).ToList();
            ViewBag.categoryList = subcategory;
            ViewBag.categorymap = categoryList.ToDictionary(c => c.Id, c => c.CategoryName);

            return View("CategoryList");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Add")]
        public ActionResult CategoryAdd()
        {
            logger.Info("Category Controller - Create a new Category");
            List<Category> parentCategories = getParentCategory();
            var parentCategoryList = parentCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.CategoryName.ToString(),
                    Value = c.Id.ToString(),
                    Selected = false
                };
            });
            ViewBag.ParentCategoryList = parentCategoryList;

            return View("CategoryAdd", new CategoryViewModel() { Active = true });
        }


        [AppAuthorize]
        [HttpPost, ActionName("Save")]
        public ActionResult CategorySave(CategoryViewModel categoryViewModel)
        {
            logger.Info("Category Controller - Saving new category to the database.");
            ViewBag.savedCategory = categoryService.AddCategory(map(categoryViewModel));
            return RedirectToAction("List", "Category");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Edit")]
        public ActionResult EditCategory(string id)
        {
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            logger.Info("Category Controller - Edit the saved category from database");
            Category category = categoryService.GetCategoryById(OriginalID);
            CategoryViewModel categoryViewModel = remap(category);
            List<Category> parentCategories = categoryService.GetAllCategories().Where(p => p.ParentCategoryId == null || p.ParentCategoryId == 0 && p.Id != category.Id).ToList();
            var parentCategoryList = parentCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.CategoryName.ToString(),
                    Value = c.Id.ToString(),
                    Selected = false
                };
            });
            categoryViewModel.EncryptedId = id;
            ViewBag.ParentCategoryList = parentCategoryList;
            return View("CategoryEdit", categoryViewModel);
        }

        [AppAuthorize]
        [HttpPost, ActionName("Edit")]
        public ActionResult UpdateCategory(CategoryViewModel categoryViewModel)
        {
            categoryViewModel.Id = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(categoryViewModel.EncryptedId));
            logger.Info("Category Controller - Update the exsisting category from database.");
            Category category = categoryService.GetCategoryById(categoryViewModel.Id);
            categoryService.UpdateCategory(map(categoryViewModel, category));
            return RedirectToAction("List", "Category");
        }

        [AppAuthorize]
        [HttpGet, ActionName("sub-category-list")]
        public ActionResult GetSubcategoriesByParentCategoryId(int parentCategoryId)
        {
            logger.Info("Category Controller - Getting All Subcategories by Ajax Call");
            List<Category> subCategories = categoryService.GetAllCategories().Where(p => p.ParentCategoryId == parentCategoryId).ToList();
            return Json(subCategories,JsonRequestBehavior.AllowGet);
        }

        [AppAuthorize]
        [HttpGet, ActionName("Delete")]
        public ActionResult DeleteCategory(string id)
        {
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            logger.Info("Category Controller - Delete the exsisting category from database.");
            var category = categoryService.GetCategoryById(OriginalID);
            categoryService.DeleteCategory(category);
            return RedirectToAction("List", "Category");
        }

        #region Help Methods

        private List<Category> getParentCategory() => categoryService.GetAllCategories().Where(p => p.ParentCategoryId == null || p.ParentCategoryId == 0 && p.Active == Convert.ToBoolean(YNStatue.Yes)).ToList();

        #endregion

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            logger.Error(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "ErrorHandler");
        }
    }
}