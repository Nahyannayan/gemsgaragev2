﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garage.Controllers
{
    public class ErrorHandlerController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET: ErrorHandler
        public ActionResult Index()
        {
            logger.Info("Error Controller - Custom Error Page");
            return View("CustomError");
        }
    }
}