﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Garage.Service;
using Garage.Models;
using Garage.Utils;

namespace Garage.Controllers
{
    public class FileApiController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly ProductImageService productImageService = new ProductImageService();
        private readonly AssetImageService assetImageService = new AssetImageService();
        private readonly CategoryService categoryService = new CategoryService();

        [Utils.Security.AppAuthorize]
        [HttpGet, ActionName("ProductImage")]
        public FileResult GetProductImage(int productImageId)
        {
            logger.Info("File API Controller - Get Product Image by Product Id");
            ProductImage productImage = productImageService.GetProductImageById(productImageId);
            if (productImage != null && System.IO.File.Exists(productImage.ImageFilePath))
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(productImage.ImageFilePath);
                string fileName = productImage.ImageName + productImage.Extension;
                return File(fileBytes, MimeTypeMap.GetMimeType(productImage.Extension), fileName);
            }
            else return null;
        }

        [Utils.Security.AppAuthorize]
        [HttpGet, ActionName("AssetImage")]
        public FileResult GetAssetImage(int assetImageId)
        {
            logger.Info("File API Controller - Get Asset Image by Asset Id");
            AssetImage assetImage = assetImageService.GetAssetImageById(assetImageId);
            if (assetImage != null && System.IO.File.Exists(assetImage.ImageFilePath))
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(assetImage.ImageFilePath);
                string fileName = assetImage.ImageName + assetImage.Extension;
                return File(fileBytes, MimeTypeMap.GetMimeType(assetImage.Extension), fileName);
            }
            else return null;
        }

        [Utils.Security.AppAuthorize]
        [HttpGet, ActionName("CategoryImage")]
        public FileResult GetCategoryImage(int categoryId)
        {
            logger.Info("File API Controller - List Get Category Image by Category Id");
            Category category = categoryService.GetCategoryById(categoryId);
            if (category != null && System.IO.File.Exists(category.ImagePath))
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(category.ImagePath);
                var currentFileName = Path.GetFileNameWithoutExtension(category.ImagePath);
                var fileExtension = Path.GetExtension(category.ImagePath).ToString().ToLower();
                return File(fileBytes, MimeTypeMap.GetMimeType(fileExtension), currentFileName + fileExtension);
            }
            else return null;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            logger.Error(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "ErrorHandler");
        }
    }
}