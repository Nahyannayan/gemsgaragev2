﻿using Garage.Models;
using Garage.Service;
using Garage.Utils;
using Garage.Utils.Security;
using Garage.ViewModels;
using Garage.ViewModels.ShoppingCart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Garage.Utils.AppEnumeration;

namespace Garage.Controllers
{
    public class LandingController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly ProductService productService = new ProductService(); 
        private readonly ProductPostTrackService productPostTrackService = new ProductPostTrackService();
        private readonly ProductImageService productImageService = new ProductImageService();
        private readonly CategoryService categoryService = new CategoryService();
        private readonly UserService userService = new UserService();
        private readonly SchoolService schoolService = new SchoolService();
        private readonly SessionObjService sessionObjService = new SessionObjService();

        [AllowAnonymous]
        public ActionResult Index()
        {
            logger.Info("Landing Controller - Application Start Page Opening");
            var isAuthenticated = Request.IsAuthenticated ? "Request Is Authenticated" : "Request Is Unauthenticated";
            logger.Info("Landing Controller - "+ isAuthenticated);
            return View();
        }

        [AllowAnonymous]
        [HttpGet, ActionName("HomePage")]
        public ActionResult HomePage()
        {
           
            logger.Info("Landing Controller - Listing products in home page");
            var recentProductList = productService.GetAllPublishedProducts().OrderByDescending(p => p.CreatedDate).Take(8).ToList();
            var mostWatchedProductList = productService.GetAllPublishedProducts().Where(p => p.ViewCount > 0).OrderByDescending(p => p.ViewCount).Take(8).ToList();
            ViewBag.RecentProducts = getProductListView(recentProductList);
            ViewBag.MostWatchedProducts = getProductListView(mostWatchedProductList);
            ViewBag.ParentCategories = getParentCategory();
            return View();
        }

        [AllowAnonymous]
        [HttpGet, ActionName("HelpPage")]
        public ActionResult HelpPage()
        {
            logger.Info("Landing Controller - Opens Help Page");
            ViewBag.ParentCategories = getParentCategory();
            return View();
        }

        [AllowAnonymous]
        [HttpGet, ActionName("RecentProducts")]
        public ActionResult RecentProducts()
        {
            logger.Info("Landing Controller - Listing Recent products in recent prodcuts page");
            //nahyan on 29jan20
            //  var productList = productService.GetAllPublishedProducts().Where(p => p.CreatedDate > DateTime.Now.AddDays(-7)).OrderByDescending(p => p.CreatedDate).ToList();
          //  var productList = productService.GetAllPublishedProducts().OrderByDescending(p => p.CreatedDate).ToList();
            var productList = productService.GetAllPublishedProducts().OrderByDescending(p => p.CreatedDate).ThenBy(p => p.UnitValue).ToList();
          //  var productList1 = productService.GetAllPublishedProducts().OrderByDescending(p => p.UnitValue).ToList();
            ViewBag.RecentProducts = getProductListView(productList);
            ViewBag.ParentCategories = getParentCategory();
            return View();
        }

        //[AppAuthorize]
        [HttpGet, ActionName("MostWatchedProducts")]
        public ActionResult MostWatchedProducts()
        {
            logger.Info("Landing Controller - Listing most watched products in home page");
            var productList = productService.GetAllPublishedProducts().Where(p=>p.ViewCount > 0).OrderByDescending(p => p.ViewCount).Take(25).ToList();
            ViewBag.MostWatchedProducts = getProductListView(productList);
            ViewBag.ParentCategories = getParentCategory();
            return View();
        }

        [AllowAnonymous]
        [HttpPost, ActionName("SearchProducts")]
        public ActionResult SearchProducts(string searchString)
        {
            logger.Info("Landing Controller - Listing search products in search result page");
            List<Product> searchedProducts;

            if (!string.IsNullOrEmpty(searchString))
            {
                 searchedProducts = productService.GetAllPublishedProducts().Where(p => p.ProductName.Contains(searchString, StringComparison.OrdinalIgnoreCase)).ToList();
            }
            else
            {
                 searchedProducts = productService.GetAllPublishedProducts().ToList();
            }
            ViewBag.SearchedProducts = getProductListView(searchedProducts);
            ViewBag.SearchString = searchString;
            ViewBag.ParentCategories = getParentCategory();
            return View();
        }

        [AppAuthorize]
        [HttpGet, ActionName("Cart")]
        public ActionResult CartList()
        {
            logger.Info("Landing Controller - Displaying cart products in cart page");
            if (Session["Cart"] != null)
            {
                var cart = (ShoppingCart)Session["Cart"];
                Session["Cart"] = cart.UpdateProductFromSession(cart);
            }
            
            ViewBag.ParentCategories = getParentCategory();
            return View();
        }

        [AppAuthorize]
        [HttpGet, ActionName("WishList")]
        public ActionResult WishList()
        {
            logger.Info("Landing Controller - Displaying wishlist products in wish list page");
            if (Session["WishList"] != null)
            {
                var wishList = (ShoppingCart)Session["WishList"];
                Session["WishList"] =  wishList.UpdateProductFromSession(wishList);
            }
            ViewBag.ParentCategories = getParentCategory();
            return View(new AddToCartForm());
        }

        [AppAuthorize]
        [HttpGet, ActionName("CartItem")]
        public ActionResult CartItem()
        {
            logger.Info("Landing Controller - Displaying cart items to School Admins except PO");
            ViewBag.SchoolCartItems = getCartItemsByCrrentUserSchool(AppUtils.getCurrentUser());
            return View();
        }

        [AppAuthorize]
        [HttpGet, ActionName("cart-item-count")]
        public ActionResult GetCartItemCount()
        {
            logger.Info("Landing Controller - Getting Cart Item Count by Ajax Call");
            var cartItemCount = getCartItemCountByCrrentUserSchool(AppUtils.getCurrentUser());
            return Json(new { cartItemCount }, JsonRequestBehavior.AllowGet);
        }

        #region Help Methods

        private List<ProductListView> getProductListView(List<Product> productList) =>
            productList.ConvertAll(p =>
            {
                var school = schoolService.GetSchoolById(p.SchoolId??0);
                var postTrackList = productPostTrackService.GetPostTrackByProductId(p.Id);
                var principalTrackEntry = postTrackList.Where(pt => pt.UserRole == (int)UserRole.Principal).FirstOrDefault();
                var publishedDate = principalTrackEntry == null ? p.CreatedDate : principalTrackEntry.CreatedDate;
                return new ProductListView()
                {
                    Product = p,
                    imageIdList = productImageService.GetProductImageByProductId(p.Id).ConvertAll(pi => pi.Id),
                    SchoolCode = school.SchoolCode,
                    PublishedDate = publishedDate
                };
            });

        private List<Category> getParentCategory() => categoryService.GetAllCategories().Where(p => p.ParentCategoryId == null || p.ParentCategoryId == 0 && p.Active == Convert.ToBoolean(YNStatue.Yes) && p.Deleted == Convert.ToBoolean(YNStatue.No)).ToList();

        private List<SchoolCartItem> getCartItemsByCrrentUserSchool(CurrentUser currentUser)
        {
            var schoolCartItems = new List<SchoolCartItem>();
            var currentSchoolIctList = userService.GetAllUsers().Where(u => u.School == currentUser.school.Id && u.Role == (int)UserRole.ICTEngineer).ToList();
            var sessionObj = sessionObjService.GetAllsessionObjs().Where(s => currentSchoolIctList.Select(u => u.Id).Contains(s.UserId) && s.ObjType == (int)SessionObjType.Cart).ToList();
            if (sessionObj != null && sessionObj.Count > 0)
            {
                schoolCartItems = sessionObj.ConvertAll(s =>
                {
                    var product = productService.GetProductById(s.ProductId ?? 0);
                    var imageIdList = productImageService.GetProductImageByProductId(product.Id).ConvertAll(pi => pi.Id);
                    var orderQuantity = s.ProductQuantity ?? 0;
                    var orderValue = orderQuantity * product.UnitValue;
                    var IctName = userService.GetUserById(s.UserId).FullName;

                    return new SchoolCartItem()
                    {
                        Product = product,
                        ImageIdList = imageIdList,
                        OrderQuantity = orderQuantity,
                        OrderValue = orderValue??0,
                        IctName = IctName
                    };
                });

                return schoolCartItems;
            }
            return schoolCartItems;
        }

        private int getCartItemCountByCrrentUserSchool(CurrentUser currentUser)
        {
            var currentSchoolIctList = userService.GetAllUsers().Where(u => u.School == currentUser.school.Id && u.Role == (int)UserRole.ICTEngineer).ToList();
            var sessionObj = sessionObjService.GetAllsessionObjs().Where(s => currentSchoolIctList.Select(u => u.Id).Contains(s.UserId) && s.ObjType == (int)SessionObjType.Cart).ToList();
            return (sessionObj == null) ? 0 : sessionObj.Count;
        }


        #endregion

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            logger.Error(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "ErrorHandler");
        }
    }
}