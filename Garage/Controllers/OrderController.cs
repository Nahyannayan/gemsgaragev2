﻿using Garage.Models;
using Garage.Service;
using Garage.ViewModels;
using Garage.ViewModels.ShoppingCart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static Garage.Utils.AppEnumeration;
using static Garage.Utils.Mapper.OrderMapper;
using static Garage.Utils.Mapper.OrderTrackMapper;
using static Garage.Utils.Mapper.ProductOrderMapper;
using static Garage.Utils.Mapper.ProductMapper;
using static Garage.Utils.Mapper.SoldProductMapper;
using Garage.Utils;
using Garage.Service.Email.EmailModels;
using Garage.Service.Email;
using Garage.Utils.Security;
using Rotativa;
using Rotativa.Options;
using Garage.ViewModels.Email;
using Garage.Service.GemsEmail;

namespace Garage.Controllers
{
    public class OrderController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly ProductService productService = new ProductService();
        private readonly OrderService orderService = new OrderService();
        private readonly UserService userService = new UserService();
        private readonly ProductOrderService productOrderService = new ProductOrderService();
        private readonly OrderTrackService orderTrackService = new OrderTrackService();
        private readonly SoldProductService soldProductService = new SoldProductService();
        private readonly SchoolService schoolService = new SchoolService();
        private readonly SessionObjService sessionObjService = new SessionObjService();
        private readonly ProductPostTrackService productPostTrackService = new ProductPostTrackService();

        [AppAuthorize]
        [HttpGet, ActionName("Orders")]
        public ActionResult Index()
        {
            logger.Info("Order Controller - Listing all the orders from database");
            ViewBag.OrderList = GetOrderDetails();
            return View("Orders");
        }

        [AppAuthorize]
        [HttpPost, ActionName("AddToCart")]
        public ActionResult AddToCart(AddToCartForm addToCartForm, bool? removeFromWishList, string returnUrl)
        {
            logger.Info("Order Controller - Adding product to the Cart Object");
            if (Session["Cart"] == null)
            {
                ShoppingCart cart = new ShoppingCart();
                Session["Cart"] = cart.AddToCart(addToCartForm);
            }
            else
            {
                var cart = (ShoppingCart)Session["Cart"];
                cart.AddToCart(addToCartForm);
            }

            if (removeFromWishList != null && removeFromWishList == true && Session["WishList"] != null)
            {
                var wishList = (ShoppingCart)Session["WishList"];
                wishList.RemoveFromCart(addToCartForm.ProductId);
            }
            sessionObjService.saveSessionObjectToDatabase();
            TempData["AddedToCart"] = true;
            if (!string.IsNullOrEmpty(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("HomePage", "Landing");
        }

        [AppAuthorize]
        [HttpPost, ActionName("AddToWishList")]
        public ActionResult AddToWishList(AddToCartForm addToCartForm, bool? removeFromCart, string returnUrl)
        {
            logger.Info("Order Controller - Adding product to the Wishlist Object");
            if (Session["WishList"] == null)
            {
                ShoppingCart wishList = new ShoppingCart();
                Session["WishList"] = wishList.AddToCart(addToCartForm);
            }
            else
            {
                var wishList = (ShoppingCart)Session["WishList"];
                wishList.AddToCart(addToCartForm);
            }

            if (removeFromCart != null && removeFromCart == true && Session["Cart"] != null)
            {
                var cart = (ShoppingCart)Session["Cart"];
                cart.RemoveFromCart(addToCartForm.ProductId);
            }
            sessionObjService.saveSessionObjectToDatabase();
            TempData["AddedToWishList"] = true;
            if (!string.IsNullOrEmpty(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("HomePage", "Landing");
        }

        [AppAuthorize]
        [HttpPost, ActionName("RemoveFromCart")]
        public ActionResult RemoveFromCart(int productId)
        {
            logger.Info("Order Controller - Removing product from the Cart Object");
            if (Session["Cart"] != null)
            {
                var cart = (ShoppingCart)Session["Cart"];
                cart.RemoveFromCart(productId);
            }
            sessionObjService.saveSessionObjectToDatabase();
            return RedirectToAction("Cart", "Landing");
        }

        [AppAuthorize]
        [HttpPost, ActionName("RemoveFromWishList")]
        public ActionResult RemoveFromWishList(int productId)
        {
            logger.Info("Order Controller - Removing product from the WishList Object");
            if (Session["WishList"] != null)
            {
                var wishList = (ShoppingCart)Session["WishList"];
                wishList.RemoveFromCart(productId);
            }
            sessionObjService.saveSessionObjectToDatabase();
            return RedirectToAction("WishList", "Landing");
        }

        [AppAuthorize]
        [HttpPost, ActionName("Checkout")]
        public ActionResult Checkout(List<ProductOrderForm> productOrderForm)
        {
            logger.Info("Order Controller - Checkout the Cart to place the order");
            if (isInValidOrder(productOrderForm))
            {
                return Json(new { isValid = false });
            }

            if (Session["Cart"] != null)
            {
                var order = orderService.PlaceOrder(productOrderForm);
                Session.Remove("Cart");
                OrderApprovalEmail(order);
            }
            sessionObjService.saveSessionObjectToDatabase();

            return Json(new { isValid = true });
        }

        [AppAuthorize]
        [HttpGet, ActionName("Track")]
        public ActionResult TrackOrder(string id)
        {
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            logger.Info("Order Controller - Tracking the Order by Order Id from database.");
            ViewBag.ReturnUrl = Request.UrlReferrer.AbsolutePath;//Return Url
            var order = orderService.GetOrderById(OriginalID);
            ViewBag.Order = order;
            var productOrder = productOrderService.GetPOByOrderId(order.Id);
            var listOfProductId = productOrder.Select(p => p.ProductId);
            var productList = productService.GetAllProducts().Where(p => listOfProductId.Contains(p.Id)).ToList();
            ViewBag.ProductDetails = getProductDetails(productList);
            ViewBag.OrderTrack = orderTrackService.GetOrderTrackByOrderId(order.Id);
            ViewBag.ProductOrder = productOrder;
            return View("OrderTrack");
        }

        [AppAuthorize]
        [HttpGet, ActionName("TrackStatus")]
        public ActionResult TrackOrderStatus(int id, bool print = false)
        {
            logger.Info("Order Controller - Tracking the Order by Order Id from database - Model.");

            var order = orderService.GetOrderById(id);
            var productOrder = productOrderService.GetPOByOrderId(order.Id).FirstOrDefault();
            var product = productService.GetProductById(productOrder.ProductId ?? 0);
            var sellerSchool = schoolService.GetSchoolById(product.SchoolId ?? 0);
            ViewBag.Product = product;
            ViewBag.ProductOrder = productOrder;
            ViewBag.SellerSchool = sellerSchool;
            ViewBag.Order = order;
            ViewBag.OrderTrack = orderTrackService.GetOrderTrackByOrderId(order.Id);
            var productTrack = productPostTrackService.GetPostTrackByProductId(product.Id).Where(pt => pt.UserRole == (int)UserRole.Principal).FirstOrDefault();
            ViewBag.PublishedDate = productTrack.CreatedDate;
            var user = userService.GetUserById(order.CreatedBy ?? 0);
            var buyerSchool = schoolService.GetSchoolById(user.School ?? 0);
            ViewBag.BuyerSchool = buyerSchool;

            if (print)
            {
                return new PartialViewAsPdf("~/Views/Order/Pdf/OrderTrackModalPdf.cshtml")
                {
                    PageOrientation = Orientation.Portrait,
                    PageSize = Size.A3,
                    CustomSwitches = "--footer-center \" [page] Page of [toPage] Pages\" --footer-line --footer-font-size \"9\" --footer-spacing 5 --footer-font-name \"calibri light\"",
                    FileName = "OrderTrack.pdf"
                };
            }
            return PartialView("OrderTrackModal");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Remove")]
        public ActionResult RemoveIndividualProductFromOrder(int orderId, int productId)
        {
            logger.Info("Order Controller - Rejecting Single Product in Order");
            Order order = orderService.GetOrderById(orderId);
            var productOrderList = productOrderService.GetPOByOrderId(orderId);
            if (productOrderList.Count == 1)
            {
                orderService.UpdateOrder(cancelOrder(order));
            }
            var productOrder = productOrderList.FirstOrDefault(po => po.ProductId == productId);
            productOrderService.UpdateProductOrder(deleteProductOrder(productOrder));
            updateStock(productOrder);
            var soldProduct = rejectMap(productOrder); // sold product reject map
            soldProductService.UpdateSoldProduct(soldProduct);
            return TrackOrder(Garage.Security.DataProtection.Encrypt(orderId.ToString()));
        }

        [AppAuthorize]
        [HttpGet, ActionName("Reject")]
        public ActionResult RejectOrder(int id)
        {
            logger.Info("Order Controller - Rejecting Order while Approval");
            Order order = orderService.GetOrderById(id);
            orderService.UpdateOrder(cancelOrder(order));
            var productOrder = productOrderService.GetPOByOrderId(order.Id);
            updateStock(productOrder, order);
            var soldProductList = rejectMap(order); // sold product reject map
            soldProductList.ForEach(p => soldProductService.UpdateSoldProduct(p));
            PurchaseRejectedMail(order);
            return RedirectToAction("Orders");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Approval")]
        public ActionResult ApprovalOrder(string id)
        {
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            logger.Info("Order Controller - Approval Order process");
            var watch = new System.Diagnostics.Stopwatch();
            watch.Start();
            var currentUser = AppUtils.getCurrentUser();
            Order order = orderService.GetOrderById(OriginalID);
            orderTrackService.AddOrderTrack(orderTrackMap(order));
            if ((UserRole)currentUser.user.Role == UserRole.Finance)
            {
                order.OrderStatus = Convert.ToInt32(OrderStatus.Procured);
                order = orderService.UpdateOrder(order);
                updateProductStatus(productOrderService.GetPOByOrderId(order.Id));

                var soldProductList = waitingForBookValueTransferMap(order);
                soldProductList.ForEach(p => soldProductService.UpdateSoldProduct(p));
                OrderApproveEmail(order);
            }
            else
            {
                order.OrderStatus = Convert.ToInt32(OrderStatus.InProgress);
                orderService.UpdateOrder(order);

                var userRole = getNextApproverByCurrentRole(currentUser.user.Role ?? 0);
                var productOrder = productOrderService.GetPOByOrderId(order.Id);
                var listOfProductId = productOrder.Select(p => p.ProductId);
                var productList = productService.GetAllProducts().Where(p => listOfProductId.Contains(p.Id)).ToList();
                List<ProductDetail> productDetails = getProductDetails(productList);
                OrderApprovalEmail(order);
            }
            watch.Stop();
            logger.Info("Order Controller - Execution Time: " + watch.ElapsedMilliseconds + " ms");
            return RedirectToAction("Orders");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Completed")]
        public ActionResult OrderCompleted(int id)
        {
            logger.Info("Order Controller - Order Complete process");
            Order order = orderService.GetOrderById(id);
            var completedOrder = orderService.UpdateOrder(orderCompleteMap(order));

            var soldProductList = soldProductService.GetSoldProductsByOrderId(order.Id);
            var updatedsoldProductList = updateDispatchedListMap(soldProductList.ToList());
            updatedsoldProductList.ForEach(sp => soldProductService.UpdateSoldProduct(sp));

            return RedirectToAction("Orders", "Order");
        }

        #region Help Methods

        private List<ProductDetail> getProductDetails(List<Product> productList) => productList.ConvertAll(p => viewMap(p));

        private void OrderApproveEmail(Order order)
        {
            var school = schoolService.GetSchoolById(order.SchoolId ?? 0);
            var productOrder = productOrderService.GetPOByOrderId(order.Id).FirstOrDefault();
            var product = productService.GetProductById(productOrder.ProductId ?? 0);
            var toBuyerEmailId = getUserMailByRoleAndId((int)UserRole.ICTEngineer, order.CreatedBy ?? 0);
            var toSellerEmailId = getUserMailByRoleAndId((int)UserRole.ICTEngineer, product.UserId ?? 0);

            var orderDetails = new EmailOrderDetails()
            {
                ProductDetail = viewMap(product),
                ProductOrder = productOrder,
                BuyerSchoolName = school.SchoolName
            };
            var emailbodyBuyer = EmailFactory.ParseTemplate(orderDetails, AppUtils.orderApproveBuyerMailTemplate);
            var emailbodySellerr = EmailFactory.ParseTemplate(orderDetails, AppUtils.orderApproveSellerMailTemplate);
            //emailService.send(toBuyerEmailId, "Purchase has been Approved", emailbodyBuyer);
            //emailService.send(toSellerEmailId, "Purchase has been Approved", emailbodySellerr);

            GemsToken.SendMail(new List<string>() { toBuyerEmailId }, "Purchase has been Approved", emailbodyBuyer);
            GemsToken.SendMail(new List<string>() { toSellerEmailId }, "Purchase has been Approved", emailbodySellerr);
        }

        private void OrderApprovalEmail(Order order)
        {
            var school = schoolService.GetSchoolById(order.SchoolId ?? 0);
            var productOrder = productOrderService.GetPOByOrderId(order.Id).FirstOrDefault();
            var product = productService.GetProductById(productOrder.ProductId ?? 0);
            var nextApprover = getNextApproverByCurrentRole(AppUtils.getCurrentUser().user.Role ?? 0);
            var toEmailIds = getAllEmailsBasedOnRole(nextApprover);

            var orderDetails = new EmailOrderDetails()
            {
                ProductDetail = viewMap(product),
                ProductOrder = productOrder,
                BuyerSchoolName = school.SchoolName
            };
            var emailbody = EmailFactory.ParseTemplate(orderDetails, AppUtils.OrderApprovalMailTemplate);

            GemsToken.SendMail(toEmailIds, "Purchase Approval", emailbody);
        }

        private void PurchaseRejectedMail(Order order)
        {
            var toEmailIds = getAllEmailsBasedOnRole((int)UserRole.ICTEngineer);

            var productOrder = productOrderService.GetPOByOrderId(order.Id).FirstOrDefault();
            var product = productService.GetProductById(productOrder.ProductId ?? 0);
            var orderDetails = new EmailOrderDetails()
            {
                ProductDetail = viewMap(product),
                ProductOrder = productOrder
            };
            var emailbody = EmailFactory.ParseTemplate(orderDetails, AppUtils.PurchaseRejectedMailTemplate);
            //emailService.send(toEmailIds, "Purchase Rejected", emailbody);
            GemsToken.SendMail(toEmailIds, "Purchase Rejected", emailbody);

        }

        private void updateStock(ProductOrder productOrder)
        {
            var product = productService.GetProductById(productOrder.ProductId ?? 0);
            product.InStock = product.InStock + productOrder.Quantity;
            productService.UpdateProduct(product);
        }

        private void updateStock(List<ProductOrder> productOrder, Order order)
        {
            List<int?> productIds = productOrder.Select(p => p.ProductId).ToList();
            var productList = productService.GetAllProducts().Where(p => productIds.Contains(p.Id)).ToList();
            if (order.OrderStatus == (int)OrderStatus.Cancelled)
            {
                productList.ForEach(p => p.InStock = p.InStock + (productOrder.Where(po => po.ProductId == p.Id).FirstOrDefault().Quantity));
            }
            else
            {
                productList.ForEach(p => p.InStock = p.InStock - (productOrder.Where(po => po.ProductId == p.Id).FirstOrDefault().Quantity));
            }
            productList.ForEach(p => productService.UpdateProduct(p));
        }

        private void updateProductStatus(List<ProductOrder> productOrder)
        {
            List<int?> productIds = productOrder.Select(p => p.ProductId).ToList();

            var productList = productService.GetAllProducts().Where(p => productIds.Contains(p.Id)).ToList();

            productList.ForEach(p =>
                {
                    var soldQuantity = orderService.getSoldQuantityByProductId(p.Id);
                    if (p.Quantity == soldQuantity)
                    {
                        p.Status = (int)ProductStatus.SoldOut;
                    }
                });

            productList.ForEach(p => productService.UpdateProduct(p));
        }

        private List<Order> getOrderList() => orderService.GetAllOrders().Where(o => o.OrderStatus == (int)OrderStatus.OrderPlaced || o.OrderStatus == (int)OrderStatus.InProgress || o.OrderStatus == (int)OrderStatus.Procured && o.Deleted == Convert.ToBoolean(YNStatue.No) && o.SchoolId == AppUtils.getCurrentUser().school.Id).OrderByDescending(p => p.CreatedDate).ToList();

        private List<Order> getCompletedOrderList() => orderService.GetAllOrders().Where(o => o.OrderStatus == (int)OrderStatus.Completed && o.Deleted == Convert.ToBoolean(YNStatue.No) && o.SchoolId == AppUtils.getCurrentUser().school.Id).OrderByDescending(p => p.CreatedDate).ToList();

        private List<Order> getAllCompletedOrders() => orderService.GetAllOrders().Where(o => o.OrderStatus == (int)OrderStatus.Completed && o.Deleted == Convert.ToBoolean(YNStatue.No)).OrderByDescending(p => p.CreatedDate).ToList();

        private bool isInValidOrder(List<ProductOrderForm> productOrderForm) => productOrderForm.Exists(po => po.quantity > (productService.GetProductById(po.productId).InStock));

        private List<string> getAllFinanceEmails() => userService.GetAllUsers().Where(u => u.School == AppUtils.getCurrentUser().school.Id && u.Role == (int)UserRole.Finance).Select(u => u.Email).ToList();

        private List<string> getAllEmailsBasedOnRole(int userRole) => userService.GetAllUsers().Where(u => u.School == AppUtils.getCurrentUser().school.Id && u.Role == userRole).Select(u => u.Email).ToList();

        private List<string> getAllCorporateITEmails() => userService.GetAllUsers().Where(u => u.Role == (int)AdminRole.CorpIT).Select(u => u.Email).ToList();

        private string getUserMailByRoleAndId(int userRole, int userId) => userService.GetAllUsers().Where(u => u.Id == userId && u.Role == userRole).Select(u => u.Email).FirstOrDefault();


        private int getNextApproverByCurrentRole(int currentUserRole)
        {
            UserRole userRole;

            switch ((UserRole)currentUserRole)
            {
                case UserRole.ICTEngineer:
                    userRole = (UserRole.MSO);
                    break;
                case UserRole.Finance:
                    userRole = (UserRole.ICTEngineer);
                    break;
                case UserRole.MSO:
                    userRole = (UserRole.Principal);
                    break;
                case UserRole.Principal:
                    userRole = (UserRole.Finance);
                    break;
                default:
                    userRole = (UserRole.Finance);
                    break;
            }
            return (int)userRole;
        }

        private List<OrderDetails> GetOrderDetails()
        {

            var Records = new List<Order>();
            Records = getOrderList();
            if (AppUtils.getCurrentUser().user.NIRole != 5)
            {
                Records = Records.Where(p => p.SchoolId == AppUtils.getCurrentUser().user.School.Value).ToList();
            }
            var OrderDetailList = Records.ConvertAll(o =>
              {
                  var lastOrderTrackEntry = orderTrackService.GetOrderTrackByOrderId(o.Id).Where(ot => ot.IsDifferentSchool == Convert.ToBoolean(YNStatue.No)).OrderByDescending(ot => ot.CreatedDate).FirstOrDefault();
                  var nextRole = getNextApproverByCurrentRole(lastOrderTrackEntry.UserRole);
                  var nextApprover = EnumHelper<UserRole>.GetDisplayValue((UserRole)nextRole) ?? "";
                  var productOrder = productOrderService.GetPOByOrderId(o.Id).FirstOrDefault();
                  var product = productService.GetProductById(productOrder.ProductId ?? 0);
                  var soldProduct = soldProductService.GetSoldProductsByOrderId(o.Id).FirstOrDefault();
                  var pickUpScheduled = soldProduct == null ? null : soldProduct.ScheduledDate;
                  return new OrderDetails()
                  {
                      Order = o,
                      ProductOrder = productOrder,
                      Product = product,
                      NextApprover = nextApprover,
                      NextApproverRole = nextRole,
                      PickUpScheduled = pickUpScheduled,
                      SchoolCode = product.SchoolId.HasValue ? AppUtils.GetSchoolDetails(product.SchoolId.Value).SchoolCode : ""
                  };
              });
            var currentUserRole = AppUtils.getCurrentUser().user.Role;
            if (currentUserRole != (int)UserRole.ICTEngineer)
            {
                OrderDetailList = OrderDetailList.Where(o => o.NextApproverRole == currentUserRole).ToList();

            }
            if (currentUserRole == (int)UserRole.Principal)
            {
                OrderDetailList = OrderDetailList.Where(o => o.Product.SchoolId != AppUtils.getCurrentUser().user.School.Value).ToList();
            }

            return OrderDetailList;
        }

        private List<CompletedOrders> GetCompletedOrderDetails()
        {
            var OrderDetailList = getAllCompletedOrders().ConvertAll(o =>
            {
                var productOrder = productOrderService.GetPOByOrderId(o.Id).FirstOrDefault();
                var product = productService.GetProductById(productOrder.ProductId ?? 0);
                return new CompletedOrders()
                {
                    Order = o,
                    ProductOrder = productOrder,
                    Product = product,
                };
            });
            return OrderDetailList;
        }

        #endregion

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            logger.Error(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "ErrorHandler");
        }
    }
}