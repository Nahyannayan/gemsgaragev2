﻿using Garage.Models;
using Garage.Service;
using Garage.Utils;
using Garage.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Garage.Utils.Mapper.ProductMapper;
using static Garage.Utils.Mapper.ProductTrackMapper;
using static Garage.Utils.Mapper.AssetTrackMapper;
using static Garage.Utils.Mapper.OrderTrackMapper;
using static Garage.Utils.Mapper.AssetMapper;
using static Garage.Utils.Mapper.ProductImageMapper;
using static Garage.Utils.Mapper.SoldProductMapper;
using static Garage.Utils.AppEnumeration;
using Garage.Service.Email.EmailModels;
using Garage.Service.Email;
using Garage.Utils.Security;
using Rotativa;
using Rotativa.Options;
using Garage.ViewModels.EmailModels;
using Garage.ViewModels.Email;
using Garage.Service.GemsEmail;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web.Mvc.Html;
using Garage.Areas.Non_IT.Services;

namespace Garage.Controllers
{
    public class ProductController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly ProductService productService = new ProductService();
        private readonly ProductPostTrackService productPostTrackService = new ProductPostTrackService();
        private readonly AssetTrackService assetTrackService = new AssetTrackService();
        private readonly CategoryService categoryService = new CategoryService();
        private readonly ProductImageService productImageService = new ProductImageService();
        private readonly UserService userService = new UserService();
        private readonly SoldProductService soldProductService = new SoldProductService();
        private readonly ProductOrderService productOrderService = new ProductOrderService();
        private readonly OrderService orderService = new OrderService();
        private readonly OrderTrackService orderTrackService = new OrderTrackService();
        private readonly SchoolService schoolService = new SchoolService();
        private readonly CurrentUser currentUser = AppUtils.getCurrentUser();
        private readonly NIGeneralService NIgeneralservice = new NIGeneralService();

        //[AppAuthorize]
        [HttpGet, ActionName("View")]
        public ActionResult ViewProduct(int id)
        {
            logger.Info("Product Controller - Viewing Individual product");
            Product product = productService.GetProductById(id);
            if (currentUser != null && !currentUser.isSuperAdmin && product.SchoolId != currentUser.school.Id)
            {
                product.ViewCount = product.ViewCount + 1;
                productService.UpdateProduct(product);
            }
            ViewBag.productDetail = viewMap(product);
            ViewBag.Conditions = NIgeneralservice.GetAllCondition().ToList().ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.DISPLAY.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            //ViewBag.productImage = productImageService.GetSingleProductImageByProductId(product.Id);
            return PartialView("_ProductDetailsModal", new AddToCartForm());
        }

        [AppAuthorize(Roles = "ICT Engineer")]
        [AppAuthorize]
        [HttpGet, ActionName("Add")]
        public ActionResult AddProduct()
        {
            logger.Info("Product Controller -Adding New product");

            ViewBag.Conditions = NIgeneralservice.GetAllCondition().ToList().ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.DISPLAY.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            List<Category> parentCategories = getParentCategory();
            var parentCategoryList = parentCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.CategoryName.ToString(),
                    Value = c.Id.ToString(),
                    Selected = false
                };
            });
            ViewBag.ParentCategoryList = parentCategoryList;

            List<Category> subCategories = categoryService.GetAllCategories().Where(p => p.ParentCategoryId != null && p.ParentCategoryId != 0).ToList();
            var subCategoryList = subCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.CategoryName.ToString(),
                    Value = c.Id.ToString(),
                    Selected = false
                };
            });
            ViewBag.SubCategoryList = subCategoryList;
            return View("ProductAdd", new ProductForm());
        }

        [AppAuthorize]
        [AppAuthorize(Roles = "ICT Engineer,Finance,MSO,Principal")]
        [HttpPost, ActionName("Save")]
        public ActionResult SaveProduct(ProductForm productForm)
        {
            logger.Info("Product Controller - Saving product to the database");

            Product savedProduct = productService.AddProduct(map(productForm));
            productForm.productViewModel.Id = savedProduct.Id;
            productImageService.AddProductImageList(productImageMapper(productForm));
            ViewBag.savedProduct = savedProduct;
            TempData["productName"] = savedProduct.ProductName;
            return RedirectToAction("Draft", "SchoolAdmin");
        }

        [AppAuthorize]
        [AppAuthorize(Roles = "ICT Engineer,Finance,MSO,Principal")]
        [HttpGet, ActionName("Edit")]
        public ActionResult EditProduct(string id)
        {
            logger.Info("Product Controller - Edit Individual product from database");
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            Product product = productService.GetProductById(OriginalID);
            ViewBag.Product = product;
            if (product.RejectedUserId != null)
            {
                var rejectedUser = userService.GetUserById(product.RejectedUserId ?? 0);
                ViewBag.User = rejectedUser;
            }
            ProductForm productForm = remap(product);
            List<Category> parentCategories = getParentCategory();
            var parentCategoryList = parentCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.CategoryName.ToString(),
                    Value = c.Id.ToString(),
                    Selected = false
                };
            });
            ViewBag.ParentCategoryList = parentCategoryList;

            List<Category> subCategories = categoryService.GetAllCategories().Where(p => p.ParentCategoryId != null && p.ParentCategoryId == product.CategoryId).ToList();
            var subCategoryList = subCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.CategoryName.ToString(),
                    Value = c.Id.ToString(),
                    Selected = false
                };
            });
            ViewBag.Conditions = NIgeneralservice.GetAllCondition().ToList().ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.DISPLAY.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            ViewBag.SubCategoryList = subCategoryList;
            return View("ProductEdit", productForm);
        }

        [AppAuthorize]
        [HttpPost, ActionName("Update")]
        public ActionResult UpdateProduct(ProductForm productForm)
        {
            logger.Info("Product Controller - Updating the exsisting product");

            Product product = productService.GetProductById(productForm.productViewModel.Id);
            var updatedProduct = productService.UpdateProduct(map(productForm, product));
            TempData["productName"] = updatedProduct.ProductName;
            TempData["editFlag"] = true;
            if (product.Status == (int)ProductStatus.Posted)
            {
                return RedirectToAction("ApprovalRequest", "SchoolAdmin");
            }
            return RedirectToAction("Draft", "SchoolAdmin");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Delete")]
        public ActionResult DeleteProduct(string id)
        {
            logger.Info("Product Controller - Delete Individual product from Database");
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            Product product = productService.GetProductById(OriginalID);
            var deletedProduct = productService.DeleteProduct(product);
            TempData["productName"] = deletedProduct.ProductName;
            TempData["deleted"] = deletedProduct.Deleted;
            return RedirectToAction("Draft", "SchoolAdmin");
        }

        [AppAuthorize]
        [HttpPost, ActionName("Post")]
        public ActionResult PostProduct(ProductForm productForm, bool? isNewProduct)
        {
            logger.Info("Product Controller - Saving the Product in the database As Post status");
            Product postProduct;
            if (isNewProduct ?? false)
            {
                Product newProduct = map(productForm);
                mapPostStatus(newProduct);
                Product savedProduct = productService.AddProduct(newProduct);
                productForm.productViewModel.Id = savedProduct.Id;
                productImageService.AddProductImageList(productImageMapper(productForm));
                postProduct = savedProduct;

                var ReferenceFiles = productForm.productImageViewModel.ReferenceFiles;
                if (!string.IsNullOrEmpty(ReferenceFiles))
                {
                    List<int> deserializefilesID = (new JavaScriptSerializer()).Deserialize<List<int>>(ReferenceFiles);

                    foreach (int id in deserializefilesID)
                    {
                        var savedfile = (new ProductImageService()).GetProductImageById(id);
                        savedfile.ProductId = savedProduct.Id;
                        (new ProductImageService()).UpdateProductImage(savedfile);
                    }
                }
            }
            else
            {
                Product product = productService.GetProductById(productForm.productViewModel.Id);
                Product postingProducts = map(productForm, product);
                postProduct = productService.UpdateProduct(mapPostStatus(postingProducts));
            }
            if (postProduct.AssetType == (int)AssetType.Reuse)
            {
                productPostTrackService.AddProductPostTrack(prodcutTrackmap(postProduct));
            }
            else
            {
                assetTrackService.AddAssetTrack(assetTrackmap(postProduct));
            }

            ApprovalMailTrigger(postProduct);
            TempData["productName"] = postProduct.ProductName;
            return RedirectToAction("ApprovalRequest", "SchoolAdmin");
        }

        [AppAuthorize]
        [HttpGet, ActionName("List")]
        public ActionResult ProductListByCategory(string categoryId, string subCategoryId)
        {
            logger.Info("Product Controller - Listing of product in E-Commerce Page by Category");
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(categoryId));
            int? _subCategoryId = null;
            if (!string.IsNullOrEmpty(subCategoryId))
            {
                _subCategoryId = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(subCategoryId));
            }
            List<Product> productList;
            Category currentCategory;
            List<Category> subCategoryList = categoryService.GetAllCategories().Where(c => c.ParentCategoryId == OriginalID).ToList();
            if (subCategoryId == null)
            {
                currentCategory = categoryService.GetCategoryById(OriginalID);
                productList = productService.GetAllPublishedProducts().Where(p => p.CategoryId == OriginalID).ToList();
            }
            else
            {
                currentCategory = categoryService.GetCategoryById(_subCategoryId ?? 0);
                productList = productService.GetAllPublishedProducts().Where(p => p.SubCategoryId == _subCategoryId).ToList();
            }
            ViewBag.ProductList = getProductListView(productList);
            ViewBag.CurrentCategory = currentCategory;
            ViewBag.SubCategoryList = subCategoryList;
            ViewBag.ParentCategories = getParentCategory();
            return View("ProductList");
        }

        [AppAuthorize(Roles = "Corporate IT,GEMS ADMIN")]
        [HttpGet, ActionName("SoldProducts")]
        public ActionResult SoldProducts()
        {
            logger.Info("Product Controller - List of Sold Products from database");

            var soldProductsList = ViewBag.SoldProducts = soldProductService.GetAllSoldProducts().Where(p => p.Status == (int)SoldProductStatus.Scheduled).OrderByDescending(p => p.CreatedDate).ToList();
            ViewBag.SoldProducts = getSoldProductDetailList(soldProductsList);
            return View();
        }

        [AppAuthorize(Roles = "Finance,ICT Engineer,GEMS ADMIN")]
        [HttpGet, ActionName("Sold")]
        public ActionResult Sold()
        {
            logger.Info("Product Controller - List of Sold Products from database for school users");
            var soldProductsList = soldProductService.GetAllSoldProducts().Where(p => p.SchoolId == currentUser.school.Id).OrderByDescending(p => p.CreatedDate).ToList();
            ViewBag.SoldProducts = getSoldProductDetailList(soldProductsList);
            return View("Sold");
        }

        [AppAuthorize(Roles = "Corporate IT,VP-Corporate IT,Corporate Internal Audit,GEMS ADMIN")]
        [HttpGet, ActionName("Dispatched")]
        public ActionResult DispatchedProducts(bool print = false)
        {
            logger.Info("Product Controller - List of Dispatched Products from database for school users");
            var soldProductsList = soldProductService.GetAllSoldProducts().Where(p => p.Status == (int)SoldProductStatus.Dispatched).OrderByDescending(p => p.CreatedDate).ToList();
            ViewBag.SoldProducts = getSoldProductDetailList(soldProductsList);
            if (print)
            {
                return new PartialViewAsPdf("~/Views/Product/Pdf/DispatchedProductsPdf.cshtml")
                {
                    PageOrientation = Orientation.Portrait,
                    PageSize = Size.A3,
                    CustomSwitches = "--footer-center \" [page] Page of [toPage] Pages\" --footer-line --footer-font-size \"9\" --footer-spacing 5 --footer-font-name \"calibri light\"",
                    FileName = "DispatchedProducts.pdf"
                };
            }
            return View("DispatchedProducts");
        }

        [AppAuthorize]
        [HttpPost, ActionName("Scheduled")]
        public ActionResult Scheduled(string value, string pk)
        {
            var scheduledDate = DateTime.Parse(value);
            var soldProductId = Convert.ToInt32(pk);
            soldProductService.UpdateSoldProduct(updateScheduleDate(scheduledDate, soldProductId));
            var soldProduct = soldProductService.GetSoldProductById(soldProductId);
            var order = orderService.GetOrderById(soldProduct.OrderId);
            orderTrackService.AddOrderTrack(sellerOrderTrackMap(order));
            var productOrderList = productOrderService.GetPOByOrderId(order.Id);
            var listOfProductId = productOrderList.Select(p => p.ProductId);
            var productList = productService.GetAllProducts().Where(p => listOfProductId.Contains(p.Id)).ToList();
            List<ProductDetail> productDetails = getProductDetails(productList);
            var buyer = userService.GetUserById(order.CreatedBy ?? 0);
            var ICTEmailList = getAllICTEmailBySchoolId(buyer.School ?? 0);
            var soldProductList = soldProductService.GetAllSoldProducts().Where(s => s.OrderId == order.Id).ToList();
            var productDetail = productDetails.Where(pd => pd.Id == soldProduct.ProductId.ToString()).FirstOrDefault();
            AfterScheduledMail(order);
            return Json(new { isUpdated = true, soldProduct }, JsonRequestBehavior.AllowGet);
        }

        [AppAuthorize]
        [HttpGet, ActionName("BookValueTransfer")]
        public ActionResult BookValueTransfer(int id)
        {
            var soldProduct = soldProductService.GetSoldProductById(id);
            var order = orderService.GetOrderById(soldProduct.OrderId);
            orderTrackService.AddOrderTrack(sellerOrderTrackMap(order));
            soldProductService.UpdateSoldProduct(waitingForScheduleMap(soldProduct));
            return RedirectToAction("Sold", "Product");
        }

        [AppAuthorize]
        [HttpGet, ActionName("RejectedProducts")]
        public ActionResult RejectedProducts(List<Product> rejectedProducts)
        {
            logger.Info("Product Controller - Listing of Rejected Products");
            if (rejectedProducts != null && (rejectedProducts.Any()))
            {
                ViewBag.DraftedProducts = rejectedProducts;
            }
            else
            {
                ViewBag.DraftedProducts = getRejectedProducts();
            }
            return View("RejectedProducts");
        }

        [AppAuthorize]
        [HttpPost, ActionName("RejectedProducts")]
        public ActionResult RejectedProducts(SearchAdminProductForm searchAdminProductForm)
        {
            logger.Info("Product Controller - Listing Rejected Products By Search");
            var searchString = searchAdminProductForm.seacrchString;
            var isOlderFirst = searchAdminProductForm.isOlderFirst;
            var rejectedProducts = getRejectedProducts(searchString, isOlderFirst ?? false);
            return RejectedProducts(rejectedProducts);
        }

        [AppAuthorize]
        [HttpGet, ActionName("Details")]
        public ActionResult ProductDetails(int id)
        {
            logger.Info("Product Controller - Viewing Product details in Admin Page");

            Product product = productService.GetProductById(id);
            ViewBag.ProductDetail = viewMap(product);
            return PartialView("_ProductDetailsViewModal");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Track")]
        public ActionResult TrackProduct(int id)
        {
            logger.Info("Product Controller - Viewing Prodcut track in modal");

            var product = productService.GetProductById(id);
            ViewBag.Product = product;
            var userListMap = userService.GetAllUsers().ToDictionary(u => u.Id, u => u);
            ViewBag.UserListMap = userListMap;
            ViewBag.PublishedUser = userListMap[product.CreatedBy ?? 0];
            ViewBag.ProductTrack = productPostTrackService.GetPostTrackByProductId(id);
            return PartialView("_ProductTrackModal");
        }

        [AppAuthorize]
        [HttpGet, ActionName("ApprovalView")]
        public ActionResult ProductApprovalView(string id)
        {
            logger.Info("Product Controller - Viewing the prodcut detail for Approval");
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            Product product = productService.GetProductById(OriginalID);
            ViewBag.Product = product;
            ProductForm productForm = remap(product);
            productForm.productViewModel.ReasonForReject = "";
            List<Category> parentCategories = getParentCategory();
            var parentCategoryList = parentCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.CategoryName.ToString(),
                    Value = c.Id.ToString(),
                    Selected = false
                };
            });
            ViewBag.ParentCategoryList = parentCategoryList;

            List<Category> subCategories = categoryService.GetAllCategories().Where(p => p.ParentCategoryId != null && p.ParentCategoryId == product.CategoryId).ToList();
            var subCategoryList = subCategories.ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.CategoryName.ToString(),
                    Value = c.Id.ToString(),
                    Selected = false
                };
            });
            ViewBag.SubCategoryList = subCategoryList;
            ViewBag.ProductTrack = productPostTrackService.GetPostTrackByProductId(product.Id).OrderByDescending(p => p.CreatedDate).FirstOrDefault();
            ViewBag.ReferenceFiles = productImageService.GetProductRefFileByProductId(product.Id).ToList();
            //ViewBag.ProductTrackList = productPostTrackService.GetPostTrackByProductId(product.Id).OrderByDescending(p => p.CreatedDate).ToList();
            ViewBag.Conditions = NIgeneralservice.GetAllCondition().ToList().ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.DISPLAY.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            return View("ProductApproval", productForm);
        }

        [AppAuthorize]
        [HttpPost, ActionName("Approval")]
        public ActionResult ApprovalProduct(ProductForm productForm)
        {
            logger.Info("Product Controller - Saving the Approval Status for the product");

            Product approvedProduct;

            Product product = productService.GetProductById(productForm.productViewModel.Id);
            Product updatedProduct = map(productForm, product);

            if ((UserRole)currentUser.user.Role == UserRole.Principal)
            {
                approvedProduct = productService.UpdateProduct(mapPublishStatus(updatedProduct));
                ProductStatusEmail(updatedProduct);
            }
            else
            {
                approvedProduct = productService.UpdateProduct(mapPostStatus(updatedProduct));
                ApprovalMailTrigger(approvedProduct);
            }

            if (approvedProduct.AssetType == (int)AssetType.Reuse)
            {
                productPostTrackService.AddProductPostTrack(prodcutTrackmap(approvedProduct));
            }
            else
            {
                assetTrackService.AddAssetTrack(assetTrackmap(approvedProduct));
            }

            TempData["productName"] = approvedProduct.ProductName;
            TempData["approved"] = true;
            return RedirectToAction("ApprovalRequest", "SchoolAdmin");
        }

        [AppAuthorize]
        [HttpPost, ActionName("Reject")]
        public ActionResult RejectProduct(ProductForm productForm)
        {
            logger.Info("Product Controller - Rejecting the product");

            Product product = productService.GetProductById(productForm.productViewModel.Id);
            product = logRejectReason(product, productForm);
            if (product.AssetType == (int)AssetType.Reuse)
            {
                productPostTrackService.DeleteProductTrackByProduct(product.Id);
            }
            else
            {
                assetTrackService.DeleteAssetTrackByProduct(product.Id);
            }
            RejectAssetEmail(product);
            TempData["productName"] = product.ProductName;
            TempData["rejected"] = true;
            return RedirectToAction("ApprovalRequest", "SchoolAdmin");
        }

        [AppAuthorize]
        [HttpGet, ActionName("ApprovalMail")]
        public ActionResult ApprovalMail(int productId, int userRole)
        {
            logger.Info("Product Controller - Approval Mail Send to Particular Role");

            var product = productService.GetProductById(productId);
            ApprovalMailRemainder(product, userRole);

            return Json(new { isSend = true }, JsonRequestBehavior.AllowGet);
        }

        #region Asset Methods

        [AppAuthorize]
        [HttpGet, ActionName("AssetApproval")]
        public ActionResult AssetApproval(string id)
        {
            logger.Info("Product Controller - Viewing the asset detail for Approval");
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            Product product = productService.GetProductById(OriginalID);
            ViewBag.Product = product;
            ProductForm productForm = remap(product);
            productForm.productViewModel.ReasonForReject = string.Empty;
            ViewBag.ParentCategoryList = getCategorySelectList(getParentCategory());
            ViewBag.SubCategoryList = getCategorySelectList(getSubCategory());
            var assetTrack = assetTrackService.GetPostTrackByAssetId(product.Id).OrderByDescending(p => p.CreatedDate).FirstOrDefault();
            ViewBag.AssetTrack = assetTrack;
            ViewBag.ReferenceFiles = productImageService.GetProductRefFileByProductId(product.Id);
            ViewBag.Conditions = NIgeneralservice.GetAllCondition().ToList().ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.DISPLAY.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            return View(productForm);
        }

        [AppAuthorize]
        [HttpPost, ActionName("AssetApproval")]
        public ActionResult Approval(ProductForm productForm)
        {
            logger.Info("Product Controller - Saving the Approval Status for the asset");

            Product approvedAsset = null;

            Product product = productService.GetProductById(productForm.productViewModel.Id);
            Product updatedProduct = map(productForm, product);
            var currentUserRole = (Role)currentUser.user.Role;
            if (currentUserRole == Role.Principal)
            {
                approvedAsset = productService.UpdateProduct(mapApprovedStatus(updatedProduct));

            }
            else
            {
                if (currentUserRole == Role.Finance || currentUserRole == Role.MSO)
                {
                    approvedAsset = productService.UpdateProduct(mapPostStatus(updatedProduct));
                }
            }
            assetTrackService.AddAssetTrack(assetTrackmap(approvedAsset));
            if (currentUserRole != Role.CFO)
            {
                DisposeApprovalMail(updatedProduct);
            }
            TempData["productName"] = updatedProduct.ProductName;
            TempData["approved"] = true;
            return RedirectToAction("ApprovalRequest", "SchoolAdmin");
        }


        [AppAuthorize]
        [HttpGet, ActionName("DisposeAsset")]
        public ActionResult ApprovedAssets(string id)
        {
            logger.Info("Product Controller - Viewing the asset detail for Approval");
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            Product product = productService.GetProductById(OriginalID);
            ViewBag.Product = product;
            ProductForm productForm = remap(product);
            ViewBag.ParentCategoryList = getCategorySelectList(getParentCategory());
            ViewBag.SubCategoryList = getCategorySelectList(getSubCategory());
            var assetTrack = assetTrackService.GetPostTrackByAssetId(product.Id).OrderByDescending(p => p.CreatedDate).FirstOrDefault();
            ViewBag.AssetTrack = assetTrack;
            ViewBag.Conditions = NIgeneralservice.GetAllCondition().ToList().ConvertAll(c =>
            {
                return new SelectListItem()
                {
                    Text = c.DISPLAY.ToString(),
                    Value = c.ID.ToString(),
                    Selected = false
                };
            });
            ViewBag.ReferenceFiles = productImageService.GetProductRefFileByProductId(product.Id).ToList();
            return View("CorpAssetApproval", productForm);
        }

        [AppAuthorize]
        [HttpPost, ActionName("DisposeAsset")]
        public ActionResult ApprovedAssets(ProductForm productForm)
        {
            logger.Info("Product Controller - Saving the Approval Status for the asset");
            Product approvedAsset = null;
            Product product = productService.GetProductById(productForm.productViewModel.Id);
            Product updatedProduct = map(productForm, product);
            var currentUserRole = (Role)currentUser.user.Role;

            if (((currentUserRole == (Role.CorpFinance))))
            {
                updatedProduct = productService.UpdateProduct(updatedProduct);
            }

            if ((currentUserRole == (Role.CorpInternalAudit)) && (!updatedProduct.ApprovalReq))
            {
                approvedAsset = productService.UpdateProduct(mapDisposedStatus(updatedProduct));
            }
            else if ((currentUserRole == (Role.CFO)) && (updatedProduct.ApprovalReq))
            {
                approvedAsset = productService.UpdateProduct(mapDisposedStatus(updatedProduct));

            }

            var result = assetTrackmap(updatedProduct);
            result.Remarks = productForm.Remarks;
            assetTrackService.AddAssetTrack(result);
            if (updatedProduct.Status != (int)ProductStatus.Disposed)
                DisposeApprovalMail(updatedProduct);
            TempData["productName"] = updatedProduct.ProductName;
            return RedirectToAction("ApprovedAsset", "Product");
        }

        [AppAuthorize]
        [HttpPost, ActionName("Reuse")]
        public ActionResult ReUseAsset(AssetReuseForm assetReuseForm)
        {
            logger.Info("Asset Controller - Reuse Asset");
            Product reUsableProduct = productService.GetProductById(assetReuseForm.ProductId);
            var productForm = remap(reUsableProduct);
            productService.UpdateProduct(mapReuseQuantity(reUsableProduct, assetReuseForm));
            Product newProduct = productService.AddProduct(mapDisposeToPublished(productForm, assetReuseForm));
            var productImages = productImageService.AddProductImageList(setNewImagesFromProduct(reUsableProduct, newProduct.Id));
            var assetTrackList = assetTrackService.GetPostTrackByAssetId(reUsableProduct.Id);
            productPostTrackService.AddProductPostTrackList(diposedTrackToPostTrack(assetTrackList, newProduct));
            ReUseMail(newProduct);
            TempData["productName"] = newProduct.ProductName;
            TempData["published"] = true;
            return RedirectToAction("DisposedAssets", "Product");
        }

        [AppAuthorize]
        [HttpPost, ActionName("RejectAsset")]
        public ActionResult RejectAsset(ProductForm productForm)
        {
            logger.Info("Product Controller - Rejecting the product");

            Product product = productService.GetProductById(productForm.productViewModel.Id);
            product = logRejectReason(product, productForm);
            assetTrackService.DeleteAssetTrackByProduct(product.Id);
            RejectAssetEmail(product);
            TempData["productName"] = product.ProductName;
            TempData["rejected"] = true;
            return RedirectToAction("ApprovedAsset", "Product");
        }

        [AppAuthorize]
        [HttpGet, ActionName("AssetTrack")]
        public ActionResult AssetTrack(int id, bool print = false)
        {
            logger.Info("Product Controller - Viewing Prodcut track in modal");
            var product = productService.GetProductById(id);
            ViewBag.Product = product;
            ViewBag.UserListMap = userService.GetAllUsers().ToDictionary(u => u.Id, u => u);
            ViewBag.AssetTrack = assetTrackService.GetPostTrackByAssetId(id);
            if (product.ApprovalReq)
            {
                if (print)
                {
                    return new PartialViewAsPdf("~/Views/Product/Pdf/CorpReqAssetTrackPdf.cshtml")
                    {
                        PageOrientation = Orientation.Portrait,
                        PageSize = Size.A3,
                        CustomSwitches = "--footer-center \" [page] Page of [toPage] Pages\" --footer-line --footer-font-size \"9\" --footer-spacing 5 --footer-font-name \"calibri light\"",
                        FileName = "CorpReqAssetTrackPdf.pdf"
                    };
                }
                return PartialView("CorpReqAssetTrack");
            }
            else
            {
                if (print)
                {
                    return new PartialViewAsPdf("~/Views/Product/Pdf/AssetTrackPdf.cshtml")
                    {
                        PageOrientation = Orientation.Portrait,
                        PageSize = Size.A3,
                        CustomSwitches = "--footer-center \" [page] Page of [toPage] Pages\" --footer-line --footer-font-size \"9\" --footer-spacing 5 --footer-font-name \"calibri light\"",
                        FileName = "AssetTrackPdf.pdf"
                    };
                }
                return PartialView("AssetTrack");
            }
        }
        [AppAuthorize]
        [HttpGet, ActionName("ApprovedAsset")]
        public ActionResult ApprovedAsset()
        {
            List<Product> result = new List<Product>();
            var approvedAssets = getAllApprovedAssets().ToList().OrderByDescending(p => p.UpdatedDate).ToList();
            var currentUserRole = currentUser.user.Role;


            foreach (Product product in approvedAssets)
            {
                var role = assetTrackService.getNextAssetApproverByAssetId(product);
                if (currentUserRole.Value == (int)role)
                {
                    result.Add(product);
                }
            }
            approvedAssets = result;



            ViewBag.ApprovedAssets = approvedAssets;
            ViewBag.SchoolNameMap = getSchoolName(approvedAssets);
            ViewBag.ApproverNameMap = getNextApproverName(approvedAssets);
            ViewBag.ValidApprovalMap = getValidApprovalMap(approvedAssets);
            return View("ApprovedAssets");
        }

        [AppAuthorize]
        [HttpGet, ActionName("DisposedAssets")]
        public ActionResult DisposedAssets(bool print = false)
        {
            ViewBag.DisposedAssets = getDisposedAssets();
            if (print)
            {
                return new PartialViewAsPdf("~/Views/Product/Pdf/DisposedProductsPdf.cshtml")
                {
                    PageOrientation = Orientation.Portrait,
                    PageSize = Size.A3,
                    CustomSwitches = "--footer-center \" [page] Page of [toPage] Pages\" --footer-line --footer-font-size \"9\" --footer-spacing 5 --footer-font-name \"calibri light\"",
                    FileName = "DisposedAssets.pdf"
                };
            }
            return View();
        }

        #endregion

        [AppAuthorize]
        [HttpGet, ActionName("AssetTransferTrack")]
        public ActionResult AssetTransferTrack(int id, bool print = false)
        {
            var soldProduct = soldProductService.GetSoldProductById(id);
            var order = orderService.GetOrderById(soldProduct.OrderId);
            var product = productService.GetProductById(soldProduct.ProductId);
            ViewBag.UserListMap = userService.GetAllUsers().ToDictionary(u => u.Id, u => u);
            ViewBag.Order = order;
            ViewBag.Product = product;
            ViewBag.OrderTrack = orderTrackService.GetOrderTrackByOrderId(order.Id);
            ViewBag.ProductTrack = productPostTrackService.GetPostTrackByProductId(product.Id);
            ViewBag.SoldProduct = soldProduct;
            ViewBag.SoldSchool = schoolService.GetSchoolById(product.SchoolId ?? 0);
            ViewBag.ProcuredSchool = schoolService.GetSchoolById(order.SchoolId ?? 0);
            ViewBag.ProductOrder = productOrderService.GetProductOrderById(soldProduct.ProductOrderId);
            if (print)
            {
                return new PartialViewAsPdf("~/Views/Product/Pdf/AssetTransferTrackPdf.cshtml")
                {
                    PageOrientation = Orientation.Portrait,
                    PageSize = Size.A3,
                    CustomSwitches = "--footer-center \" [page] Page of [toPage] Pages\" --footer-line --footer-font-size \"9\" --footer-spacing 5 --footer-font-name \"calibri light\"",
                    FileName = "AssetTransferTrackPdf.pdf"
                };
            }
            return View("AssetTransferTrack");
        }


        #region New

        [AppAuthorize]
        [HttpPost, ActionName("UploadFiles")]
        public JsonResult SaveReferenceFiles()
        {
            int? ProductID = null;
            var FileExtension = System.Configuration.ConfigurationManager.AppSettings["FileExtension"].ToString();
            logger.Info("Product Controller - Saving product to the database");
            List<ProductImage> FileDetails = new List<ProductImage>();
            if (!string.IsNullOrEmpty(Request.Form.ToString()))
                ProductID = Convert.ToInt32(Request.Form.GetValues("ProductID")[0].ToString());
            for (int i = 0; i < Request.Files.Count; i++)
            {
                if (FileExtension.Split(',').Contains(System.IO.Path.GetExtension(Request.Files[i].FileName).Replace(".", String.Empty)))
                {
                    FileDetails.Add(AppUtils.SaveProductFiles(Request.Files[i], ProductID, false));
                }
            }
            string myJsonString = (new JavaScriptSerializer()).Serialize(FileDetails);
            return Json(FileDetails, JsonRequestBehavior.AllowGet);

        }

        [AppAuthorize]
        [HttpGet, ActionName("DownloadFile")]
        public FileResult Download(int ID)
        {

            var result = (new ProductImageService()).GetProductImageById(ID);
            byte[] fileBytes = System.IO.File.ReadAllBytes(ConfigurationManager.AppSettings["NonITFilePath"] + result.ImageFilePath);
            string fileName = result.ImageName;
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

        }

        [AppAuthorize]
        [HttpPost, ActionName("DeleteFile")]
        public JsonResult DeleteProductFile(int ID)
        {

            logger.Info("Product Controller - Delete product file to the database");
            var FileDetails = (new ProductImageService()).DeleteProductImage(new ProductImage() { Id = ID });
            return Json(FileDetails, JsonRequestBehavior.AllowGet);

        }
        [AppAuthorize]
        [HttpPost, ActionName("BulkApproval")]
        public bool BulkApproval(string IDs, string ProductRef, string Remarks)
        {
            bool returnvalue = false;
            try
            {
                var currentUserRole = (AppEnumeration.Role)currentUser.user.Role;
                var arr = IDs.Split(',');
                var reference = ProductRef.Split(',');
                if (currentUserRole == Role.Principal)
                {
                    for (int i = 0; i < arr.Length; i++)
                    {
                        string ProductID = arr[i];
                        Product updatedProduct = productService.GetProductById(int.Parse(ProductID));
                        updatedProduct.UpdatedBy = (int?)(AppEnumeration.Role)currentUser.user.Id;
                        updatedProduct.UpdatedDate = DateTime.Now;
                        updatedProduct.PrincipalNote = Remarks;
                        if (updatedProduct.AssetType == (int)AssetType.Reuse)
                        {
                            if ((UserRole)currentUser.user.Role == UserRole.Principal)
                            {
                                updatedProduct = productService.UpdateProduct(mapPublishStatus(updatedProduct));
                                ProductStatusEmail(updatedProduct);
                            }
                            else
                            {
                                updatedProduct = productService.UpdateProduct(mapPostStatus(updatedProduct));
                                ApprovalMailTrigger(updatedProduct);
                            }

                            if (updatedProduct.AssetType == (int)AssetType.Reuse)
                            {
                                productPostTrackService.AddProductPostTrack(prodcutTrackmap(updatedProduct));
                            }
                            else
                            {
                                assetTrackService.AddAssetTrack(assetTrackmap(updatedProduct));
                            }
                        }
                        else
                        {


                            if (currentUserRole == Role.Principal)
                            {
                                updatedProduct = productService.UpdateProduct(mapApprovedStatus(updatedProduct));

                            }
                            else
                            {
                                if (currentUserRole == Role.Finance || currentUserRole == Role.MSO)
                                {
                                    updatedProduct = productService.UpdateProduct(mapPostStatus(updatedProduct));
                                }
                            }
                            assetTrackService.AddAssetTrack(assetTrackmap(updatedProduct));
                            if (currentUserRole != Role.CFO)
                            {
                                DisposeApprovalMail(updatedProduct);
                            }
                        }

                        // 
                    }
                }

                if (currentUserRole == Role.CorpIT || currentUserRole == Role.VPCorpIT || currentUserRole == Role.CorpFinance || currentUserRole == Role.CorpInternalAudit ||
                    currentUserRole == Role.SVPTreasury || currentUserRole == Role.CFO
                    )
                {
                    for (int i = 0; i < arr.Length; i++)
                    {
                        string ProductID = arr[i];
                        Product product = productService.GetProductById(int.Parse(ProductID));
                        product.UpdatedBy = (int?)(AppEnumeration.Role)currentUser.user.Id;
                        product.UpdatedDate = DateTime.Now;
                        if (((currentUserRole == (Role.CorpFinance))))
                        {

                            product = productService.UpdateProduct(product);
                        }

                        if ((currentUserRole == (Role.CorpInternalAudit)) && (!product.ApprovalReq))
                        {
                            product = productService.UpdateProduct(mapDisposedStatus(product));
                        }
                        else if ((currentUserRole == (Role.CFO)) && (product.ApprovalReq))

                            product = productService.UpdateProduct(mapDisposedStatus(product));


                        var result = assetTrackmap(product);
                        result.Remarks = Remarks;
                        assetTrackService.AddAssetTrack(result);
                        if (currentUserRole != Role.CFO)
                        {
                            DisposeApprovalMail(product);
                        }

                    }
                }

                TempData["succeed"] = "1";
                returnvalue = true;
            }
            catch (Exception ex)
            {

            }
            return returnvalue;
        }
        #endregion


        #region Help Methods

        private List<ProductListView> getProductListView(List<Product> productList) =>
            productList.ConvertAll(p =>
            {
                var school = schoolService.GetSchoolById(p.SchoolId ?? 0);
                var postTrackList = productPostTrackService.GetPostTrackByProductId(p.Id);
                var principalTrackEntry = postTrackList.Where(pt => pt.UserRole == (int)UserRole.Principal).FirstOrDefault();
                var publishedDate = principalTrackEntry == null ? p.CreatedDate : principalTrackEntry.CreatedDate;
                return new ProductListView()
                {
                    Product = p,
                    imageIdList = productImageService.GetProductImageByProductId(p.Id).ConvertAll(pi => pi.Id),
                    SchoolCode = school.SchoolCode,
                    PublishedDate = publishedDate
                };
            });

        private void ReUseMail(Product product)
        {
            var toEmailId = getUserById(product.UserId ?? 0).Email;
            var productDetails = new EmailProductDetails() { ProductDetail = viewMap(product) };
            var emailbody = EmailFactory.ParseTemplate(productDetails, AppUtils.reuseMailTemplate);
            //emailService.send(toEmailId, "Asset Reused", emailbody);
            GemsToken.SendMail(new List<string>() { toEmailId }, "Asset Reused", emailbody);
        }

        private void ApprovalMailTrigger(Product product)
        {
            if (!currentUser.isSuperAdmin && currentUser.user.Role != (int)UserRole.Principal)
            {
                var nextApprover = currentUser.user.Role + 1;
                List<string> toEmailIds = getAllEmailsBasedOnRole(nextApprover ?? 0, currentUser.school.Id);
                var productDetails = new EmailProductDetails() { ProductDetail = viewMap(product) };

                var emailbody = EmailFactory.ParseTemplate(productDetails, AppUtils.AssetApprovalMailTemplate);
                GemsToken.SendMail(toEmailIds, "Asset Approval", emailbody);
            }
        }

        private void DisposeApprovalMail(Product product)
        {

            var nextApprover = currentUser.user.Role + 1;
            List<string> toEmailIds;
            if (currentUser.isSuperAdmin || currentUser.user.Role == (int)Role.Principal)
            {
                toEmailIds = getAllEmailsBasedOnlyOnRole(nextApprover ?? 0);
            }
            else
            {
                toEmailIds = getAllEmailsBasedOnRole(nextApprover ?? 0, currentUser.school.Id);
            }
            var productDetails = new EmailProductDetails() { ProductDetail = viewMap(product) };

            var emailbody = EmailFactory.ParseTemplate(productDetails, AppUtils.AssetApprovalMailTemplate);

            if (product.ApprovalReq && currentUser.user.Role != (int)Role.CFO)
            {
                //emailService.send(toEmailIds, "Asset Approval", emailbody);
                GemsToken.SendMail(toEmailIds, "Asset Approval", emailbody);
            }
            else if (currentUser.user.Role != (int)Role.CorpInternalAudit)
            {
                //emailService.send(toEmailIds, "Asset Approval", emailbody);
                GemsToken.SendMail(toEmailIds, "Asset Approval", emailbody);
            }
            else
            {

            }

        }


        private void ApprovalMailRemainder(Product product, int nextApprover)
        {
            if (!currentUser.isSuperAdmin && currentUser.user.Role != (int)UserRole.Principal)
            {
                List<string> toEmailIds = getAllEmailsBasedOnRole(nextApprover, currentUser.school.Id);
                var productDetails = new EmailProductDetails() { ProductDetail = viewMap(product) };

                var emailbody = EmailFactory.ParseTemplate(productDetails, AppUtils.AssetApprovalMailTemplate);
                //emailService.send(toEmailIds, "Asset Approval", emailbody);
                GemsToken.SendMail(toEmailIds, "Asset Approval", emailbody);
            }
        }



        private void AfterScheduledMail(Order order)
        {
            var toEmailIds = getAllEmailsBasedOnRole((int)UserRole.ICTEngineer, order.SchoolId ?? 0);

            var productOrder = productOrderService.GetPOByOrderId(order.Id).FirstOrDefault();
            var product = productService.GetProductById(productOrder.ProductId ?? 0);
            var orderDetails = new EmailOrderDetails()
            {
                ProductDetail = viewMap(product),
                ProductOrder = productOrder
            };
            var emailbody = EmailFactory.ParseTemplate(orderDetails, AppUtils.AfterScheduledMailTemplate);
            //emailService.send(toEmailIds, "After Scheduled", emailbody);
            GemsToken.SendMail(toEmailIds, "After Scheduled", emailbody);

        }

        private void ProductStatusEmail(Product product)
        {
            List<string> toEmailIds = getAllEmailsBasedOnRole((int)UserRole.ICTEngineer, product.SchoolId ?? 0);
            var productDetails = new EmailProductDetails() { ProductDetail = viewMap(product) };
            if (product.Status == (int)ProductStatus.Published)
            {
                var emailbody = EmailFactory.ParseTemplate(productDetails, AppUtils.publishedMailTemplate);
                //emailService.send(toEmailIds, "Product Published", emailbody);
                GemsToken.SendMail(toEmailIds, "Product Published", emailbody);
            }
        }


        private void RejectAssetEmail(Product product)
        {
            List<string> toEmailIds = getAllEmailsBasedOnRole((int)UserRole.ICTEngineer, product.SchoolId ?? 0);
            var productDetails = new EmailProductDetails() { ProductDetail = viewMap(product) };

            var emailbody = EmailFactory.ParseTemplate(productDetails, AppUtils.rejectedMailTemplate);
            //emailService.send(toEmailIds, "Product Rejected", emailbody);
            GemsToken.SendMail(toEmailIds, "Product Rejected", emailbody);
        }

        private List<Category> getParentCategory() => categoryService.GetAllCategories().Where(p => p.ParentCategoryId == null || p.ParentCategoryId == 0 && p.Active == Convert.ToBoolean(YNStatue.Yes) && p.Deleted == Convert.ToBoolean(YNStatue.No)).ToList();

        private List<Category> getSubCategory() => categoryService.GetAllCategories().Where(p => p.ParentCategoryId != null && p.ParentCategoryId != 0).ToList();

        private List<Product> getRejectedProducts() => productService.GetAllProducts().Where(p => p.Status == (int)ProductStatus.Reject && p.SchoolId == currentUser.school.Id && p.Deleted == Convert.ToBoolean(YNStatue.No)).ToList();

        private List<Product> getRejectedProducts(string searchString, bool isOlderFirst) => isOlderFirst ? getRejectedProducts().OrderBy(p => p.CreatedDate).ToList() : getRejectedProducts().Where(p => p.ProductName.Contains(searchString)).OrderByDescending(p => p.CreatedDate).ToList();

        private List<string> getAllFinanceEmails() => userService.GetAllUsers().Where(u => u.School == currentUser.school.Id && u.Role == (int)AppEnumeration.UserRole.Finance).Select(u => u.Email).ToList();

        private List<string> getAllEmailsBasedOnRole(int userRole, int schoolId) => userService.GetAllUsers().Where(u => u.School == schoolId && u.Role == userRole).Select(u => u.Email).ToList();

        private List<string> getAllEmailsBasedOnlyOnRole(int userRole) => userService.GetAllUsers().Where(u => u.Role == userRole).Select(u => u.Email).ToList();

        private UserRegistration getUserById(int userId) => userService.GetUserById(userId);

        private List<string> getAllCorporateITEmails() => userService.GetAllUsers().Where(u => u.Role == (int)AdminRole.CorpIT).Select(u => u.Email).ToList();

        private List<SoldProductDetail> getSoldProductDetailList(List<SoldProduct> soldProductList)
        {
            var result = soldProductList.ToList();
            return result.ConvertAll(s =>
            {
                var schoolId = orderService.GetOrderById(s.OrderId).SchoolId;
                var school = schoolService.GetSchoolById(schoolId ?? 0);
                var product = productService.GetProductById(s.ProductId);
                var soldSchool = schoolService.GetSchoolById(product.SchoolId ?? 0);
                var productTrackList = productPostTrackService.GetPostTrackByProductId(product.Id);
                var publishedDate = productTrackList.Where(pt => pt.UserRole == (int)UserRole.Principal).Select(pt => pt.CreatedDate).FirstOrDefault();
                var orderTrackList = orderTrackService.GetOrderTrackByOrderId(s.OrderId);
                var buyerFinanceTrackEntry = orderTrackList.Where(ot => ot.UserRole == (int)UserRole.Finance && ot.IsDifferentSchool == Convert.ToBoolean(YNStatue.No)).FirstOrDefault();
                return new SoldProductDetail()
                {
                    SoldProduct = s,
                    Product = product,
                    ProductOrder = productOrderService.GetProductOrderById(s.ProductOrderId),
                    School = school,
                    SoldSchool = soldSchool,
                    OrderApprovedDate = buyerFinanceTrackEntry?.CreatedDate,
                    PublishedDate = publishedDate,
                    PickUpDate = s.ScheduledDate
                };
            });
        }

        private Dictionary<int, string> getNextApproverName(List<Product> productList)
        {
            Dictionary<int, string> approverNameMap = new Dictionary<int, string>();
            var nextApproverName = string.Empty;
            foreach (Product product in productList)
            {
                var role = assetTrackService.getNextAssetApproverByAssetId(product);
                if (role != 0)
                {
                    nextApproverName = EnumHelper<AppEnumeration.AdminRole>.GetDisplayValue((AppEnumeration.AdminRole)role);
                }
                approverNameMap.Add(product.Id, nextApproverName);
            }
            return approverNameMap;
        }
        private Dictionary<int, string> getSchoolName(List<Product> productList)
        {
            Dictionary<int, string> SchoolNameMap = new Dictionary<int, string>();
            var schoolname = string.Empty;
            foreach (Product product in productList)
            {

                var diposedSchool = schoolService.GetSchoolById(product.SchoolId ?? 0);
                SchoolNameMap.Add(product.Id, diposedSchool.SchoolCode);
            }
            return SchoolNameMap;
        }


        private Dictionary<int, bool> getValidApprovalMap(List<Product> productList)
        {
            Dictionary<int, bool> validApprovalMap = new Dictionary<int, bool>();

            foreach (Product product in productList)
            {
                var validApproval = false;
                var role = assetTrackService.getNextAssetApproverByAssetId(product);
                if (role == AppUtils.getCurrentUser().user.Role)
                {
                    validApproval = true;
                }
                validApprovalMap.Add(product.Id, validApproval);
            }
            return validApprovalMap;
        }


        private Dictionary<int, string> getProductApprovalStatusMap(List<Product> productList)
        {
            Dictionary<int, string> approvalMap = new Dictionary<int, string>();
            foreach (var product in productList)
            {
                if (product.Status == (int)AppEnumeration.ProductStatus.Posted)
                {
                    var role = productPostTrackService.GetPostTrackByProductId(product.Id).OrderByDescending(p => p.CreatedDate).FirstOrDefault().UserRole;
                    var roleEnum = (AppEnumeration.UserRole)role;
                    if (AppEnumeration.UserRole.Principal != roleEnum)
                    {
                        var nextApprover = roleEnum + 1;
                        approvalMap.Add(product.Id, EnumHelper<UserRole>.GetDisplayValue(nextApprover));
                    }
                }
            }
            return approvalMap;
        }

        private Product logRejectReason(Product product, ProductForm productForm)
        {
            return productService.UpdateProduct(mapRejectStatus(product, productForm));
        }

        private List<Category> getCurrentSubCategory(int parentCategoryId) => categoryService.GetAllCategories().Where(p => p.ParentCategoryId == parentCategoryId).ToList();

        private List<SelectListItem> getCategorySelectList(List<Category> categories) => categories.ConvertAll(c => new SelectListItem() { Text = c.CategoryName.ToString(), Value = c.Id.ToString(), Selected = false });

        private List<Product> getAllApprovedAssets() => productService.GetAllProducts().Where(p => p.AssetType == (int)AssetType.Dispose && p.Deleted == Convert.ToBoolean(YNStatue.No) && p.Status == (int)ProductStatus.Approved).ToList();

        private List<AssetDisposed> getDisposedAssets()
        {
            var productList = productService.GetAllProducts().Where(p => p.AssetType == (int)AssetType.Dispose && p.Deleted == Convert.ToBoolean(YNStatue.No) && p.Status == (int)ProductStatus.Disposed).ToList();
            var assetDiposedList = productList.ConvertAll(p =>
            {
                var diposedSchool = schoolService.GetSchoolById(p.SchoolId ?? 0);
                AssetTrack assetTrackDisposed;
                if (p.ApprovalReq)
                {
                    assetTrackDisposed = assetTrackService.GetPostTrackByAssetId(p.Id).Where(at => at.UserRole == (int)AdminRole.VPCorpIT).FirstOrDefault();
                }
                else
                {
                    assetTrackDisposed = assetTrackService.GetPostTrackByAssetId(p.Id).Where(at => at.UserRole == (int)AdminRole.CorpInternalAudit).FirstOrDefault();
                }
                var assetTrackPosted = assetTrackService.GetPostTrackByAssetId(p.Id).Where(at => at.UserRole == (int)UserRole.ICTEngineer).FirstOrDefault();
                return new AssetDisposed()
                {
                    Product = p,
                    DisposedDate = assetTrackDisposed.CreatedDate,
                    PostedDate = assetTrackPosted.CreatedDate,
                    School = diposedSchool
                };
            });

            return assetDiposedList;
        }
        private List<Product> getSchoolDisposedAssets() => productService.GetAllProducts().Where(p => p.SchoolId == currentUser.school.Id && p.AssetType == (int)AssetType.Dispose && p.Deleted == Convert.ToBoolean(YNStatue.No) && p.Status == (int)ProductStatus.Disposed).ToList();

        private List<ProductDetail> getProductDetails(List<Product> productList) => productList.ConvertAll(p => viewMap(p));

        private List<string> getAllICTEmailBySchoolId(int schoolId) => userService.GetAllUsers().Where(u => u.Deleted == Convert.ToBoolean(YNStatue.No) && u.School == schoolId && u.Role == (int)AppEnumeration.Role.ICTEngineer).Select(u => u.Email).ToList();


        #endregion

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            logger.Error(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "ErrorHandler");
        }
    }
}