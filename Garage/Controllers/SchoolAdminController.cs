﻿using Garage.Models;
using Garage.Service;
using Garage.Service.Email;
using Garage.Utils;
using Garage.Utils.Security;
using Garage.ViewModels;
using Rotativa;
using Rotativa.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Garage.Utils.AppEnumeration;

namespace Garage.Controllers
{
    public class SchoolAdminController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
   
        private readonly ProductService productService = new ProductService();
        private readonly AssetService assetService = new AssetService();
        private readonly AssetTrackService assetTrackService = new AssetTrackService();
        private readonly ProductPostTrackService productPostTrackService = new ProductPostTrackService();
        private readonly CategoryService categoryService = new CategoryService();
        private readonly ProductImageService productImageService = new ProductImageService();
        private readonly UserService userService = new UserService();
        private readonly SoldProductService soldProductService = new SoldProductService();
        private readonly ProductOrderService productOrderService = new ProductOrderService();
        private readonly OrderService orderService = new OrderService();
        private readonly OrderTrackService orderTrackService = new OrderTrackService();
        private readonly SchoolService schoolService = new SchoolService();

        [AppAuthorize (Roles = "ICT Engineer")]
        [HttpGet, ActionName("Draft")]
        public ActionResult Draft()
        {
            logger.Info("SchoolAdmin Controller - Listing of Drafted Products for Individual user");
            ViewBag.DraftList = getDraftList();
            return View();
        }

         [AppAuthorize ]
        [HttpGet, ActionName("ApprovalRequest")]
        public ActionResult ApprovalRequest()
        {
            logger.Info("SchoolAdmin Controller - Listing of Approval Request for Individual user");
            ViewBag.ApprovalRequestList = getApprovalRequest();
            return View();
        }

        [AppAuthorize(Roles = "MSO,Finance,ICT Engineer")]
        [HttpGet, ActionName("DisposedAsset")]
        public ActionResult Disposed(bool print = false)
        {
            ViewBag.DisposedProductList = getDisposedProducts();
            ViewBag.UserListMap = userService.GetAllUsers().ToDictionary(u => u.Id, u => u);
            if (print)
            {
                return new PartialViewAsPdf("~/Views/SchoolAdmin/Pdf/DisposedAssetPdf.cshtml")
                {
                    PageOrientation = Orientation.Portrait,
                    PageSize = Size.A3,
                    CustomSwitches = "--footer-center \" [page] Page of [toPage] Pages\" --footer-line --footer-font-size \"9\" --footer-spacing 5 --footer-font-name \"calibri light\"",
                    FileName = "DisposedAsset.pdf"
                };
            }
            return View();
        }

        [AppAuthorize(Roles = "MSO,Finance,ICT Engineer")]
        [HttpGet, ActionName("Dispatched")]
        public ActionResult DispatchedProducts(bool print = false)
        {
            logger.Info("Product Controller - List of Dispatched Products from database for school users");
            var soldProductsList = soldProductService.GetAllSoldProducts().Where(p => p.SchoolId == AppUtils.getCurrentUser().school.Id && p.Status == (int)SoldProductStatus.Dispatched).OrderByDescending(p => p.CreatedDate).ToList();
            ViewBag.SoldProducts = getSoldProductDetailList(soldProductsList);
            if (print)
            {
                return new PartialViewAsPdf("~/Views/SchoolAdmin/Pdf/DispatchedProductsPdf.cshtml")
                {
                    PageOrientation = Orientation.Portrait,
                    PageSize = Size.A3,
                    CustomSwitches = "--footer-center \" [page] Page of [toPage] Pages\" --footer-line --footer-font-size \"9\" --footer-spacing 5 --footer-font-name \"calibri light\"",
                    FileName = "DispatchedAsset.pdf"
                };
            }
            return View("~/Views/SchoolAdmin/DispatchedProducts.cshtml");
        }

        [AppAuthorize(Roles = "MSO,Finance,ICT Engineer")]
        [HttpGet, ActionName("Completed")]
        public ActionResult CompletedOrders(bool print = false)
        {
            logger.Info("Order Controller - Listing all the orders from database");
            ViewBag.OrderList = GetCompletedOrderDetails();
            if (print)
            {
                return new PartialViewAsPdf("~/Views/SchoolAdmin/Pdf/CompletedOrdersPdf.cshtml")
                {
                    PageOrientation = Orientation.Portrait,
                    PageSize = Size.A3,
                    CustomSwitches = "--footer-center \" [page] Page of [toPage] Pages\" --footer-line --footer-font-size \"9\" --footer-spacing 5 --footer-font-name \"calibri light\"",
                    FileName = "CompletedOrders.pdf"
                };
            }
            return View("CompletedOrders");
        }

        #region Help Methods

        private List<CompletedOrders> GetCompletedOrderDetails()
        {
            var OrderDetailList = getCompletedOrderList().ConvertAll(o =>
            {
                var productOrder = productOrderService.GetPOByOrderId(o.Id).FirstOrDefault();
                var product = productService.GetProductById(productOrder.ProductId ?? 0);
                var orderTrack = orderTrackService.GetOrderTrackByOrderId(o.Id);
                var soldProduct = soldProductService.GetSoldProductsByOrderId(o.Id).FirstOrDefault();
                return new CompletedOrders()
                {
                    Order = o,
                    ProductOrder = productOrder,
                    Product = product,
                    PurchaseDate = orderTrack.Where(ot => ot.UserRole == (int)UserRole.Finance && ot.IsDifferentSchool == Convert.ToBoolean(YNStatue.No)).Select(ot => ot.CreatedDate).FirstOrDefault(),
                    ScheduledDate = soldProduct.ScheduledDate ?? DateTime.Now
                };
            });
            return OrderDetailList;
        }

        private List<Order> getCompletedOrderList() => orderService.GetAllOrders().Where(o => o.OrderStatus == (int)OrderStatus.Completed && o.Deleted == Convert.ToBoolean(YNStatue.No) && o.SchoolId == AppUtils.getCurrentUser().school.Id).OrderByDescending(p => p.CreatedDate).ToList();

        private List<Product> getDraftedProducts() => productService.GetAllProducts().Where(p => p.Status == (int)ProductStatus.SavedAsDraft && p.UserId == AppUtils.getCurrentUser().user.Id && p.Deleted == Convert.ToBoolean(YNStatue.No)).OrderByDescending(p => p.CreatedDate).ToList();

        private List<Product> getPostedProducts() => productService.GetAllProducts().Where(p => (p.Status == (int)ProductStatus.Posted || p.Status == (int)ProductStatus.Published || p.Status == (int)ProductStatus.Approved) && p.Deleted == Convert.ToBoolean(YNStatue.No)).OrderByDescending(p => p.CreatedDate).ToList();

        private List<AssetDisposed> getDisposedProducts()
        {
            var productList = productService.GetAllProducts().Where(p => p.Status == (int)ProductStatus.Disposed && p.SchoolId == AppUtils.getCurrentUser().school.Id && p.Deleted == Convert.ToBoolean(YNStatue.No)).OrderByDescending(p => p.CreatedDate).ToList();

            var assetDiposedList = productList.ConvertAll(p =>
            {
                AssetTrack assetTrackDisposed;
                if (p.ApprovalReq)
                {
                    assetTrackDisposed = assetTrackService.GetPostTrackByAssetId(p.Id).Where(at => at.UserRole == (int)AdminRole.CFO).FirstOrDefault();
                }
                else
                {
                    assetTrackDisposed = assetTrackService.GetPostTrackByAssetId(p.Id).Where(at => at.UserRole == (int)AdminRole.CorpInternalAudit).FirstOrDefault();
                }
                var assetTrackPosted = assetTrackService.GetPostTrackByAssetId(p.Id).Where(at => at.UserRole == (int)UserRole.ICTEngineer).FirstOrDefault();
                return new AssetDisposed()
                {
                    Product = p,
                    DisposedDate = assetTrackDisposed.CreatedDate,
                    PostedDate = assetTrackPosted.CreatedDate,
                    School = AppUtils.GetSchoolDetails(p.SchoolId.Value)
                };
            });

            return assetDiposedList.OrderByDescending(p => p.DisposedDate).ToList();
        }

        private List<DraftProduct> getDraftList()
        {
            List<DraftProduct> Draft = new List<DraftProduct>();
            var DraftedProducts = getDraftedProducts().ConvertAll(p => new DraftProduct() { Id = p.Id, ProductName = p.ProductName, CreatedDate = p.CreatedDate ?? DateTime.Now, DraftType = (DraftType)p.AssetType, Product = p });
            Draft.AddRange(DraftedProducts);
            return Draft;
        }

        private List<ApprovalRequest> getApprovalRequest()
        {
            List<ApprovalRequest> approvalRequest = new List<ApprovalRequest>();
            var currentUser = AppUtils.getCurrentUser();
            var result = getPostedProducts().ToList();
            if (currentUser.user.Role.HasValue && currentUser.user.Role.Value == (int)AppEnumeration.NIAdminRole.GCOREALM)
            {
                result = result.Where(p => p.Status != (int)ProductStatus.Published).ToList();
            }
            if (AppUtils.getCurrentUser().user.Role != (int)Role.CorpIT)
            {
                result = result.Where(p => p.SchoolId == AppUtils.getCurrentUser().school.Id).ToList();
            }
            var productApprovalRequest = result.ConvertAll(p =>
            {
                var nextApproverName = string.Empty;
                var role = 0;
                if (p.AssetType == (int)AssetType.Reuse)
                {
                    role = productPostTrackService.getNextProductApproverByProductId(p);
                }
                else
                {
                    role = assetTrackService.getNextAssetApproverByAssetId(p);
                }

                if (role != 0)
                {
                    nextApproverName = EnumHelper<AppEnumeration.Role>.GetDisplayValue((AppEnumeration.Role)role);
                }

                return new ApprovalRequest()
                {
                    Id = p.Id,
                    ProductName = p.ProductName,
                    CreatedDate = p.CreatedDate ?? DateTime.Now,
                    PublishedDate = p.UpdatedDate ?? DateTime.Now,
                    NextApprover = nextApproverName,
                    NextApproverAsInt = role,
                    Product = p,
                    SchoolCode = p.SchoolId.HasValue ? AppUtils.GetSchoolDetails(p.SchoolId).SchoolCode : ""
                };
            });
            // added on  31 march
            //if (currentUser.user.Role == (int)Utils.AppEnumeration.AdminRole.CorpIT)
            //{
            //    productApprovalRequest = productApprovalRequest.Where(p => p.NextApproverAsInt == currentUser.user.Role).ToList();
            //}
            approvalRequest.AddRange(productApprovalRequest);
            approvalRequest.OrderByDescending(a => a.CreatedDate).ToList();
            return approvalRequest;
        }

        private List<SoldProductDetail> getSoldProductDetailList(List<SoldProduct> soldProductList)
            => soldProductList.ConvertAll(s =>
            {
                var schoolId = orderService.GetOrderById(s.OrderId).SchoolId;
                var school = schoolService.GetSchoolById(schoolId ?? 0);
                var product = productService.GetProductById(s.ProductId);
                var soldSchool = schoolService.GetSchoolById(product.SchoolId ?? 0);
                var productTrackList = productPostTrackService.GetPostTrackByProductId(product.Id);
                var publishedDate = productTrackList.Where(pt => pt.UserRole == (int)UserRole.Principal).Select(pt => pt.CreatedDate).FirstOrDefault();
                var orderTrackList = orderTrackService.GetOrderTrackByOrderId(s.OrderId);
                var buyerFinanceTrackEntry = orderTrackList.Where(ot => ot.UserRole == (int)UserRole.Finance && ot.IsDifferentSchool == Convert.ToBoolean(YNStatue.No)).FirstOrDefault();

                return new SoldProductDetail()
                {
                    SoldProduct = s,
                    Product = product,
                    ProductOrder = productOrderService.GetProductOrderById(s.ProductOrderId),
                    School = school,
                    SoldSchool = soldSchool,
                    OrderApprovedDate = buyerFinanceTrackEntry?.CreatedDate,
                    PublishedDate = publishedDate,
                    PickUpDate = s.ScheduledDate
                };
            });

        #endregion
    }
}