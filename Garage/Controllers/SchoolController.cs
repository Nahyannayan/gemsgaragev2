﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Garage.Models;
using Garage.Service;
using Garage.Utils.Security;
using Garage.ViewModels;
using static Garage.Utils.AppEnumeration;
using static Garage.Utils.Mapper.SchoolMapper;

namespace Garage.Controllers
{
    public class SchoolController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly SchoolService schoolService = new SchoolService();

        [AppAuthorize (Roles = "Corporate IT")]
        [HttpGet, ActionName("List")]
        public ActionResult SchoolList()
        {
            logger.Info("School Controller - Listing the School in Corporate admin page");

            ViewBag.SchoolList = getSchoolList();
            return View("SchoolList");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Add")]
        public ActionResult SchoolAdd()
        {
            logger.Info("School Controller - Create School form");
            return View("SchoolAdd", new SchoolForm() { Active = true });

        }

         [AppAuthorize]
        [HttpPost, ActionName("Add")]
        public ActionResult SchoolSave(SchoolForm schoolForm)
        {
            logger.Info("School Controller - Saving the School in the database");

            ViewBag.savedSchool = schoolService.AddSchool(map(schoolForm));
            return RedirectToAction("List", "School");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Edit")]
        public ActionResult EditSchool(string id)
        {
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            logger.Info("School Controller - Edit the School in the database");

            School school = schoolService.GetSchoolById(OriginalID);
            SchoolForm schoolForm = remap(school);
            schoolForm.EncryptedId = id;
            return View("SchoolEdit", schoolForm);
        }

        [AppAuthorize]
        [HttpPost, ActionName("Edit")]
        public ActionResult UpdateSchool(SchoolForm schoolForm)
        {
            logger.Info("School Controller - Updating the exsisting school");
            schoolForm.Id = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(schoolForm.EncryptedId));
            School school = schoolService.GetSchoolById(schoolForm.Id);
            schoolService.UpdateSchool(map(schoolForm, school));
            return RedirectToAction("List", "School");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Delete")]
        public ActionResult DeleteSchool(string id)
        {
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            logger.Info("School Controller - Delete the School record from Database");

            School school = schoolService.GetSchoolById(OriginalID);
            schoolService.DeleteSchool(school);
            return RedirectToAction("List", "School");
        }


        #region Help Methods

        private List<School> getSchoolList() => schoolService.GetAllSchools().Where(s => s.Deleted == Convert.ToBoolean(YNStatue.No)).ToList();

        #endregion

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            logger.Error(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "ErrorHandler");
        }
    }
}
