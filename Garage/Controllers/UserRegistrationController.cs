﻿using Garage.Models;
using Garage.Service;
using Garage.Utils.Security;
using Garage.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Garage.Utils.AppEnumeration;
using static Garage.Utils.Mapper.UserMapper;


namespace Garage.Controllers
{
    public class UserRegistrationController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly UserService userService = new UserService();
        private readonly SchoolService schoolService = new SchoolService();

        [AppAuthorize (Roles = "Corporate IT")]
        [HttpGet, ActionName("List")]
        public ActionResult UserList()
        {
            logger.Info("User Registration Controller - Listing all the users in the database");

            ViewBag.userList = userService.GetAllUsers().Where(u => u.Deleted == Convert.ToBoolean(YNStatue.No) && u.IsITAccess == true).OrderByDescending(u => u.CreatedDate).ToList();
            return View("UserRegistrationList");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Add")]
        public ActionResult UserAdd()
        {
            logger.Info("User Registration Controller - Create a User Registration Form");

            ViewBag.SchoolList = getSchoolSelectList();
            return View("UserRegistrationAdd", new UserRegistrationForm() { Active = true });
        }

        [AppAuthorize]
        [HttpPost, ActionName("Save")]
        public ActionResult UserSave(UserRegistrationForm userRegistrationForm)
        {
            logger.Info("User Registration Controller - Saving the user in the database");

            ViewBag.savedCategory = userService.AddUser(map(userRegistrationForm));
            return RedirectToAction("List", "UserRegistration");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Edit")]
        public ActionResult UserEdit(string id)
        {
            logger.Info("User Registration Controller - Edit the user");
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            UserRegistration userRegistration = userService.GetUserById(OriginalID);
            UserRegistrationForm userRegistrationForm = remap(userRegistration);
            userRegistrationForm.EncryptedId = id;
            ViewBag.SchoolList = getSchoolSelectList();
            return View("UserRegistrationEdit", userRegistrationForm);
        }

        [AppAuthorize]
        [HttpPost, ActionName("Update")]
        public ActionResult UpdateUserRegistration(UserRegistrationForm userRegistrationForm)
        {
            logger.Info("User Registration Controller - update the exsisting user from database");
            userRegistrationForm.Id = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(userRegistrationForm.EncryptedId));
            UserRegistration userRegistration = userService.GetUserById(userRegistrationForm.Id);
            userService.UpdateUser(map(userRegistrationForm, userRegistration));
            return RedirectToAction("List", "UserRegistration");
        }

        [AppAuthorize]
        [HttpGet, ActionName("Delete")]
        public ActionResult UserRegistration(string id)
        {
            int OriginalID = Convert.ToInt32(Garage.Security.DataProtection.Decrypt(id));
            logger.Info("User Registration Controller - Delete Individual User from Database");

            UserRegistration userRegistration = userService.GetUserById(OriginalID);
            var deletedUser = userService.DeleteUser(userRegistration);

            return RedirectToAction("List", "UserRegistration");
        }

        [HttpPost]
        public JsonResult CheckEmailId(string email)
        {
            UserRegistration userRegistration = new UserRegistration();
            bool isValid = !userService.GetAllUsers().Where(u => u.Deleted == Convert.ToBoolean(YNStatue.No)).ToList().Exists(p => p.Email.Equals(email, StringComparison.CurrentCultureIgnoreCase));
            return Json(isValid);
        }

        #region Help Methods

        private List<School> getSchoolList() => schoolService.GetAllSchools().Where(s => s.Deleted == Convert.ToBoolean(YNStatue.No)).ToList();

        private List<SelectListItem> getSchoolSelectList() => getSchoolList().ConvertAll(s =>
                {
                    return new SelectListItem()
                    {
                        Text = s.SchoolName + " - " + s.SchoolCode,
                        Value = s.Id.ToString(),
                        Selected = false
                    };
                });


        #endregion

        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;
            logger.Error(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "ErrorHandler");
        }


    }
}