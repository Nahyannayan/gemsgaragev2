﻿

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace Garage.Models
{

using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;


public partial class GarageContext : DbContext
{
    public GarageContext()
        : base("name=GarageContext")
    {

    }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
        throw new UnintentionalCodeFirstException();
    }


    public virtual DbSet<Asset> Assets { get; set; }

    public virtual DbSet<AssetImage> AssetImages { get; set; }

    public virtual DbSet<AssetTrack> AssetTracks { get; set; }

    public virtual DbSet<Category> Categories { get; set; }

    public virtual DbSet<Order> Orders { get; set; }

    public virtual DbSet<ProductImage> ProductImages { get; set; }

    public virtual DbSet<ProductOrder> ProductOrders { get; set; }

    public virtual DbSet<ProductPostTrack> ProductPostTracks { get; set; }

    public virtual DbSet<School> Schools { get; set; }

    public virtual DbSet<SessionObj> SessionObjs { get; set; }

    public virtual DbSet<SoldProduct> SoldProducts { get; set; }

    public virtual DbSet<UserRegistration> UserRegistrations { get; set; }

    public virtual DbSet<Product> Products { get; set; }

    public virtual DbSet<OrderTrack> OrderTracks { get; set; }

}

}

