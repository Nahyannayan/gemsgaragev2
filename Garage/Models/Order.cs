
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace Garage.Models
{

using System;
    using System.Collections.Generic;
    
public partial class Order
{

    public int Id { get; set; }

    public string OrderNo { get; set; }

    public Nullable<int> OrderStatus { get; set; }

    public string Remark { get; set; }

    public Nullable<int> SchoolId { get; set; }

    public Nullable<int> CreatedBy { get; set; }

    public Nullable<System.DateTime> CreatedDate { get; set; }

    public Nullable<int> UpdatedBy { get; set; }

    public Nullable<System.DateTime> UpdatedDate { get; set; }

    public Nullable<bool> Deleted { get; set; }

}

}
