﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Garage.Repository
{
    interface IGenericRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T GetById(object id);
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);
        T Insert(T obj);
        IEnumerable<T> InsertAll(IEnumerable<T> obj);
        T Update(T obj);
        T Delete(object id);
        void Save();
    }
}
