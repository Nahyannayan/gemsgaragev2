﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Garage.Security
{
    public static class DataProtection
    {
        public static string _securityKey =   ConfigurationManager.AppSettings["securityKey"].ToString();  


        public static string Encrypt(object PlainText)
        {

            byte[] toEncryptedArray = UTF8Encoding.UTF8.GetBytes(PlainText.ToString());

            MD5CryptoServiceProvider objMD5CryptoService = new MD5CryptoServiceProvider();


            byte[] securityKeyArray = objMD5CryptoService.ComputeHash(UTF8Encoding.UTF8.GetBytes(_securityKey));

            objMD5CryptoService.Clear();

            var objTripleDESCryptoService = new TripleDESCryptoServiceProvider();

            objTripleDESCryptoService.Key = securityKeyArray;

            objTripleDESCryptoService.Mode = CipherMode.ECB;

            objTripleDESCryptoService.Padding = PaddingMode.PKCS7;

            var objCrytpoTransform = objTripleDESCryptoService.CreateEncryptor();

            byte[] resultArray = objCrytpoTransform.TransformFinalBlock(toEncryptedArray, 0, toEncryptedArray.Length);

            objTripleDESCryptoService.Clear();

            StringBuilder sbBytes = new StringBuilder(resultArray.Length * 2);
            foreach (byte b in resultArray)
            {
                sbBytes.AppendFormat("{0:X2}", b);
            }
            return sbBytes.ToString();
        }


        public static string Decrypt(object CipherText)
        {
            int numberChars = CipherText.ToString().Length;
            byte[] toEncryptArray = new byte[numberChars / 2];

            for (int i = 0; i < numberChars; i += 2)
            {
                toEncryptArray[i / 2] = Convert.ToByte(CipherText.ToString().Substring(i, 2), 16);
            }

            MD5CryptoServiceProvider objMD5CryptoService = new MD5CryptoServiceProvider();

            byte[] securityKeyArray = objMD5CryptoService.ComputeHash(UTF8Encoding.UTF8.GetBytes(_securityKey));

            objMD5CryptoService.Clear();

            var objTripleDESCryptoService = new TripleDESCryptoServiceProvider();

            objTripleDESCryptoService.Key = securityKeyArray;

            objTripleDESCryptoService.Mode = CipherMode.ECB;

            objTripleDESCryptoService.Padding = PaddingMode.PKCS7;

            var objCrytpoTransform = objTripleDESCryptoService.CreateDecryptor();

            byte[] resultArray = objCrytpoTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            objTripleDESCryptoService.Clear();

            return UTF8Encoding.UTF8.GetString(resultArray);

        }

    }
}