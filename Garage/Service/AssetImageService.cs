﻿using Garage.Models;
using Garage.Repository;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using static Garage.Utils.AppEnumeration;

namespace Garage.Service
{
    public class AssetImageService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private GenericRepository<AssetImage> assetImageRepository = null;

        public AssetImageService()
        {
            this.assetImageRepository = new GenericRepository<AssetImage>();
        }

        public AssetImageService(GarageContext context)
        {
            this.assetImageRepository = new GenericRepository<AssetImage>(context);
        }

        public IEnumerable<AssetImage> GetAllAssetImages() => assetImageRepository.GetAll().Where(a => a.Deleted == Convert.ToBoolean(YNStatue.No));

        public AssetImage GetAssetImageById(int id) => assetImageRepository.GetById(id);

        public AssetImage AddAssetImage(AssetImage assetImage)
        {
            logger.Info("save asset image to DB");
            var savedAssetImage = assetImageRepository.Insert(assetImage);
            assetImageRepository.Save();
            logger.Info("saved asset image to DB");
            return savedAssetImage;
        }

        public IEnumerable<AssetImage> AddAssetImageList(List<AssetImage> categories)
        {
            logger.Info("save list of asset image to DB");
            var savedAssetImage = assetImageRepository.InsertAll(categories);
            assetImageRepository.Save();
            logger.Info("saved list of asset image to DB");
            return savedAssetImage;
        }

        public AssetImage UpdateAssetImage(AssetImage assetImage)
        {
            logger.Info("update asset image to DB");
            var updatedAssetImage = assetImageRepository.Update(assetImage);
            assetImageRepository.Save();
            logger.Info("update asset image to DB");
            return updatedAssetImage;
        }

        public AssetImage DeleteAssetImage(AssetImage assetImage)
        {
            logger.Info("delete asset image to DB");
            assetImage.Deleted = Convert.ToBoolean(YNStatue.Yes);
            var deletedAssetImage = assetImageRepository.Update(assetImage);
            assetImageRepository.Save();
            logger.Info("deleted asset image to DB");
            return deletedAssetImage;
        }

        public AssetImage GetSingleAssetImageByAssetId(int assetId) => assetImageRepository.GetAll().Where(ai => ai.AssetId == assetId).FirstOrDefault();
        
    }
}