﻿using Garage.Models;
using Garage.Repository;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using static Garage.Utils.AppEnumeration;

namespace Garage.Service
{
    public class AssetService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private GenericRepository<Asset> assetRepository = null;

        public AssetService()
        {
            this.assetRepository = new GenericRepository<Asset>();
        }

        public AssetService(GarageContext context)
        {
            this.assetRepository = new GenericRepository<Asset>(context);
        }

        public IEnumerable<Asset> GetAllAssets() => assetRepository.GetAll().Where(a => a.Deleted == Convert.ToBoolean(YNStatue.No));

        public Asset GetAssetById(int id) => assetRepository.GetById(id);

        public Asset AddAsset(Asset asset)
        {
            logger.Info("save asset to DB");
            var savedAsset = assetRepository.Insert(asset);
            assetRepository.Save();
            logger.Info("saved asset to DB");
            return savedAsset;
        }

        public IEnumerable<Asset> AddAssetList(List<Asset> assets)
        {
            logger.Info("save list of asset to DB");
            var savedAsset = assetRepository.InsertAll(assets);
            assetRepository.Save();
            logger.Info("saved list of asset to DB");
            return savedAsset;
        }

        public Asset UpdateAsset(Asset asset)
        {
            logger.Info("update asset to DB");
            var updatedAsset = assetRepository.Update(asset);
            assetRepository.Save();
            logger.Info("updated asset to DB");
            return updatedAsset;
        }

        public Asset DeleteAsset(Asset asset)
        {
            logger.Info("delete asset to DB");
            asset.Deleted = Convert.ToBoolean(YNStatue.Yes);
            var deletedAsset = assetRepository.Update(asset);
            assetRepository.Save();
            logger.Info("deleted asset to DB");
            return deletedAsset;
        }

        public IEnumerable<Asset> GetAllDisposedAssets() => assetRepository.GetAll().Where(p => p.Status == (int)ProductStatus.Disposed);
    }
}