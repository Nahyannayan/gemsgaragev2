﻿using Garage.Models;
using Garage.Repository;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using static Garage.Utils.AppEnumeration;

namespace Garage.Service
{
    public class AssetTrackService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private GenericRepository<AssetTrack> assetTrackRepository = null;

        public AssetTrackService()
        {
            this.assetTrackRepository = new GenericRepository<AssetTrack>();
        }

        public AssetTrackService(GarageContext context)
        {
            this.assetTrackRepository = new GenericRepository<AssetTrack>(context);
        }

        public IEnumerable<AssetTrack> GetAllAssetTrack() => assetTrackRepository.GetAll().Where(a => a.Deleted == Convert.ToBoolean(YNStatue.No));

        public AssetTrack GetPostTrackById(int id) => assetTrackRepository.GetById(id);

        public AssetTrack AddAssetTrack(AssetTrack assetTrack)
        {
            logger.Info("save product post track to DB");
            var savedAssetTrack = assetTrackRepository.Insert(assetTrack);
            assetTrackRepository.Save();
            logger.Info("saved product post track to DB");
            return savedAssetTrack;
        }

        public IEnumerable<AssetTrack> AddAssetTrackList(List<AssetTrack> assetTracks)
        {
            logger.Info("save list of product post track to DB");
            var savedAssetTrack = assetTrackRepository.InsertAll(assetTracks);
            assetTrackRepository.Save();
            logger.Info("saved list of product post track to DB");
            return savedAssetTrack;
        }

        public AssetTrack UpdateProduct(AssetTrack assetTrack)
        {
            logger.Info("update product post track to DB");
            var updatedAssetTrack = assetTrackRepository.Update(assetTrack);
            assetTrackRepository.Save();
            logger.Info("updated product post track to DB");
            return updatedAssetTrack;
        }

        public AssetTrack DeleteAssetTrack(AssetTrack assetTrack)
        {
            logger.Info("delete product post track to DB");
            assetTrack.Deleted = Convert.ToBoolean(YNStatue.Yes);
            var deletedAssetTrack = assetTrackRepository.Update(assetTrack);
            assetTrackRepository.Save();
            logger.Info("deleted product post track to DB");
            return deletedAssetTrack;
        }

        public IEnumerable<AssetTrack> DeleteAssetTrackByProduct(int assetId)
        {
            logger.Info("Delete all product post track by Product Id to DB");
            var deletedProductTrack = assetTrackRepository.GetAll().Where(a => a.AssetId == assetId);
            deletedProductTrack = assetTrackRepository.DeleteAll(deletedProductTrack);
            logger.Info("deleted product post track to DB");
            return deletedProductTrack;
        }

        public List<AssetTrack> GetPostTrackByAssetId(int id) => assetTrackRepository.GetAll().Where(p => p.AssetId == id).ToList();

        public int getNextAssetApproverByAssetId(Product product)
        {
            int result = 0;
            var assetTrackList = GetPostTrackByAssetId(product.Id);
            if (assetTrackList != null && assetTrackList.Count > 0)
            {
                var role = assetTrackList.OrderByDescending(pt => pt.CreatedDate).FirstOrDefault().UserRole;
                if (role != (int)Role.CFO)
                {
                    result = (int)((Role)role + 1);
                }
            }
            return result;
        }

    }
}