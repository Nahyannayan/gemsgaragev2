﻿using Garage.Models;
using Garage.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;
using System.ComponentModel.DataAnnotations;
using log4net;
using System.Reflection;

namespace Garage.Service
{
    public class CategoryService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private GenericRepository<Category> categoryRepository = null;

        public CategoryService()
        {
            this.categoryRepository = new GenericRepository<Category>();
        }

        public CategoryService(GarageContext context)
        {
            this.categoryRepository = new GenericRepository<Category>(context);
        }

        public IEnumerable<Category> GetAllCategories() => categoryRepository.GetAll().Where(c => c.Deleted == Convert.ToBoolean(YNStatue.No));

        public Category GetCategoryById(int id) => categoryRepository.GetById(id);

        public Category AddCategory(Category category)
        {
            logger.Info("save category to DB");
            var savedCategory = categoryRepository.Insert(category);
            categoryRepository.Save();
            logger.Info("saved category to DB");
            return savedCategory;
        }
        
        public IEnumerable<Category> AddCategoryList(List<Category> categories)
        {
            logger.Info("save list of category to DB");
            var savedCategory = categoryRepository.InsertAll(categories);
            categoryRepository.Save();
            logger.Info("saved list of categories to DB");
            return savedCategory;
        }

        public Category UpdateCategory(Category category)
        {
            logger.Info("update category to DB");
            var updatedCategory = categoryRepository.Update(category);
            categoryRepository.Save();
            logger.Info("updated category to DB");
            return updatedCategory;
        }

        public Category DeleteCategory(Category category)
        {
            logger.Info("delete category to DB");
            category.Deleted = Convert.ToBoolean(YNStatue.Yes);
            var deletedCategory = categoryRepository.Update(category);
            categoryRepository.Save();
            logger.Info("deleted category to DB");
            return deletedCategory;
        }

    }
}