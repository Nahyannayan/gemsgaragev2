﻿using log4net;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Reflection;
using System.Resources;

namespace Garage.Service.Email
{
    public class AppEmailService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private ResourceManager resourceManager = new ResourceManager("Garage.EmailData", Assembly.GetExecutingAssembly());
        private string fromEmail = "gemsgarage@gemseducation.com";

        public void send(List<string> toAddresses, string subject, string body)
        {
            try
            {
                logger.Info("Preparing recipient for mail");
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(fromEmail);
                toAddresses.ForEach(a => mail.To.Add(a));
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
#if !DEBUG
                //SmtpServer().Send(mail);
#endif

#if DEBUG
                //GmailSmtpServer().Send(mail);
#endif
                logger.Info("Mail has been sent successfully");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                //throw new Exception(ex.Message);
            }
            finally
            {
                // final cleanup code
            }
        }

        public void send(string toAddress, string subject, string body)
        {
            try
            {
                logger.Info("Preparing recipient for mail");
                MailMessage mail = new MailMessage(fromEmail, toAddress, subject, body);
                mail.IsBodyHtml = true;
#if !DEBUG
               // SmtpServer().Send(mail);
#endif

#if DEBUG
                //GmailSmtpServer().Send(mail);
#endif
                logger.Info("Mail has been sent successfully");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                //throw new Exception(ex.Message);
            }
            finally
            {
                // final cleanup code
            }
        }

        private SmtpClient SmtpServer()
        {
            SmtpClient SmtpServer = null;
            try
            {
                SmtpServer = new SmtpClient(resourceManager.GetString("Host"));
                SmtpServer.Port = Convert.ToInt32(resourceManager.GetString("Port"));
                SmtpServer.UseDefaultCredentials = true;
                SmtpServer.EnableSsl = false;
                //SmtpServer.Credentials = new System.Net.NetworkCredential(resourceManager.GetString("Username"), resourceManager.GetString("Password"));
                //SmtpServer.EnableSsl = true;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                //throw new Exception(ex.Message);
            }
            finally
            {

            }
            return SmtpServer;
        }

        private SmtpClient GmailSmtpServer()
        {
            SmtpClient SmtpServer = null;
            try
            {
                ResourceManager resourceManagerForGmail = new ResourceManager("Garage.GmailData", Assembly.GetExecutingAssembly());
                SmtpServer = new SmtpClient(resourceManagerForGmail.GetString("Host"));
                SmtpServer.Port = Convert.ToInt32(resourceManagerForGmail.GetString("Port"));
                SmtpServer.Credentials = new System.Net.NetworkCredential(resourceManagerForGmail.GetString("Username"), resourceManagerForGmail.GetString("Password"));
                SmtpServer.EnableSsl = true;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                //throw new Exception(ex.Message);
            }
            finally
            {

            }
            return SmtpServer;
        }
    }
}