﻿using Garage.Models;
using Garage.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.Service.Email.EmailModels
{
    public class OrderApproval
    {
        public List<ProductDetail>  ProductDetailList { get; set; }
        public List<ProductOrder>  ProductOrderList { get; set; }
    }
}