﻿using Garage.Models;
using Garage.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.Service.Email.EmailModels
{
    public class OrderScheduled
    {
        public ProductDetail  ProductDetail { get; set; }
        public ProductOrder  ProductOrder { get; set; }
        public SoldProduct SoldProduct { get; set; }
        public School School { get; set; }
    }
}