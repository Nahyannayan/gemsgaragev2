﻿using Garage.Models;
using Garage.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.Service.Email.EmailModels
{
    public class ProductApproval
    {
        public ProductDetail ProductDetail { get; set; }
        public ProductImage ProductImage { get; set; }
    }
}