﻿using System.IO;

namespace Garage.Service.Email
{
    public static class TemplateExtensions
    {
        public static string ReadTemplateContent(this string path)
        {
            string content;
            using (var reader = new StreamReader(path))
            {
                content = reader.ReadToEnd();
            }

            return content;
        }
    }
}