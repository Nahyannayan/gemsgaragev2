﻿using Garage.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Web;

namespace Garage.Service.GemsEmail
{
    public class GemsToken
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void GetToken()
        {
            var client = new HttpClient();

            var dataObj = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("grant_type", "password"),
                new KeyValuePair<string, string>("username", "GARAGE.GEMS"),
                new KeyValuePair<string, string>("password", "garageapi2020"),
                new KeyValuePair<string, string>("AppId", "5745")
            };
            var content = new FormUrlEncodedContent(dataObj);
            content.Headers.Clear();
            content.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

            if (HttpContext.Current.Request.Cookies["gemstoken"] == null)
            {
                HttpResponseMessage response = client.PostAsync("https://phoenixapi.gemseducation.com/gemsapi/token", content).Result;  // Blocking call!  
                if (response.IsSuccessStatusCode)
                {
                    var jsonData = response.Content.ReadAsStringAsync().Result;
                    var responseData = new
                    {
                        access_token = "",
                        token_type = "",
                        expires_in = "",
                        userName = "",
                        issued = "",
                        expires = ""
                    };

                    var result = JsonConvert.DeserializeAnonymousType(jsonData, responseData);
                    HttpCookie cookie = new HttpCookie("gemstoken");
                    cookie.Values.Set("token", result.access_token);
                    cookie.Expires = DateTime.Now.AddHours(7);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }

                //logger.Info("GemsToken - Fetch Gems Email Token : " + result);
            }
        }

        public static void SendMail(List<string> toAddressess, string subject, string message, bool NONITAssets = false)
        {
            try
            {
                GetToken();
                var cookie = HttpContext.Current.Request.Cookies["gemstoken"];
                var token = cookie["token"];
                var toEmail = string.Join(",", toAddressess);
                var dataObj = new
                {
                    EmailType = "GEMSGARAGE",
                    ToEmail = toEmail,
                    CCEmail = "",
                    Subject = subject,
                    Message = message

                };

                var json = JsonConvert.SerializeObject(dataObj);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                var url = "https://phoenixapi.gemseducation.com/gemsapi/api/Circulation/SendEmail";
                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
                var response = client.PostAsync(url, data).Result;

                string result = response.Content.ReadAsStringAsync().Result;
                logger.Info("GemsToken - After Send Email : " + result);
                (new Areas.Non_IT.Services.NIGeneralService()).AddEmailLogs(new Areas.Non_IT.Models.EmailLog()
                {
                    TOADDRESS = toEmail,
                    EMAILBODY = message,
                    SUBJECT = subject,
                    ISNON_IT = NONITAssets,
                    RESPONSE = result,
                    CREATEDBY = NONITAssets == false ? AppUtils.getCurrentUser().user.Id : Areas.Non_IT.AppUtils.getCurrentUser().user.Id,
                    CREATEDDATE = DateTime.Now

                });
            }
            catch (Exception ex)
            {
                (new Areas.Non_IT.Services.NIGeneralService()).AddEmailLogs(new Areas.Non_IT.Models.EmailLog()
                {
                    TOADDRESS = string.Join(",", toAddressess),
                    EMAILBODY = message,
                    SUBJECT = subject,
                    ISNON_IT = NONITAssets,
                    RESPONSE = ex.Message,
                    CREATEDBY = NONITAssets == false ? AppUtils.getCurrentUser().user.Id : Areas.Non_IT.AppUtils.getCurrentUser().user.Id,
                    CREATEDDATE = DateTime.Now

                });

            }
        }
    }
}