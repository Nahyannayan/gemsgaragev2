﻿using Garage.Models;
using Garage.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.Mapper.OrderMapper;
using static Garage.Utils.Mapper.ProductOrderMapper;
using static Garage.Utils.Mapper.SoldProductMapper;
using static Garage.Utils.Mapper.OrderTrackMapper;
using static Garage.Utils.AppEnumeration;
using System.ComponentModel.DataAnnotations;
using log4net;
using System.Reflection;
using Garage.ViewModels;

namespace Garage.Service
{
    public class OrderService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private GenericRepository<Order> orderRepository = null;

        private ProductOrderService productOrderService = new ProductOrderService();
        private ProductService productService = new ProductService();
        private SoldProductService soldProductService = new SoldProductService();
        private OrderTrackService orderTrackService = new OrderTrackService();

        public OrderService()
        {
            this.orderRepository = new GenericRepository<Order>();
        }

        public OrderService(GarageContext context)
        {
            this.orderRepository = new GenericRepository<Order>(context);
        }

        public IEnumerable<Order> GetAllOrders() => orderRepository.GetAll().Where(o => o.Deleted == Convert.ToBoolean(YNStatue.No));


        public Order GetOrderById(int id) => orderRepository.GetById(id);

        public Order AddOrder(Order order)
        {
            logger.Info("save order to DB");
            var savedOrder = orderRepository.Insert(order);
            orderRepository.Save();
            logger.Info("saved order to DB");
            return savedOrder;
        }
        
        public IEnumerable<Order> AddOrderList(List<Order> categories)
        {
            logger.Info("save list of orders to DB");
            var savedOrder = orderRepository.InsertAll(categories);
            orderRepository.Save();
            logger.Info("saved list of orders to DB");
            return savedOrder;
        }

        public Order UpdateOrder(Order order)
        {
            logger.Info("update order to DB");
            var updatedOrder = orderRepository.Update(order);
            orderRepository.Save();
            logger.Info("updated order to DB");
            return updatedOrder;
        }

        public Order DeleteOrder(Order order)
        {
            logger.Info("delete order to DB");
            order.Deleted = Convert.ToBoolean(YNStatue.Yes);
            var deletedOrder = orderRepository.Update(order);
            orderRepository.Save();
            logger.Info("deleted order to DB");
            return deletedOrder;
        }

        public decimal getSoldQuantityByProductId(int productId)
        {
            var productOrders = productOrderService.GetAllProductOrders();
            var orders = GetAllOrders();
            var productOrderList = productOrders
                .Join(orders, po => po.OrderId, o => o.Id, (po, o) => new { po, o })
                .Where(obj => obj.o.OrderStatus == (int)OrderStatus.Procured && obj.po.ProductId == productId && obj.po.Deleted == Convert.ToBoolean(YNStatue.No))
                .Select(obj => obj.po);

            return productOrderList.Sum(po => po.Quantity)??0;
        }
        
        public Order PlaceOrder(List<ProductOrderForm> productOrderForms)
        {
            Order savedOrder = null;
            var index = 0;
            foreach (var productOrderForm in productOrderForms)
            {
                savedOrder = AddOrder(map(index));// Create New Order
                var productOrder = productOrderService.AddProductOrder(map(savedOrder, productOrderForm));// Create New Product Order Again Order
                var product = productService.GetProductById(productOrder.ProductId ?? 0);
                product.InStock = product.InStock - productOrder.Quantity;// Reduce and Update Product Quantity for Product Order
                productService.UpdateProduct(product);
                orderTrackService.AddOrderTrack(orderTrackMap(savedOrder));// Initiate Track entry for this order
                soldProductService.AddSoldProductList(map(savedOrder)); // Add sold Product for all order
                index = index + 1;
            }
            return savedOrder;
        }
    }
}