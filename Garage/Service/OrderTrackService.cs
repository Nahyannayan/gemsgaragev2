﻿using Garage.Models;
using Garage.Repository;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using static Garage.Utils.AppEnumeration;

namespace Garage.Service
{
    public class OrderTrackService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private GenericRepository<OrderTrack> orderTrackRepository = null;

        public OrderTrackService()
        {
            this.orderTrackRepository = new GenericRepository<OrderTrack>();
        }

        public OrderTrackService(GarageContext context)
        {
            this.orderTrackRepository = new GenericRepository<OrderTrack>(context);
        }

        public IEnumerable<OrderTrack> GetAllOrderTracks() => orderTrackRepository.GetAll().Where(o => o.Deleted == Convert.ToBoolean(YNStatue.No));

        public OrderTrack GetOrderTrackById(int id) => orderTrackRepository.GetById(id);

        public OrderTrack AddOrderTrack(OrderTrack orderTrack)
        {
            logger.Info("save order track to DB");
            var savedOrderTrack = orderTrackRepository.Insert(orderTrack);
            orderTrackRepository.Save();
            logger.Info("saved order track to DB");
            return savedOrderTrack;
        }

        public IEnumerable<OrderTrack> AddOrderTrackList(List<OrderTrack> orderTracks)
        {
            logger.Info("save list of order track to DB");
            var savedOrderTrack = orderTrackRepository.InsertAll(orderTracks);
            orderTrackRepository.Save();
            logger.Info("saved list of order track to DB");
            return savedOrderTrack;
        }

        public OrderTrack UpdateOrderTrack(OrderTrack orderTrack)
        {
            logger.Info("update order track to DB");
            var updatedOrderTrack = orderTrackRepository.Update(orderTrack);
            orderTrackRepository.Save();
            logger.Info("updated order track to DB");
            return updatedOrderTrack;
        }

        public OrderTrack DeleteOrderTrack(OrderTrack orderTrack)
        {
            logger.Info("delete order track to DB");
            orderTrack.Deleted = Convert.ToBoolean(YNStatue.Yes);
            var deletedOrderTrack = orderTrackRepository.Update(orderTrack);
            orderTrackRepository.Save();
            logger.Info("deleted order track to DB");
            return deletedOrderTrack;
        }

        public List<OrderTrack> GetOrderTrackByOrderId(int id) => orderTrackRepository.GetAll().Where(p => p.OrderId == id).ToList();

    }
}