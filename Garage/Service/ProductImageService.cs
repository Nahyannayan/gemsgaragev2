﻿using Garage.Models;
using Garage.Repository;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using static Garage.Utils.AppEnumeration;

namespace Garage.Service
{
    public class ProductImageService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private GenericRepository<ProductImage> productImageRepository = null;

        public ProductImageService()
        {
            this.productImageRepository = new GenericRepository<ProductImage>();
        }

        public ProductImageService(GarageContext context)
        {
            this.productImageRepository = new GenericRepository<ProductImage>(context);
        }

        public IEnumerable<ProductImage> GetAllProductImages() => productImageRepository.GetAll().Where(p => p.Deleted == Convert.ToBoolean(YNStatue.No));

        public ProductImage GetProductImageById(int id) => productImageRepository.GetById(id);

        public ProductImage AddProductImage(ProductImage productImage)
        {
            logger.Info("save product image to DB");
            
            var savedProductImage = productImageRepository.Insert(productImage);
            productImageRepository.Save();
            logger.Info("saved product image to DB");
            return savedProductImage;
        }

        public IEnumerable<ProductImage> AddProductImageList(List<ProductImage> categories)
        {
            logger.Info("save list of product image to DB");
            var savedProductImage = productImageRepository.InsertAll(categories);
            productImageRepository.Save();
            logger.Info("saved list of product image to DB");
            return savedProductImage;
        }

        public ProductImage UpdateProductImage(ProductImage productImage)
        {
            logger.Info("update product image to DB");
            var updatedProductImage = productImageRepository.Update(productImage);
            productImageRepository.Save();
            logger.Info("update product image to DB");
            return updatedProductImage;
        }

        public ProductImage DeleteProductImage(ProductImage productImage)
        {
            logger.Info("delete product image to DB");
            productImage.Deleted = Convert.ToBoolean(YNStatue.Yes);
            var deletedProductImage = productImageRepository.Update(productImage);
            productImageRepository.Save();
            logger.Info("deleted product image to DB");
            return deletedProductImage;
        }

       

        public ProductImage GetSingleProductImageByProductId(int productId) => productImageRepository.GetAll().Where(pi => pi.ProductId == productId && pi.IsProductImage == true).FirstOrDefault();

        public List<ProductImage> GetProductImageByProductId(int productId) => productImageRepository.GetAll().Where(pi => pi.ProductId == productId && pi.IsProductImage == true).ToList();

        public List<ProductImage> GetProductRefFileByProductId(int productId) => productImageRepository.GetAll().Where(pi => pi.ProductId == productId && pi.IsProductImage == false).ToList();

    }
}