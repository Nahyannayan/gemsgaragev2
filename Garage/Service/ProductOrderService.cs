﻿using Garage.Models;
using Garage.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;
using System.ComponentModel.DataAnnotations;
using log4net;
using System.Reflection;

namespace Garage.Service
{
    public class ProductOrderService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private GenericRepository<ProductOrder> productOrderRepository = null;

        public ProductOrderService()
        {
            this.productOrderRepository = new GenericRepository<ProductOrder>();
        }

        public ProductOrderService(GarageContext context)
        {
            this.productOrderRepository = new GenericRepository<ProductOrder>(context);
        }

        public IEnumerable<ProductOrder> GetAllProductOrders() => productOrderRepository.GetAll().Where(p =>p.Deleted == Convert.ToBoolean(YNStatue.No));

        public ProductOrder GetProductOrderById(int id) => productOrderRepository.GetById(id);

        public ProductOrder AddProductOrder(ProductOrder productOrder)
        {
            logger.Info("save product order to DB");
            var savedProductOrder = productOrderRepository.Insert(productOrder);
            productOrderRepository.Save();
            logger.Info("saved product order to DB");
            return savedProductOrder;
        }
        
        public IEnumerable<ProductOrder> AddProductOrderList(List<ProductOrder> categories)
        {
            logger.Info("save list of product order to DB");
            var savedProductOrder = productOrderRepository.InsertAll(categories);
            productOrderRepository.Save();
            logger.Info("saved list of product order to DB");
            return savedProductOrder;
        }

        public ProductOrder UpdateProductOrder(ProductOrder productOrder)
        {
            logger.Info("update product order to DB");
            var updatedProductOrder = productOrderRepository.Update(productOrder);
            productOrderRepository.Save();
            logger.Info("updated product order to DB");
            return updatedProductOrder;
        }

        public ProductOrder DeleteProductOrder(ProductOrder productOrder)
        {
            logger.Info("delete product order to DB");
            productOrder.Deleted = Convert.ToBoolean(YNStatue.Yes);
            var deletedProductOrder = productOrderRepository.Update(productOrder);
            productOrderRepository.Save();
            logger.Info("updated product order to DB");
            return deletedProductOrder;
        }

        public List<ProductOrder> GetPOByOrderId(int id) => productOrderRepository.GetAll().Where(p => p.OrderId == id && p.Deleted == Convert.ToBoolean(YNStatue.No)).ToList();

        public List<ProductOrder> GetPOByProdAndOrderId(int orderId , int prodId) => productOrderRepository.GetAll().Where(p => p.OrderId == orderId && p.ProductId == prodId && p.Deleted == Convert.ToBoolean(YNStatue.No)).ToList();


    }
}