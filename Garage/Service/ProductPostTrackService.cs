﻿using Garage.Models;
using Garage.Repository;
using Garage.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using static Garage.Utils.AppEnumeration;

namespace Garage.Service
{
    public class ProductPostTrackService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private GenericRepository<ProductPostTrack> productPostTrackRepository = null;

        public ProductPostTrackService()
        {
            this.productPostTrackRepository = new GenericRepository<ProductPostTrack>();
        }

        public ProductPostTrackService(GarageContext context)
        {
            this.productPostTrackRepository = new GenericRepository<ProductPostTrack>(context);
        }

        public IEnumerable<ProductPostTrack> GetAllProductPostTrack() => productPostTrackRepository.GetAll().Where(p => p.Deleted == Convert.ToBoolean(YNStatue.No));

        public ProductPostTrack GetPostTrackById(int id) => productPostTrackRepository.GetById(id);

        public ProductPostTrack AddProductPostTrack(ProductPostTrack productPostTrack)
        {
            logger.Info("save product post track to DB");
            var savedProductPostTrack = productPostTrackRepository.Insert(productPostTrack);
            productPostTrackRepository.Save();
            logger.Info("saved product post track to DB");
            return savedProductPostTrack;
        }

        public IEnumerable<ProductPostTrack> AddProductPostTrackList(List<ProductPostTrack> productPostTracks)
        {
            logger.Info("save list of product post track to DB");
            var savedProductPostTrack = productPostTrackRepository.InsertAll(productPostTracks);
            productPostTrackRepository.Save();
            logger.Info("saved list of product post track to DB");
            return savedProductPostTrack;
        }

        public ProductPostTrack UpdateProduct(ProductPostTrack productPostTrack)
        {
            logger.Info("update product post track to DB");
            var updatedProductPostTrack = productPostTrackRepository.Update(productPostTrack);
            productPostTrackRepository.Save();
            logger.Info("updated product post track to DB");
            return updatedProductPostTrack;
        }

        public ProductPostTrack DeleteProductPostTrack(ProductPostTrack productPostTrack)
        {
            logger.Info("delete product post track to DB");
            productPostTrack.Deleted = Convert.ToBoolean(YNStatue.Yes);
            var deletedProductPostTrack = productPostTrackRepository.Update(productPostTrack);
            productPostTrackRepository.Save();
            logger.Info("deleted product post track to DB");
            return deletedProductPostTrack;
        }

        public IEnumerable<ProductPostTrack> DeleteProductTrackByProduct(int productId)
        {
            logger.Info("Delete all product post track by Product Id to DB");
            var deletedProductTrack = productPostTrackRepository.GetAll().Where(p => p.ProductId == productId);
            deletedProductTrack = productPostTrackRepository.DeleteAll(deletedProductTrack);
            logger.Info("deleted product post track to DB");
            return deletedProductTrack;
        }

        public List<ProductPostTrack> GetPostTrackByProductId(int id) => productPostTrackRepository.GetAll().Where(p => p.ProductId == id).ToList();

        public int getNextProductApproverByProductId(Product product)
        {
            int result = 0;
            var postTrackList = GetPostTrackByProductId(product.Id);
            if (postTrackList != null && postTrackList.Count > 0)
            {
                var role = postTrackList.OrderByDescending(pt => pt.CreatedDate).FirstOrDefault().UserRole;
                if (role != (int)UserRole.Principal)
                {
                    result = (int)((UserRole)role + 1);
                }
            }
            return result;
        }
    }
}