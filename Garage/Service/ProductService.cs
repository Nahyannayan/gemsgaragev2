﻿using Garage.Models;
using Garage.Repository;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using static Garage.Utils.AppEnumeration;

namespace Garage.Service
{
    public class ProductService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private GenericRepository<Product> productRepository = null;

        public ProductService()
        {
            this.productRepository = new GenericRepository<Product>();
        }

        public ProductService(GarageContext context)
        {
            this.productRepository = new GenericRepository<Product>(context);
        }

        public IEnumerable<Product> GetAllProducts() => productRepository.GetAll().Where(p => p.Deleted == Convert.ToBoolean(YNStatue.No));

        public Product GetProductById(int id) => productRepository.GetById(id);

        public Product AddProduct(Product product)
        {
            logger.Info("save product to DB");
            var savedProduct = productRepository.Insert(product);
            productRepository.Save();
            logger.Info("saved product to DB");
            return savedProduct;
        }

        public IEnumerable<Product> AddProductList(List<Product> products)
        {
            logger.Info("save list of product to DB");
            var savedProduct = productRepository.InsertAll(products);
            productRepository.Save();
            logger.Info("saved list of product to DB");
            return savedProduct;
        }

        public Product UpdateProduct(Product product)
        {
            logger.Info("update product to DB");
            var updatedProduct = productRepository.Update(product);
            productRepository.Save();
            logger.Info("updated product to DB");
            return updatedProduct;
        }

        public Product DeleteProduct(Product product)
        {
            logger.Info("delete product to DB");
            product.Deleted = Convert.ToBoolean(YNStatue.Yes);
            var deletedProduct = productRepository.Update(product);
            productRepository.Save();
            logger.Info("deleted product to DB");
            return deletedProduct;
        }

        public IEnumerable<Product> GetAllPublishedProducts() => productRepository.GetAll().Where(p => p.Status == (int)ProductStatus.Published);
    }
}