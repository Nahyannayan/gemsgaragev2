﻿using Garage.Models;
using Garage.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;
using System.ComponentModel.DataAnnotations;
using log4net;
using System.Reflection;

namespace Garage.Service
{
    public class SchoolService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private GenericRepository<School> schoolRepository = null;

        public SchoolService()
        {
            this.schoolRepository = new GenericRepository<School>();
        }

        public SchoolService(GarageContext context)
        {
            this.schoolRepository = new GenericRepository<School>(context);
        }

        public IEnumerable<School> GetAllSchools() => schoolRepository.GetAll().Where(s => s.Deleted == Convert.ToBoolean(YNStatue.No));

        public School GetSchoolById(int id) => schoolRepository.GetById(id);

        public School AddSchool(School school)
        {
            logger.Info("save school to DB");
            var savedSchool = schoolRepository.Insert(school);
            schoolRepository.Save();
            logger.Info("saved school to DB");
            return savedSchool;
        }
        
        public IEnumerable<School> AddSchoolList(List<School> schools)
        {
            logger.Info("save list of school to DB");
            var savedSchool = schoolRepository.InsertAll(schools);
            schoolRepository.Save();
            logger.Info("saved list of school to DB");
            return savedSchool;
        }

        public School UpdateSchool(School school)
        {
            logger.Info("update school to DB");
            var updatedSchool = schoolRepository.Update(school);
            schoolRepository.Save();
            logger.Info("updated school to DB");
            return updatedSchool;
        }

        public School DeleteSchool(School school)
        {
            logger.Info("delete school to DB");
            school.Deleted = Convert.ToBoolean(YNStatue.Yes);
            var deletedSchool = schoolRepository.Update(school);
            schoolRepository.Save();
            logger.Info("deleted school to DB");
            return deletedSchool;
        }

    }
}