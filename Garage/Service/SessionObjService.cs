﻿using Garage.Models;
using Garage.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;
using System.ComponentModel.DataAnnotations;
using log4net;
using System.Reflection;
using Garage.Utils;

namespace Garage.Service
{
    public class SessionObjService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private GenericRepository<SessionObj> sessionObjRepository = null;

        public SessionObjService()
        {
            this.sessionObjRepository = new GenericRepository<SessionObj>();
        }

        public SessionObjService(GarageContext context)
        {
            this.sessionObjRepository = new GenericRepository<SessionObj>(context);
        }

        public IEnumerable<SessionObj> GetAllsessionObjs() => sessionObjRepository.GetAll().Where(s => s.Deleted == Convert.ToBoolean(YNStatue.No));

        public SessionObj GetSessionObjById(int id) => sessionObjRepository.GetById(id);

        public IEnumerable<SessionObj> GetSessionObjByUserId(int id)
        {
            return sessionObjRepository.GetAll().Where(s => s.UserId == id);
        }

        public SessionObj AddSessionObj(SessionObj sessionObj)
        {
            logger.Info("save sessionObj to DB");
            var savedSessionObj = sessionObjRepository.Insert(sessionObj);
            sessionObjRepository.Save();
            logger.Info("saved sessionObj to DB");
            return savedSessionObj;
        }
        
        public IEnumerable<SessionObj> AddSessionObjList(List<SessionObj> categories)
        {
            logger.Info("save list of sessionObj to DB");
            var savedSessionObj = sessionObjRepository.InsertAll(categories);
            sessionObjRepository.Save();
            logger.Info("saved list of categories to DB");
            return savedSessionObj;
        }

        public SessionObj UpdateSessionObj(SessionObj sessionObj)
        {
            logger.Info("update sessionObj to DB");
            var updatedSessionObj = sessionObjRepository.Update(sessionObj);
            sessionObjRepository.Save();
            logger.Info("updated sessionObj to DB");
            return updatedSessionObj;
        }

        public SessionObj DeleteSessionObj(SessionObj sessionObj)
        {
            logger.Info("delete sessionObj to DB");
            sessionObj.Deleted = Convert.ToBoolean(YNStatue.Yes);
            var deletedSessionObj = sessionObjRepository.Update(sessionObj);
            sessionObjRepository.Save();
            logger.Info("deleted sessionObj to DB");
            return deletedSessionObj;
        }

        public void DeleteSessionObjByUserId(int userId)
        {
            logger.Info("delete sessionObj by user id to DB");
            List<SessionObj> sessionObjList = sessionObjRepository.GetAll().Where(s => s.UserId == userId).ToList();
            if (sessionObjList != null && sessionObjList.Count > 0 )
            {
              sessionObjRepository.DeleteAll(sessionObjList);
            }
            logger.Info("Delete Session Object by user Id to DB");
        }

        public void saveSessionObjectToDatabase()
        {
            DeleteSessionObjByUserId(AppUtils.getCurrentUser().user.Id);

            var cart = AppUtils.getCart();
            if (cart != null)
            {
                var cartSessionObj = cart.CartItems.ToList().ConvertAll(s =>
                {
                    return new SessionObj()
                    {
                        UserId = AppUtils.getCurrentUser().user.Id,
                        ProductId = s.Value.Product.Id,
                        ProductImageId = s.Value.ProductImageId,
                        ProductQuantity = s.Value.ProductOrderQuantity,
                        ObjType = (int)SessionObjType.Cart,
                        CreatedBy = AppUtils.getCurrentUser().user.Id,
                        CreatedDate = DateTime.Now,
                        Deleted = Convert.ToBoolean(YNStatue.No)
                    };
                });
                AddSessionObjList(cartSessionObj);
            }

            var wishList = AppUtils.getWishList();
            if (wishList != null)
            {
                var wishListSessionObj = wishList.CartItems.ToList().ConvertAll(s =>
                {
                    return new SessionObj()
                    {
                        UserId = AppUtils.getCurrentUser().user.Id,
                        ProductId = s.Value.Product.Id,
                        ProductImageId = s.Value.ProductImageId,
                        ProductQuantity = s.Value.ProductOrderQuantity,
                        ObjType = (int)SessionObjType.WishList,
                        CreatedBy = AppUtils.getCurrentUser().user.Id,
                        CreatedDate = DateTime.Now,
                        Deleted = Convert.ToBoolean(YNStatue.No)
                    };
                });
                AddSessionObjList(wishListSessionObj);
            }
        }

    }
}