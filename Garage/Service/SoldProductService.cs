﻿using Garage.Models;
using Garage.Repository;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using static Garage.Utils.AppEnumeration;

namespace Garage.Service
{
    public class SoldProductService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private GenericRepository<SoldProduct> soldProductRepository = null;

        public SoldProductService()
        {
            this.soldProductRepository = new GenericRepository<SoldProduct>();
        }

        public SoldProductService(GarageContext context)
        {
            this.soldProductRepository = new GenericRepository<SoldProduct>(context);
        }

        public IEnumerable<SoldProduct> GetAllSoldProducts() => soldProductRepository.GetAll().Where(s => s.Deleted == Convert.ToBoolean(YNStatue.No));

        public SoldProduct GetSoldProductById(int id) => soldProductRepository.GetById(id);

        public SoldProduct GetSoldProductByProdOrderId(int prodOrderId) => soldProductRepository.GetAll().Where(p => p.ProductOrderId == prodOrderId).FirstOrDefault();

        public SoldProduct AddSoldProduct(SoldProduct soldProduct)
        {
            logger.Info("save soldProduct to DB");
            var savedSoldProduct = soldProductRepository.Insert(soldProduct);
            soldProductRepository.Save();
            logger.Info("saved soldProduct to DB");
            return savedSoldProduct;
        }

        public IEnumerable<SoldProduct> AddSoldProductList(List<SoldProduct> soldProducts)
        {
            logger.Info("save list of soldProduct to DB");
            var savedSoldProduct = soldProductRepository.InsertAll(soldProducts);
            soldProductRepository.Save();
            logger.Info("saved list of soldProduct to DB");
            return savedSoldProduct;
        }

        public SoldProduct UpdateSoldProduct(SoldProduct soldProduct)
        {
            logger.Info("update soldProduct to DB");
            var updatedSoldProduct = soldProductRepository.Update(soldProduct);
            soldProductRepository.Save();
            logger.Info("updated soldProduct to DB");
            return updatedSoldProduct;
        }

        public SoldProduct DeleteSoldProduct(SoldProduct soldProduct)
        {
            logger.Info("delete soldProduct to DB");
            soldProduct.Deleted = Convert.ToBoolean(YNStatue.Yes);
            var deletedSoldProduct = soldProductRepository.Update(soldProduct);
            soldProductRepository.Save();
            logger.Info("deleted soldProduct to DB");
            return deletedSoldProduct;
        }

        public IEnumerable<SoldProduct> GetSoldProductsByOrderId(int id) => soldProductRepository.GetAll().Where(p => p.OrderId == id);
    }
}