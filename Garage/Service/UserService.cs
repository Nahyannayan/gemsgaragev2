﻿using Garage.Models;
using Garage.Repository;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using static Garage.Utils.AppEnumeration;

namespace Garage.Service
{
    public class UserService
    {
        private readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private GenericRepository<UserRegistration> userRepository = null;
 

        public UserService()
        {
            this.userRepository = new GenericRepository<UserRegistration>();
        }

        public UserService(GarageContext context)
        {
            this.userRepository = new GenericRepository<UserRegistration>(context);
        }

        public IEnumerable<UserRegistration> GetAllUsers() => userRepository.GetAll().Where(u => u.Deleted == Convert.ToBoolean(YNStatue.No));

        public UserRegistration GetUserById(int id) => userRepository.GetById(id);

        public UserRegistration AddUser(UserRegistration user)
        {
            logger.Info("save user to DB");
            var savedProduct = userRepository.Insert(user);
            userRepository.Save();
            logger.Info("saved user to DB");
            return savedProduct;
        }

        public IEnumerable<UserRegistration> AddUserList(List<UserRegistration> categories)
        {
            logger.Info("save list of user to DB");
            var savedProduct = userRepository.InsertAll(categories);
            userRepository.Save();
            logger.Info("saved list of user to DB");
            return savedProduct;
        }

        public UserRegistration UpdateUser(UserRegistration user)
        {
            logger.Info("update user to DB");
            var updatedProduct = userRepository.Update(user);
            userRepository.Save();
            logger.Info("updated user to DB");
            return updatedProduct;
        }

        public UserRegistration DeleteUser(UserRegistration user)
        {
            logger.Info("deleted user to DB");
            user.Deleted = Convert.ToBoolean(YNStatue.Yes);
            var deletedProduct = userRepository.Update(user);
            userRepository.Save();
            logger.Info("deleted user to DB");
            return deletedProduct;
        }
         
        public UserRegistration FindUserByEmail(string email) => userRepository.GetAll().Where(u => u.Email.Equals(email, StringComparison.OrdinalIgnoreCase) && u.Deleted == Convert.ToBoolean(YNStatue.No)).FirstOrDefault();
    }
}