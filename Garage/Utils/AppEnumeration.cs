﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Garage.Utils
{
    public class AppEnumeration
    {
        public enum YNStatue
        {
            [Display(Name = "No"), Description("False")]
            No,
            [Display(Name = "Yes"), Description("True")]
            Yes
        }

        public enum ConditionEnum
        {
            [Display( Order = 6, Name = "Above average wear and tear. The item may need a bit of repair to work properly" , GroupName ="6"), Description("Above average wear and tear. The item may need a bit of repair to work properly")]
            AboveAverage = 1,
            [Display(Order = 5, Name = "Normal wear and tear for the age of the item, a few problems here and there", GroupName = "5"), Description("Normal wear and tear for the age of the item, a few problems here and there")]
            Normal = 2,
            [Display(Order = 4, Name = "A bit of wear and tear, but in good working condition", GroupName = "4"), Description("A bit of wear and tear, but in good working condition")]
            GoodCondition = 3,
            [Display(Order = 3, Name = "Almost no noticeable problems or flaws", GroupName = "3"), Description("Almost no noticeable problems or flaws")]
            NoProblems = 4,
            [Display(Order = 2, Name = "Perfect inside and out", GroupName = "2"), Description("Perfect inside and out")]
            Perfect = 5,
            [Display(Order = 1, Name = "Flawless", GroupName = "1"), Description("Flawless")]
            Flawless = 6,
            [Display(Order = 8, Name = "Damaged and unreusable", GroupName = "8"), Description("Damaged and unreusable")]
            Damaged = 7,
            [Display(Order = 7, Name = "Outdated and can no longer support current application specs", GroupName = "7"), Description("Outdated and can no longer support current application specs")]
            Outdated = 8
        }

        public enum ProductStatus
        {
            [Display(Name = "Saved As Draft"), Description("Saved As Draft")]
            SavedAsDraft = 1,
            [Display(Name = "Posted"), Description("Posted")]
            Posted = 2,
            [Display(Name = "Published"), Description("Published")]
            Published = 3,
            [Display(Name = "Sold Out"), Description("Sold Out")]
            SoldOut = 4,
            [Display(Name = "Rejected"), Description("Rejected")]
            Reject = 5,
            [Display(Name = "Approved"), Description("Approved")]
            Approved = 6,
            [Display(Name = "Re-Usable"), Description("Re-Usable")]
            Reusable = 7,
            [Display(Name = "Disposed"), Description("Disposed")]
            Disposed = 8
        }

        public enum OrderStatus
        {
            [Display(Name = "Order Placed"), Description("Order Placed")]
            OrderPlaced = 1,
            [Display(Name = "In Progress"), Description("In Progress")]
            InProgress = 2,
            [Display(Name = "Order Approved"), Description("Procured")]
            Procured = 3,
            [Display(Name = "Cancelled"), Description("Cancelled")]
            Cancelled = 4,
            [Display(Name = "Removed"), Description("Removed")]
            Removed = 5,
            [Display(Name = "Completed"), Description("Completed")]
            Completed = 6
        }

        public enum SessionObjType
        {
            [Display(Name = "Wish List"), Description("Wish List")]
            WishList = 1,
            [Display(Name = "Cart"), Description("Cart")]
            Cart = 2
        }

        public enum UserRole
        {
            [Display(Name = "ICT Engineer"), Description("ICT Engineer")]
            ICTEngineer = 1,
            [Display(Name = "Finance"), Description("Finance")]
            Finance = 2,
            [Display(Name = "MSO"), Description("MSO")]
            MSO = 3,
            [Display(Name = "Principal"), Description("Principal")]
            Principal = 4
        }

        public enum AdminRole
        {
            [Display(Name = "Corporate IT"), Description("Corporate IT")]
            CorpIT = 5,
            [Display(Name = "VP-Corporate IT"), Description("VP-Corporate IT")]
            VPCorpIT = 6,
            [Display(Name = "Corporate Finance"), Description("Corporate Finance")]
            CorpFinance = 7,
            [Display(Name = "Corporate Internal Audit"), Description("Corporate Internal Audit")]
            CorpInternalAudit = 8,
            [Display(Name = "SVP Treasury"), Description("SVP Treasury")]
            SVPTreasury = 9,
            [Display(Name = "CFO"), Description("CFO")]
            CFO = 10
        }



        public enum NIAdminRole
        {
            [Display(Name = "GCO-REALM"), Description("GCO-REALM")]
            GCOREALM = 5,
            [Display(Name = "SSC FINANCE"), Description("SSC FINANCE")]
            SSCFINANCE = 6,
            [Display(Name = "SSC INTERNAL AUDIT"), Description("SSC INTERNAL AUDIT")]
            SSCINTERNALAUDIT = 7,
            [Display(Name = "SVP Treasury"), Description("SSC INTERNAL AUDIT")]
            SVPTreasury = 8,
            [Display(Name = "CFO"), Description("CFO")]
            CFO = 9,
            [Display(Name = "GEMS ADMIN"), Description("GEMS ADMIN")]
            GEMSADMINROLE = 10,

        }
        public enum Role
        {
            [Display(Name = "ICT Engineer"), Description("ICT Engineer")]
            ICTEngineer = 1,
            [Display(Name = "Finance"), Description("Finance")]
            Finance = 2,
            [Display(Name = "MSO"), Description("MSO")]
            MSO = 3,
            [Display(Name = "Principal"), Description("Principal")]
            Principal = 4,
            [Display(Name = "Corporate IT"), Description("Corporate IT")]
            CorpIT = 5,
            [Display(Name = "VP-Corporate IT"), Description("VP-Corporate IT")]
            VPCorpIT = 6,
            [Display(Name = "Corporate Finance"), Description("Corporate Finance")]
            CorpFinance = 7,
            [Display(Name = "Corporate Internal Audit"), Description("Corporate Internal Audit")]
            CorpInternalAudit = 8,
            [Display(Name = "SVP Treasury"), Description("SVP Treasury")]
            SVPTreasury = 9,
            [Display(Name = "CFO"), Description("CFO")]
            CFO = 10
        }

        public enum NIDisposeUserRole
        {
         
            [Display(Name = "MSO ASSISTANT"), Description("MSO ASSISTANT")]
            MSOASSISTANT = 1,
            [Display(Name = "MSO"), Description("MSO")]
            MSO = 2,
            [Display(Name = "ACCOUNTS"), Description("ACCOUNTS")]
            ACCOUNTS = 3,
            [Display(Name = "HOS"), Description("HOS")]
            HOS = 4,
            [Display(Name = "GCO REALM"), Description("GCO REALM")]
            GCOREALM = 5,
            [Display(Name = "SSC FINANCE"), Description("SSC FINANCE")]
            SSCFINANCE = 6,
            [Display(Name = "SSC INTERNAL AUDIT"), Description("SSC INTERNAL AUDIT")]
            SSCINTERNALAUDIT = 7,
            [Display(Name = "SVP Treasury"), Description("SVP TREASURY")]
            SVPTreasury = 8,
            [Display(Name = "CFO"), Description("CFO")]
            CFO = 9
        }
        public enum City
        {
            [Display(Name = "Abu Dhabi"), Description("Abu Dhabi")]
            AbuDhabi = 1,
            [Display(Name = "Al Ain"), Description("Al Ain")]
            AlAin = 2,
            [Display(Name = "Dubai"), Description("Dubai")]
            Dubai = 3,
            [Display(Name = "Fujairah"), Description("Fujairah")]
            Fujairah = 4,
            [Display(Name = "Qatar"), Description("Qatar")]
            Qatar = 5,
            [Display(Name = "Ras Al Khaimah"), Description("Ras Al Khaimah")]
            RasAlKhaimah = 6,
            [Display(Name = "Sharjah"), Description("Sharjah")]
            Sharjah = 7,

            //by nahyan on 29jan 2020
            [Display(Name = "Singapore"), Description("Singapore")]
            Singapore = 8
        }

        public enum EmailType
        {
            OrderApproval,
            OrderApproved,
            ProductApproval,
            ProductPublished,
            OrderScheduled
        }

        public enum SoldProductStatus
        {
            [Display(Name = "Waiting For Approval"), Description("Waiting For Approval")]
            WaitingForApproval = 1,
            [Display(Name = "Rejected"), Description("Rejected")]
            Rejected = 2,
            [Display(Name = "Waiting For Book Value Transfer"), Description("Waiting For Book Value Transfer")]
            WaitingForBookValueTransfer = 3,
            [Display(Name = "Waiting For Schedule"), Description("Waiting For Schedule")]
            WaitingForSchedule = 4,
            [Display(Name = "Scheduled"), Description("Scheduled")]
            Scheduled = 5,
            [Display(Name = "Dispatched"), Description("Dispatched")]
            Dispatched = 6,
        }

        //TODO: Remove if not used
        //public enum ApprovalType
        //{
        //    [Display(Name = "Reuse"), Description("Product Post Approval")]
        //    Product = 1,
        //    [Display(Name = "Dispose"), Description("Product Dispose Approval")]
        //    Asset = 2
        //}

        public enum DraftType
        {
            [Display(Name = "Reuse"), Description("Product Post Draft")]
            Product = 1,
            [Display(Name = "Dispose"), Description("Product Dispose Draft")]
            Asset = 2
        }

        public enum AssetType
        {
            [Display(Name = "Reuse"), Description("Product Post Approval")]
            Reuse = 1,
            [Display(Name = "Dispose"), Description("Product Dispose Approval")]
            Dispose = 2
        }

        public enum NIUserRole
        {
            [Display(Name = "MSO ASSISTANT"), Description("MSO ASSISTANT")]
            MSOASSISTANT = 1,
            [Display(Name = "MSO"), Description("MSO")]
            MSO = 2,
            [Display(Name = "ACCOUNTS"), Description("ACCOUNTS")]
            ACCOUNTS = 3,
            [Display(Name = "HOS"), Description("HOS")]
            HOS = 4,
            [Display(Name = "GCO-REALM"), Description("GCO-REALM")]
            GCOREALM = 5,
            [Display(Name = "PROCUREMENT"), Description("PROCUREMENT")]
            PROCUREMENT = 11
                
        }
    }
}