﻿using Garage.Areas.Non_IT.ViewModels;
using Garage.Models;
using Garage.Service;
using Garage.ViewModels;
using Garage.ViewModels.ShoppingCart;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Configuration;

namespace Garage.Utils
{
    public class AppUtils
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //Email template path
        public static string publishedMailTemplate = HttpContext.Current.Server.MapPath("~/Views/EmailTemplate/AssetPublished/AssetPublished.cshtml");
        public static string rejectedMailTemplate = HttpContext.Current.Server.MapPath("~/Views/EmailTemplate/AssetRejected/AssetRejected.cshtml");
        public static string orderApproveBuyerMailTemplate = HttpContext.Current.Server.MapPath("~/Views/EmailTemplate/OrderApprovedBuyer/OrderApprovedBuyer.cshtml");
        public static string orderApproveSellerMailTemplate = HttpContext.Current.Server.MapPath("~/Views/EmailTemplate/OrderApprovedSeller/OrderApprovedSeller.cshtml");
        public static string reuseMailTemplate = HttpContext.Current.Server.MapPath("~/Views/EmailTemplate/AssetReuse/AssetReuse.cshtml");
        public static string PurchaseRejectedMailTemplate = HttpContext.Current.Server.MapPath("~/Views/EmailTemplate/PurchaseRejected/PurchaseRejected.cshtml");
        public static string AfterScheduledMailTemplate = HttpContext.Current.Server.MapPath("~/Views/EmailTemplate/AfterScheduled/AfterScheduled.cshtml");
        public static string AssetApprovalMailTemplate = HttpContext.Current.Server.MapPath("~/Views/EmailTemplate/AssetApproval/AssetApproval.cshtml");
        public static string OrderApprovalMailTemplate = HttpContext.Current.Server.MapPath("~/Views/EmailTemplate/OrderApproval/OrderApproval.cshtml");

        private static readonly UserService userService = new UserService();
        private static readonly SchoolService schoolService = new SchoolService();

        public static string baseUrl = WebConfigurationManager.AppSettings["baseUrl"];
        public static string visitorRole = "Visitor";
        public static string noPreviewImage = "/Content/img/no-preview-image.jpg";

        public static string uploadProductImage(HttpPostedFileBase files)
        {
            var path = String.Empty;

            if (files != null && files.ContentLength > 0)
            {
                var currentFileName = Path.GetFileNameWithoutExtension(files.FileName);
                var currentTime = DateTime.Now.ToString("_dd-MM-yyyy_HHmmss");
                var currentDate = DateTime.Now.ToString("dd-MM-yyyy");
                var fileExtension = Path.GetExtension(files.FileName).ToString().ToLower();
                var fileName = currentFileName + currentTime + fileExtension;
                var directory = HttpContext.Current.Server.MapPath("~/App_Data/ProductImages/" + currentDate);
                Directory.CreateDirectory(directory);
                path = Path.Combine(directory, fileName);
                files.SaveAs(path);
            }
            return path;
        }

        //public static CurrentUser getCurrentUser() => (CurrentUser)HttpContext.Current.Session["CurrentUser"];

        public static CurrentUser getCurrentUser()
        {
            logger.Info("Get Current User From Claim");
            CurrentUser currentUser = null;
            var claims = ClaimsPrincipal.Current.Claims;
            var currentUserJson = claims.Where(c => c.Type == "CurrentUser").Select(c => c.Value).SingleOrDefault();

            if (string.IsNullOrEmpty(currentUserJson))
            {
                if (HttpContext.Current.Session["currentUser"] != null)
                {

                    var _currentUser = (NICurrentUser)HttpContext.Current.Session["currentUser"];
                    currentUser = new CurrentUser();
                    currentUser.isSuperAdmin = _currentUser.isNISuperAdmin;
                    currentUser.school = new School();
                    currentUser.school.Active = _currentUser.school.Active;
                    currentUser.school.CreatedBy = _currentUser.school.CreatedBy;
                    currentUser.school.CreatedDate = _currentUser.school.CreatedDate;
                    currentUser.school.Deleted = _currentUser.school.Deleted;
                    currentUser.school.Location = _currentUser.school.Location;
                    currentUser.school.Id = _currentUser.school.Id;
                    currentUser.school.SchoolCode = _currentUser.school.SchoolCode;
                    currentUser.school.SchoolName = _currentUser.school.SchoolName;

                    currentUser.user = new UserRegistration();

                    _currentUser.user.Id = currentUser.user.Id;
                    _currentUser.user.IsITAccess = currentUser.user.IsITAccess;
                    _currentUser.user.IsNonITAccess = currentUser.user.IsNonITAccess;
                    _currentUser.user.NIRole = currentUser.user.NIRole;
                    _currentUser.user.Role = currentUser.user.Role;
                    _currentUser.user.School = currentUser.user.School;
                    _currentUser.user.UserId = currentUser.user.UserId;
                    return currentUser;
                }
                else
                    return null;
            }
            else
            {
                currentUser = JsonConvert.DeserializeObject<CurrentUser>(currentUserJson);
                if (currentUser.user.Role.HasValue)
                    currentUser.isSuperAdmin = Enum.IsDefined(typeof(Garage.Utils.AppEnumeration.AdminRole), currentUser.user.Role);
            }
            logger.Info("Fetched Current User From Claim");
            return currentUser;
        }

        public static ShoppingCart getCart() => (ShoppingCart)HttpContext.Current.Session["Cart"];

        public static ShoppingCart getWishList() => (ShoppingCart)HttpContext.Current.Session["WishList"];

        public static string getDateOnly(DateTime datetime)
        {
            return datetime.ToString("dd-MMM-yyyy");
        }
        public static ProductImage SaveProductFiles(HttpPostedFileBase Files, int? ProductID, bool IsProductImage)
        {
            ProductImage ObjProductFile = new ProductImage();

            var currentDate = DateTime.Now.ToString("dd-MM-yyyy");
            string path = ConfigurationManager.AppSettings["NonITFilePath"] + @"\" + currentDate;


            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            HttpPostedFileBase file = Files;
            var FileNAme = DateTime.Now.ToString("HHmmss") + "_" + System.IO.Path.GetFileName(file.FileName);
            var FilePath = path + @"\" + FileNAme;
            ObjProductFile.ImageName = System.IO.Path.GetFileName(file.FileName);
            ObjProductFile.Extension = System.IO.Path.GetExtension(file.FileName);
            ObjProductFile.ImageFilePath = @"\" + currentDate + @"\" + FileNAme;
            ObjProductFile.Deleted = false;
            ObjProductFile.IsProductImage = false;
            ObjProductFile.ProductId = ProductID;
            ObjProductFile.CreatedDate = DateTime.Now;
            ObjProductFile.CreatedBy = AppUtils.getCurrentUser().user.Id;
            var result = (new ProductImageService()).AddProductImage(ObjProductFile);
            file.SaveAs(FilePath);

            return ObjProductFile;
        }
        public static School GetSchoolDetails(int? SchoolID)
        {
            if (SchoolID.HasValue)
                return schoolService.GetSchoolById(SchoolID.Value);
            else
                return null;
        }
    }
}