﻿using Garage.Models;
using Garage.ViewModels;
using System;
using Garage.Service;
using static Garage.Utils.AppEnumeration;
using System.IO;
using System.Collections.Generic;
using System.Web;

namespace Garage.Utils.Mapper
{
    public class AssetImageMapper
    {
        private static readonly CategoryService categoryService = new CategoryService();

        public static List<AssetImage> assetImageMapper(AssetForm assetForm)
        {
            List<AssetImage> assetImagelist = new List<AssetImage>();
            var newImageFiles = assetForm.ImageFiles;
            foreach (HttpPostedFileBase imageFile in newImageFiles)
            {
                if (imageFile != null)
                {
                    AssetImage assetImage = new AssetImage();

                    var imagepath = AppUtils.uploadProductImage(imageFile);
                    assetImage.ImageFilePath = imagepath;
                    assetImage.ImageName = Path.GetFileNameWithoutExtension(imageFile.FileName);
                    assetImage.Extension = Path.GetExtension(imageFile.FileName).ToString().ToLower();
                    assetImage.AssetId = assetForm.Id;
                    assetImage.CreatedBy = AppUtils.getCurrentUser().user.Id;
                    assetImage.CreatedDate = DateTime.Now;
                    assetImage.UpdatedBy = AppUtils.getCurrentUser().user.Id;
                    assetImage.UpdatedDate = DateTime.Now;
                    assetImage.Deleted = Convert.ToBoolean(YNStatue.No);
                    assetImagelist.Add(assetImage);
                }
            }
            return assetImagelist;
        }

        public static List<AssetImage> assetImageMapper(AssetForm assetForm, List<AssetImage> assetImageList)
        {
            var newImageFiles = assetForm.ImageFiles;
            foreach (HttpPostedFileBase imageFile in newImageFiles)
            {
                if (imageFile != null)
                {
                    AssetImage assetImage = new AssetImage();
                    var imagepath = AppUtils.uploadProductImage(imageFile);
                    assetImage.ImageFilePath = imagepath;
                    assetImage.ImageName = Path.GetFileNameWithoutExtension(imageFile.FileName);
                    assetImage.Extension = Path.GetExtension(imageFile.FileName).ToString().ToLower();
                    assetImage.AssetId = assetForm.Id;
                    assetImage.CreatedBy = AppUtils.getCurrentUser().user.Id;
                    assetImage.CreatedDate = DateTime.Now;
                    assetImage.UpdatedBy = AppUtils.getCurrentUser().user.Id;
                    assetImage.UpdatedDate = DateTime.Now;
                    assetImage.Deleted = Convert.ToBoolean(YNStatue.No);
                    assetImageList.Add(assetImage);
                }
            }
            return assetImageList;
        }
    }
}