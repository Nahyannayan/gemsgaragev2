﻿using Garage.Models;
using Garage.ViewModels;
using System;
using Garage.Service;
using static Garage.Utils.AppEnumeration;

namespace Garage.Utils.Mapper
{
    public class AssetMapper
    {
        private static readonly CategoryService categoryService = new CategoryService();
        private static readonly UserService userService = new UserService();
        private static readonly AssetImageService assetImageService = new AssetImageService();
        private static readonly SchoolService schoolService = new SchoolService();

        public static Asset map(AssetForm assetForm)
        {
            Asset asset = new Asset();

            //asset.ProductName = assetForm.ProductName;
            //asset.Description = assetForm.Description;
            //asset.CategoryId = assetForm.CategoryId;
            //asset.SubCategoryId = assetForm.SubCategoryId;
            //asset.Quantity = assetForm.Quantity;
            //asset.Value = assetForm.Value;
            //asset.City = AppUtils.getCurrentUser().school.Location;
            //asset.Location = AppUtils.getCurrentUser().school.SchoolName;
            //asset.OriginalUnitValue = assetForm.OriginalUnitValue;
            //asset.DateOfPurchase = assetForm.DateOfPurchase;
            //asset.ExpectedRealisable = assetForm.ExpectedRealisable;
            //asset.ReasonForSelling = assetForm.ReasonForSelling;
            //asset.Note = assetForm.Note;
            //asset.Status = (int)AssetStatus.SavedAsDraft;
            //asset.UserId = AppUtils.getCurrentUser().user.Id;
            //asset.SchoolId = AppUtils.getCurrentUser().school.Id;
            //asset.Status = (int)AssetStatus.SavedAsDraft;
            //asset.CreatedBy = AppUtils.getCurrentUser().user.Id;
            //asset.CreatedDate = DateTime.Now;
            //asset.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            //asset.UpdatedDate = DateTime.Now;
            //asset.Deleted = Convert.ToBoolean(YNStatue.No);

            return asset;
        }

        public static Asset map(AssetForm assetForm, Asset asset)
        {

            asset.ProductName = assetForm.ProductName;
            asset.Description = assetForm.Description;
            asset.CategoryId = assetForm.CategoryId;
            asset.SubCategoryId = assetForm.SubCategoryId;
            asset.Quantity = assetForm.Quantity;
            asset.Value = assetForm.Value;
            asset.City = Convert.ToInt32(assetForm.City);
            asset.Location = AppUtils.getCurrentUser().school.SchoolName;
            asset.OriginalUnitValue = assetForm.OriginalUnitValue;
            asset.DateOfPurchase = assetForm.DateOfPurchase;
            asset.ExpectedRealisable = assetForm.ExpectedRealisable;
            asset.ReasonForSelling = assetForm.ReasonForSelling;
            asset.Note = assetForm.Note;
            asset.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            asset.UpdatedDate = DateTime.Now;
            return asset;
        }

        public static AssetForm remap(Asset asset)
        {
            Category category = categoryService.GetCategoryById(asset.CategoryId ?? 0);
            AssetImage assetImage = assetImageService.GetSingleAssetImageByAssetId(asset.Id);
            UserRegistration user = userService.GetUserById(asset.UserId ?? 0);
            School school = schoolService.GetSchoolById(asset.SchoolId ?? 0);

            AssetForm assetForm = new AssetForm();
            assetForm.Id = asset.Id;
            assetForm.ImageId = assetImage.Id;
            assetForm.ProductName = asset.ProductName;
            assetForm.Description = asset.Description;
            assetForm.CategoryName = category.CategoryName;
            assetForm.CategoryId = category.Id;
            if (asset.SubCategoryId != null)
            {
                Category subCategory = categoryService.GetCategoryById(asset.SubCategoryId ?? 0);
                assetForm.SubCategoryName = subCategory.CategoryName;
                assetForm.SubCategoryId = asset.SubCategoryId;
            }
            assetForm.Quantity = asset.Quantity;
            assetForm.Value = asset.Value;
            assetForm.City = asset.City;
            assetForm.Location = AppUtils.getCurrentUser().school.SchoolName;
            assetForm.OriginalUnitValue = asset.OriginalUnitValue;
            if (asset.DateOfPurchase != null)
            {
                assetForm.DateOfPurchase = asset.DateOfPurchase;
                assetForm.Age = (DateTime.Now.Year - asset.DateOfPurchase.Value.Year).ToString();
            }
            assetForm.ExpectedRealisable = asset.ExpectedRealisable;
            assetForm.ReasonForSelling = asset.ReasonForSelling;
            assetForm.Note = asset.Note;
            assetForm.UserName = user.FullName;
            assetForm.UserRole = EnumHelper<UserRole>.GetDisplayValue((UserRole)user.Role);
            assetForm.SchoolName = school.SchoolName;
            assetForm.StatusDisplay = ((ProductStatus)asset.Status).ToString();
            return assetForm;
        }

        public static AssetForm viewMap(Asset asset)
        {
            Category category = categoryService.GetCategoryById(asset.CategoryId ?? 0);
            AssetImage assetImage = assetImageService.GetSingleAssetImageByAssetId(asset.Id);
            UserRegistration user = userService.GetUserById(asset.UserId ?? 0);
            School school = schoolService.GetSchoolById(asset.SchoolId ?? 0);

            AssetForm assetForm = new AssetForm();
            assetForm.Id = asset.Id;
            assetForm.ImageId = assetImage.Id;
            assetForm.ProductName = asset.ProductName;
            assetForm.Description = asset.Description;
            assetForm.CategoryName = category.CategoryName;
            if (asset.SubCategoryId != null)
            {
                Category subCategory = categoryService.GetCategoryById(asset.SubCategoryId ?? 0);
                assetForm.SubCategoryName = subCategory.CategoryName;
            }
            assetForm.Quantity = asset.Quantity;
            assetForm.Value = asset.Value;
            assetForm.City = asset.City;
            assetForm.Location = AppUtils.getCurrentUser().school.SchoolName;
            assetForm.OriginalUnitValue = asset.OriginalUnitValue;
            if (asset.DateOfPurchase != null)
            {
                assetForm.DateOfPurchase = asset.DateOfPurchase;
                assetForm.Age = (DateTime.Now.Year - asset.DateOfPurchase.Value.Year).ToString();
            }
            assetForm.ExpectedRealisable = asset.ExpectedRealisable;
            assetForm.ReasonForSelling = asset.ReasonForSelling;
            assetForm.Note = asset.Note;
            assetForm.UserName = user.FullName;
            assetForm.UserRole = EnumHelper<UserRole>.GetDisplayValue((UserRole)user.Role);
            assetForm.SchoolName = school.SchoolName;
            assetForm.StatusDisplay = ((ProductStatus)asset.Status).ToString();
            return assetForm;
        }

        public static Product assetReusableToProduct(Product asset)
        {
            //TO DO Get Product proper Mapping

            Product product = new Product();
            product.ProductName = asset.ProductName;
            product.Description = asset.Description;
            product.CategoryId = asset.CategoryId;
            if (asset.SubCategoryId != null)
            {
                product.SubCategoryId = asset.SubCategoryId;
            }
            product.Quantity = asset.Quantity;
            product.InStock = asset.Quantity;
            product.UnitValue = asset.UnitValue;
            product.City = AppUtils.getCurrentUser().school.Location;
            product.Location = AppUtils.getCurrentUser().school.SchoolName;
            product.OriginalUnitValue = asset.OriginalUnitValue;
            product.DateOfPurchase = DateTime.Now;
            product.Condition = (int)ConditionEnum.AboveAverage;
            product.ExpectedRealisable = asset.ExpectedRealisable;
            product.ReasonForSelling = asset.ReasonForSelling;
            product.Note = asset.Note;
            product.Status = (int)ProductStatus.Published;
            product.UserId = asset.UserId;
            product.SchoolId = asset.SchoolId;
            product.ViewCount = 0;
            product.CreatedBy = AppUtils.getCurrentUser().user.Id;
            product.CreatedDate = DateTime.Now;
            product.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            product.UpdatedDate = DateTime.Now;
            product.Deleted = Convert.ToBoolean(YNStatue.No);
            return product;
        }

        public static ProductImage assetImageReusableToProductImage(AssetImage assetImage, Product product)
        {
            //TO DO Get Product proper Mapping

            ProductImage productImage = new ProductImage();
            productImage.ImageFilePath = assetImage.ImageFilePath;
            productImage.ImageName = assetImage.ImageName;
            productImage.Extension = assetImage.Extension;
            productImage.ProductId = product.Id;
            productImage.CreatedBy = AppUtils.getCurrentUser().user.Id;
            productImage.CreatedDate = DateTime.Now;
            productImage.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            productImage.UpdatedDate = DateTime.Now;
            productImage.Deleted = Convert.ToBoolean(YNStatue.No);
            return productImage;
        }

        //public static Asset mapPostStatus(Asset asset)
        //{
        //    asset.Status = (int)AssetStatus.Posted;
        //    asset.UpdatedBy = AppUtils.getCurrentUser().user.Id;
        //    asset.UpdatedDate = DateTime.Now;
        //    return asset;
        //}

        //public static Asset mapApproveStatus(Asset asset)
        //{
        //    asset.Status = (int)AssetStatus.Approved;
        //    asset.UpdatedBy = AppUtils.getCurrentUser().user.Id;
        //    asset.UpdatedDate = DateTime.Now;
        //    return asset;
        //}
        //public static Asset mapDisposedStatus(Asset asset)
        //{
        //    asset.Status = (int)AssetStatus.Disposed;
        //    asset.UpdatedBy = AppUtils.getCurrentUser().user.Id;
        //    asset.UpdatedDate = DateTime.Now;
        //    return asset;
        //}

        //public static Asset mapRejectStatus(Asset asset)
        //{
        //    asset.Status = (int)AssetStatus.Reject;
        //    asset.UpdatedBy = AppUtils.getCurrentUser().user.Id;
        //    asset.UpdatedDate = DateTime.Now;
        //    return asset;
        //}
    }
}