﻿using Garage.Models;
using Garage.ViewModels;
using System;
using Garage.Service;
using static Garage.Utils.AppEnumeration;

namespace Garage.Utils.Mapper
{
    public class AssetTrackMapper
    {
        public static AssetTrack assetTrackmap(Product product)
        {
            AssetTrack assetTrack = new AssetTrack();

            assetTrack.UserId = AppUtils.getCurrentUser().user.Id;
            assetTrack.UserRole = AppUtils.getCurrentUser().user.Role ?? 0;
            assetTrack.AssetId = product.Id;    
            assetTrack.CreatedBy = AppUtils.getCurrentUser().user.Id;
            assetTrack.CreatedDate = DateTime.Now;
            assetTrack.Deleted = Convert.ToBoolean(YNStatue.No);
            return assetTrack;
        }
    }
        
}