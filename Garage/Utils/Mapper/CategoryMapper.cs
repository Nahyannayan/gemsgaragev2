﻿using Garage.Models;
using Garage.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;


namespace Garage.Utils.Mapper
{
      
    public class CategoryMapper
    {
        public static Category map(CategoryViewModel categoryViewModel)
        {
            Category category = new Category();
            category.CategoryName = categoryViewModel.CategoryName;
            category.ParentCategoryId = categoryViewModel.ParentCategoryId;
            if (categoryViewModel.ImageFile != null && categoryViewModel.ImageFile.ContentLength != 0)
            {
                category.ImagePath = AppUtils.uploadProductImage(categoryViewModel.ImageFile);
            }
            category.Active = categoryViewModel.Active;
            category.CreatedBy = AppUtils.getCurrentUser().user.Id;
            category.CreatedDate = DateTime.Now;
            category.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            category.UpdatedDate = DateTime.Now;
            category.Deleted = Convert.ToBoolean(YNStatue.No);
            return category;

        }
        public static Category map(CategoryViewModel categoryViewModel, Category category)
        {
            category.CategoryName = categoryViewModel.CategoryName;
            category.ParentCategoryId = categoryViewModel.ParentCategoryId;
            category.Active = categoryViewModel.Active;
            if (categoryViewModel.ImageFile != null && categoryViewModel.ImageFile.ContentLength != 0)
            {
                category.ImagePath = AppUtils.uploadProductImage(categoryViewModel.ImageFile);
            }
            category.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            category.UpdatedDate = DateTime.Now;
            category.Deleted = Convert.ToBoolean(YNStatue.No);
            return category;

        }

        public static CategoryViewModel remap(Category category)
        {
            CategoryViewModel categoryViewModel = new CategoryViewModel();
            categoryViewModel.CategoryName = category.CategoryName;
            categoryViewModel.ParentCategoryId = category.ParentCategoryId??0;
            categoryViewModel.Active = category.Active??false;
            return categoryViewModel;
        }

    }
}