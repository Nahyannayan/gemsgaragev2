﻿using Garage.Models;
using Garage.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;


namespace Garage.Utils.Mapper
{
      
    public class OrderMapper
    {
        public static Order map(int index)
        {
            var currentUser = AppUtils.getCurrentUser();
            Order order = new Order();
            order.OrderNo = "#OD" + Convert.ToString((int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds) + index;
            order.OrderStatus = Convert.ToInt32(OrderStatus.OrderPlaced);
            order.SchoolId = currentUser.school.Id;
            order.CreatedBy = currentUser.user.Id;
            order.CreatedDate = DateTime.Now;
            order.UpdatedBy = currentUser.user.Id;
            order.UpdatedDate = DateTime.Now;
            order.Deleted = Convert.ToBoolean(YNStatue.No);
            return order;

        }

        public static Order cancelOrder(Order order)
        {
            order.OrderStatus = (int)OrderStatus.Cancelled;
            order.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            order.UpdatedDate = DateTime.Now;
            return order;
        }

        public static Order orderCompleteMap(Order order)
        {
            order.OrderStatus = (int)OrderStatus.Completed;
            order.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            order.UpdatedDate = DateTime.Now;
            return order;
        }
    }
}