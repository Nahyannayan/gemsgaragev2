﻿using Garage.Models;
using Garage.ViewModels;
using System;
using Garage.Service;
using static Garage.Utils.AppEnumeration;

namespace Garage.Utils.Mapper
{
    public class OrderTrackMapper
    {
        public static OrderTrack orderTrackMap(Order order)
        {
            var currentUser = AppUtils.getCurrentUser();
            OrderTrack orderTrack = new OrderTrack();

            orderTrack.UserId = currentUser.user.Id;      
            orderTrack.UserRole = currentUser.user.Role ?? 0; 
            orderTrack.TrackStatus = currentUser.user.Role ?? 0;  //TODO: Optinal if not used, Remove from table. 
            orderTrack.OrderId = order.Id;
            orderTrack.CreatedBy = currentUser.user.Id;
            orderTrack.CreatedDate = DateTime.Now;
            orderTrack.Deleted = Convert.ToBoolean(YNStatue.No);
            return orderTrack;
        }

        public static OrderTrack sellerOrderTrackMap(Order order)
        {
            var currentUser = AppUtils.getCurrentUser();
            OrderTrack orderTrack = new OrderTrack();

            orderTrack.UserId = currentUser.user.Id;      
            orderTrack.UserRole = currentUser.user.Role ?? 0; // Seller Finance or ICT
            orderTrack.TrackStatus = currentUser.user.Role ?? 0;  //TODO: Optinal if not used, Remove from table. 
            orderTrack.OrderId = order.Id;
            orderTrack.IsDifferentSchool = Convert.ToBoolean(YNStatue.Yes);
            orderTrack.CreatedBy = currentUser.user.Id;
            orderTrack.CreatedDate = DateTime.Now;
            orderTrack.Deleted = Convert.ToBoolean(YNStatue.No);
            return orderTrack;
        }
    }
        
}