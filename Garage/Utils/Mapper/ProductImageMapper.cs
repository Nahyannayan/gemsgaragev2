﻿using Garage.Models;
using Garage.ViewModels;
using System;
using Garage.Service;
using static Garage.Utils.AppEnumeration;
using System.IO;
using System.Collections.Generic;
using System.Web;

namespace Garage.Utils.Mapper
{
    public class ProductImageMapper
    {
        private static readonly CategoryService categoryService = new CategoryService();
        private static readonly ProductImageService productImageService = new ProductImageService();

        public static List<ProductImage> productImageMapper(ProductForm productForm)
        {
            List<ProductImage> productImagelist = new List<ProductImage>();
            var newImageFiles = productForm.productImageViewModel.ImageFile;
            foreach (HttpPostedFileBase imageFile in newImageFiles)
            {
                ProductImage productImage = new ProductImage();
                if (imageFile != null)
                {
                    var imagepath = AppUtils.uploadProductImage(imageFile);
                    productImage.ImageFilePath = imagepath;
                    productImage.ImageName = Path.GetFileNameWithoutExtension(imageFile.FileName).Substring(0,1);
                    productImage.Extension = Path.GetExtension(imageFile.FileName).ToString().ToLower();
                    productImage.ProductId = productForm.productViewModel.Id;
                    productImage.CreatedBy = AppUtils.getCurrentUser().user.Id;
                    productImage.CreatedDate = DateTime.Now;
                    productImage.UpdatedBy = AppUtils.getCurrentUser().user.Id;
                    productImage.UpdatedDate = DateTime.Now;
                    productImage.Deleted = Convert.ToBoolean(YNStatue.No);
                    productImage.IsProductImage = true;
                    productImagelist.Add(productImage);

                }
            }
            return productImagelist;
        }

        public static List<ProductImage> productImageMapper(ProductForm productForm,List<ProductImage> productImageList)
        {
            var newImageFiles = productForm.productImageViewModel.ImageFile;
            foreach (HttpPostedFileBase imageFile in newImageFiles)
            {
                ProductImage productImage = new ProductImage();
                if (imageFile != null)
                {
                    var imagepath = AppUtils.uploadProductImage(imageFile);
                    productImage.ImageFilePath = imagepath;
                    productImage.ImageName = Path.GetFileNameWithoutExtension(imageFile.FileName);
                    productImage.Extension = Path.GetExtension(imageFile.FileName).ToString().ToLower();
                    productImage.ProductId = productForm.productViewModel.Id;
                    productImage.CreatedBy = AppUtils.getCurrentUser().user.Id;
                    productImage.CreatedDate = DateTime.Now;
                    productImage.UpdatedBy = AppUtils.getCurrentUser().user.Id;
                    productImage.UpdatedDate = DateTime.Now;
                    productImage.Deleted = Convert.ToBoolean(YNStatue.No);
                    productImage.IsProductImage = true;
                    productImageList.Add(productImage);
                }
            }
            return productImageList;
        }

        public static List<ProductImage> setNewImagesFromProduct(Product product,int newProductId)
        {
            List<ProductImage> productImagelist = new List<ProductImage>();
            var productImages = productImageService.GetProductImageByProductId(product.Id);
            foreach (ProductImage savedProductImage in productImages)
            {
                ProductImage productImage = new ProductImage();
                if (savedProductImage != null)
                {
                   
                    productImage.ImageFilePath = savedProductImage.ImageFilePath;
                    productImage.ImageName = savedProductImage.ImageName;
                    productImage.Extension = savedProductImage.Extension;
                    productImage.ProductId = newProductId;
                    productImage.CreatedBy = savedProductImage.CreatedBy;
                    productImage.CreatedDate = DateTime.Now;
                    productImage.UpdatedBy = savedProductImage.UpdatedBy;
                    productImage.UpdatedDate = DateTime.Now;
                    productImage.Deleted = Convert.ToBoolean(YNStatue.No);
                    productImage.IsProductImage = true;
                    productImagelist.Add(productImage);
                }
            }
            return productImagelist;
        }
    }
}