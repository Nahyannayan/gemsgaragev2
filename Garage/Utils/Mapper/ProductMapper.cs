﻿using Garage.Models;
using Garage.ViewModels;
using System;
using Garage.Service;
using static Garage.Utils.AppEnumeration;
using System.Collections.Generic;
using System.Linq;

namespace Garage.Utils.Mapper
{
    public class ProductMapper
    {
        private static readonly CategoryService categoryService = new CategoryService();
        private static readonly UserService userService = new UserService();
        private static readonly ProductImageService productImageService = new ProductImageService();
        private static readonly SchoolService schoolService = new SchoolService();
        private static readonly ProductService productService = new ProductService();
        private static readonly ProductPostTrackService productPostTrackService = new ProductPostTrackService();

        public static Product map(ProductForm productForm)
        {
            Product product = new Product();
            var newProduct = productForm.productViewModel;
            product.ProductName = newProduct.ProductName; ;
            product.Description = newProduct.Description;
            product.CategoryId = newProduct.CategoryId;
            product.SubCategoryId = newProduct.SubCategoryId;
            product.Quantity = newProduct.Quantity;
            product.InStock = newProduct.Quantity;
            product.UnitValue = newProduct.UnitValue;
            product.City = AppUtils.getCurrentUser().school.Location;
            product.Location = AppUtils.getCurrentUser().school.SchoolName;
            product.OriginalUnitValue = newProduct.OriginalUnitValue;
            product.DepreciationValue = newProduct.DepreciationValue;
            product.DateOfPurchase = newProduct.DateOfPurchase;
            product.Condition = (int)newProduct.Condition;
            product.ExpectedRealisable = newProduct.ExpectedRealisable;
            product.ActualValue = newProduct.ActualValue;
            product.ReasonForSelling = newProduct.ReasonForSelling;
            product.Reference = newProduct.Reference;
            product.DAXAssestItemTag = newProduct.DAXReference; 
            product.Note = newProduct.Note;
            product.FinanceNote = newProduct.FinanceNote;
            product.MsoNote = newProduct.MsoNote;
            product.PrincipalNote = newProduct.PrincipalNote;
            product.AssetType = (int)newProduct.assetType;
            product.Status = (int)ProductStatus.SavedAsDraft;
            product.UserId = AppUtils.getCurrentUser().user.Id;
            product.SchoolId = AppUtils.getCurrentUser().school.Id;
            product.Status = (int)ProductStatus.SavedAsDraft;
            product.ViewCount = 0;
            product.CreatedBy = AppUtils.getCurrentUser().user.Id;
            product.CreatedDate = DateTime.Now;
            product.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            product.UpdatedDate = DateTime.Now;
            product.Deleted = Convert.ToBoolean(YNStatue.No);

            return product;
        }

        public static Product mapDisposeToPublished(ProductForm productForm, AssetReuseForm assetReuseForm)
        {
            Product product = new Product();
            Product oldProduct = productService.GetProductById(productForm.productViewModel.Id);
            UserRegistration user = userService.GetUserById(oldProduct.UserId ?? 0);
            School school = schoolService.GetSchoolById(oldProduct.SchoolId ?? 0);

            var newProduct = productForm.productViewModel;
            product.ProductName = newProduct.ProductName; 
            product.Description = newProduct.Description;
            product.CategoryId = newProduct.CategoryId;
            product.SubCategoryId = newProduct.SubCategoryId;
            product.Quantity = assetReuseForm.ReuseQuantity;
            product.InStock = assetReuseForm.ReuseQuantity;
            product.UnitValue = newProduct.UnitValue;
            product.City = school.Location;
            product.Location = school.SchoolName;
            product.OriginalUnitValue = newProduct.OriginalUnitValue;
            product.DepreciationValue = newProduct.DepreciationValue;
            product.DateOfPurchase = newProduct.DateOfPurchase;
            product.Condition = (int)newProduct.Condition;
            product.ExpectedRealisable = newProduct.ExpectedRealisable;
            product.ActualValue = newProduct.ActualValue;
            product.ReasonForSelling = newProduct.ReasonForSelling;
            product.Reference = newProduct.Reference;
            product.DAXAssestItemTag = newProduct.DAXReference;
            product.Note = newProduct.Note;
            product.FinanceNote = newProduct.FinanceNote;
            product.MsoNote = newProduct.MsoNote;
            product.PrincipalNote = newProduct.PrincipalNote;
            product.AssetType = (int)AssetType.Reuse;
            product.Status = (int)ProductStatus.Published;
            product.UserId = user.Id;
            product.SchoolId = school.Id;
            product.ViewCount = 0;
            product.CreatedBy = AppUtils.getCurrentUser().user.Id;
            product.CreatedDate = DateTime.Now;
            product.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            product.UpdatedDate = DateTime.Now;
            product.Deleted = Convert.ToBoolean(YNStatue.No);

            return product;
        }

        public static Product map(ProductForm productForm,Product product)
        {
            var currentUser = AppUtils.getCurrentUser();

            var updatedProduct = productForm.productViewModel;
            product.ProductName = updatedProduct.ProductName; 
            product.Description = updatedProduct.Description;
            product.CategoryId = updatedProduct.CategoryId;
            product.SubCategoryId = updatedProduct.SubCategoryId;
            product.Quantity = updatedProduct.Quantity;
            product.InStock = updatedProduct.Quantity;
            if(updatedProduct.UnitValue != null)
            {
                product.UnitValue = updatedProduct.UnitValue;
            }
            if (!currentUser.isSuperAdmin)
            {
                product.City = currentUser.school.Location;
                product.Location = currentUser.school.SchoolName;
            }
            if (updatedProduct.OriginalUnitValue != null)
            {
                product.OriginalUnitValue = updatedProduct.OriginalUnitValue;
            }
            if (updatedProduct.DepreciationValue != null)
            {
                product.DepreciationValue = updatedProduct.DepreciationValue;
            }
            if (updatedProduct.DateOfPurchase != null)
            {
                product.DateOfPurchase = updatedProduct.DateOfPurchase;
            }
            product.Condition = (int)updatedProduct.Condition;
            if (updatedProduct.ExpectedRealisable != null)
            {
                product.ExpectedRealisable = updatedProduct.ExpectedRealisable;
            }
            if (updatedProduct.ActualValue != null)
            {
                product.ActualValue = updatedProduct.ActualValue;
            }
            product.ReasonForSelling = updatedProduct.ReasonForSelling;
            if (updatedProduct.Reference != null)
            {
                product.Reference = updatedProduct.Reference;
            }
            if (updatedProduct.DAXReference != null)
            {
                product.DAXAssestItemTag = updatedProduct.DAXReference;
            }
           
            if (!string.IsNullOrEmpty(updatedProduct.Note))
            {
                product.Note = updatedProduct.Note;
            }
            if (!string.IsNullOrEmpty(updatedProduct.FinanceNote))
            {
                product.FinanceNote = updatedProduct.FinanceNote;
            }
            if (!string.IsNullOrEmpty(updatedProduct.MsoNote))
            {
                product.MsoNote = updatedProduct.MsoNote;
            }
            if (!string.IsNullOrEmpty(updatedProduct.PrincipalNote))
            {
                product.PrincipalNote = updatedProduct.PrincipalNote;
            }
            if (updatedProduct.assetType != null && updatedProduct.assetType > 0)
            {
                product.AssetType = (int)updatedProduct.assetType;
            }
            if (updatedProduct.ApprovalReq)
            {
                product.ApprovalReq = updatedProduct.ApprovalReq;
            }
            
            product.UpdatedBy = currentUser.user.Id;
            product.UpdatedDate = DateTime.Now;
            return product;
        }

        public static ProductForm remap(Product product)
        {
            ProductForm productForm = new ProductForm();
            ProductViewModel productViewModel = new ProductViewModel();

           productViewModel.Id = product.Id; ;
           productViewModel.ProductName = product.ProductName; ;
           productViewModel.Description = product.Description;
           productViewModel.CategoryId = product.CategoryId;
           productViewModel.SubCategoryId = product.SubCategoryId;
           productViewModel.Quantity = (int)product.Quantity;
           productViewModel.InStock = product.InStock;
           productViewModel.UnitValue = product.UnitValue;
           productViewModel.City = product.City.ToString();
           productViewModel.Location = product.Location;
           productViewModel.OriginalUnitValue = product.OriginalUnitValue;
           productViewModel.DepreciationValue = product.DepreciationValue;
           productViewModel.DateOfPurchase = product.DateOfPurchase;
           productViewModel.Condition = (ConditionEnum)product.Condition;
           productViewModel.ExpectedRealisable = product.ExpectedRealisable;
           productViewModel.ActualValue = product.ActualValue;
           productViewModel.ReasonForSelling = product.ReasonForSelling;
           productViewModel.ReasonForReject = "";
           productViewModel.Reference = product.Reference;
           productViewModel.DAXReference = product.DAXAssestItemTag;
           productViewModel.Note = product.Note;
           productViewModel.FinanceNote = product.FinanceNote;
           productViewModel.MsoNote = product.MsoNote;
           productViewModel.PrincipalNote = product.PrincipalNote;
           productViewModel.assetType = (AssetType)product.AssetType;
           productViewModel.ApprovalReq = product.ApprovalReq;
           productViewModel.Status = ((ProductStatus)product.Status).ToString();
           productForm.productViewModel = productViewModel;

           productForm.imageIdList = productImageService.GetProductImageByProductId(product.Id).ConvertAll(pi => pi.Id);
            return productForm;
        }

        public static ProductDetail viewMap(Product product)
        {
            var postTrackList = productPostTrackService.GetPostTrackByProductId(product.Id);
            var principalTrackEntry = postTrackList.Where(pt => pt.UserRole == (int)UserRole.Principal).FirstOrDefault();
            var publishedDate = principalTrackEntry == null ? product.CreatedDate : principalTrackEntry.CreatedDate;
            var currentUser = AppUtils.getCurrentUser();


            ProductDetail productDetail = new ProductDetail();
            productDetail.Product = product;
            productDetail.Id = product.Id.ToString();
            Category category = categoryService.GetCategoryById(product.CategoryId??0);
            UserRegistration user = userService.GetUserById(product.UserId??0);
            School school = schoolService.GetSchoolById(product.SchoolId??0);
            if (product.SubCategoryId != null)
            {
                Category subCategory = categoryService.GetCategoryById(product.SubCategoryId ?? 0);
                productDetail.SubCategoryId = subCategory.CategoryName;
            }
           //productDetail.ProductImageId = productImage.Id.ToString();
           productDetail.ProductName = product.ProductName;
           productDetail.Description = product.Description;
           productDetail.CategoryId = category.CategoryName;
           productDetail.Quantity =  Convert.ToInt32(product.Quantity).ToString();
           productDetail.InStock =  Convert.ToInt32(product.InStock);
           productDetail.UnitValue = product.UnitValue.ToString();
           productDetail.UnitValueIndecimal = product.UnitValue??0;
           productDetail.City = EnumHelper<City>.GetDisplayValue((City)product.City);
           productDetail.Location = school.SchoolName;
            productDetail.OriginalUnitValue = product.OriginalUnitValue.ToString();
           productDetail.DepreciationValue = product.DepreciationValue.ToString();
            if (product.DateOfPurchase != null)
            {
                productDetail.DateOfPurchase = product.DateOfPurchase;
                productDetail.age = (DateTime.Now.Year - product.DateOfPurchase.Value.Year).ToString();
            }

           productDetail.Condition = EnumHelper<ConditionEnum>.GetDisplayValue((ConditionEnum)product.Condition);
           productDetail.ExpectedRealisable = product.ExpectedRealisable.ToString();
           productDetail.ActualValue = product.ActualValue.ToString();
           productDetail.ReasonForSelling = product.ReasonForSelling;
           productDetail.RejectReason = product.RejectReason;
           productDetail.Reference = product.Reference;
           productDetail.DAXAssestItemTag = product.DAXAssestItemTag;
           productDetail.Note = product.Note;
           productDetail.FinanceNote = product.FinanceNote;
           productDetail.MsoNote = product.MsoNote;
           productDetail.PrinicipalNote = product.PrincipalNote;
           productDetail.UserId = user.Id;
           productDetail.UserName = user.FullName;
           productDetail.UserRole = EnumHelper<UserRole>.GetDisplayValue((UserRole)user.Role);
           productDetail.SchoolName = school.SchoolName;
           productDetail.SchoolId = school.Id;
           productDetail.Status = ((ProductStatus)product.Status).ToString();

           productDetail.imageIdList = productImageService.GetProductImageByProductId(product.Id).ConvertAll(pi => pi.Id);
           productDetail.PublishedDate = publishedDate;
            return productDetail;
        }

        public static Product mapPostStatus(Product product)
        {
            product.Status = (int)ProductStatus.Posted;
            product.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            product.UpdatedDate = DateTime.Now;
            return product;
        }
        public static Product mapProductApprovedStatus(Product product)
        {
            product.Status = (int)ProductStatus.Approved;
            product.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            product.UpdatedDate = DateTime.Now;
            return product;
        }

        public static Product mapPublishStatus(Product product)
        {
            product.Status = (int)ProductStatus.Published;
            product.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            product.UpdatedDate = DateTime.Now;
            return product;
        }

        public static Product mapSoldOutStatus(Product product)
        {
            product.Status = (int)ProductStatus.SoldOut;
            product.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            product.UpdatedDate = DateTime.Now;
            return product;
        }

        public static Product mapRejectStatus(Product product,ProductForm productForm)
        {
            product.RejectReason = productForm.productViewModel.ReasonForReject;
            product.RejectedUserId = AppUtils.getCurrentUser().user.Id;
            product.Status = (int)ProductStatus.SavedAsDraft;
            product.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            product.UpdatedDate = DateTime.Now;
            product.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            product.UpdatedDate = DateTime.Now;
            return product;
        }

        public static Product getProductFromRejectProduct(Product product)
        {
            Product newProduct = new Product();

            newProduct.ProductName = product.ProductName;
            newProduct.Description = product.Description;
            newProduct.CategoryId = product.CategoryId;
            newProduct.SubCategoryId = product.SubCategoryId;
            newProduct.Quantity = product.Quantity;
            newProduct.InStock = product.Quantity;
            newProduct.UnitValue = product.UnitValue;
            newProduct.City = product.City;
            newProduct.Location = product.Location;
            newProduct.OriginalUnitValue = product.OriginalUnitValue;
            newProduct.DepreciationValue = product.DepreciationValue;
            newProduct.DateOfPurchase = product.DateOfPurchase;
            newProduct.Condition = product.Condition;
            newProduct.ExpectedRealisable = product.ExpectedRealisable;
            newProduct.ActualValue = product.ActualValue;
            newProduct.ReasonForSelling = product.ReasonForSelling;
            newProduct.Note = product.Note;
            newProduct.FinanceNote = product.FinanceNote;
            newProduct.MsoNote = product.MsoNote;
            newProduct.PrincipalNote = product.PrincipalNote;
            newProduct.AssetType = product.AssetType;
            newProduct.Status = (int)ProductStatus.SavedAsDraft;
            newProduct.UserId = product.UserId;
            newProduct.SchoolId = AppUtils.getCurrentUser().school.Id;
            newProduct.ViewCount = 0;
            newProduct.CreatedBy = AppUtils.getCurrentUser().user.Id;
            newProduct.CreatedDate = DateTime.Now;
            newProduct.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            newProduct.UpdatedDate = DateTime.Now;
            newProduct.Deleted = Convert.ToBoolean(YNStatue.No);


            return newProduct;
        }

        public static Product mapApprovedStatus(Product product)
        {
            product.Status = (int)ProductStatus.Approved;
            product.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            product.UpdatedDate = DateTime.Now;
            return product;
        }

        public static Product mapReuseQuantity(Product product, AssetReuseForm assetReuseForm)
        {
            // UserRegistration user = userService.GetUserById(product.UserId ?? 0);
            product.Quantity = product.Quantity - assetReuseForm.ReuseQuantity;
            //product.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            product.UpdatedDate = DateTime.Now;
            return product;
        }

        public static Product mapDisposedStatus(Product product)
        {
            product.Status = (int)ProductStatus.Disposed;
            product.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            product.UpdatedDate = DateTime.Now;
            return product;
        }
    }
}