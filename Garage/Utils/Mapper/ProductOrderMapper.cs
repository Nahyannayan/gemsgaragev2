﻿using Garage.Models;
using Garage.Service;
using Garage.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;


namespace Garage.Utils.Mapper
{

    public class ProductOrderMapper
    {
        private static readonly ProductService productService = new ProductService();

        public static List<ProductOrder> map(Order order, List<ProductOrderForm> productOrderForms)
        {
            var currentUser = AppUtils.getCurrentUser();
            var productOrderList = productOrderForms.ConvertAll(pof =>
            {
                var product = productService.GetProductById(pof.productId);
                return new ProductOrder()
                {
                    OrderId = order.Id,
                    ProductId = pof.productId,
                    Quantity = pof.quantity,
                    UnitValue = product.UnitValue,
                    ProductValue = (pof.quantity>0 && product.UnitValue>0)? pof.quantity * product.UnitValue:0,
                    CreatedBy = currentUser.user.Id,
                    CreatedDate = DateTime.Now,
                    UpdatedBy = currentUser.user.Id,
                    UpdatedDate = DateTime.Now,
                    Deleted = Convert.ToBoolean(YNStatue.No)
                };

            });

            return productOrderList;

        }

        public static ProductOrder map(Order order, ProductOrderForm productOrderForm)
        {
            var product  = productService.GetProductById(productOrderForm.productId);
            var currentUser = AppUtils.getCurrentUser();
            var productOrder =  new ProductOrder()
            {
                OrderId = order.Id,
                ProductId = product.Id,
                Quantity = productOrderForm.quantity,
                UnitValue = product.UnitValue,
                ProductValue = (productOrderForm.quantity > 0 && product.UnitValue > 0) ? productOrderForm.quantity * product.UnitValue : 0,
                CreatedBy = currentUser.user.Id,
                CreatedDate = DateTime.Now,
                UpdatedBy = currentUser.user.Id,
                UpdatedDate = DateTime.Now,
                Deleted = Convert.ToBoolean(YNStatue.No)
            };

            return productOrder;
        }

        public static ProductOrder deleteProductOrder(ProductOrder productOrder)
        {
            productOrder.Deleted = Convert.ToBoolean(YNStatue.Yes);
            productOrder.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            productOrder.UpdatedDate = DateTime.Now;
            return productOrder;
        }
    }
}