﻿using Garage.Models;
using Garage.ViewModels;
using System;
using Garage.Service;
using static Garage.Utils.AppEnumeration;
using System.Collections.Generic;
using System.Linq;

namespace Garage.Utils.Mapper
{
    public class ProductTrackMapper
    {
        private static readonly CategoryService categoryService = new CategoryService();

        public static ProductPostTrack prodcutTrackmap(Product product)
        {
            var currentUser = AppUtils.getCurrentUser();
            ProductPostTrack productPostTrack = new ProductPostTrack();

            productPostTrack.UserId = currentUser.user.Id;
            productPostTrack.UserRole = currentUser.user.Role ?? 0;
            productPostTrack.TrackStatus = currentUser.user.Role ?? 0;    //TODO: Optinal if not used, Remove from table. 
            productPostTrack.ProductId = product.Id;
            productPostTrack.CreatedBy = currentUser.user.Id;
            productPostTrack.CreatedDate = DateTime.Now;
            productPostTrack.Deleted = Convert.ToBoolean(YNStatue.No);
            return productPostTrack;
        }

        public static List<ProductPostTrack> diposedTrackToPostTrack(List<AssetTrack> asstTrackList,Product product)
        {
            List<ProductPostTrack> productPostTrackList = new List<ProductPostTrack>();
            foreach (var assetTrack in asstTrackList)
            {
                if(assetTrack.UserRole <= (int)UserRole.Principal)
                {
                    ProductPostTrack productPostTrack = new ProductPostTrack();
                    productPostTrack.UserId = assetTrack.UserId;
                    productPostTrack.UserRole = assetTrack.UserRole;
                    productPostTrack.TrackStatus = assetTrack.UserRole;    //TODO: Optinal if not used, Remove from table. 
                    productPostTrack.ProductId = product.Id;
                    productPostTrack.CreatedBy = assetTrack.CreatedBy;
                    productPostTrack.CreatedDate = assetTrack.CreatedDate;
                    productPostTrack.Deleted = assetTrack.Deleted;
                    productPostTrackList.Add(productPostTrack);
                }
            }
            return productPostTrackList;
        }
    }

}