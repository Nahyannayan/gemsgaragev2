﻿using Garage.Models;
using Garage.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;

namespace Garage.Utils.Mapper
{
    public class SchoolMapper
    {
        public static School map(SchoolForm schoolForm)
        {
            School school = new School();
           
            school.SchoolCode = schoolForm.SchoolCode;
            school.SchoolName = schoolForm.SchoolName;
            school.Location = schoolForm.Location;
            school.Active = schoolForm.Active;
            school.CreatedBy = AppUtils.getCurrentUser().user.Id;
            school.CreatedDate = DateTime.Now;
            school.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            school.UpdatedDate = DateTime.Now;
            school.Deleted = Convert.ToBoolean(YNStatue.No);

            return school;
        }

        public static School map(SchoolForm schoolForm, School school)
        {

            school.SchoolCode = schoolForm.SchoolCode;
            school.SchoolName = schoolForm.SchoolName;
            school.Location = schoolForm.Location;
            school.Active = schoolForm.Active;
            school.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            school.UpdatedDate = DateTime.Now;
            school.Deleted = Convert.ToBoolean(YNStatue.No);

            return school;
        }

        public static SchoolForm remap(School school)
        {
            SchoolForm schoolForm = new SchoolForm();

            schoolForm.Id = school.Id;
            schoolForm.SchoolCode = school.SchoolCode;
            schoolForm.SchoolName = school.SchoolName;
            schoolForm.Location = school.Location??0;
            schoolForm.Active = school.Active??false;
            schoolForm.Deleted = school.Deleted??false;

            return schoolForm;
        }
    }
}