﻿using Garage.Models;
using Garage.ViewModels;
using System;
using Garage.Service;
using static Garage.Utils.AppEnumeration;
using System.Collections.Generic;
using System.Linq;

namespace Garage.Utils.Mapper
{
    public class SoldProductMapper
    {
        private static readonly ProductOrderService productOrderService = new ProductOrderService();
        private static readonly ProductService productService = new ProductService();
        private static readonly SoldProductService soldProductService = new SoldProductService();

        public static List<SoldProduct> map(Order order)
        {
            var productOrderList = productOrderService.GetPOByOrderId(order.Id);
            var soldProductList = new List<SoldProduct>();

           foreach (var productOrder in productOrderList)
            {
                SoldProduct soldProduct = new SoldProduct();
                var product = productService.GetProductById(productOrder.ProductId??0);

                soldProduct.OrderId = order.Id;
                soldProduct.ProductOrderId = productOrder.Id;
                soldProduct.ProductId = product.Id;
                soldProduct.UserId = product.UserId??0;
                soldProduct.SchoolId = product.SchoolId??0;
                soldProduct.ScheduledDate = null;
                soldProduct.DispatchDate = null;
                soldProduct.Status = (int)SoldProductStatus.WaitingForApproval;
                soldProduct.CreatedBy = AppUtils.getCurrentUser().user.Id;
                soldProduct.CreatedDate = DateTime.Now;
                soldProduct.Deleted = Convert.ToBoolean(YNStatue.No);
                soldProductList.Add(soldProduct);
            }
            return soldProductList;
        }

        public static List<SoldProduct> rejectMap(Order order)
        {
            var soldProductList = soldProductService.GetSoldProductsByOrderId(order.Id).ToList();
            soldProductList.ForEach(s => {
                s.Status = (int)SoldProductStatus.Rejected;
                s.UpdatedBy = AppUtils.getCurrentUser().user.Id;
                s.UpdatedDate = DateTime.Now;
            });
            return soldProductList;
        }
        public static SoldProduct rejectMap(ProductOrder prodOrder)
        {
            var soldProduct = soldProductService.GetSoldProductByProdOrderId(prodOrder.Id);

            soldProduct.Status = (int)SoldProductStatus.Rejected;
            soldProduct.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            soldProduct.UpdatedDate = DateTime.Now;
           
            return soldProduct;
        }

        public static List<SoldProduct> waitingForBookValueTransferMap(Order order)
        {
            var soldProductList = soldProductService.GetSoldProductsByOrderId(order.Id).ToList();
            soldProductList.ForEach(s => {
                s.Status = (int)SoldProductStatus.WaitingForBookValueTransfer;
                s.UpdatedBy = AppUtils.getCurrentUser().user.Id;
                s.UpdatedDate = DateTime.Now;
            });
            return soldProductList;
        }

        public static SoldProduct waitingForScheduleMap(SoldProduct soldProduct)
        {
                soldProduct.Status = (int)SoldProductStatus.WaitingForSchedule;
                soldProduct.UpdatedBy = AppUtils.getCurrentUser().user.Id;
                soldProduct.UpdatedDate = DateTime.Now;

            return soldProduct;
        }

        public static SoldProduct updateScheduleDate(DateTime scheduledDate, int id)
        {
            var soldProduct = soldProductService.GetSoldProductById(id);
            soldProduct.ScheduledDate = scheduledDate;
            soldProduct.Status = (int)SoldProductStatus.Scheduled;
            soldProduct.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            soldProduct.UpdatedDate = DateTime.Now;

            return soldProduct;
        }

        public static SoldProduct updateDispatchedMap(SoldProduct soldProduct)
        {
            soldProduct.Status = (int)SoldProductStatus.Dispatched;
            soldProduct.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            soldProduct.UpdatedDate = DateTime.Now;

            return soldProduct;
        }

        public static List<SoldProduct> updateDispatchedListMap(List<SoldProduct> soldProducts)
        {
            soldProducts.ForEach(soldProduct => {
                soldProduct.Status = (int)SoldProductStatus.Dispatched;
                soldProduct.UpdatedBy = AppUtils.getCurrentUser().user.Id;
                soldProduct.UpdatedDate = DateTime.Now;
            });

            return soldProducts;
        }
    }

}