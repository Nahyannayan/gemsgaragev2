﻿using Garage.Models;
using Garage.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;

namespace Garage.Utils.Mapper
{
    public class UserMapper
    {
        public static UserRegistration map(UserRegistrationForm userRegistrationForm)
        {
            UserRegistration userRegistration = new UserRegistration();

            userRegistration.UserId = userRegistrationForm.UserId;
            userRegistration.FullName = userRegistrationForm.FullName;
            userRegistration.Email = userRegistrationForm.Email;
            userRegistration.School = userRegistrationForm.School;
            userRegistration.Role = userRegistrationForm.Role;
            userRegistration.Active = userRegistrationForm.Active;
            userRegistration.CreatedBy = AppUtils.getCurrentUser().user.Id;
            userRegistration.CreatedDate = DateTime.Now;
            userRegistration.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            userRegistration.UpdatedDate = DateTime.Now;
            userRegistration.IsITAccess = true;
            userRegistration.Deleted = Convert.ToBoolean(YNStatue.No);

            return userRegistration;
        }

        public static UserRegistrationForm remap(UserRegistration userRegistration)
        {
            UserRegistrationForm userRegistrationForm = new UserRegistrationForm();

            userRegistrationForm.Id = userRegistration.Id;
            userRegistrationForm.UserId = userRegistration.UserId;
            userRegistrationForm.FullName = userRegistration.FullName;
            userRegistrationForm.Email = userRegistration.Email;
            userRegistrationForm.School = userRegistration.School??0;
            userRegistrationForm.Role = userRegistration.Role??0;
            userRegistrationForm.Active = userRegistration.Active??false;
            userRegistrationForm.Deleted = Convert.ToBoolean(YNStatue.No);

            return userRegistrationForm;
        }

        public static UserRegistration map(UserRegistrationForm userRegistrationForm, UserRegistration userRegistration)
        {
            
            userRegistration.UserId = userRegistrationForm.UserId;
            userRegistration.FullName = userRegistrationForm.FullName;
            userRegistration.Email = userRegistrationForm.Email;
            userRegistration.School = userRegistrationForm.School;
            userRegistration.Role = userRegistrationForm.Role;
            userRegistration.Active = userRegistrationForm.Active;
            userRegistration.UpdatedBy = AppUtils.getCurrentUser().user.Id;
            userRegistration.UpdatedDate = DateTime.Now;
            userRegistration.Deleted = Convert.ToBoolean(YNStatue.No);

            return userRegistration;
        }


    }
}