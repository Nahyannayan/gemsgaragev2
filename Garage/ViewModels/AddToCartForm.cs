﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.ViewModels
{
    public class AddToCartForm
    {
        public int ProductId { get; set; }
        public int? UserQuantity { get; set; }
    }
}