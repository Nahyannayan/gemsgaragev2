﻿using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;

namespace Garage.ViewModels
{
    public class ApprovalRequest
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime PublishedDate { get; set; }
        public string NextApprover { get; set; } = string.Empty;
        public int NextApproverAsInt { get; set; } = 0;
        public Product Product { get; set; }
        public Asset Asset { get; set; }
        public string SchoolCode { get; set; }
    }
}