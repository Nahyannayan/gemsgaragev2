﻿using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.ViewModels
{
    public class AssetDisposed
    {
        public Product Product { get; set; }
        public School School { get; set; }
        public DateTime PostedDate { get; set; }
        public DateTime DisposedDate { get; set; }
    }
}