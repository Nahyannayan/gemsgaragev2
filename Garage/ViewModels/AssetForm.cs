﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.ViewModels
{
    public class AssetForm
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public int? CategoryId { get; set; }
        public int? SubCategoryId { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Value { get; set; }
        public int? City { get; set; }
        public string Location { get; set; }
        public decimal? OriginalUnitValue { get; set; }
        public DateTime? DateOfPurchase { get; set; }
        public decimal? ExpectedRealisable { get; set; }
        public string ReasonForSelling { get; set; }
        public string Note { get; set; }
        public int? Status { get; set; }
        public string PublishedBy { get; set; }

        public HttpPostedFileBase[] ImageFiles { get; set; }

        //For View
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public string Age { get; set; }
        public string CityName { get; set; }
        public string LocationName { get; set; }
        public string StatusDisplay { get; set; }
        public int ImageId { get; set; }
        public string SchoolName { get; set; }
        public string UserName { get; set; }
        public string UserRole { get; set; }
    }
}