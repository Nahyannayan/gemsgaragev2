﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.ViewModels
{
    public class AssetReuseForm
    {
        public int ProductId { get; set; }
        public int ReuseQuantity { get; set; }
    }
}