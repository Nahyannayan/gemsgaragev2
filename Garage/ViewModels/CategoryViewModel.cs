﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.ViewModels
{
    public class CategoryViewModel
    {

        public int Id { get; set; }

        public string EncryptedId { get; set; }
        public string CategoryName { get; set; }
        public int ParentCategoryId { get; set; }
        public bool Active { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }
    }
}