﻿using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.ViewModels
{
    public class CompletedOrders
    {
        public Order Order { get; set; }
        public ProductOrder ProductOrder { get; set; }
        public Product Product { get; set; }
        public DateTime PurchaseDate {get;set;}
        public DateTime ScheduledDate {get;set;}
    }
}