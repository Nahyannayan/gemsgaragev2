﻿using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.ViewModels
{
    public class CurrentUser
    {
        public UserRegistration user { get; set; }
        public School school { get; set; }
        public bool isSuperAdmin { get; set; }
    }
}