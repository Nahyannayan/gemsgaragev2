﻿using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;

namespace Garage.ViewModels
{
    public class DraftProduct
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public DateTime CreatedDate { get; set; }
        public DraftType DraftType { get; set; }

        public Product Product { get; set; }
        public Asset Asset { get; set; }
    }
}