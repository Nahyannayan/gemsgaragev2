﻿using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.ViewModels.Email
{
    public class EmailOrderDetails
    {
        public ProductDetail ProductDetail { get; set; }
        public ProductOrder ProductOrder { get; set; }
        public string BuyerSchoolName { get; set; }
    }
}