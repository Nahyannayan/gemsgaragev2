﻿using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.ViewModels.EmailModels
{
    public class EmailProductDetails
    {
        public ProductDetail ProductDetail { get; set; }
    }
}