﻿using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.ViewModels
{
    public class OrderDetails
    {
        public Order Order { get; set; }
        public ProductOrder ProductOrder { get; set; }
        public Product Product { get; set; }
        public string NextApprover { get; set; }
        public int NextApproverRole { get; set; }
        public DateTime? PickUpScheduled { get; set; }

        public string SchoolCode { get; set; }
    }
}