﻿
using Garage.Models;
using System;
using System.Collections.Generic;

namespace Garage.ViewModels
{
    public class ProductDetail
    {
        public string Id { get; set; }
        public string ProductImageId { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public string CategoryId { get; set; }
        public string SubCategoryId { get; set; }
        public string Quantity { get; set; }
        public int InStock { get; set; }
        public string UnitValue { get; set; }
        public decimal UnitValueIndecimal { get; set; }
        public string City { get; set; }
        public string Location { get; set; }
        public string OriginalUnitValue { get; set; }
        public string DepreciationValue { get; set; }
        public DateTime? DateOfPurchase { get; set; }
        public string age { get; set; }
        public string Condition { get; set; }
        public string ExpectedRealisable { get; set; }
        public string ActualValue { get; set; }
        public string ReasonForSelling { get; set; }
        public string RejectReason { get; set; }
        public string Reference { get; set; }
        public string DAXAssestItemTag { get; set; }
        
        public string Note { get; set; }
        public string FinanceNote { get; set; }
        public string MsoNote { get; set; }
        public string PrinicipalNote { get; set; }
        public string Status { get; set; }
        public string PublishedBy { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserRole { get; set; }
        public int SchoolId { get; set; }
        public string SchoolName { get; set; }

        public List<int> imageIdList { get; set; }
        public DateTime? PublishedDate { get; set; }
        public Product Product { get; set; }
    }
}