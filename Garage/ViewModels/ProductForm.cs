﻿
using System.Collections.Generic;
using System.Web;
using static Garage.Utils.AppEnumeration;

namespace Garage.ViewModels
{
    public class ProductForm
    {
        public ProductViewModel productViewModel { get; set; }
        public ProductImageViewModel productImageViewModel { get; set; }

        public List<int> imageIdList { get; set; }

        public string Remarks { get; set; }
    }
}