﻿using System;
using System.Web;

namespace Garage.ViewModels
{
    public class ProductImageViewModel
    {
        public int Id { get; set; }
        public string ImageName { get; set; }
        public string Extension { get; set; }
        public int ProductId { get; set; }
        public HttpPostedFileBase[] ImageFile { get; set; }
        public System.String ReferenceFiles { get; set; }
        public string CreatedBy { get; set; }
    }
}