﻿using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.ViewModels
{
    public class ProductListView
    {
        public Product Product { get; set; }
        public List<int> imageIdList { get; set; }
        public string SchoolCode { get; set; }
        public DateTime? PublishedDate { get; set; }
    }
}