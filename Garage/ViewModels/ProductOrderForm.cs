﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.ViewModels
{
    public class ProductOrderForm
    {
        public int productId { get; set; }
        public int quantity { get; set; }
    }
}