﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.ViewModels
{
    public class SchoolForm
    {
        public int Id { get; set; }
        public string EncryptedId { get; set; }
        public string SchoolCode { get; set; }
        public string SchoolName { get; set; }
        public string ICTName { get; set; }
        public string ICTEmail { get; set; }
        public string MSOName { get; set; }
        public string MSOEmail { get; set; }
        public string AccountsOfficerName { get; set; }
        public string AccountsOfficerEmail { get; set; }
        public string PrincipalName { get; set; }
        public string PrincipalEmail { get; set; }
        public int Location { get; set; }
        public bool Active { get; set; }
        public bool Deleted { get; set; }
    }
}