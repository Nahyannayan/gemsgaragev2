﻿using Garage.Models;
using Garage.Service;
using Garage.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Garage.Utils.AppEnumeration;

namespace Garage.ViewModels.ShoppingCart
{
    public class ShoppingCart
    {
        public Dictionary<int, ShoppingCartDetails> CartItems { get; set; }

        private readonly ProductService productService = new ProductService();
        private readonly ProductImageService productImageService = new ProductImageService();

        public ShoppingCart()
        {
            this.CartItems = new Dictionary<int, ShoppingCartDetails>();
        }

        public class ShoppingCartDetails
        {
            public Product Product { get; set; }
            public int ProductOrderQuantity { get; set; }
            public int ProductImageId { get; set; }
        }

        public ShoppingCart AddToCart(AddToCartForm addToCartForm)
        {
            if (this.CartItems.ContainsKey(addToCartForm.ProductId)) { this.CartItems.Remove(addToCartForm.ProductId); }
            var product = productService.GetProductById(addToCartForm.ProductId);
            var ProductImageId = productImageService.GetSingleProductImageByProductId(product.Id).Id;
            var ProductOrderQuantity = addToCartForm.UserQuantity ?? 0;
            var cartItem = new ShoppingCartDetails()
            {
                Product = product,
                ProductOrderQuantity = ProductOrderQuantity,
                ProductImageId = ProductImageId
            };
            this.CartItems.Add(product.Id, cartItem);
            return this;
        }

        public ShoppingCart UpdateProductFromSession(ShoppingCart cart)
        {
            var listOfProductId = cart.CartItems.Select(p=> p.Key).ToList();
            var productList = productService.GetAllProducts().Where(p => listOfProductId.Contains(p.Id)).ToList();
            productList.ForEach(p =>
            {
                var shoppingCart = cart.CartItems[p.Id];
                shoppingCart.Product = p;
            });
            return cart;
        }

        public ShoppingCart RemoveFromCart(int productId)
        {
            if (this.CartItems.ContainsKey(productId)) { this.CartItems.Remove(productId); }
            return this;
        }

        public List<SessionObj> AddCartToDb (ShoppingCart shoppingCart)
        {
            var cartItems = shoppingCart.CartItems.Values.ToList();
            return cartItems.ConvertAll(c => new SessionObj()
            {
                UserId = AppUtils.getCurrentUser().user.Id,
                ProductId = c.Product.Id,
                ProductImageId = c.ProductImageId,
                ProductQuantity = c.ProductOrderQuantity,
                ObjType = (int)SessionObjType.Cart,
                CreatedBy = AppUtils.getCurrentUser().user.Id,
                CreatedDate = DateTime.Now,
                Deleted = Convert.ToBoolean(YNStatue.No)
        });
            
        }
    }
}