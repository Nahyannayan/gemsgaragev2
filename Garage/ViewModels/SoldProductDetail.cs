﻿using Garage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Garage.ViewModels
{
    public class SoldProductDetail
    {
        public SoldProduct SoldProduct { get; set; }
        public Product Product { get; set; }
        public ProductOrder ProductOrder { get; set; }
        public School School { get; set; }
        public School SoldSchool { get; set; }
        public DateTime? OrderApprovedDate { get; set; }
        public DateTime? PublishedDate { get; set; }
        public DateTime? PickUpDate { get; set; }
    }
}